import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class AreaController extends BaseController {
  Rx<List<AreaData>> _listArea = Rx([]);
  List<AreaData> get listArea => _listArea.value;

  set listArea(List<AreaData> listArea) {
    _listArea.value = listArea;
  }

  Future<List<AreaData>> area() async {
    List<AreaData> response = await commonRepository.area();
    listArea = response;
    return response;
  }

  Future<BaseResponse> areaDelete(
      {required int id, GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.areaDelete(id);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }
}
