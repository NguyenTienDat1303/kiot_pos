import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class AreaEditController extends BaseController {
  Rx<List<StoreData>> _storeList = Rx([]);

  List<StoreData> get storeList => _storeList.value;

  set storeList(List<StoreData> storeList) {
    _storeList.value = storeList;
  }

  Rxn<AreaDetailData> _detailArea = Rxn();

  AreaDetailData? get detailArea => _detailArea.value;

  set detailArea(AreaDetailData? detailArea) {
    _detailArea.value = detailArea;
  }

  Future<List<StoreData>> loadStoreList() async {
    List<StoreData> response = await commonRepository.store();
    storeList = response;
    return response;
  }

  Future<BaseResponse> areaCreate(
      {required String areaName,
      required int numberTable,
      required int from1,
      required int to1,
      required int area_price1,
      required int from2,
      required int to2,
      required int area_price2,
      required int from3,
      required int to3,
      required int area_price3,
      required int storeId,
      GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.areaCreate(
        areaName,
        numberTable,
        from1,
        to1,
        area_price1,
        from2,
        to2,
        area_price2,
        from3,
        to3,
        area_price3,
        storeId);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Future<AreaDetailData> getDetailArea(int id) async {
    AreaDetailData response = await commonRepository.detailArea(id);
    detailArea = response;

    return response;
  }

  Future<BaseResponse> areaEdit(
      {required int id,
      required String areaName,
      required int numberTable,
      required int from1,
      required int to1,
      required int area_price1,
      required int from2,
      required int to2,
      required int area_price2,
      required int from3,
      required int to3,
      required int area_price3,
      required int storeId,
      GestureTapCallback? callback}) async {
    print('jisadgjkasvdasvjkd ${storeId}');
    BaseResponse response = await commonRepository.areaEdit(
        id,
        areaName,
        numberTable,
        from1,
        to1,
        area_price1,
        from2,
        to2,
        area_price2,
        from3,
        to3,
        area_price3,
        storeId);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }
}
