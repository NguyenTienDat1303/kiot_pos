import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderReturnController extends BaseController {
  Rx<List<PermissionData>> _permissionList = Rx([]);

  List<PermissionData> get permissionList => _permissionList.value;

  set permissionList(List<PermissionData> permissionList) {
    _permissionList.value = permissionList;
  }

  Rx<int> _version = Rx(-1);

  int get version => _version.value;

  set version(int version) {
    _version.value = version;
  }

  @override
  void onReady() {
    super.onReady();
    loadPermissonList();
  }

  Future<List<PermissionData>> loadPermissonList() async {
    List<PermissionData> permissionResponse =
        await commonRepository.permission();
    permissionList = permissionResponse;
    var a = await sessionRepository.getField<String>(LocalPref.version);
    version = int.parse(a ?? "0");
    return permissionList;
  }
}
