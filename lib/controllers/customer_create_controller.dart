import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class CustomerCreateController extends BaseController {
  Rx<List<GroupCustomerResponse>> _listGroupCustomer = Rx([]);
  List<GroupCustomerResponse> get listGroupCustomer => _listGroupCustomer.value;

  set listGroupCustomer(List<GroupCustomerResponse> listGroupCustomer) {
    _listGroupCustomer.value = listGroupCustomer;
  }

  Future<List<GroupCustomerResponse>> getGroupCustomer() async {
    List<GroupCustomerResponse> response =
        await commonRepository.getGroupCustomer();

    listGroupCustomer = response;

    return response;
  }

  Future<BaseResponse> createCustomer(
      {String? customerCode,
      required String customerName,
       String? customerPhone,
      String? customerEmail,
      String? customerAddr,
      String? notes,
      String? customerBirthday,
      int? customerGender,
       int? customerGroup,
      int? customerCoin,
      GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.createCustomer(
        customerCode,
        customerName,
        customerPhone,
        customerEmail,
        customerAddr,
        notes,
        customerBirthday,
        customerGender,
        customerGroup,
        customerCoin);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }
}
