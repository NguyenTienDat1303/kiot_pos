import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OverviewController extends BaseController {
  Rxn<OverviewResponse> _overviewResponse = Rxn();

  OverviewResponse? get overviewResponse => _overviewResponse.value;

  set overviewResponse(OverviewResponse? overviewResponse) {
    _overviewResponse.value = overviewResponse;
  }

  Rx<OverviewChart> _overviewChartType = Rx(OverviewChart.one_year);

  OverviewChart get overviewChartType => _overviewChartType.value;

  set overviewChartType(OverviewChart overviewChartType) {
    _overviewChartType.value = overviewChartType;
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future<OverviewResponse> loadOverview() async {
    OverviewResponse response = await commonRepository
        .overview((overviewChartType.index + 1).toString());
    overviewResponse = response;
    return response;
  }
}
