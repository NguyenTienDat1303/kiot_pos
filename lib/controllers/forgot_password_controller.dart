import 'package:kiot_pos/kiot_pos.dart';

class ForgotPasswordController extends BaseController {
  Future<BaseResponse> forgotPassword(String info) async {
    BaseResponse response = await commonRepository.forgotPassword(info);
    return response;
  }
}
