import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class CustomerController extends BaseController {
  Rx<List<CustomerResponse>> _listCustomer = Rx([]);
  List<CustomerResponse> get listCustomer => _listCustomer.value;

  set listCustomer(List<CustomerResponse> listCustomer) {
    _listCustomer.value = listCustomer;
  }

  Future<List<CustomerResponse>> getListCustomer({required String tuKhoa}) async {
    List<CustomerResponse> response =
        await commonRepository.getListCustomer(tuKhoa);
    listCustomer = response;
    return response;
  }
}
