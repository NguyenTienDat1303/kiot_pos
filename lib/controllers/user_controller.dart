import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class UserController extends BaseController {
  Rxn<UserResponse> _detailUser = Rxn();

  UserResponse? get detailUser => _detailUser.value;

  set detailUser(UserResponse? detailUser) {
    _detailUser.value = detailUser;
  }

  Future<UserResponse> getUserDetail() async {
    UserResponse response = await commonRepository.getUserDetail();
    detailUser = response;
    return response;
  }

  Future<BaseResponse> userEdit({required int id,required String displayName,required String email,GestureTapCallback? onTap}) async {
    BaseResponse response =
        await commonRepository.userEdit(id, displayName, email);
   
    return response;
  }
}
