import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/ui/widgets/widget_dialog.dart';

class CatalogueController extends BaseController {
  Future<BaseResponse> catalogueDelete(
      {required int danhMucID, GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.catalogueDelete(danhMucID);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }
  Future<BaseResponse> catalogueEdit(
      {required int danhMucID, required String tenDanhMuc, GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.catalogueEdit(danhMucID, tenDanhMuc);
    
    return response;
  }
}
