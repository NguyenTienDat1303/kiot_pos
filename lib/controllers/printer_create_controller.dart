import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class PrinterCreateController extends BaseController {
  Rx<String> _description = Rx("");

  String get description => _description.value;

  set description(String description) {
    _description.value = description;
  }

  Rx<int> _kieuIn = Rx(1);

  int get kieuIn => _kieuIn.value;

  set kieuIn(int kieuIn) {
    _kieuIn.value = kieuIn;
  }

  Rx<int> _khoGiay = Rx(1);

  int get khoGiay => _khoGiay.value;

  set khoGiay(int khoGiay) {
    _khoGiay.value = khoGiay;
  }

  Rx<String> _ip = Rx("");

  String get ip => _ip.value;

  set ip(String ip) {
    _ip.value = ip;
  }

  Future<BaseResponse> printCreate() async {
    BaseResponse response = await commonRepository.printerCreate(
        description, kieuIn, khoGiay, ip, "9100");
    return response;
  }
}
