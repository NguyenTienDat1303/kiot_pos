import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OverviewCollectPayController extends BaseController {
  Rx<OverviewCollectPayResponse?> _overview = Rx(null);

  OverviewCollectPayResponse? get overview => _overview.value;

  set overview(OverviewCollectPayResponse? overview) {
    _overview.value = overview;
  }

  Rx<List<CollectResponse>> _listCollect = Rx([]);

  List<CollectResponse> get listCollect => _listCollect.value;

  set listCollect(List<CollectResponse> listCollect) {
    _listCollect.value = listCollect;
  }

  Rx<List<PayResponse>> _listPay = Rx([]);

  List<PayResponse> get listPay => _listPay.value;

  set listPay(List<PayResponse> listPay) {
    _listPay.value = listPay;
  }

  Future<OverviewCollectPayResponse> getOverviewCollectPay() async {
    OverviewCollectPayResponse response =
        await commonRepository.getOverviewCollectPay();
    overview = response;
    return response;
  }

  Future<List<CollectResponse>> getListCollect(String tuKhoa, int? isDaXoa,
      int? hinhThucThu, String? tuNgay, String? denNgay, int per_page) async {
    List<CollectResponse> response = await commonRepository.getListCollect(
        tuKhoa, isDaXoa, hinhThucThu, tuNgay, denNgay, per_page);
    listCollect = response;
    return response;
  }

  Future<List<PayResponse>> getListPay(String tuKhoa, int? isDaXoa,
      int? hinhThucChi, String? tuNgay, String? denNgay, int per_page) async {
    List<PayResponse> response = await commonRepository.getListPay(
        tuKhoa, isDaXoa, hinhThucChi, tuNgay, denNgay, per_page);
    listPay = response;
    return response;
  }
}
