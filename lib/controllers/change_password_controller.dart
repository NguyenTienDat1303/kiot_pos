import 'package:kiot_pos/kiot_pos.dart';

class ChangePasswordController extends BaseController {
  Future<BaseResponse> changePassword(
      String password, String newPassword, String newRePassword) async {
    BaseResponse response = await commonRepository.changePassword(
        password, newPassword, newRePassword);
    return response;
  }
}
