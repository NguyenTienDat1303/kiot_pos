import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class BillDetailController extends BaseController {
  Rx<List<CustomerResponse>> _listCustomer = Rx([]);
  List<CustomerResponse> get listCustomer => _listCustomer.value;

  set listCustomer(List<CustomerResponse> listCustomer) {
    _listCustomer.value = listCustomer;
  }

  Future<List<CustomerResponse>> getListCustomer(
      {required String tuKhoa}) async {
    List<CustomerResponse> response =
        await commonRepository.getListCustomer(tuKhoa);
    listCustomer = response;
    return response;
  }

  Rx<int> _idBill = Rx(-1);

  int get idBill => _idBill.value;

  set idBill(int idBill) {
    _idBill.value = idBill;
  }

  onRefreshCollect() {
    _detailCollect.refresh();
  }
  onRefreshPay() {
    _detailPay.refresh();
  }

  Rx<CollectResponse?> _detailCollect = Rx(null);

  CollectResponse? get detailCollect => _detailCollect.value;

  set detailCollect(CollectResponse? detailCollect) {
    _detailCollect.value = detailCollect;
  }

  Rx<PayResponse?> _detailPay = Rx(null);

  PayResponse? get detailPay => _detailPay.value;

  set detailPay(PayResponse? detailPay) {
    _detailPay.value = detailPay;
  }

  Rx<List<UsersResponse>> _listUser = Rx([]);

  List<UsersResponse> get listUser => _listUser.value;

  set listUser(List<UsersResponse> listUser) {
    _listUser.value = listUser;
  }

  Rx<List<ProducerResponse>> _listProducer = Rx([]);

  List<ProducerResponse> get listProducer => _listProducer.value;

  set listProducer(List<ProducerResponse> listProducer) {
    _listProducer.value = listProducer;
  }

  Future<List<ProducerResponse>> producer() async {
    List<ProducerResponse> response = await commonRepository.producer();
    listProducer = response;
    return response;
  }

  Future<List<UsersResponse>> users() async {
    List<UsersResponse> response = await commonRepository.users();
    listUser = response;
    return response;
  }

  Future<CollectResponse> getDetailCollect(int id) async {
    CollectResponse response = await commonRepository.getDetailCollect(id);
    detailCollect = response;
    return response;
  }

  Future<PayResponse> getDetailPay(int id) async {
    PayResponse response = await commonRepository.getDetailPay(id);
    detailPay = response;
    return response;
  }

  Future<BaseResponse> collectDelete(
      {required int id, GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.collectDelete(id);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Future<BaseResponse> collectCreate(
      {required int receiptFor,
      required int totalMoney,
      required String receiptDate,
      // required int storeId,
      required String notes,
      required int receiptMethod,
      required int typeId,
      required int customerId,
      GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.collectCreate(
        receiptFor,
        totalMoney,
        receiptDate,
        idStore,
        notes,
        receiptMethod,
        typeId,
        customerId);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Rx<int> _idStore = Rx(-1);

  int get idStore => _idStore.value;

  set idStore(int idStore) {
    _idStore.value = idStore;
  }

  @override
  void onReady() {
    super.onReady();
    getIdStore();
  }

  getIdStore() async {
    idStore = (await sessionRepository.getField<int>(LocalPref.store_id))!;
  }

  Future<BaseResponse> collectEdit(
      {required int id,
      required int receiptFor,
      required int totalMoney,
      required String receiptDate,
      // required int storeId,
      required String notes,
      required int receiptMethod,
      required int typeId,
      required int customerId,
      GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.collectEdit(
        id,
        receiptFor,
        totalMoney,
        receiptDate,
        idStore,
        notes,
        receiptMethod,
        typeId,
        customerId);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Future<BaseResponse> payDelete(
      {required int id, GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.payDelete(id);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Future<BaseResponse> payCreate(
      {required int paymentFor,
      required int totalMoney,
      required String paymentDate,
      // required int storeId,
      required String notes,
      required int paymentMethod,
      required int typeId,
      required int customerId,
      GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.payCreate(
        paymentFor,
        totalMoney,
        paymentDate,
        idStore,
        notes,
        paymentMethod,
        typeId,
        customerId);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Future<BaseResponse> payEdit(
      {required int id,
      required int paymentFor,
      required int totalMoney,
      required String paymentDate,
      // required int storeId,
      required String notes,
      required int paymentMethod,
      required int typeId,
      required int customerId,
      GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.payEdit(
        id,
        paymentFor,
        totalMoney,
        paymentDate,
        idStore,
        notes,
        paymentMethod,
        typeId,
        customerId);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }
}
