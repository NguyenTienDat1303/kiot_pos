import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class MoveTableController extends BaseController {
  Rxn<AreaData> _areaSelected = Rxn();

  AreaData? get areaSelected => _areaSelected.value;

  set areaSelected(AreaData? areaSelected) {
    _areaSelected.value = areaSelected;
    loadTableList();
  }

  Rxn<int> _tableStatus = Rxn();

  int? get tableStatus => _tableStatus.value;

  set tableStatus(int? tableStatus) {
    _tableStatus.value = tableStatus;
  }

  Rx<List<StoreData>> _storeList = Rx([]);

  List<StoreData> get storeList => _storeList.value;

  set storeList(List<StoreData> storeList) {
    _storeList.value = storeList;
  }

  Rx<List<AreaData>> _areaList = Rx([]);

  List<AreaData> get areaList => _areaList.value;

  set areaList(List<AreaData> areaList) {
    _areaList.value = areaList;
  }

  Rx<List<TableData>> _tableList = Rx([]);

  List<TableData> get tableList => _tableList.value;

  set tableList(List<TableData> tableList) {
    _tableList.value = tableList;
  }

  Rx<double> _tamTinh = Rx(0);

  double get tamTinh => _tamTinh.value;

  set tamTinh(double tamTinh) {
    _tamTinh.value = tamTinh;
  }

  @override
  void onReady() async {
    super.onReady();
  }

  Future<List<StoreData>> loadStoreList() async {
    List<StoreData> response = await commonRepository.store();
    storeList = response;
    return response;
  }

  Future<List<AreaData>> loadAreaList() async {
    List<AreaData> response = await commonRepository.area();
    areaList = response;
    return response;
  }

  Future<TableListResponse> loadTableList() async {
    TableListResponse response =
        await commonRepository.table(areaId:areaSelected!.id!, trangThai: tableStatus);
    tableList = response.danhSach;
    tamTinh = response.tamTinh;
    return response;
  }
}
