import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderCreateProductController extends BaseController {
  Rx<List<CategoryData>> _categoryList = Rx([]);

  List<CategoryData> get categoryList => _categoryList.value;

  set categoryList(List<CategoryData> categoryList) {
    _categoryList.value = categoryList;
  }

  Rx<List<ProductData>> _productList = Rx([]);

  List<ProductData> get productList => _productList.value;

  set productList(List<ProductData> productList) {
    _productList.value = productList;
  }

  Rxn<CategoryData> _categorySelected = Rxn();

  CategoryData? get categorySelected => _categorySelected.value;

  set categorySelected(CategoryData? categorySelected) {
    _categorySelected.value = categorySelected;
    loadProductList(categorySelected!.danhMuc!.id, tuKhoa);
  }

  Rx<String> _tuKhoa = Rx("");

  String get tuKhoa => _tuKhoa.value;

  set tuKhoa(String tuKhoa) {
    _tuKhoa.value = tuKhoa;
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future<List<CategoryData>> loadCategoryList({required String tuKhoa}) async {
    List<CategoryData> response = await commonRepository.category(tuKhoa);
    response.insert(
        0,
        CategoryData(
            danhMuc: CategoryDetail(
                id: null, prdGroupName: 'order_create_product.all')));
    categoryList = response;
    categorySelected = categoryList.length > 0 ? categoryList.first : null;
    return response;
  }

  Future<List<CategoryData>> loadCategoryListNotAll() async {
    List<CategoryData> response = await commonRepository.category('');

    categoryList = response;
    categorySelected = categoryList.length > 0 ? categoryList.first : null;
    return response;
  }

  Future<BaseResponse> catalogueCreate(
      {required String tenDanhMuc, required int parentID}) async {
    BaseResponse response =
        await commonRepository.catalogueCreate(tenDanhMuc, parentID);

    return response;
  }

  Future<List<ProductData>> loadProductList(
      int? categoryId, String tuKhoa) async {
    List<ProductData> response =
        await commonRepository.product(categoryId, tuKhoa);
    productList = response;
    return response;
  }
}
