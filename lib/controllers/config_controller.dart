import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class ConfigController extends BaseController {
  Rx<bool> _printKitchen = Rx(false);

  bool get printKitchen => _printKitchen.value;

  set printKitchen(bool printKitchen) {
    _printKitchen.value = printKitchen;
    sessionRepository.saveField(LocalPref.print_kitchen, printKitchen);
  }

  Rx<bool> _printDouble = Rx(false);

  bool get printDouble => _printDouble.value;

  set printDouble(bool printDouble) {
    _printDouble.value = printDouble;
    sessionRepository.saveField(LocalPref.print_double, printDouble);
  }

  Rx<bool> _printAuto = Rx(false);

  bool get printAuto => _printAuto.value;

  set printAuto(bool printAuto) {
    _printAuto.value = printAuto;
    sessionRepository.saveField(LocalPref.print_auto, printAuto);
  }

  Rx<List<PrinterConfig>> _printerConfigList = Rx([]);

  List<PrinterConfig> get printerConfigList => _printerConfigList.value;

  set printerConfigList(List<PrinterConfig> printerConfigList) {
    _printerConfigList.value = printerConfigList;
  }

  Rx<List<PrinterData>> _printDataList = Rx([]);

  List<PrinterData> get printDataList => _printDataList.value;

  set printDataList(List<PrinterData> printDataList) {
    _printDataList.value = printDataList;
  }

  Rx<int> _printType = Rx(PrintType.bluetooth);

  int get printType => _printType.value;

  set printType(int printType) {
    _printType.value = printType;
    sessionRepository.saveField(LocalPref.print_type, printType);
  }

  @override
  void onReady() {
    super.onReady();
    loadConfigs();
    // printerList();
    loadPrinterConfig();
  }

  Future<void> loadConfigs() async {
    try {
      printKitchen =
          await sessionRepository.getField<bool?>(LocalPref.print_kitchen) ??
              false;
      printDouble =
          await sessionRepository.getField<bool?>(LocalPref.print_double) ??
              false;
      printAuto =
          await sessionRepository.getField<bool?>(LocalPref.print_auto) ??
              false;
      printType =
          await sessionRepository.getField<int?>(LocalPref.print_type) ??
              PrintType.lan;
    } catch (e) {}
  }

  Future<List<PrinterConfig>> loadPrinterConfig() async {
    var printerConfigDao = sessionRepository.database.printerConfigDao;
    var results = await printerConfigDao.findAll();
    print("results ${results.length}");
    printerConfigList = results;
    return results;
  }

  Future<void> editPrinterConfig(PrinterConfig printerConfig) async {
    var printerConfigDao = sessionRepository.database.printerConfigDao;
    printerConfigDao.edit(printerConfig);
    _printerConfigList.refresh();
  }

  Future<List<PrinterData>> printerList() async {
    var results = await commonRepository.printerList();
    printDataList = results;
    return results;
  }
}
