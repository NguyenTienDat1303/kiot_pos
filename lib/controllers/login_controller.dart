import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class LoginController extends BaseController {
  Rx<String> _username = Rx("");

  String get username => _username.value;

  set username(String username) {
    _username.value = username;
  }

  Rx<String> _storeName = Rx("");

  String get storeName => _storeName.value;

  set storeName(String storeName) {
    _storeName.value = storeName;
  }

  Future<bool> loadPrefs() async {
  var prefUsername =
        await sessionRepository.getField<String?>(LocalPref.username);
    var prefStoreName =
        await sessionRepository.getField<String?>(LocalPref.store_name);
    username = prefUsername ?? "";
    storeName = prefStoreName ?? "";

    return true;
  }

  Future<LoginResponse> login(
      String storeName, String username, String password) async {
    LoginResponse loginResponse =
        await commonRepository.login(username, password);

    sessionRepository.saveField<String>(LocalPref.store_name, storeName);
    sessionRepository.saveField<String>(LocalPref.username, username);
    sessionRepository.saveField<String>(LocalPref.password, password);

    sessionRepository.saveField<String>(
        LocalPref.auth_key, loginResponse.user!.authKey!);
    sessionRepository.saveField<int>(LocalPref.uid, loginResponse.user!.id!);
    sessionRepository.saveField<int>(
        LocalPref.store_id, loginResponse.user!.storeId!);
    sessionRepository.saveField<String>(
        LocalPref.version, loginResponse.options!);
    return loginResponse;
  }
}
