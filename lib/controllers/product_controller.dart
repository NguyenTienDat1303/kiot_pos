import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class ProductController extends BaseController {
  Rx<int> _idProduct = Rx(-1);
  set idProduct(int idProduct) {
    _idProduct.value = idProduct;
  }

  int get idProduct => _idProduct.value;

  Rx<DetailProductResponse?> _detailProduct = Rx(null);

  set detailProduct(DetailProductResponse? detailProduct) {
    _detailProduct.value = detailProduct;
  }

  DetailProductResponse? get detailProduct => _detailProduct.value;

  Rx<List<ProducerResponse>> _listProducer = Rx([]);

  List<ProducerResponse> get listProducer => _listProducer.value;

  set listProducer(List<ProducerResponse> listProducer) {
    _listProducer.value = listProducer;
  }

  Rx<List<UsersResponse>> _listUser = Rx([]);

  List<UsersResponse> get listUser => _listUser.value;

  set listUser(List<UsersResponse> listUser) {
    _listUser.value = listUser;
  }

  Rx<List<CategoryData>> _categoryList = Rx([]);

  List<CategoryData> get categoryList => _categoryList.value;

  set categoryList(List<CategoryData> categoryList) {
    _categoryList.value = categoryList;
  }

  Rx<List<UnitResponse>> _listUnit = Rx([]);

  List<UnitResponse> get listUnit => _listUnit.value;

  set listUnit(List<UnitResponse> listUnit) {
    _listUnit.value = listUnit;
  }

  Future<List<UnitResponse>> unit() async {
    List<UnitResponse> response = await commonRepository.unit();
    listUnit = response;
    return response;
  }

  Future<List<ProducerResponse>> producer() async {
    List<ProducerResponse> response = await commonRepository.producer();
    listProducer = response;
    return response;
  }

  Future<List<UsersResponse>> users() async {
    List<UsersResponse> response = await commonRepository.users();
    listUser = response;
    return response;
  }

  Future<DetailProductResponse> getDetailProduct(String id) async {
    DetailProductResponse response =
        await commonRepository.getDetailProduct(id);
    detailProduct = response;
    return response;
  }

  Future<BaseResponse> productDelete(int id,
      {GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.productDelete(id);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Future<BaseResponse> producerCreate(String prd_manuf_name,
      {GestureTapCallback? callback}) async {
    BaseResponse response =
        await commonRepository.producerCreate(prd_manuf_name);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Future<BaseResponse> unitCreate(String prd_unit_name,
      {GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.unitCreate(prd_unit_name);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Future<BaseResponse> productCreate(
      {required String prdName,
      String? prdCode,
      int? prd_sls,
      File? prd_image_url,
      required int prd_edit_price,
      required int prd_print,
      required int prd_allownegative,
      required int prd_material,
      required int prdAdd,
      required String infor,
      int? prd_origin_price,
      int? prd_sell_price,
      int? prd_sell_price2,
      int? prd_group_id,
      int? prd_manufacture_id,
      int? prd_unit_id,
      required int prd_new,
      required int prd_hot,
      required int prd_highlight,
      int? prd_max,
      int? prd_min,
      required List<UnitConversionResponse> quyDoi}) async {
    print(prd_origin_price);
    print('hahahaha ${prd_sell_price}');
    print('hahahaha ${prd_sell_price2}');
    BaseResponse response = await commonRepository.productCreate(
        prdName,
        prdCode,
        prd_sls,
        prd_image_url,
        prd_edit_price,
        prd_print,
        prd_allownegative,
        prd_material,
        prdAdd,
        infor,
        prd_origin_price,
        prd_sell_price,
        prd_sell_price2,
        prd_group_id,
        prd_manufacture_id,
        prd_unit_id,
        prd_new,
        prd_hot,
        prd_highlight,
        prd_max,
        prd_min,
        null,
        quyDoi);
    return response;
  }

  Future<BaseResponse> productEdit(
      {required int id,
      required String prdName,
      String? prdCode,
      int? prd_sls,
      File? prd_image_url,
      required int prd_edit_price,
      required int prd_print,
      required int prd_allownegative,
      required int prd_material,
      required int prdAdd,
      required String infor,
      int? prd_origin_price,
      int? prd_sell_price,
      int? prd_sell_price2,
      int? prd_group_id,
      int? prd_manufacture_id,
      int? prd_unit_id,
      required int prd_new,
      required int prd_hot,
      required int prd_highlight,
      int? prd_max,
      int? prd_min,
      required List<UnitConversionResponse> quyDoi}) async {
    BaseResponse response = await commonRepository.productEdit(
        id,
        prdName,
        prdCode,
        prd_sls,
        prd_image_url,
        prd_edit_price,
        prd_print,
        prd_allownegative,
        prd_material,
        prdAdd,
        infor,
        prd_origin_price,
        prd_sell_price,
        prd_sell_price2,
        prd_group_id,
        prd_manufacture_id,
        prd_unit_id,
        prd_new,
        prd_hot,
        prd_highlight,
        prd_max,
        prd_min,
        quyDoi);
    return response;
  }

  Future<List<CategoryData>> loadCategoryList(String tuKhoa) async {
    List<CategoryData> response = await commonRepository.category(tuKhoa);

    categoryList = response;

    return response;
  }

  Future<BaseResponse> catalogueCreate(
      {required String tenDanhMuc, GestureTapCallback? callback}) async {
    BaseResponse response =
        await commonRepository.catalogueCreate(tenDanhMuc, 1);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }
}
