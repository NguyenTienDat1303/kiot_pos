import 'package:kiot_pos/kiot_pos.dart';

class SplashController extends BaseController {
  @override
  void onReady() {
    super.onReady();
  }

  Future<void> initDatabase() async {
    var printerConfigDao = sessionRepository.database.printerConfigDao;
    var printerConfigList = await printerConfigDao.findAll();
    if (printerConfigList.isEmpty) {
      for (int i = 0; i < 1; i++) {
        await printerConfigDao.add(PrinterConfig(null, null, PrintName.invoice,
            i, null, PrintFormat.invoice, null, null));
      }
      for (int i = 0; i < 1; i++) {
        await printerConfigDao.add(PrinterConfig(null, null, PrintName.kitchen,
            i, null, PrintFormat.kitchen, null, null));
      }
      for (int i = 0; i < 1; i++) {
        await printerConfigDao.add(PrinterConfig(
            null,
            null,
            PrintName.receipt_pay,
            i,
            null,
            PrintFormat.receipt_pay,
            null,
            null));
      }
      for (int i = 0; i < 1; i++) {
        await printerConfigDao.add(PrinterConfig(
            null,
            null,
            PrintName.receipt_collect,
            i,
            null,
            PrintFormat.receipt_collect,
            null,
            null));
      }
      for (int i = 0; i < 1; i++) {
        await printerConfigDao.add(PrinterConfig(null, null, PrintName.stamp, i,
            null, PrintFormat.stamp, null, null));
      }
    }
  }

  Future<String> findNavigationTo() async {
    var authKey =
        (await sessionRepository.getField<String?>(LocalPref.auth_key));

    if (authKey != null && authKey!.isNotEmpty) {
      return AppRoutes.home;
    } else {
      return AppRoutes.login;
    }
  }
}
