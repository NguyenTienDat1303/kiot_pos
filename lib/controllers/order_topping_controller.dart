import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderToppingController extends BaseController {
  Rx<List<ToppingData>> _toppingList = Rx([]);

  List<ToppingData> get toppingList => _toppingList.value;

  set toppingList(List<ToppingData> toppingList) {
    _toppingList.value = toppingList;
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future<List<ToppingData>> loadToppingList(String tuKhoa) async {
    List<ToppingData> response = await commonRepository.topping(tuKhoa);
    toppingList = response;
    return response;
  }
}
