import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class StaffController extends BaseController {
  Rx<List<StaffDetailResponse>> _listStaff = Rx([]);
  List<StaffDetailResponse> get listStaff => _listStaff.value;

  set listStaff(List<StaffDetailResponse> listStaff) {
    _listStaff.value = listStaff;
  }

  Future<List<StaffDetailResponse>> searchStaff({required String tuKhoa}) async {
    List<StaffDetailResponse> response = await commonRepository.searchStaff(tuKhoa);
    listStaff = response;
    return response;
  }

   Rx<StaffDetailResponse?> _detailStaff = Rx(StaffDetailResponse());

  StaffDetailResponse? get detailStaff => _detailStaff.value;

  set detailStaff(StaffDetailResponse? listStaff) {
    _detailStaff.value = detailStaff;
  }

  Future<StaffDetailResponse> getDetailStaff({required int id}) async {
    StaffDetailResponse response = await commonRepository.getDetailStaff(id);
    detailStaff = response;
    return response;
  }
}
