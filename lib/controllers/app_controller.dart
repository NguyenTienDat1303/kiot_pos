import 'package:get/get.dart';

class AppController extends GetxController {
  Rx<int> _loadingStack = Rx(0);

  int get loadingStack => _loadingStack.value;

  set loadingStack(int loadingStack) {
    _loadingStack.value = loadingStack;
  }

  pushLoading() {
    _loadingStack.value = loadingStack + 1;

    print("pushLoading $loadingStack");
  }

  popLoading() {
    loadingStack--;
    print("pushLoading $loadingStack");
  }
}
