import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class StaffDetailController extends BaseController {
  Rx<StaffDetailResponse?> _detailStaff = Rx(StaffDetailResponse());

  StaffDetailResponse? get detailStaff => _detailStaff.value;

  set detailStaff(StaffDetailResponse? detailStaff) {
    _detailStaff.value = detailStaff;
  }

  Future<StaffDetailResponse> getDetailStaff({required int id}) async {
    StaffDetailResponse response = await commonRepository.getDetailStaff(id);
    detailStaff = response;
    return response;
  }

  Future<BaseResponse> staffCreate(
      {required String employeeCode,
      required String employeeName,
      required String employeePhone,
      required String employeePrice}) async {
    BaseResponse response = await commonRepository.staffCreate(
        employeeCode, employeeName, employeePhone, employeePrice);

    return response;
  }

  Future<BaseResponse> staffEdit(
      {required int id,
      required String employeeCode,
      required String employeeName,
      required String employeePhone,
      required String employeePrice}) async {
    BaseResponse response = await commonRepository.staffEdit(
        id, employeeCode, employeeName, employeePhone, employeePrice);
    return response;
  }

  Future<BaseResponse> staffDelete({required int id}) async {
    BaseResponse response = await commonRepository.staffDelete(id);
    return response;
  }
}
