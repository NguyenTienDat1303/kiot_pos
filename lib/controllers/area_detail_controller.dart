import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class AreaDetailController extends BaseController {
  Rx<List<TableData>> _tableList = Rx([]);

  List<TableData> get tableList => _tableList.value;

  set tableList(List<TableData> tableList) {
    _tableList.value = tableList;
  }

  Future<List<TableData>> loadTableList({required int id}) async {
    List<TableData> response = (await commonRepository.table(areaId: id, )).danhSach;
    tableList = response;
    return response;
  }

  Future<BaseResponse> tableEdit(
      {required int id,
      required String name,
      GestureTapCallback? callback}) async {
    BaseResponse response = await commonRepository.tableEdit(id, name);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }
}
