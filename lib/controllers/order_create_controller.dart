import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderCreateController extends BaseController {
  TableData? tableData = null;
  int? orderId = null;

  // Rx<List<ProductData>> _productList = Rx([]);

  // List<ProductData> get productList => _productList.value;

  // set productList(List<ProductData> productList) {
  //   _productList.value = productList;
  // }

  // int get productListTotalPrice => productList.fold(
  //     0,
  //     (previousValue, element) =>
  //         previousValue + element.selectedQuantity * element.prdSellPrice!);
  // int get productListTotalQuantity => productList.fold(
  //     0, (previousValue, element) => previousValue + element.selectedQuantity);

  // List<ProductData> get promotionList =>
  //     productList.where((e) => e.prdSellPrice == 0).toList();
  // List<ProductData> get unPromotionList =>
  //     productList.where((e) => e.prdSellPrice != 0).toList();

  Rx<LoadTableResponse> _orderDetail = Rx(LoadTableResponse(
      donHang: OrderDetailData(), sanPhams: [], khuyenMai: [], nhanVien: []));

  LoadTableResponse get orderDetail => _orderDetail.value;

  set orderDetail(LoadTableResponse orderDetail) {
    _orderDetail.value = orderDetail;
  }

  Rx<List<ReturnAddDto>> _orderReturn = Rx([]);

  List<ReturnAddDto> get orderReturn => _orderReturn.value;

  set orderReturn(List<ReturnAddDto> orderReturn) {
    _orderReturn.value = orderReturn;
  }

  @override
  void onReady() {
    super.onReady();
  }

  refreshOrderDetail() {
    _orderDetail.refresh();
  }

  Future<LoadTableResponse?> loadTable() async {
    LoadTableResponse? response;

    // if (id != null) {
    //   response = await commonRepository.loadTable(tableData!.ban!.id!);
    //   orderDetail = response;
    //   orderId = orderDetail.donHang!.id;
    // } else if (orderId != null) {
    //   response = await commonRepository.loadTable(orderId!);
    //   orderDetail = response;
    //   orderId = orderDetail.donHang!.id;
    // } else {
    //   orderDetail = LoadTableResponse(
    //       donHang: OrderDetailData(),
    //       sanPhams: [],
    //       khuyenMai: [],
    //       nhanVien: []);
    // }
    response = await commonRepository.loadTable(tableData!.ban!.id!);
    orderDetail = response;
    if (response.donHang != null) {
      orderId = response.donHang!.id!;
    }

    _orderDetail.refresh();
    return response;
  }

  Future<bool> syncData({int orderStatus = OrderStatus.save}) async {
    print("syncData1 ${orderDetail.nhanVien!.length}");
    var orderDetailDto = orderDetail.sanPhams!
        .map((product) => DetailOrderDto(
            id: product.sanPham!.id!.toString(),
            quantity: product.quantity,
            price: product.price,
            cc1: product.cc1,
            cc2: product.cc2,
            vat: product.vat,
            discount: product.discount,
            discountVnd: product.discountVnd,
            note: product.note,
            detailAdd: product.topping != null
                ? product.topping!
                    .map((topping) => DetailAddDto(
                        idAdd: topping.topping!.id,
                        quantityAdd: topping.quantityAdd,
                        priceAdd: topping.priceAdd))
                    .toList()
                : null))
        .toList();
    var detailQuantityOrderDto = orderDetail.khuyenMai!
        .map((product) => DetailOrderDto(
              id: product.sanPham!.id!.toString(),
              quantity: product.quantity,
            ))
        .toList();
    var detailEmployeeDto = orderDetail.nhanVien!
        .map((e) => DetailEmployeeDto(
              id: e.nhanVien!.id.toString(),
              start: e.start,
              end: e.end,
            ))
        .toList();
    print("syncData2 ${detailQuantityOrderDto!.length}");
    LoadTableResponse? paymentResult = null;
    try {
      if (orderStatus == OrderStatus.tempPayment) {
        await commonRepository.tempPayment(orderDetailDto,
            id: orderId,
            tableId: tableData != null ? tableData!.ban!.id : null,
            orderStatus: orderStatus,
            detailEmployee: detailEmployeeDto,
            customerId:
                orderDetail.tenkhach != null ? orderDetail.tenkhach!.id : null,
            coupon: orderDetail.donHang!.coupon ?? 0,
            customerPay: orderDetail.donHang!.customerPay ?? 0,
            paymentMethod: orderDetail.donHang!.paymentMethod ?? 1,
            vat: orderDetail.donHang!.vat ?? 0,
            notes: "",
            detailQuantityOrder: detailQuantityOrderDto);
      } else {
        await commonRepository.payment(orderDetailDto,
            id: orderId,
            tableId: tableData != null ? tableData!.ban!.id : null,
            orderStatus: orderStatus,
            detailEmployee: detailEmployeeDto,
            customerId:
                orderDetail.tenkhach != null ? orderDetail.tenkhach!.id : null,
            coupon: orderDetail.donHang!.coupon ?? 0,
            customerPay: orderDetail.donHang!.customerPay ?? 0,
            paymentMethod: orderDetail.donHang!.paymentMethod ?? 1,
            vat: orderDetail.donHang!.vat ?? 0,
            notes: "",
            detailQuantityOrder: detailQuantityOrderDto);
      }

      var response = paymentResult;
      response = await loadTable();
      return true;
    } catch (e) {
      await loadTable();
      return false;
    }
  }

  addProduct(ProductData productData) {
    var product = orderDetail.findProduct(productData.id!, version: null);
    if (product != null) {
      product.quantity = product.quantity! + 1;
    } else {
      var odProductData = productData.toODProductData();
      odProductData.price != "0";
      orderDetail.sanPhams!.add(odProductData);
    }
    _orderDetail.refresh();
  }

  addProductList(int type, List<ProductData> productDataList) {
    if (type == OrderProductSelectType.gift) {
      productDataList.forEach((e) {
        e.prdSellPrice = 0;

        orderDetail.khuyenMai!.add(e.toODProductData());
      });
    }
    if (type == OrderProductSelectType.normal) {
      productDataList.forEach((e) {
        orderDetail.sanPhams!.add(e.toODProductData());
      });
    }

    _orderDetail.refresh();
  }

  removeProductAt(int type, int index) {
    if (type == OrderProductSelectType.gift) {
      orderDetail.khuyenMai!.removeAt(index);
    }
    if (type == OrderProductSelectType.normal) {
      orderDetail.sanPhams!.removeAt(index);
    }

    _orderDetail.refresh();
    syncData();
  }

  copyProduct(ODProductData productData) {
    // print("toppingListClone ${productData.toppingList.length}");
    var cloneProductData = productData.copyWith();
    cloneProductData.version = StringUtil.generateRandomString(100);
    orderDetail.sanPhams!.add(cloneProductData);
    _orderDetail.refresh();
    syncData();
  }

  updateProductAt(int type, int index, ODProductData productData) {
    print("updateProduct ${productData.discountVnd}");
    late ODProductData product;

    if (type == OrderProductSelectType.gift) {
      product = orderDetail.khuyenMai![index];
    }
    if (type == OrderProductSelectType.normal) {
      product = orderDetail.sanPhams![index];
    }

    if (productData.quantity! != 0) {
      product.price = productData.price;
      product.discount = productData.discount;
      product.discountVnd = productData.discountVnd;
      product.cc1 = productData.cc1;
      product.cc2 = productData.cc2;
      product.vat = productData.vat;
      product.quantity = productData.quantity;
      product.note = productData.note;
    } else {
      print("removeProduct");
      removeProductAt(type, index);
    }
    // _orderDetail.refresh();
    syncData();
  }

  addTopping(ODProductData productData, ToppingData toppingData) {
    var topping = productData.findTopping(toppingData.id!);
    print("productData.price ${productData.price}");

    if (topping != null) {
      if (productData.price! == 0) topping!.priceAdd = 0;
      topping.quantityAdd = topping.quantityAdd! + 1;
    } else {
      if (productData.price! == 0) toppingData.prdSellPrice = 0;
      productData.topping!.add(toppingData.toODToppingData());
    }
    _orderDetail.refresh();
  }

  deleteTopping(ODProductData productData, ToppingData toppingData) {
    var topping = productData.findTopping(toppingData.id!);
    if (topping != null) {
      if (topping.quantityAdd! > 1) {
        topping.quantityAdd = topping.quantityAdd! - 1;
      } else {
        productData.topping!.remove(topping);
      }
    } else {}

    _orderDetail.refresh();
  }

  Future<bool> addStaff(StaffResponse staffResponse) async {
    var staffData = staffResponse.toODStaffData();
    staffData.start =
        DateFormat(AppValue.backend_datetime_format).format(DateTime.now());
    orderDetail.nhanVien!.add(staffData);

    await syncData();
    return true;
    // if (!orderDetail.nhanVien!.contains(staffData)) {
    //   orderDetail.nhanVien!.add(staffData);

    //   await syncData();
    //   return true;
    // } else
    //   print("da them roi");
    // return false;
  }

  Future<void> deleteStaff(ODStaffData staffData) async {
    orderDetail.nhanVien!.remove(staffData);
    await syncData();
  }

  Future<void> stopStaff(ODStaffData staffData) async {
    staffData.end =
        DateFormat(AppValue.backend_datetime_format).format(DateTime.now());
    await syncData();
  }

  Future<void> changeStartStaff(ODStaffData staffData, DateTime start) async {
    staffData.start =
        DateFormat(AppValue.backend_datetime_format).format(start);
    await syncData();
  }

  Future<bool> addCustomer(CustomerResponse customerResponse) async {
    var customerData = customerResponse.toODCustomerData();
    if (orderDetail.tenkhach != customerData) {
      orderDetail.tenkhach = customerData;
      await syncData();
      return true;
    } else
      return false;
  }

  Future<BaseResponse> moveTable(
    TableData oldTableData,
    TableData newTableData,
    int type,
  ) async {
    BaseResponse response = await commonRepository.moveTable(
        oldTableData.ban!.id!,
        newTableData.ban!.id!,
        oldTableData.donHangID!,
        type,
        newOrderID: newTableData.donHangID);

    return response;
  }

  Future<BaseResponse> getReturn(
      {required int id,
      required List<ReturnAddDto> sanPhams,
      required String notes}) async {
    BaseResponse response =
        await commonRepository.orderReturn(id, sanPhams, notes);

    return response;
  }

  Future<DetailProductResponse> addProductFromQr(String id) async {
    DetailProductResponse response =
        await commonRepository.getDetailProduct(id);

    addProduct(ProductData(
      id: response.sanPham!.id,
      prdCode: response.sanPham!.prdCode,
      prdName: response.sanPham!.prdName,
      prdSellPrice: response.sanPham!.prdSellPrice,
      prdSls: response.sanPham!.prdSls,
      prdImageUrl: response.sanPham!.prdImageUrl,
    ));

    return response;
  }
}
