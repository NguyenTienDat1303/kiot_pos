import 'package:flutter/gestures.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class HomeController extends BaseController {
  Rx<List<PermissionData>> _permissionList = Rx([]);

  List<PermissionData> get permissionList => _permissionList.value;

  set permissionList(List<PermissionData> permissionList) {
    _permissionList.value = permissionList;
  }

  Rx<int> _version = Rx(-1);

  int get version => _version.value;

  set version(int version) {
    _version.value = version;
  }

  Rx<String> _drawName = Rx("");

  String get drawName => _drawName.value;

  set drawName(String drawName) {
    _drawName.value = drawName;
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future<List<PermissionData>> loadPermissonList() async {
    List<PermissionData> permissionResponse =
        await commonRepository.permission();
    permissionList = permissionResponse;
    var localVersion =
        await sessionRepository.getField<String>(LocalPref.version);

    // permissionList.removeWhere((e) => e.id == 53);
    // _permissionList.refresh();

    version = int.parse(localVersion ?? "0");
    // version = int.parse("3");
    return permissionList;
  }

  bool authorization(
    List<int> versions,
    List<int> permissions,
  ) {
    var isContainAllPermission = permissions.every(
        (item) => permissionList.firstWhereOrNull((e) => e.id == item) != null);
    var isMatchVersion = versions.contains(version);

    return isContainAllPermission && isMatchVersion;
  }

  Rxn<UserResponse> _detailUser = Rxn();

  UserResponse? get detailUser => _detailUser.value;

  set detailUser(UserResponse? detailUser) {
    _detailUser.value = detailUser;
  }

  Future<UserResponse> getUserDetail() async {
    UserResponse response = await commonRepository.getUserDetail();
    detailUser = response;
    return response;
  }

  Future<BaseResponse> userEdit(
      {required int id,
      required String displayName,
      required String email,
      GestureTapCallback? onTap}) async {
    BaseResponse response =
        await commonRepository.userEdit(id, displayName, email);

    return response;
  }
}
