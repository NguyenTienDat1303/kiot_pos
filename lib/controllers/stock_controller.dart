import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/data/entities/response/stock_detail_response.dart';
import 'package:kiot_pos/data/entities/response/stock_search_response.dart';
import 'package:kiot_pos/kiot_pos.dart';

class StockController extends BaseController {
  Rx<int> _idStock = Rx(-1);
  set idStock(int idStock) {
    _idStock.value = idStock;
  }

  onFresh() {
    _listStockCreate.refresh();
  }

  int get idStock => _idStock.value;

  Rx<int> _isCompleted = Rx(0);
  int get isCompleted => _isCompleted.value;

  set isCompleted(int isCompleted) {
    _isCompleted.value = isCompleted;
  }

  Rx<List<StockSearchResponse>> _listStock = Rx([]);
  List<StockSearchResponse> get listStock => _listStock.value;

  set listStock(List<StockSearchResponse> listStock) {
    _listStock.value = listStock;
  }

  Rx<List<ChiTiet>> _listStockCreate = Rx([]);
  List<ChiTiet> get listStockCreate => _listStockCreate.value;

  set listStockCreate(List<ChiTiet> listStockCreate) {
    _listStockCreate.value = listStockCreate;
  }

  Future<List<StockSearchResponse>> getStockSearch({
    required String tuKhoa,
  }) async {
    List<StockSearchResponse> response =
        await commonRepository.getStockSearch(10, tuKhoa);
    listStock = response;
    return response;
  }

  Rxn<StockDetailResponse> _detailStock = Rxn();

  StockDetailResponse? get detailStock => _detailStock.value;

  set detailStock(StockDetailResponse? detailStock) {
    _detailStock.value = detailStock;
  }

  Future<StockDetailResponse> getDetailStock({required int id}) async {
    StockDetailResponse response = await commonRepository.getDetailStock(id);
    detailStock = response;
    return response;
  }

  Future<BaseResponse> stockEdit(
      {required int id,
      required int adjustStatus,
      required List<StockDto> data,
      GestureTapCallback? callback}) async {
    BaseResponse response =
        await commonRepository.stockEdit(id, adjustStatus, data);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Future<BaseResponse> stockCreate(
      {required int adjustStatus,
      required List<StockDto> data,
      required String notes,
      GestureTapCallback? callback}) async {
    BaseResponse response =
        await commonRepository.stockCreate(adjustStatus, data);
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        onTapOK: () {
          Get.back();
          if (callback != null) {
            callback();
          }
        },
        children: [Text(response.message!, style: AppStyles.DEFAULT_16)]));
    return response;
  }

  Future<BaseResponse> stockDelete({
    required int id,
  }) async {
    BaseResponse response = await commonRepository.stockDelete(id);

    return response;
  }

  Future<ItemQrResponse> loadQr({
    required String code,
  }) async {
    ItemQrResponse response = await commonRepository.loadQr(code);

    return response;
  }

  Future<DetailProductResponse> getDetailProduct(String id) async {
    DetailProductResponse response =
        await commonRepository.getDetailProduct(id);
    return response;
  }
}
