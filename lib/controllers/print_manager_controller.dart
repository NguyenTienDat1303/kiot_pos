import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class PrintManagerController extends BaseController {
  PrinterBluetoothManager bluetoothManager = PrinterBluetoothManager();

  Rx<List<PrinterBluetooth>> _bluetoothPrinters = Rx([]);

  List<PrinterBluetooth> get bluetoothPrinters => _bluetoothPrinters.value;

  set bluetoothPrinters(List<PrinterBluetooth> bluetoothPrinters) {
    _bluetoothPrinters.value = bluetoothPrinters;
  }

  Rx<bool> _scanLoading = Rx(false);

  bool get scanLoading => _scanLoading.value;

  set scanLoading(bool scanLoading) {
    _scanLoading.value = scanLoading;
  }

  Rx<List<PrintData>> _printFormData = Rx([]);

  List<PrintData> get printFormData => _printFormData.value;

  set printFormData(List<PrintData> printFormData) {
    _printFormData.value = printFormData;
  }

  Rx<bool> _printKitchen = Rx(false);

  bool get printKitchen => _printKitchen.value;

  set printKitchen(bool printKitchen) {
    _printKitchen.value = printKitchen;
  }

  Rx<bool> _printDouble = Rx(false);

  bool get printDouble => _printDouble.value;

  set printDouble(bool printDouble) {
    _printDouble.value = printDouble;
  }

  Rx<bool> _printAuto = Rx(false);

  bool get printAuto => _printAuto.value;

  set printAuto(bool printAuto) {
    _printAuto.value = printAuto;
  }

  Rx<int> _printType = Rx(PrintType.lan);

  int get printType => _printType.value;

  set printType(int printType) {
    _printType.value = printType;
    sessionRepository.saveField(LocalPref.print_type, printType);
  }

  @override
  void onReady() {
    super.onReady();
    loadPrintFormList();

    // startScanBluetooth();
  }

  Future<void> loadConfigs() async {
    try {
      printKitchen =
          await sessionRepository.getField<bool?>(LocalPref.print_kitchen) ??
              false;
      printDouble =
          await sessionRepository.getField<bool?>(LocalPref.print_double) ??
              false;
      printAuto =
          await sessionRepository.getField<bool?>(LocalPref.print_auto) ??
              false;
      printType =
          await sessionRepository.getField<int?>(LocalPref.print_type) ??
              PrintType.lan;
    } catch (e) {}
  }

  void startScanBluetooth() {
    print("startScanBluetooth");
    bluetoothManager.scanResults.listen((printers) async {
      print("scanResults ${printers.length}");
      bluetoothPrinters = printers;
    });
    bluetoothPrinters = [];
    scanLoading = true;
    Future.delayed(Duration(seconds: 2), () {
      scanLoading = false;
      print("stop");
    });
    bluetoothManager.startScan(Duration(seconds: 2));
  }

  void stopScanBluetooth() {
    bluetoothManager.stopScan();
  }

  Future<List<PrintData>> loadPrintFormList() async {
    List<PrintData> reponseList = await commonRepository.printFormList();
    printFormData = reponseList;
    return reponseList;
  }

  Rx<List<PrintingPaper>> _printingPaperList = Rx([]);

  List<PrintingPaper> get printingPaperList => _printingPaperList.value;

  set printingPaperList(List<PrintingPaper> printingPaperList) {
    _printingPaperList.value = printingPaperList;
  }

  Rx<List<PrinterConfig>> _printerConfigList = Rx([]);

  List<PrinterConfig> get printerConfigList => _printerConfigList.value;

  set printerConfigList(List<PrinterConfig> printerConfigList) {
    _printerConfigList.value = printerConfigList;
  }

  Future<List<PrinterConfig>> loadPrinterConfigByType(int type) async {
    var printerConfigDao = sessionRepository.database.printerConfigDao;
    var results = await printerConfigDao.findByFormat(type);
    print("type ${type}");
    print("results ${results.length}");
    printerConfigList = results;
    return results;
  }

  Future<bool> printForm(PrintAction action,
      {TableData? tableData,
      LoadTableResponse? loadTableResponse,
      CollectResponse? collectResponse,
      PayResponse? payResponse}) async {
    HomeController homeController = Get.find<HomeController>();

    var printKitchen =
        await sessionRepository.getField<bool?>(LocalPref.print_kitchen) ??
            false;
    var printDouble =
        await sessionRepository.getField<bool?>(LocalPref.print_double) ??
            false;
    var printAuto =
        await sessionRepository.getField<bool?>(LocalPref.print_auto) ?? false;
    var printType =
        await sessionRepository.getField<int?>(LocalPref.print_type) ??
            PrintType.lan;
    printingPaperList.clear();
    switch (action) {
      case PrintAction.payment:
        printingPaperList.add(PrintingPaper(
            KiotBlueprint.getInvoicePrintData(
                printFormData[0].content!, loadTableResponse!),
            PrintFormat.invoice,
            orderDetail: loadTableResponse));
        if (printDouble) {
          printingPaperList.add(PrintingPaper(
              KiotBlueprint.getInvoicePrintData(
                  printFormData[0].content!, loadTableResponse!),
              PrintFormat.invoice,
              orderDetail: loadTableResponse));
        }
        break;
      case PrintAction.payment_temp:
        printingPaperList.add(PrintingPaper(
            KiotBlueprint.getInvoicePrintData(
                printFormData[0].content!, loadTableResponse!),
            PrintFormat.invoice,
            orderDetail: loadTableResponse));
        if (printKitchen) {
          printingPaperList.add(PrintingPaper(
            KiotBlueprint.getKitchenPrintData(
                printFormData[6].content!, tableData!, loadTableResponse!),
            PrintFormat.kitchen,
            orderDetail: loadTableResponse,
          ));
        }
        break;
      case PrintAction.kitchen:
        printingPaperList.add(PrintingPaper(
          KiotBlueprint.getKitchenPrintData(
              printFormData[6].content!, tableData!, loadTableResponse!),
          PrintFormat.kitchen,
          orderDetail: loadTableResponse,
        ));

        break;
      case PrintAction.receipt_collect:
        printingPaperList.add(PrintingPaper(
          KiotBlueprint.getReceiptCollectPrintData(
              printFormData[4].content!, collectResponse!),
          PrintFormat.receipt_pay,
        ));
        break;
      case PrintAction.receipt_pay:
        printingPaperList.add(PrintingPaper(
          KiotBlueprint.getReceiptPayPrintData(
              printFormData[5].content!, payResponse!),
          PrintFormat.receipt_pay,
        ));

        break;
      case PrintAction.stamp:
        loadTableResponse!.sanPhams!.forEach((e) {
          for (int i = 0; i < (e.quantity ?? 0); i++) {
            printingPaperList.add(PrintingPaper(
                KiotBlueprint.getStampPrintData(
                    printFormData[7].content!, tableData!, loadTableResponse, e),
                PrintFormat.stamp,
                orderDetail: loadTableResponse,
                productData: e));
          }
        });
        break;
    }

    _printingPaperList.refresh();

    await Get.toNamed(AppRoutes.print_preview);
    return true;
  }
}
