import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderController extends BaseController {
  Rx<List<OrderResponse>> _listOrder = Rx([]);

  List<OrderResponse> get listOrder => _listOrder.value;

  set listOrder(List<OrderResponse> listOrder) {
    _listOrder.value = listOrder;
  }

  

  Future<List<OrderResponse>> getListOrder(
      String tuKhoa,
      int? isDaXoa,
      int? phongID,
      int? trangThaiID,
      String? tuNgay,
      String? denNgay,
      int perPage) async {
    List<OrderResponse> response = await commonRepository.getListOrder(
        tuKhoa, isDaXoa, phongID, trangThaiID, tuNgay, denNgay, perPage);
    listOrder = response;
    return response;
  }
}
