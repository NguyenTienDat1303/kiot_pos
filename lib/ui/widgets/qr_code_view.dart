import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_qr_reader/flutter_qr_reader.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/res/colors.dart';

class QrcodeReaderView extends StatefulWidget {
  final double scanBoxRatio;
  final Color boxLineColor;

  const QrcodeReaderView(
      {Key? key, required this.scanBoxRatio, required this.boxLineColor, t})
      : super(key: key);

  @override
  QrcodeReaderViewState createState() => new QrcodeReaderViewState();
}

class QrcodeReaderViewState extends State<QrcodeReaderView>
    with TickerProviderStateMixin {
  QrReaderViewController? _controller;
  AnimationController? _animationController;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _initAnimation();
  }

  void _initAnimation() {
    setState(() {
      _animationController = AnimationController(
          vsync: this, duration: Duration(milliseconds: 1200));
    });
    _animationController!
      ..addListener(_upState)
      ..addStatusListener((state) {
        if (state == AnimationStatus.completed) {
          _timer = Timer(Duration(seconds: 1), () {
            _animationController?.reverse(from: 1.0);
          });
        } else if (state == AnimationStatus.dismissed) {
          _timer = Timer(Duration(seconds: 1), () {
            _animationController?.forward(from: 0.0);
          });
        }
      });
    _animationController!.forward(from: 0.0);
  }

  void _clearAnimation() {
    _timer?.cancel();
    if (_animationController != null) {
      _animationController?.dispose();
      _animationController = null;
    }
  }

  void _upState() {
    setState(() {});
  }

  void _onCreateController(QrReaderViewController controller) async {
    _controller = controller;
    _controller!.startCamera(_onQrBack);
  }

  bool isScan = false;

  Future _onQrBack(data, _) async {
    if (isScan == true) return;
    isScan = true;
    stopScan();
    await onScan(data);
  }

  Future onScan(String data) async {
    if (data != '') {
      Get.back(result: data);
    }
    scaffoldKey.currentState;
  }

  void startScan() {
    isScan = false;
    _controller!.startCamera(_onQrBack);
    _initAnimation();
  }

  void stopScan() {
    _clearAnimation();
    _controller!.stopCamera();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black,
      child: LayoutBuilder(builder: (context, constraints) {
        final qrScanSize = constraints.maxWidth * widget.scanBoxRatio;
        if (constraints.maxHeight < qrScanSize * 1.5) {}
        return Stack(
          children: <Widget>[
            SizedBox(
              width: constraints.maxWidth,
              height: constraints.maxHeight,
              child: QrReaderView(
                width: constraints.maxWidth,
                height: constraints.maxHeight,
                callback: _onCreateController,
              ),
            ),
            // if (widget.headerWidget != null) ,
            Positioned(
                top: 0,
                left: 0,
                child: Container(
                  height: (constraints.maxHeight - qrScanSize) * 0.333333,
                  width: constraints.maxHeight,
                  color: Colors.black26,
                )),
            Positioned(
                top: ((constraints.maxHeight - qrScanSize) * 0.333333) +
                    qrScanSize,
                left: 0,
                child: Container(
                  height: (constraints.maxHeight - qrScanSize),
                  width: constraints.maxHeight,
                  color: Colors.black26,
                )),
            Positioned(
                top: (constraints.maxHeight - qrScanSize) * 0.333333,
                left: 0,
                child: Container(
                  height: qrScanSize,
                  width: ((constraints.maxWidth - qrScanSize) / 2) + 2,
                  color: Colors.black26,
                )),
            Positioned(
                top: (constraints.maxHeight - qrScanSize) * 0.333333,
                left:
                    ((constraints.maxWidth - qrScanSize) / 2) + qrScanSize - 2,
                child: Container(
                  height: qrScanSize,
                  width: ((constraints.maxWidth - qrScanSize) / 2) + 2,
                  color: Colors.black26,
                )),

            Positioned(
              left: (constraints.maxWidth - qrScanSize) / 2,
              top: (constraints.maxHeight - qrScanSize) * 0.333333,
              child: CustomPaint(
                painter: QrScanBoxPainter(
                    _animationController!.value,
                    _animationController?.status == AnimationStatus.forward,
                    widget.boxLineColor),
                child: SizedBox(
                  width: qrScanSize,
                  height: qrScanSize,
                ),
              ),
            ),
          ],
        );
      }),
    );
  }

  @override
  void dispose() {
    _clearAnimation();
    super.dispose();
  }
}

class QrScanBoxPainter extends CustomPainter {
  double animationValue;
  bool isForward;
  Color boxLineColor = AppColors.Orange1.withOpacity(0.8);

  QrScanBoxPainter(this.animationValue, this.isForward, this.boxLineColor);

  @override
  void paint(Canvas canvas, Size size) {
    final borderRadius = BorderRadius.all(Radius.circular(0)).toRRect(
      Rect.fromLTWH(0, 0, size.width, size.height),
    );
    canvas.drawRRect(
      borderRadius,
      Paint()
        ..color = Colors.transparent
        ..style = PaintingStyle.stroke
        ..strokeWidth = 1,
    );
    final borderPaint = Paint()
      ..color = AppColors.Orange1
      ..style = PaintingStyle.stroke
      ..strokeWidth = 4;
    final path = new Path();
    // leftTop
    path.moveTo(0, 30);
    path.lineTo(0, 0);
    // path.quadraticBezierTo(0, 0, 12, 0);
    path.lineTo(30, 0);
    // rightTop
    path.moveTo(size.width - 30, 0);
    path.lineTo(size.width, 0);
    // path.quadraticBezierTo(size.width, 0, size.width, 12);
    path.lineTo(size.width, 30);
    // rightBottom
    path.moveTo(size.width, size.height - 30);
    path.lineTo(size.width, size.height);
    // path.quadraticBezierTo(
    //     size.width, size.height, size.width - 12, size.height);
    path.lineTo(size.width - 30, size.height);
    // leftBottom
    path.moveTo(30, size.height);
    path.lineTo(0, size.height);
    // path.quadraticBezierTo(0, size.height, 0, size.height - 12);
    path.lineTo(0, size.height - 30);

    canvas.drawPath(path, borderPaint);

    canvas.clipRRect(
        BorderRadius.all(Radius.circular(0)).toRRect(Offset.zero & size));

    final linePaint = Paint();
    final lineSize = size.height;
    final leftPress = (size.height + lineSize) * animationValue - lineSize;
    linePaint.style = PaintingStyle.stroke;
    linePaint.shader = LinearGradient(
      colors: [Colors.transparent, boxLineColor],
      begin: isForward ? Alignment.topCenter : Alignment(0.0, 2.0),
      end: isForward ? Alignment(0.0, 0.5) : Alignment.topCenter,
    ).createShader(Rect.fromLTWH(0, leftPress, size.width, lineSize));
    // for (int i = 0; i < size.height / 5; i++) {
    //   canvas.drawLine(
    //     Offset(
    //       i * 5.0,
    //       leftPress,
    //     ),
    //     Offset(i * 5.0, leftPress + lineSize),
    //     linePaint,
    //   );
    // }
    // for (int i = 0; i < lineSize / 5; i++) {
    //   canvas.drawLine(
    //     Offset(0, leftPress + i * 5.0),
    //     Offset(
    //       size.width,
    //       leftPress + i * 5.0,
    //     ),
    //     linePaint,
    //   );
    // }
  }

  @override
  bool shouldRepaint(QrScanBoxPainter oldDelegate) =>
      animationValue != oldDelegate.animationValue;

  @override
  bool shouldRebuildSemantics(QrScanBoxPainter oldDelegate) =>
      animationValue != oldDelegate.animationValue;
}
