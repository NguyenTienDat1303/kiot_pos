import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';

import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:kiot_pos/res/res.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/widgets/bottom_sheet.dart';

class AvatarCreate extends StatefulWidget {
  File? image;
  String? imageNetWork;
  final ValueChanged<File> onChange;
  AvatarCreate(
      {Key? key, this.image, required this.onChange, this.imageNetWork})
      : super(key: key);

  @override
  State<AvatarCreate> createState() => _AvatarCreateState();
}

class _AvatarCreateState extends State<AvatarCreate> {
  onBottomSheetPickerAvatar() {
    Get.bottomSheet(ConstrainedBox(
        constraints: BoxConstraints(maxHeight: 180),
        child: Container(
            child: LhBottomSheetUi(
          backgroundColor: Colors.grey.withOpacity(0),
          color: Colors.white,
          child: Container(
            // margin: EdgeInsets.only(top: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ChooseImage(Icons.photo_library, 'Thư viện', () {
                      Get.back();
                      selectImage(0);
                    }),
                    ChooseImage(Icons.photo_camera, 'Máy ảnh', () {
                      Get.back();
                      selectImage(1);
                    }),
                  ],
                ),
              ],
            ),
          ),
        ))));
  }

  void selectImage(int choose) async {
    List<Media>? res;
    if (choose == 0) {
      res = await ImagesPicker.pick(
        count: 1,
        pickType: PickType.image,
        cropOpt: CropOption(
          aspectRatio: CropAspectRatio.custom,
          cropType: CropType.rect, // currently for android
        ),
      );
    } else {
      res = await ImagesPicker.openCamera(
        pickType: PickType.image,
        cropOpt: CropOption(
          aspectRatio: CropAspectRatio.custom,
          cropType: CropType.rect, // currently for android
        ),
      );
    }
    if (res != null) {
      setState(() {
        widget.onChange(File(res![0].path));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return _renderAvatar();
  }

  Widget ChooseImage(IconData icon, String name, GestureTapCallback? onTap) {
    final sizeWidth = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: onTap,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              child: Icon(
                icon,
                size: sizeWidth * 0.15,
              ),
            ),
            Text(
              name,
              style: AppStyles.DEFAULT_16.copyWith(fontWeight: FontWeight.w400),
            )
          ],
        ),
      ),
    );
  }

  _renderAvatar() {
    final sizeHeight = MediaQuery.of(context).size.height;
    final sizeWidth = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: onBottomSheetPickerAvatar,
      child: Stack(
        children: [
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(width: 1, color: Colors.black)),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(sizeWidth),
                child: widget.image != null
                    ? Image.file(
                        widget.image!,
                        fit: BoxFit.cover,
                      )
                    : widget.imageNetWork != null && widget.imageNetWork != ''
                        ? CachedNetworkImage(
                            imageUrl: widget.imageNetWork!,
                            fit: BoxFit.cover,
                            progressIndicatorBuilder:
                                (context, url, downloadProgress) =>
                                    CircularProgressIndicator(
                                        value: downloadProgress.progress),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          )
                        : Image.asset(
                            './assets/images/logo.png',
                            fit: BoxFit.cover,
                          )),
          ),
          Positioned.fill(
              child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              child: Image.asset('./assets/images/camera_icon.png', height: 23),
            ),
          )),
        ],
      ),
    );
  }
}
