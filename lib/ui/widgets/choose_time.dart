import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/res/values.dart';
import 'package:get/get.dart';

import 'base_info_widget.dart';

class ChooseTime extends StatefulWidget {
  final int indexTabBar;
  final ValueChanged<String> onChangeFrom;
  final ValueChanged<String> onChangeTo;
  const ChooseTime(
      {Key? key,
      required this.indexTabBar,
      required this.onChangeFrom,
      required this.onChangeTo})
      : super(key: key);

  @override
  State<ChooseTime> createState() => _ChooseTimeState();
}

class _ChooseTimeState extends State<ChooseTime> {
  String dayStart = AppValue.DATE_FORMAT.format(DateTime.now());
  String dayEnd = AppValue.DATE_FORMAT.format(DateTime.now());
  String day = '';
  DateTime lastSelect = DateTime.now();
  int lastHour = TimeOfDay.now().hour;
  int lastMinute = TimeOfDay.now().minute;

  Future onDayPicker(BuildContext context) async {
    final initialDate = DateTime.now();
    final newDay = await showDatePicker(
        context: context,
        initialDate: initialDate,
        initialEntryMode: DatePickerEntryMode.calendarOnly,
        builder: (context, child) {
          return Theme(
              data: ThemeData.light().copyWith(
                colorScheme: ColorScheme.light(
                  primary: AppColors.Blue,
                  onPrimary: Colors.white,
                  surface: AppColors.Blue,
                  onSurface: AppColors.Gray1,
                ),
              ),
              child: child!);
        },
        firstDate: DateTime(DateTime.now().year - 100),
        lastDate: DateTime(DateTime.now().year + 100));
    if (newDay == null) return;
    setState(() {
      lastSelect = newDay;
      day = AppValue.DATE_FORMAT.format(newDay);
      dayStart = day;
      widget.onChangeFrom(dayStart);
    });
  }

  DateTime lastSelect1 = DateTime.now();
  Future onDayPicker1(BuildContext context) async {
    final initialDate = lastSelect;
    final newDay = await showDatePicker(
        context: context,
        initialDate: initialDate,
        initialEntryMode: DatePickerEntryMode.calendarOnly,
        builder: (context, child) {
          return Theme(
              data: ThemeData.light().copyWith(
                colorScheme: ColorScheme.light(
                  primary: AppColors.Blue,
                  onPrimary: Colors.white,
                  surface: AppColors.Blue,
                  onSurface: AppColors.Gray1,
                ),
              ),
              child: child!);
        },
        firstDate: initialDate,
        lastDate: DateTime(DateTime.now().year + 10));
    if (newDay == null) return;
    setState(() {
      lastSelect1 = newDay;
      day = AppValue.DATE_FORMAT.format(newDay);
      dayEnd = day;
      widget.onChangeTo(dayEnd);
    });
  }

  List<ItemPickerDay> listPickerDay = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    listPickerDay = [
      ItemPickerDay(dayStart, 'Từ'),
      ItemPickerDay(dayEnd, 'Đến'),
    ];
    return Row(
      children: [
        ...List.generate(
          listPickerDay.length,
          (index) => Expanded(
            child: Container(
              padding: EdgeInsets.only(
                  left: index == 1 ? 6 : 0, right: index == 0 ? 6 : 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    listPickerDay[index].type,
                    style: AppStyles.DEFAULT_16.copyWith(
                      color: AppColors.Gray1,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 4),
                    child: GestureDetector(
                      onTap: () {
                        if (index == 0) {
                          onDayPicker(context);
                          setState(() {
                            dayEnd = '';
                          });
                        } else {
                          onDayPicker1(context);
                        }
                      },
                      child: CellWhite(
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                listPickerDay[index].day,
                                style: AppStyles.DEFAULT_16.copyWith(
                                  color: AppColors.Gray1,
                                ),
                              ),
                            ),
                            Image.asset('./assets/images/lich_icon.png',
                                height: 24,
                                color: widget.indexTabBar == 2
                                    ? AppColors.Orange
                                    : AppColors.Gray1),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class ItemPickerDay {
  final String day, type;

  ItemPickerDay(this.day, this.type);
}

class PickerTime {
  static void showCupertinoTimePicker(BuildContext context,
      {String? title,
      required ValueChanged<DateTime> onSubmitted,
      VoidCallback? onDelete,
      bool use24hFormat = true,
      DateTime? minimumDate,
      DateTime? initialDateTime,
      DateTime? maximumDate}) {
    DateTime datetime = minimumDate!;
    FocusScope.of(context).unfocus();
    showCupertinoModalPopup(
        context: context,
        builder: (_) => Wrap(
              children: [
                Container(
                  color: Color.fromARGB(255, 255, 255, 255),
                  child: Column(
                    children: [
                      Container(
                        height: 300,
                        child: Column(
                          children: [
                            Material(
                              child: Container(
                                color: Colors.grey.shade100,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 20),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        '${title ?? ''}',
                                        style: AppStyles.DEFAULT_18_BOLD
                                            .copyWith(color: Colors.lightBlue),
                                      ),
                                    ),
                                    RichText(
                                      text: new TextSpan(
                                        children: [
                                          if (onDelete != null)
                                            new TextSpan(
                                              text:
                                                  'cupertino_picker.delete'.tr,
                                              style: AppStyles.DEFAULT_18_BOLD
                                                  .copyWith(color: Colors.red),
                                              recognizer:
                                                  new TapGestureRecognizer()
                                                    ..onTap = () {
                                                      onDelete();
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                            ),
                                          if (onDelete != null)
                                            new TextSpan(
                                              text: ' | ',
                                              style: AppStyles.DEFAULT_18_BOLD
                                                  .copyWith(
                                                      color: Colors.lightBlue),
                                            ),
                                          new TextSpan(
                                              text:
                                                  'cupertino_picker.select'.tr,
                                              style: AppStyles.DEFAULT_18_BOLD
                                                  .copyWith(
                                                      color: AppColors.Blue),
                                              recognizer:
                                                  new TapGestureRecognizer()
                                                    ..onTap = () {
                                                      onSubmitted(datetime);
                                                      Navigator.of(context)
                                                          .pop();
                                                    }),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: CupertinoDatePicker(
                                initialDateTime: initialDateTime ?? minimumDate,
                                use24hFormat: use24hFormat,
                                minuteInterval: 5,
                                mode: CupertinoDatePickerMode.time,
                                onDateTimeChanged: (value) {
                                  datetime = value;
                                },
                                minimumDate: minimumDate,
                                maximumDate: maximumDate,
                              ),
                            ),
                          ],
                        ),
                      ),

                      // Close the modal
                    ],
                  ),
                ),
              ],
            ));
  }

  static Future<bool> showConfirmDialog(
    BuildContext context,
    String title,
    String message, {
    String? positiveTitle,
    VoidCallback? positiveTap,
    TextStyle? positiveStyle,
    String? negativeTitle,
    VoidCallback? negativeTap,
    TextStyle? negativeStyle,
  }) async {
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(false);
                if (negativeTap != null) negativeTap();
              },
              child: Text(
                negativeTitle ?? 'No',
                style: negativeStyle,
              ),
            ),
            negativeTitle != null || negativeTap != null
                ? TextButton(
                    onPressed: () {
                      Navigator.of(context).pop(true);
                      if (positiveTap != null) positiveTap();
                    },
                    child: Text(
                      positiveTitle ?? 'Yes',
                      style: positiveStyle,
                    ),
                  )
                : SizedBox.shrink(),
          ],
        );
      },
    );
  }

  static Widget alertDialogConfirm(String title, String message,
      {String? positiveTitle,
      VoidCallback? positiveTap,
      TextStyle? positiveStyle,
      String? negativeTitle,
      VoidCallback? negativeTap,
      TextStyle? negativeStyle}) {
    return AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: <Widget>[
        negativeTitle != null || negativeTap != null
            ? TextButton(
                onPressed: () {
                  if (negativeTap != null) negativeTap();
                },
                child: Text(
                  negativeTitle ?? 'No',
                  style: negativeStyle,
                ),
              )
            : const SizedBox.shrink(),
        TextButton(
          onPressed: () {
            if (positiveTap != null) positiveTap();
          },
          child: Text(
            positiveTitle ?? 'Yes',
            style: positiveStyle,
          ),
        ),
      ],
    );
  }

  static void setStatusBarStyle(
      {required Brightness brightness,
      Brightness? statusBarIconBrightness,
      Brightness? systemNavigationBarIconBrightness,
      Color? systemNavigationBarColor,
      Color? statusBarColor,
      Color? systemNavigationBarDividerColor}) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarBrightness: brightness,
      statusBarIconBrightness: statusBarIconBrightness,
      systemNavigationBarIconBrightness: systemNavigationBarIconBrightness,
      systemNavigationBarColor:
          systemNavigationBarColor, // navigation bar color
      statusBarColor: statusBarColor, // status bar color

      systemNavigationBarDividerColor: systemNavigationBarDividerColor,
      systemStatusBarContrastEnforced: true,
      systemNavigationBarContrastEnforced: true,
    ));
  }

  static void setStatusBarBrightness(Brightness brightness) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarBrightness: brightness,
        statusBarIconBrightness:
            brightness == Brightness.light ? Brightness.dark : Brightness.light,
        systemNavigationBarIconBrightness:
            brightness == Brightness.light ? Brightness.dark : Brightness.light,
        systemNavigationBarColor: brightness == Brightness.light
            ? Colors.black.withOpacity(0.4)
            : Colors.white.withOpacity(0.1),
        statusBarColor: Colors.transparent,
        systemNavigationBarDividerColor: Colors.transparent));
  }
}
