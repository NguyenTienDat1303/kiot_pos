import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/controllers/controllers.dart';

class GuardContainer extends StatelessWidget {
  final Widget child;
  final List<int> permissions;
  final List<int> version;
  final Function(List<int> permissions, int version)? onTap;
  const GuardContainer(
      {Key? key,
      required this.child,
      required this.permissions,
      required this.version,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetX<HomeController>(
      builder: (controller) {
        var isContainAllPermission = permissions
            .every((item) => controller.permissionList.contains(item));
        var isMatchVersion = version.contains(controller.version.toInt());
        return Column(
          children: [
            Container(
                height: 0,
                child: Text(controller.permissionList.length.toString())),
            Visibility(
                visible: isContainAllPermission && isMatchVersion,
                child: GestureDetector(
                    onTap: () {
                      if (onTap != null) {
                        onTap!(
                            controller.permissionList.map((e) => e.id).toList(),
                            controller.version);
                      }
                    },
                    child: child)),
          ],
        );
      },
    );
  }
}
