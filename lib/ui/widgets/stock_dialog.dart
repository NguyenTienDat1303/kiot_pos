import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';

class StockDialog extends StatefulWidget {
  final String? title, nameButtonLeft, nameButtonRight;
  final List<Widget> children;
  final bool isTwoButton;
  final VoidCallback? backDropTap;
  final GestureTapCallback? onTapOK, onTapLeft, onTapRight;

  final EdgeInsets? outerPadding;
  final EdgeInsets? innerPadding;
  final AlignmentGeometry? alignment;

  const StockDialog(
      {Key? key,
      this.title,
      required this.children,
      this.backDropTap,
      this.outerPadding,
      this.innerPadding,
      this.alignment,
      this.isTwoButton = false,
      this.onTapOK,
      this.nameButtonLeft,
      this.nameButtonRight,
      this.onTapLeft,
      this.onTapRight})
      : super(key: key);

  @override
  _StockDialogState createState() => _StockDialogState();
}

class _StockDialogState extends State<StockDialog> {
  static const _outerPadding =
      EdgeInsets.symmetric(vertical: 20, horizontal: 30);
  static const _innerPadding =
      EdgeInsets.symmetric(vertical: 16, horizontal: 16);
  @override
  Widget build(BuildContext context) {
    // final sizeHeight = MediaQuery.of(context).size.height;
    // final sizeWidth = MediaQuery.of(context).size.width;

    return Material(
      color: Colors.transparent,
      child: Stack(
        children: [
          _renderBackdrop(),
          Center(
            child: Container(
              margin: widget.outerPadding ?? _outerPadding,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12.0)),
              child: Stack(
                children: [
                  SingleChildScrollView(
                    padding: EdgeInsets.only(top: 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: AppColors.Blue,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12))),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if (widget.title != null)
                                Expanded(
                                    child: Container(
                                        padding: EdgeInsets.all(16),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          widget.title!,
                                          style: AppStyles.DEFAULT_16_BOLD
                                              .copyWith(
                                            fontWeight: FontWeight.w600,
                                            color: AppColors.White,
                                          ),
                                        ))),
                              Material(
                                color: AppColors.Blue,
                                borderRadius: BorderRadius.circular(12),
                                child: InkWell(
                                  onTap: () {
                                    Get.back();
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(16),
                                    child: Icon(
                                      Icons.close,
                                      size: 30,
                                      color: AppColors.White,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [...widget.children],
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.all(16),
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(top: 24),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                GestureDetector(
                                  onTap: widget.onTapLeft ?? () => Get.back(),
                                  child: Container(
                                    alignment: Alignment.center,
                                    width: 120,
                                    height: 40,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        color: AppColors.Blue),
                                    child: Text(
                                      'Ghi đè',
                                      style: AppStyles.DEFAULT_16.copyWith(
                                          color: AppColors.White,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ),
                                ),
                                Container(
                                    margin: EdgeInsets.only(left: 16),
                                    child: GestureDetector(
                                      onTap: widget.onTapRight,
                                      child: Container(
                                        alignment: Alignment.center,
                                        width: 120,
                                        height: 40,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          color: AppColors.Orange,
                                        ),
                                        child: Text(
                                          'Cộng thêm',
                                          style: AppStyles.DEFAULT_16.copyWith(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w700),
                                        ),
                                      ),
                                    ))
                              ],
                            ))
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _renderBackdrop() {
    return Positioned.fill(
        child: GestureDetector(
      onTap: widget.backDropTap ?? () => Get.back(),
      child: Container(
        color: Colors.transparent,
      ),
    ));
  }

  double _width = 0;
}
