import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';

typedef Widget WidgetBuilder<T>(final int id, final T item, T? selected);
typedef void GestureTapCallbackCustom<T>(final int id, T? selected);

class WidgetBottomSheet<T> extends StatefulWidget {
  WidgetBuilder<T> builder;
  GestureTapCallbackCustom<T> onTap;
  List<T> list;
  String? value;
  String hintText;
  WidgetBottomSheet(
      {Key? key,
      required this.builder,
      required this.onTap,
      required this.list,
      this.value,
      required this.hintText})
      : super(key: key);

  @override
  State<WidgetBottomSheet> createState() => _WidgetBottomSheetState<T>();
}

class _WidgetBottomSheetState<T> extends State<WidgetBottomSheet<T>> {
  T? item;
  @override
  Widget build(BuildContext context) {
    return GunInkwell(
      padding: EdgeInsets.zero,
      onTap: () {
        Get.bottomSheet(Container(
            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 10),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(15),
                  topRight: const Radius.circular(15),
                )),
            child: SingleChildScrollView(
              child: Column(
                  children: List.generate(
                      widget.list.length,
                      (index) => Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  item = widget.list[index];
                                });
                                widget.onTap(index, widget.list[index]);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 1,
                                            color: Colors.grey.shade300))),
                                width: double.infinity,
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, top: 10, bottom: 20),
                                child: widget.builder(
                                    index, widget.list[index], item),
                              ),
                            ),
                          ))),
            )));
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8),
        decoration: BoxDecoration(
            border:
                Border(bottom: BorderSide(width: 1, color: AppColors.Gray3))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget.value ?? widget.hintText,
              style: AppStyles.DEFAULT_16.copyWith(
                color: item != null ? Colors.black : AppColors.Gray5,
              ),
            ),
            Icon(Icons.arrow_drop_down)
          ],
        ),
      ),
    );
  }
}
