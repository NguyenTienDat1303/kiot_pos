import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:kiot_pos/kiot_pos.dart';

class AppTextField extends StatelessWidget {
  const AppTextField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
        style: AppStyles.normalTextStyle,
        // textAlign: TextAlign.center,
        textAlignVertical: TextAlignVertical.center,
        decoration: InputDecoration(
            // isDense: true,
            // alignLabelWithHint: true,
            // contentPadding: EdgeInsets.symmetric(vertical: 10),
            prefixIconConstraints: BoxConstraints(minWidth: 30, minHeight: 30),
            prefixIcon: Container(
                margin: EdgeInsets.only(right: 5),
                width: 20,
                child: Image.asset('assets/images/login_store.png')),
            suffix: Text(
              ".kiotsoft.com",
              style:
                  AppStyles.normalTextStyle.copyWith(color: AppColors.primary),
            )));
  }
}
