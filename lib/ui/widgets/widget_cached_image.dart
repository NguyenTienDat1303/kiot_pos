import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class WidgetCachedImage extends StatelessWidget {
  final String imageUrl;
  const WidgetCachedImage({Key? key, required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl.length > 0
          ? imageUrl
          : "https://d1hjkbq40fs2x4.cloudfront.net/2016-01-31/files/1045.jpg",
      fit: BoxFit.cover,
      placeholder: (context, url) => Center(child: CircularProgressIndicator()),
      errorWidget: (context, url, error) {
        return Icon(Icons.error);
      },
    );
  }
}
