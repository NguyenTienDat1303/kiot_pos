import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kiot_pos/res/res.dart';

class WidgetInput extends StatelessWidget {
  final TextEditingController? controller;
  final String? hintText;
  final Function(String)? onChanged;
  final Function(String)? onFieldSubmitted;
  final FocusNode? focusNode;
  final Function()? onEditingComplete;
  final String? Function(String?)? validator;
  final Widget? childRight, childLeft;
  final TextInputType? keyboardType;
  final TextAlign? textAlign;
  String? initialValue;
  List<TextInputFormatter>? inputFormatters;
  WidgetInput(
      {Key? key,
      this.controller,
      this.hintText,
      this.onChanged,
      this.onFieldSubmitted,
      this.focusNode,
      this.onEditingComplete,
      this.validator,
      this.childRight,
      this.keyboardType,
      this.textAlign,
      this.inputFormatters,
      this.initialValue,
      this.childLeft})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        controller: controller,
        style: AppStyles.DEFAULT_16.copyWith(color: Colors.black),
        onChanged: onChanged,
        focusNode: focusNode,
        keyboardType: keyboardType,
        onFieldSubmitted: onFieldSubmitted,
        onEditingComplete: onEditingComplete,
        textAlign: textAlign ?? TextAlign.left,
        initialValue: initialValue,
        inputFormatters: inputFormatters,
        decoration: InputDecoration(
            hintText: hintText,
            contentPadding: EdgeInsets.zero,
            hintStyle: AppStyles.DEFAULT_16.copyWith(color: AppColors.Gray4),
            prefixIconConstraints:
                BoxConstraints(minHeight: 10.h, minWidth: 10.w),
            prefixIcon: childLeft,
            suffixIconConstraints:
                BoxConstraints(minHeight: 10.h, minWidth: 10.w),
            suffixIcon: childRight),
        validator: validator,
      ),
    );
  }
}
