import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/widgets/gun_text.dart';
import 'package:kiot_pos/res/res.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/res/values.dart';

class WidgetButton extends StatelessWidget {
  final VoidCallback? onTap;
  final String? text;
  final TextStyle? textStyle;
  final Widget? icon;
  final EdgeInsetsGeometry? padding;
  final Color? backgroundColor, colorBorder;
  final bool? isFullWidth, isBorder;
  final double? height, width;
  const WidgetButton(
      {Key? key,
      this.backgroundColor,
      this.textStyle,
      this.onTap,
      this.padding,
      this.isBorder,
      this.text,
      this.icon,
      this.colorBorder,
      this.isFullWidth,
      this.height,
      this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
          // padding: MaterialStateProperty.all(padding),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          // overlayColor: MaterialStateProperty.all(Colors.grey.shade300),
          padding: MaterialStateProperty.all(
             padding ?? EdgeInsets.symmetric(vertical: 10)),
          minimumSize:
              MaterialStateProperty.all(Size(width ?? 0.0,height ?? 44.0)),
          backgroundColor:
              MaterialStateProperty.all(backgroundColor ?? AppColors.primary),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(100),
              side:isBorder == true
                  ? BorderSide(color: AppColors.primary)
                  : BorderSide.none))),
      onPressed:onTap,
      child: Container(
        height:height,
        width:width,
        child: Row(
          mainAxisSize:
             isFullWidth == true ? MainAxisSize.max : MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
           icon ?? const SizedBox.shrink(),
           icon != null ? Container(
            width: 8,
           ) : SizedBox.shrink(),
           text != null
                ? GunText(
                   text!,
                    style:textStyle ??
                        AppStyles.DEFAULT_18_BOLD.copyWith(
                            color: Colors.white),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  )
                : SizedBox.shrink(),
          ],
        ),
      ),
    );
  }
}
