import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BaseInfoWidget extends StatelessWidget {
  final String? title;
  final String contextLeft1,
      contextLeft2,
      contextLeft3,
      contextRight1,
      contextRight2,
      contextRight3;
  final GestureTapCallback? onTap;

  const BaseInfoWidget(
      {Key? key,
      this.title,
      required this.contextLeft1,
      required this.contextLeft2,
      required this.contextLeft3,
      required this.contextRight1,
      required this.contextRight2,
      required this.contextRight3,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.White,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
              color: Color(0xff000000).withOpacity(0.12),
              blurRadius: 10,
              offset: Offset(0, 2))
        ],
      ),
      child: Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        child: InkWell(
          onTap: onTap,
          child: Container(
            margin: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (title != null)
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      child: GunText(
                        title!,
                        style: AppStyles.DEFAULT_16_BOLD
                            .copyWith(color: Colors.black),
                      )),
                OneRow(title: contextLeft1, subtitle: contextRight1),
                OneRow(title: contextLeft2, subtitle: contextRight2),
                OneRow(title: contextLeft3, subtitle: contextRight3)
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class OneRow extends StatelessWidget {
  final String title, subtitle;
  const OneRow({Key? key, required this.title, required this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(title,
            style: AppStyles.DEFAULT_14.copyWith(color: AppColors.Blue)),
        Text(subtitle,
            style: AppStyles.DEFAULT_14.copyWith(color: AppColors.Gray1)),
      ]),
    );
  }
}

class EditDeleteInfo extends StatelessWidget {
  final String name;
  final GestureTapCallback? onDelete;
  final GestureTapCallback? onEdit;
  final GestureTapCallback? onTap;
  const EditDeleteInfo(
      {Key? key, required this.name, this.onDelete, this.onEdit, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.h),
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.White,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Color(0xff000000).withOpacity(0.12),
                blurRadius: 10,
                offset: Offset(0, 2))
          ],
        ),
        child: Material(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          child: InkWell(
            onTap: onTap,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.all(16),
                      child: Text(name,
                          style: AppStyles.DEFAULT_16.copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.w500)),
                    ),
                  ),
                  Row(children: [
                    if (onDelete != null)
                      Material(
                        color: Colors.white,
                        child: InkWell(
                          onTap: onDelete,
                          child: Container(
                              color: Colors.transparent,
                              padding: EdgeInsets.all(8),
                              child: Image.asset(
                                  './assets/images/delete_icon.png',
                                  width: 24.r)),
                        ),
                      ),
                    if (onEdit != null)
                      Material(
                        color: Colors.white,
                        child: InkWell(
                          onTap: onEdit,
                          child: Container(
                              color: Colors.transparent,
                              padding: EdgeInsets.all(8),
                              child: Image.asset(
                                  './assets/images/edit_icon.png',
                                  width: 24.r)),
                        ),
                      ),
                  ])
                ]),
          ),
        ),
      ),
    );
  }
}

class CellWhite extends StatelessWidget {
  final Widget child;
  final Color? backgroundColor;
  final EdgeInsetsGeometry? padding;
  const CellWhite(
      {Key? key, required this.child, this.backgroundColor, this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding ?? EdgeInsets.symmetric(horizontal: 5, vertical: 11),
      decoration: BoxDecoration(
          color: backgroundColor ?? AppColors.White,
          boxShadow: [
            BoxShadow(
                color: Color(0xff000000).withOpacity(0.12),
                blurRadius: 10,
                offset: Offset(0, 2))
          ],
          borderRadius: BorderRadius.circular(5)),
      child: child,
    );
  }
}
