import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/res/res.dart';
import 'package:kiot_pos/ui/ui.dart';

typedef Widget WidgetBuilder<T>(final int id, final T item, T? selected);
typedef void GestureTapCallbackCustom<T>(final int id, T? selected);

class WidgetDropdown<T> extends StatefulWidget {
   TextEditingController? controller;
  final WidgetBuilder<T> builder;
  final T? initialIndex;
  final bool isAutoComplete, isChoose;
  final FocusNode focusNode;
  final String hintText;
  final List<T> list;
  final ValueChanged<String>? onChange;
  final bool isIcon;
  final double? radius;
  final GestureTapCallbackCustom<T> onTap;
   String? initialValue;
   WidgetDropdown({
    Key? key,
     this.controller,
    required this.focusNode,
    this.onChange,
    required this.list,
    this.isAutoComplete = false,
    this.isIcon = true,
    required this.onTap,
    this.initialIndex,
    required this.builder,
    required this.hintText,
    this.isChoose = true,
    this.radius,
    this.initialValue,
  }) : super(key: key);

  @override
  _WidgetDropdownState createState() => _WidgetDropdownState<T>();
}

class _WidgetDropdownState<T> extends State<WidgetDropdown<T>> {
  final LayerLink _layerLink = LayerLink();
  OverlayEntry? listSuggestionsEntry;
  bool showOverlay = false;
  final GlobalKey key = GlobalKey();
  double? height, width, xPosition, yPosition;
  T? item;
  @override
  void initState() {
    super.initState();
    if (widget.initialIndex != null) {
      setState(() {
        item = widget.initialIndex!;
      });
    } else {
      if (widget.list.isNotEmpty) {
        item = widget.list[0];
      }
      print('hadghjadha ${item}');
    }
    widget.focusNode.addListener(() {
      onOverlay();
    });
  }

  // Coordinates
  void updateOverlayButton() {
    print(key.currentContext);
    if (key.currentContext != null) {
      RenderBox renderBox = key.currentContext!.findRenderObject() as RenderBox;
      height = renderBox.size.height;
      width = renderBox.size.width;
      Offset offset = renderBox.localToGlobal(Offset.zero);
      xPosition = offset.dx;
      yPosition = offset.dy;
      listSuggestionsEntry = new OverlayEntry(builder: (context) {
        return Stack(
          children: [
            GestureDetector(
              onTap: () {
                if (listSuggestionsEntry != null) {
                  setState(() {
                    listSuggestionsEntry!.remove();
                    listSuggestionsEntry = null;
                    showOverlay = !showOverlay;
                  });
                }
              },
              child: new Container(
                color: Colors.transparent,
              ),
            ),
            Positioned(
                width: width,
                child: CompositedTransformFollower(
                    link: _layerLink,
                    offset: Offset(0, 44.h + 16.h),
                    showWhenUnlinked: false,
                    child: Material(
                      child: widget.list.length == 0
                          ? _renderNotData()
                          : widget.list.length > 0 && widget.list.length <= 4
                              ? _renderDataUnder4()
                              : widget.list.length > 4
                                  ? _renderDataAbove4()
                                  : SizedBox.shrink(),
                    )))
          ],
        );
      });
    }
  }

  void updateOverlay() {
    if (listSuggestionsEntry == null) {
      final Size textFieldSize = (context.findRenderObject() as RenderBox).size;
      final width = textFieldSize.width;
      listSuggestionsEntry = new OverlayEntry(builder: (context) {
        return Positioned(
            width: width,
            child: CompositedTransformFollower(
                link: _layerLink,
                offset: Offset(0, (44.h + 16.h)),
                showWhenUnlinked: false,
                child: Material(
                  child: widget.list.length == 0
                      ? _renderNotData()
                      : widget.list.length > 0 && widget.list.length <= 4
                          ? _renderDataUnder4()
                          : widget.list.length > 4
                              ? _renderDataAbove4()
                              : SizedBox.shrink(),
                )));
      });
      // Overlay.of(context)!.insert(listSuggestionsEntry!);
    }
  }

  _renderNotData() {
    return _renderDropDown(
      child: GunText(
        'Chưa có dữ liệu ',
        textAlign: TextAlign.center,
        style: AppStyles.DEFAULT_16.copyWith(color: Colors.black),
      ),
    );
  }

  _renderDropDown({required Widget child}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 22.h),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(8),
            bottomRight: Radius.circular(8),
          ),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color.fromARGB(15, 0, 0, 0),
              spreadRadius: 0,
              blurRadius: 36,
              offset: Offset(0, 4), // changes position of shadow
            )
          ]),
      child: child,
    );
  }

  _renderDataUnder4() {
    return _renderDropDown(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ...List.generate(
                widget.list.length, (index) => _renderChooseItem(index: index))
          ],
        ),
      ),
    );
  }

  _renderDataAbove4() {
    return Container(
      height: 230.h,
      child: _renderDropDown(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...List.generate(widget.list.length,
                  (index) => _renderChooseItem(index: index))
            ],
          ),
        ),
      ),
    );
  }

  _renderChooseItem({required int index}) {
    return Container(
      margin: EdgeInsets.only(top: index == 0 ? 0 : 27.h),
      child: GestureDetector(
        onTap: () {
          print(widget.list);
          widget.onTap(index, widget.list[index]);

          if (listSuggestionsEntry != null) {
            setState(() {
              item = widget.list[index];
              listSuggestionsEntry!.remove();
              listSuggestionsEntry = null;
              if (widget.isAutoComplete) {
                showOverlay = !showOverlay;
              } else {
                widget.focusNode.unfocus();
              }
            });
          }
        },
        child: Container(
          color: Colors.transparent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              widget.builder(index, widget.list[index], item),
              if (widget.isChoose)
                Container(
                  padding: EdgeInsets.all(6.h),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 1, color: AppColors.Blue)),
                  child: CircleAvatar(
                    backgroundColor: item == widget.list[index]
                        ? AppColors.Blue
                        : Colors.white,
                    radius: 6.r,
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }

  onOverlay() {
    if (widget.focusNode.hasFocus) {
      updateOverlay();
      Overlay.of(context)!.insert(listSuggestionsEntry!);
    } else {
      if (listSuggestionsEntry != null) {
        setState(() {
          listSuggestionsEntry!.remove();
        });
      }
    }
  }

  onOverlayButton() {
    setState(() {
      showOverlay = !showOverlay;
    });
    if (showOverlay) {
      updateOverlayButton();
      Overlay.of(context)!.insert(listSuggestionsEntry!);
    } else {
      if (listSuggestionsEntry != null) {
        setState(() {
          listSuggestionsEntry!.remove();
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final sizeHeight = MediaQuery.of(context).size.height;
    return CompositedTransformTarget(
        link: _layerLink,
        child: widget.isAutoComplete
            ? Stack(
                children: [
                  GestureDetector(
                    key: key,
                    onTap: () {
                      FocusManager.instance.primaryFocus?.unfocus();
                      onOverlayButton();
                    },
                    child: AbsorbPointer(child: _renderInput()),
                  ),
                ],
              )
            : _renderInput());
  }

  _renderInput() {
    return WidgetInput(
      initialValue: widget.initialValue,
      controller: widget.controller,
      focusNode: widget.focusNode,
      hintText: widget.hintText,
      onChanged: (e) {
        if (widget.onChange != null) {
          widget.onChange!(e);
        }
      },
      childRight: Image.asset(
        './assets/images/expanded_icon.png',
        height: 10.h,
      ),
    );
  }
}
