import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';

class DialogCustom extends StatefulWidget {
  final String? title, nameButtonLeft, nameButtonRight;
  final List<Widget> children;
  final bool isTwoButton;
  final VoidCallback? backDropTap;
  final GestureTapCallback? onTapOK, onTapLeft, onTapRight;

  final EdgeInsets? outerPadding;
  final EdgeInsets? innerPadding;
  final AlignmentGeometry? alignment;

  const DialogCustom(
      {Key? key,
      this.title,
      required this.children,
      this.backDropTap,
      this.outerPadding,
      this.innerPadding,
      this.alignment,
      this.isTwoButton = false,
      this.onTapOK,
      this.nameButtonLeft,
      this.nameButtonRight,
      this.onTapLeft,
      this.onTapRight})
      : super(key: key);

  @override
  _DialogCustomState createState() => _DialogCustomState();
}

class _DialogCustomState extends State<DialogCustom> {
  static const _outerPadding =
      EdgeInsets.symmetric(vertical: 20, horizontal: 30);
  static const _innerPadding =
      EdgeInsets.symmetric(vertical: 16, horizontal: 16);
  @override
  Widget build(BuildContext context) {
    // final sizeHeight = MediaQuery.of(context).size.height;
    // final sizeWidth = MediaQuery.of(context).size.width;

    return Material(
      color: Colors.transparent,
      child: Stack(
        children: [
          _renderBackdrop(),
          Center(
            child: Container(
              margin: widget.outerPadding ?? _outerPadding,
              padding: widget.innerPadding ?? _innerPadding,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12.0)),
              child: Stack(
                children: [
                  SingleChildScrollView(
                    padding: EdgeInsets.only(top: 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (widget.title != null)
                              Expanded(
                                  child: Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        widget.title!,
                                        style:
                                            AppStyles.DEFAULT_18_BOLD.copyWith(
                                          fontWeight: FontWeight.w600,
                                          color: AppColors.Gray1,
                                        ),
                                      )))
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 16),
                          child: Column(
                            children: [...widget.children],
                          ),
                        ),
                        Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(top: 24),
                            child: widget.isTwoButton
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Expanded(
                                          child: TextButton(
                                              onPressed: widget.onTapLeft ??
                                                  () => Get.back(),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          AppColors.Blue4),
                                                  shape: MaterialStateProperty
                                                      .all(RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      500)))),
                                              child: Text(
                                                widget.nameButtonLeft ?? '',
                                                style: AppStyles.DEFAULT_16
                                                    .copyWith(
                                                        color: AppColors.Blue,
                                                        fontWeight:
                                                            FontWeight.w700),
                                              ))),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Expanded(
                                          child: TextButton(
                                              onPressed: widget.onTapRight,
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          AppColors.primary),
                                                  shape: MaterialStateProperty
                                                      .all(RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      500)))),
                                              child: Text(
                                                widget.nameButtonRight ?? '',
                                                style: AppStyles.DEFAULT_16
                                                    .copyWith(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.w700),
                                              ))),
                                    ],
                                  )
                                : Container(
                                    width: double.infinity,
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 30),
                                    child: TextButton(
                                        onPressed:
                                            widget.onTapOK ?? () => Get.back(),
                                        style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all(
                                                    AppColors.primary),
                                            shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            500)))),
                                        child: Text(
                                          'OK',
                                          style: AppStyles.DEFAULT_16.copyWith(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w700),
                                        )),
                                  ))
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _renderBackdrop() {
    return Positioned.fill(
        child: GestureDetector(
      onTap: widget.backDropTap ?? () => Get.back(),
      child: Container(
        color: Colors.transparent,
      ),
    ));
  }

  double _width = 0;
}
