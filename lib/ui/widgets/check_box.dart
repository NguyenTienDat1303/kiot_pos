import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:kiot_pos/res/res.dart';

class CheckBoxCustom extends StatefulWidget {
  final ValueChanged<bool> onChanged;
  final GestureTapCallback? onTap2;
  final GestureTapCallback onTap;
  final bool isCheck;
  final String? textSpan1, textSpan2, textSpan3;
  const CheckBoxCustom(
      {Key? key,
      required this.onChanged,
      this.isCheck = false,
      this.textSpan1,
      this.textSpan2,
      this.textSpan3,
      this.onTap2,
      required this.onTap})
      : super(key: key);

  @override
  _CheckBoxCustomState createState() => _CheckBoxCustomState();
}

class _CheckBoxCustomState extends State<CheckBoxCustom> {
  bool _check = true;

  @override
  void initState() {
    super.initState();
    _check = widget.isCheck;
  }

  @override
  Widget build(BuildContext context) {
    final sizeHeight = MediaQuery.of(context).size.height;
    final sizeWidth = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        setState(() {
          _check = !_check;
        });
        widget.onTap();
        widget.onChanged(_check);
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 24,
            width: 24,
            decoration: BoxDecoration(
                border: Border.all(width: 1.5, color: AppColors.Blue),
                borderRadius: BorderRadius.circular(5)),
            child: Center(
                child: _check
                    ? Icon(
                        Icons.done,
                        size: 20,
                      )
                    : null),
          ),
          Expanded(
            child: Container(
                margin: EdgeInsets.only(left: sizeWidth * 0.02),
                child: RichText(
                    text: TextSpan(
                        text: widget.textSpan1,
                        style: AppStyles.DEFAULT_14.copyWith(color: Colors.black),
                        children: [
                      TextSpan(
                          text: widget.textSpan2,
                          style: AppStyles.DEFAULT_14
                              .copyWith(color: AppColors.Blue),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = widget.onTap2),
                      TextSpan(
                          text: widget.textSpan3,
                          style:
                              AppStyles.DEFAULT_14.copyWith(color: Colors.black))
                    ]))),
          )
        ],
      ),
    );
  }
}
