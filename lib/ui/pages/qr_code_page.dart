import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/res.dart';
import 'package:kiot_pos/ui/widgets/qr_code_view.dart';
import 'package:permission_handler/permission_handler.dart';

class QrCodePage extends StatefulWidget {
  const QrCodePage({Key? key}) : super(key: key);

  @override
  State<QrCodePage> createState() => _QrCodePageState();
}

class _QrCodePageState extends State<QrCodePage> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final sizeHeight = MediaQuery.of(context).size.height;

    return GunPage(
      appBarTop: GunAppBar(
        type: GunAppBarType.TOP,
        backgroundColor: Colors.black,
        leadingColor: AppColors.White,
        leading: [
          GunAction.icon(
              onTap: () {
                Get.back(result: '');
              },
              icon: Icon(
                Icons.chevron_left,
                size: 40,
                color: AppColors.White,
              ))
        ],
      ),
      child: Stack(
        children: [
          _renderViewScan(),
          Positioned.fill(
              top: sizeHeight * 0.12,
              child: Container(
                alignment: Alignment.topCenter,
                child: Text(
                  'Di chuyển camera tới mã QR để quét',
                  style: AppStyles.DEFAULT_16.copyWith(
                      color: AppColors.White, fontWeight: FontWeight.w600),
                ),
              ))
        ],
      ),
    );
  }

  Widget _renderViewScan() {
    return Container(
      child: QrcodeReaderView(
        key: scaffoldKey,
        scanBoxRatio: 1,
        boxLineColor: AppColors.Orange1.withOpacity(0.8),
      ),
    );
  }
}
