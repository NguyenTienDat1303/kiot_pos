import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class OverviewPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const OverviewPage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<OverviewPage> createState() => _OverviewPageState();
}

class _OverviewPageState extends State<OverviewPage> {
  OverviewController _controller = Get.find<OverviewController>();

  @override
  void initState() {
    super.initState();
    _controller.loadOverview();
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'overview.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leading: [
            GunAction.icon(
                onTap: () {
                  widget.homePageKey.currentState!.openDrawer();
                },
                icon: Image.asset('assets/images/drawer_menu.png'))
          ],
        ),
        child: GetX<OverviewController>(
          builder: (controller) {
            if (controller.overviewResponse != null) {
              _chartData = <_ChartData>[];
              for (int i = 0;
                  i < controller.overviewResponse!.chart!.length;
                  i++) {
                print("index $i");
                // var yValue = controller.overviewResponse!.chart![i].doanhThu!;

                _chartData!.add(_ChartData(
                    controller.overviewResponse!.chart![i].label!,
                    int.parse(controller.overviewResponse!.chart![i].doanhThu ??
                        "0")));
              }
              var max = _chartData!.reduce(
                  (current, next) => current.y > next.y ? current : next);
              return SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Container(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'overview.result'.tr,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      IntrinsicHeight(
                        child: Row(
                          children: [
                            Expanded(
                                child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 20),
                              decoration: BoxDecoration(
                                  color: Color(0XFFF2F2F2),
                                  borderRadius: BorderRadius.circular(5)),
                              child: Row(children: [
                                Container(
                                    width: 35,
                                    child: Image.asset(
                                        'assets/images/overview_table_used.png')),
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    FittedBox(
                                      child: Text(
                                        "${controller.overviewResponse!.banDangDung}/${controller.overviewResponse!.tongSoBan}",
                                        style: TextStyle(
                                            color: Color(0XFF205BA1),
                                            fontSize: 24.r,
                                            fontWeight: FontWeight.w700),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "overview.table_used".tr,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ))
                              ]),
                            )),
                            SizedBox(
                              width: 15,
                            ),
                            Expanded(
                                child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 20),
                              decoration: BoxDecoration(
                                  color: Color(0XFFF2F2F2),
                                  borderRadius: BorderRadius.circular(5)),
                              child: Row(children: [
                                Container(
                                    width: 35,
                                    child: Image.asset(
                                        'assets/images/overview_table_order.png')),
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      "${AppValue.APP_MONEY_FORMAT.format(controller.overviewResponse!.doanhThu!)} ",
                                      style: TextStyle(
                                          color: Color(0XFF205BA1),
                                          fontSize: 24,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "overview.revenue".tr,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ))
                              ]),
                            )),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Color(0XFFF2F2F2),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 20),
                              child: Row(children: [
                                Container(
                                    margin: EdgeInsets.only(right: 20),
                                    width: 35,
                                    child: Image.asset(
                                        'assets/images/overview_table_revenue.png')),
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${controller.overviewResponse!.doanhThu}",
                                      style: TextStyle(
                                          color: Color(0XFFEB5757),
                                          fontSize: 24,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "overview.order".tr,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ))
                              ]),
                            ),
                            Container(
                              width: double.infinity,
                              height: 1,
                              color: Color(0XFFBDBDBD),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 20),
                              child: Row(children: [
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${AppValue.APP_MONEY_FORMAT.format(controller.overviewResponse!.doanhThu!)} ",
                                      style: TextStyle(
                                          color: Color(0XFF219653),
                                          fontSize: 24,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "overview.revenue".tr,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                )),
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${controller.overviewResponse!.giaTriHuyTra}",
                                      style: TextStyle(
                                          color: Color(0XFFF2994A),
                                          fontSize: 24,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "overview.debit".tr,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ))
                              ]),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Text(
                        'overview.revenue'.tr + "(VNĐ)",
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'overview.revenue_chart'.tr,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: Color(0XFF929191))),
                        child: Row(
                          children: List.generate(
                              OverviewChart.values.length,
                              (index) => Expanded(
                                    child: Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        onTap: () {
                                          _controller.overviewChartType =
                                              OverviewChart.values[index];
                                          _controller.loadOverview();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: _controller
                                                          .overviewChartType ==
                                                      OverviewChart
                                                          .values[index]
                                                  ? Color(0XFF454545)
                                                  : Colors.transparent),
                                          padding: EdgeInsets.symmetric(
                                              vertical: 10, horizontal: 10),
                                          child: Text(
                                            'overview.${OverviewChart.values[index].toString().split('.').last}'
                                                .tr,
                                            style: TextStyle(
                                                color: _controller
                                                            .overviewChartType ==
                                                        OverviewChart
                                                            .values[index]
                                                    ? Colors.white
                                                    : Colors.black,
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )),
                        ),
                      ),
                      SfCartesianChart(
                          plotAreaBorderWidth: 0,
                          primaryXAxis: CategoryAxis(
                              labelPlacement: LabelPlacement.onTicks,
                              majorGridLines: const MajorGridLines(width: 0),
                              placeLabelsNearAxisLine: false),
                          primaryYAxis: NumericAxis(
                              axisLine: const AxisLine(width: 0),
                              minimum: 0,
                              maximum: max.y.toDouble(),
                              majorTickLines: const MajorTickLines(size: 0)),
                          series: _getDefaultAreaSeries())
                    ],
                  ),
                ),
              );
            } else
              return SizedBox();
          },
        ));
  }

  List<_ChartData>? _chartData;

  /// Return the list of  area series which need to be animated.
  List<AreaSeries<_ChartData, String>> _getDefaultAreaSeries() {
    return <AreaSeries<_ChartData, String>>[
      AreaSeries<_ChartData, String>(
          dataSource: _chartData!,
          color: const Color.fromRGBO(75, 135, 185, 0.6),
          borderColor: const Color.fromRGBO(75, 135, 185, 1),
          borderWidth: 2,
          xValueMapper: (_ChartData sales, _) => sales.x,
          yValueMapper: (_ChartData sales, _) => sales.y)
    ];
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Return the random value in area series.

}

class _ChartData {
  _ChartData(
    this.x,
    this.y,
  );
  final String x;
  final int y;
}
