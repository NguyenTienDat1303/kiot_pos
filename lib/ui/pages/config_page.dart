import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class ConfigPage extends StatefulWidget {
  const ConfigPage({Key? key}) : super(key: key);

  @override
  State<ConfigPage> createState() => _ConfigPageState();
}

class _ConfigPageState extends State<ConfigPage> {
  ConfigController _controller = Get.find<ConfigController>();

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'config.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
        ),
        child: SingleChildScrollView(
          // padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
          child: Column(
            children: [
              GetX<ConfigController>(
                builder: (controller) {
                  return Column(
                    children: [
                      ConfigMenuSwitch(
                        value: true,
                        imageAssetPath: 'assets/images/config_sound.png',
                        title: 'config.sound_notification'.tr,
                        onChanged: (val) {},
                        onTap: () {
                          // Get.toNamed(AppRoutes.print_bluetooth_printer);
                        },
                      ),
                      ConfigMenuSwitch(
                        value: true,
                        title: 'config.sound_order'.tr,
                        onChanged: (val) {},
                        onTap: () {},
                      ),
                      ConfigMenuSwitch(
                        value: true,
                        title: 'config.sound_birthday'.tr,
                        onChanged: (val) {},
                        onTap: () {},
                      ),
                      ConfigMenuSwitch(
                        value: true,
                        title: 'config.sound_bill'.tr,
                        onChanged: (val) {},
                        onTap: () {},
                      ),
                      ConfigMenuSwitch(
                        value: true,
                        title: 'config.sound_check'.tr,
                        onChanged: (val) {},
                        onTap: () {},
                      ),
                      ConfigMenuSwitch(
                        value: controller.printKitchen,
                        title: 'config.print_kitchen'.tr,
                        imageAssetPath: 'assets/images/config_print.png',
                        onChanged: (val) {
                          controller.printKitchen = !controller.printKitchen;
                        },
                        onTap: () {
                          controller.printKitchen = !controller.printKitchen;
                        },
                      ),
                      ConfigMenuSwitch(
                        value: controller.printDouble,
                        title: 'config.print_double'.tr,
                        imageAssetPath: 'assets/images/config_print.png',
                        onChanged: (val) {
                          controller.printDouble = !controller.printDouble;
                        },
                        onTap: () {
                          controller.printDouble = !controller.printDouble;
                        },
                      ),
                      ConfigMenuSwitch(
                        value: controller.printAuto,
                        title: 'config.print_auto'.tr,
                        imageAssetPath: 'assets/images/config_print.png',
                        onChanged: (val) {
                          controller.printAuto = !controller.printAuto;
                        },
                        onTap: () {
                          controller.printAuto = !controller.printAuto;
                        },
                      ),
                      ConfigMenuSwitch(
                        value: true,
                        title: 'config.print_config'.tr,
                        imageAssetPath: 'assets/images/config_setting.png',
                        onTap: () async {
                          await Get.toNamed(AppRoutes.printer_create);
                          _controller.printerList();
                        },
                      ),
                      // ConfigMenuSwitch(
                      //   value: true,
                      //   title: 'config.sound_notification'.tr,
                      //   onChanged: (val) {},
                      //   onTap: () {},
                      // ),
                    ],
                  );
                },
              ),
              GetX<ConfigController>(builder: (controller) {
                return Column(
                  children: List.generate(
                    controller.printerConfigList.length,
                    (index) => PrinterConfigWidget(
                      config: controller.printerConfigList[index],
                      onTap: () {
                        Get.bottomSheet(PrinterConfigBottomSheet(
                          config: controller.printerConfigList[index],
                          onTap: (config) {
                            print("config ${config.id}");
                            print("config ${config.ipAddress}");
                            print("config ${config.port}");
                            print("config ${config.paperSize}");
                            _controller.editPrinterConfig(config);
                            Get.back();
                          },
                        ));
                      },
                    ),
                  ),
                );
              }),
              // GetX<ConfigController>(builder: (controller) {
              //   return Column(
              //     children: List.generate(
              //       controller.printDataList.length,
              //       (index) => PrinterDataWidget(
              //         data: controller.printDataList[index],
              //         onTap: () {
              //           // Get.bottomSheet(PrinterConfigBottomSheet(
              //           //   config: controller.printerConfigList[index],
              //           //   onTap: (config) {
              //           //     print("config ${config.id}");
              //           //     print("config ${config.ipAddress}");
              //           //     print("config ${config.port}");
              //           //     print("config ${config.paperSize}");
              //           //     _controller.editPrinterConfig(config);
              //           //     Get.back();
              //           //   },
              //           // ));
              //         },
              //       ),
              //     ),
              //   );
              // }),
              GetX<ConfigController>(
                builder: (controller) {
                  return Column(
                    children: [
                      ListTile(
                        title: const Text('Lan'),
                        leading: Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.all(AppColors.primary),
                          value: controller.printType == PrintType.lan,
                          onChanged: (bool? value) {
                            setState(() {
                              controller.printType = PrintType.lan;
                            });
                          },
                        ),
                      ),
                      ListTile(
                        title: const Text('Bluetooth'),
                        leading: Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.all(AppColors.primary),
                          value: controller.printType == PrintType.bluetooth,
                          onChanged: (bool? value) {
                            print(controller.printType);
                            setState(() {
                              controller.printType = PrintType.bluetooth;
                            });
                          },
                        ),
                      ),
                    ],
                  );
                },
              )
            ],
          ),
        ));
  }
}

class ConfigMenuSwitch extends StatelessWidget {
  final bool value;
  final String title;
  final String? imageAssetPath;
  final ValueChanged<bool>? onChanged;
  final VoidCallback? onTap;

  const ConfigMenuSwitch(
      {Key? key,
      required this.value,
      required this.title,
      this.imageAssetPath,
      this.onChanged,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onTap,
        child: Column(
          children: [
            Container(
              // color: Colors.red,
              child: Row(children: [
                Container(
                    margin: EdgeInsets.only(right: 20),
                    width: 40,
                    height: 40,
                    child: imageAssetPath != null
                        ? FractionallySizedBox(
                            widthFactor: 0.5,
                            child: Image.asset(imageAssetPath!))
                        : SizedBox()),
                Expanded(
                    child: Text(
                  title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
                onChanged != null
                    ? Column(
                        children: [
                          Switch(value: value, onChanged: onChanged),
                          Text(
                            value ? 'config.on'.tr : 'config.off'.tr,
                            style: TextStyle(
                                fontSize: 10, fontWeight: FontWeight.w400),
                          )
                        ],
                      )
                    : SizedBox()
              ]),
            ),
            Container(
              margin: EdgeInsets.only(top: 5),
              width: double.infinity,
              height: 0.8,
              color: Colors.grey.shade300,
            )
          ],
        ),
      ),
    );
  }
}

class PrinterConfigWidget extends StatelessWidget {
  final PrinterConfig config;
  final VoidCallback? onTap;
  const PrinterConfigWidget({Key? key, required this.config, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GunInkwell(
      padding: EdgeInsets.zero,
      onTap: onTap,
      child: Container(
        width: double.infinity,
        child: Row(children: [
          Container(width: 40, height: 50, child: SizedBox()),
          Expanded(
              child: Text(
            'print_name.${config.name!}'.tr + " ${config.order! + 1}",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
          )),
          Container(
              width: 40,
              height: 40,
              child: FractionallySizedBox(
                  widthFactor: 0.5, child: Icon(Icons.arrow_right))),
        ]),
      ),
    );
  }
}

class PrinterDataWidget extends StatelessWidget {
  final PrinterData data;
  final VoidCallback? onTap;
  const PrinterDataWidget({Key? key, required this.data, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GunInkwell(
      padding: EdgeInsets.zero,
      onTap: onTap,
      child: Container(
        width: double.infinity,
        child: Row(children: [
          Container(width: 40, height: 50, child: SizedBox()),
          Expanded(
              child: Text(
            '${data.description}',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
          )),
          Container(
              width: 40,
              height: 40,
              child: FractionallySizedBox(
                  widthFactor: 0.5, child: Icon(Icons.arrow_right))),
        ]),
      ),
    );
  }
}

class PrinterConfigBottomSheet extends StatefulWidget {
  final PrinterConfig config;
  final Function(PrinterConfig config)? onTap;
  const PrinterConfigBottomSheet({Key? key, required this.config, this.onTap})
      : super(key: key);

  @override
  State<PrinterConfigBottomSheet> createState() =>
      _PrinterConfigBottomSheetState();
}

class _PrinterConfigBottomSheetState extends State<PrinterConfigBottomSheet> {
  ConfigController _controller = Get.find<ConfigController>();
  late PrinterConfig _config;

  TextEditingController _lanEditController = TextEditingController();
  TextEditingController _portEditController = TextEditingController();
  @override
  void initState() {
    super.initState();
    _config = widget.config;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.white,
      padding: EdgeInsets.zero,
      child: SingleChildScrollView(
        child: Column(children: [
          ...List.generate(
            PrintType.supported.length,
            (index) {
              return Column(
                children: [
                  ListTile(
                    contentPadding: EdgeInsets.zero,
                    minVerticalPadding: 0,
                    title: Text('print_type.${PrintType.supported[index]}'.tr),
                    leading: Radio<int>(
                      value: PrintType.supported[index],
                      groupValue: _config.type,
                      onChanged: (int? value) {
                        setState(() {
                          _config.type = value;
                        });
                      },
                    ),
                  ),
                  if (_config!.type == PrintType.supported[index])
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Column(
                        children: [
                          if (PrintType.supported[index] == PrintType.lan)
                            Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(child: Text("Địa chỉ IP")),
                                    Expanded(
                                        child: TextFormField(
                                      // controller: _lanEditController,
                                      initialValue: _config.ipAddress,
                                      onChanged: (val) {
                                        _config.ipAddress = val;
                                      },
                                      textAlign: TextAlign.end,
                                      decoration: InputDecoration(
                                          alignLabelWithHint: true,
                                          hintText: '192.168.1.x'),
                                    ))
                                  ],
                                ),
                                // Row(
                                //   children: [
                                //     Expanded(child: Text("Port")),
                                //     Expanded(
                                //         child: TextFormField(
                                //       // controller: _portEditController,
                                //       initialValue: _config.port,
                                //       onChanged: (val) {
                                //         _config.port = val;
                                //       },
                                //       textAlign: TextAlign.end,
                                //       decoration: InputDecoration(
                                //           alignLabelWithHint: true,
                                //           hintText: 'xxxx'),
                                //     ))
                                //   ],
                                // ),
                              ],
                            ),
                          Row(
                            children: [
                              Expanded(child: Text("Khổ giấy")),
                              DropdownButton<int>(
                                hint: Text('x.mm'),
                                value: _config.paperSize,
                                icon: const Icon(Icons.arrow_downward),
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                onChanged: (int? newValue) {
                                  setState(() {
                                    _config.paperSize = newValue!;
                                  });
                                },
                                items: PrintrPaperSize.supported
                                    .map<DropdownMenuItem<int>>((int value) {
                                  return DropdownMenuItem<int>(
                                    value: value,
                                    child:
                                        Text('printr_paper_size.${value}'.tr),
                                  );
                                }).toList(),
                              )
                            ],
                          ),
                        ],
                      ),
                    )
                ],
              );
            },
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            child: InkWell(
                onTap: () {
                  if (widget.onTap != null) {
                    widget.onTap!(_config);
                  }
                },
                child: Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(vertical: 10),
                    decoration: BoxDecoration(
                        color: Color(0xFF205BA1),
                        borderRadius: BorderRadius.circular(500)),
                    child: Text(
                      'Áp dụng',
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ))),
          )
        ]),
      ),
    );
  }
}
