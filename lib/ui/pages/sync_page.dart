import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';

class SyncPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const SyncPage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<SyncPage> createState() => _SyncPageState();
}

class _SyncPageState extends State<SyncPage> {
  @override
  Widget build(BuildContext context) {
    return GunPage(child: Text("Sync"));
  }
}
