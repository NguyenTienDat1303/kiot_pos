import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';

class AreaDetailPage extends StatefulWidget {
  const AreaDetailPage({Key? key}) : super(key: key);

  @override
  State<AreaDetailPage> createState() => _AreaDetailPageState();
}

class _AreaDetailPageState extends State<AreaDetailPage> {
  AreaDetailController areaDetailController = Get.find<AreaDetailController>();
  TextEditingController textController = TextEditingController();
  TextEditingController tableController = TextEditingController();
  AreaData? area;
  int idArea = -1;
  @override
  void initState() {
    super.initState();
    if (Get.arguments != null) {
      var argument = Get.arguments;
      setState(() {
        area = argument[0];
        idArea = argument[1];
      });
      Future.delayed(Duration(seconds: 0), () {
        areaDetailController.loadTableList(id: idArea);
      });
    }
  }

  bool isSearch = false;
  @override
  Widget build(BuildContext context) {
    return GetX<AreaDetailController>(
      builder: (controller) {
        return GunPage(
            appBarBottom: GunAppBar(
              type: GunAppBarType.BOTTOM,
              height: 0,
            ),
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              titles: Row(
                children: [
                  isSearch
                      ? Expanded(
                          child: TextField(
                          controller: textController,
                          onChanged: (e) {
                            setState(() {});
                          },
                          onSubmitted: (e) {
                            areaDetailController.loadTableList(id: idArea);
                          },
                          autofocus: true,
                          decoration: InputDecoration(
                              isDense: true, border: InputBorder.none),
                        ))
                      : GunText(
                          area != null ? area!.areaName ?? '' : '',
                          style: AppStyles.pageTitleStyle,
                        ),
                ],
              ),
              leadingColor: Colors.black,
              actions: [
                GunAction.icon(
                  onTap: () {
                    setState(() {
                      isSearch = !isSearch;
                      textController.clear();
                    });
                    if (isSearch == false) {
                      areaDetailController.loadTableList(id: idArea);
                    }
                  },
                  iconFactor: isSearch ? 0.4 : 0.7,
                  icon: Image.asset(
                    isSearch
                        ? './assets/images/close_icon.png'
                        : './assets/images/search_icon.png',
                    color: AppColors.Orange,
                  ),
                ),
              ],
            ),
            child: Column(
              children: [
                Expanded(child: _renderBody(controller.tableList, controller)),
              ],
            ));
      },
    );
  }

  _renderBody(List<TableData> list, AreaDetailController controller) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          children: [
            ...List.generate(
                list.length,
                (index) => EditDeleteInfo(
                      name: list[index].ban != null
                          ? list[index].ban!.tableName ?? ''
                          : '',
                      onEdit: () {
                        setState(() {
                          tableController.text = list[index].ban != null
                              ? list[index].ban!.tableName ?? ''
                              : '';
                        });
                        Get.dialog(DialogCustom(
                            backDropTap: () {
                              null;
                            },
                            title: 'Chỉnh sửa',
                            isTwoButton: true,
                            nameButtonLeft: 'Huỷ',
                            nameButtonRight: 'Chỉnh sửa',
                            onTapLeft: () {
                              Get.back();
                            },
                            onTapRight: () {
                              Get.back();
                              controller.tableEdit(
                                  id: list[index].ban!.id!,
                                  name: tableController.text,
                                  callback: () {
                                    controller.loadTableList(id: idArea);
                                  });
                            },
                            children: [
                              TextField(
                                controller: tableController,
                                onChanged: (e) {
                                  setState(() {});
                                },
                                autofocus: true,
                              )
                            ]));
                      },
                    ))
          ],
        ),
      ),
    );
  }
}
