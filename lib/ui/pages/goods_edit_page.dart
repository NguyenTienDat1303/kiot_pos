import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/ui.dart';
import 'package:kiot_pos/ui/widgets/avatar_create.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kiot_pos/ui/widgets/check_box.dart';
import 'package:pattern_formatter/pattern_formatter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:permission_handler/permission_handler.dart';

class GoodsEditPage extends StatefulWidget {
  const GoodsEditPage({Key? key}) : super(key: key);

  @override
  State<GoodsEditPage> createState() => _GoodsEditPageState();
}

class _GoodsEditPageState extends State<GoodsEditPage> {
  OrderCreateProductController _controller =
      Get.find<OrderCreateProductController>();
  ProductController productController = Get.find<ProductController>();
  GlobalKey<FormState> key = GlobalKey();
  List<ItemInputKhuVuc> listInput1 = [];
  List<ItemInputKhuVuc> listInput2 = [];
  List<ItemInputKhuVuc> listInput3 = [];
  bool isEditPrice = false;
  bool isSellNegative = false;
  bool isTichDiem = true;
  bool isTopping = false;
  bool isNguyenLieu = false;
  bool isInTem = true;
  List<ItemUnitConversion> listUnitConversion = [];
  UnitResponse? donViTinh;
  ProducerResponse? nsx;
  UsersResponse? user;
  CategoryDetail? danhMuc;
  UnitResponse? donViQuyDoi;
  File? image;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      productController.unit();
      productController.producer();
      productController.loadCategoryList('');
    });
    for (var i = 0; i < 3; i++) {
      listInput1.add(ItemInputKhuVuc(
          controller: TextEditingController(), focusNode: FocusNode()));
      listInput2.add(ItemInputKhuVuc(
          controller: TextEditingController(), focusNode: FocusNode()));
    }
    for (var i = 0; i < 2; i++) {
      listInput3.add(ItemInputKhuVuc(
          controller: TextEditingController(), focusNode: FocusNode()));
    }
    if (productController.idProduct == -1) {
      for (var i = 0; i < 1; i++) {
        listUnitConversion.add(ItemUnitConversion(
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode())));
      }
    } else {
      if (productController.detailProduct != null &&
          productController.idProduct != -1) {
        var data = productController.detailProduct;

        listInput1[0].controller.text = data!.sanPham!.prdName!;
        listInput1[1].controller.text =
            double.parse(data.sanPham!.prdSls ?? '0').toInt().toString();
        listInput1[2].controller.text = data.sanPham!.prdCode!;
        listInput2[0].controller.text =
            AppValue.App_money_format.format(data.sanPham!.prdOriginPrice);
        listInput2[1].controller.text =
            AppValue.App_money_format.format(data.sanPham!.prdSellPrice);
        listInput2[2].controller.text =
            AppValue.App_money_format.format(data.sanPham!.prdSellPrice2);
        listInput3[0].controller.text =
            double.parse(data.sanPham!.prdMax ?? '0').toInt().toString();
        listInput3[1].controller.text =
            double.parse(data.sanPham!.prdMin ?? '0').toInt().toString();

        if (data.quyDoi != null && data.quyDoi!.isNotEmpty) {
          donViQuyDoi = UnitResponse(
              id: 1, prdUnitName: 'Hàng hoá có đơn vị tính và quy đổi');
        } else {
          donViQuyDoi = UnitResponse(
              id: 0, prdUnitName: 'Hàng hoá không có đơn vị tính và quy đổi');
        }

        productController.listUnit.forEach((e) {
          if (e.prdUnitName == data.sanPham!.unitName) {
            donViTinh = UnitResponse(id: e.id, prdUnitName: e.prdUnitName!);
          }
        });
        nsx = ProducerResponse(
            id: data.sanPham!.prdManufactureId, prdManufName: data.nhaSanXuat);
        danhMuc = CategoryDetail(
            id: data.sanPham!.prdGroupId, prdGroupName: data.danhMuc);

        isEditPrice = data.sanPham!.prdEditPrice == 1 ? true : false;
        isSellNegative = data.sanPham!.prdAllownegative == 1 ? true : false;
        isNguyenLieu = data.sanPham!.prd_material == 1 ? true : false;
        isInTem = data.sanPham!.prd_print == 1 ? true : false;
        isTopping = data.sanPham!.prd_add == 1 ? true : false;
        isTichDiem = data.sanPham!.prd_point == 1 ? true : false;
      }
      if (productController.detailProduct!.quyDoi != null) {
        var data = productController.detailProduct;
        for (var i = 0; i < data!.quyDoi!.length; i++) {
          listUnitConversion.add(ItemUnitConversion(
            ItemInputKhuVuc(
                controller:
                    TextEditingController(text: data.quyDoi![i].unitName),
                focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(
                    text: data.quyDoi![i].number.toString()),
                focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(
                    text: data.quyDoi![i].prdOriginPrice.toString()),
                focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(
                    text: data.quyDoi![i].prdSellPrice.toString()),
                focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(
                    text: data.quyDoi![i].prdSellPrice2.toString()),
                focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller:
                    TextEditingController(text: data.quyDoi![i].prdCode),
                focusNode: FocusNode()),
          ));
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
      appBarBottom: GunAppBar(
        type: GunAppBarType.BOTTOM,
        height: 0,
      ),
      appBarTop: GunAppBar(
        type: GunAppBarType.TOP,
        title: productController.detailProduct != null
            ? productController.detailProduct!.sanPham!.prdName
            : 'Tạo sản phẩm',
        titleStyle: AppStyles.pageTitleStyle,
        leadingColor: Colors.black,
        actions: [
          GunAction.icon(
              icon: Container(
            width: 24,
          ))
        ],
      ),
      child: Container(
        child: Column(
          children: [Expanded(child: _renderBody())],
        ),
      ),
    );
  }

  _renderBody() {
    return GetX<ProductController>(
      builder: (controller) {
        return Container(
          color: AppColors.Background,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  color: AppColors.White,
                  child: Form(
                    key: key,
                    child: Column(
                      children: [
                        controller.detailProduct != null
                            ? _renderAvatar(
                                imageNetwork: controller
                                    .detailProduct!.sanPham!.prdImageUrl)
                            : _renderAvatar(),
                        _renderInput1(),
                        _renderUnit(controller.listUnit),
                        _renderInput2(),
                        _renderCatalogue(controller.categoryList),
                        _renderProducer(controller.listProducer),
                        _renderInput3(),
                        _renderCheckBox(),
                        _renderUnitConversion(),
                      ],
                    ),
                  ),
                ),
                if ((donViQuyDoi != null && donViQuyDoi!.id == 1))
                  _renderListUnitConversion(),
                _renderButton()
              ],
            ),
          ),
        );
      },
    );
  }

  _renderAvatar({String? imageNetwork}) {
    return AvatarCreate(
        image: image,
        imageNetWork: imageNetwork,
        onChange: (e) {
          setState(() {
            image = e;
          });
        });
  }

  _renderInput1() {
    return Column(
      children: [
        ...List.generate(
          listInput1.length,
          (index) => WidgetInput(
            controller: listInput1[index].controller,
            focusNode: listInput1[index].focusNode,
            textAlign: TextAlign.right,
            keyboardType:
                index == 1 ? TextInputType.number : TextInputType.text,
            validator: index == 0
                ? FormBuilderValidators.required(errorText: 'error_miss'.tr)
                : null,
            hintText: index == 0
                ? 'Tên hàng hóa'
                : index == 1
                    ? 'Số lượng'
                    : '(Nếu không nhập, hệ thống sẽ tự sinh)',
            childRight: index == 2
                ? Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () async {
                        Map<Permission, PermissionStatus> statuses =
                            await [Permission.camera].request();
                        print(statuses[Permission.camera] ==
                            PermissionStatus.permanentlyDenied);
                        if (statuses[Permission.camera] ==
                            PermissionStatus.permanentlyDenied) {
                          AppSettings.openAppSettings();
                        } else {
                          var result = await Get.toNamed(AppRoutes.qr_code);
                          setState(() {
                            listInput1[index].controller.text = result;
                          });
                        }
                      },
                      child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Image.asset(
                            'assets/images/menu_qr.png',
                            width: 24,
                          )),
                    ),
                  )
                : SizedBox.shrink(),
            childLeft: Container(
              padding: EdgeInsets.only(right: 8),
              child: GunText(
                index == 0
                    ? 'Tên hàng hóa'
                    : index == 1
                        ? 'Số lượng'
                        : 'Mã SP',
                maxLines: 1,
                style: AppStyles.DEFAULT_16.copyWith(color: Colors.black),
              ),
            ),
          ),
        )
      ],
    );
  }

  _renderUnit(List<UnitResponse> list) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.only(bottom: 12.r, top: 12.r),
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 1, color: AppColors.Gray3))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Đơn vị tính', style: AppStyles.DEFAULT_16),
                    PopupMenuButton(
                      child: Row(
                        children: [
                          Text(
                              donViTinh != null
                                  ? donViTinh!.prdUnitName ?? ''
                                  : 'Chọn',
                              style: AppStyles.DEFAULT_16),
                          Image.asset(
                            './assets/images/expanded_icon.png',
                            height: 10.h,
                          ),
                        ],
                      ),
                      itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                        ...List.generate(
                          list.length,
                          (index) => PopupMenuItem(
                              onTap: () {
                                setState(() {
                                  donViTinh = list[index];
                                });
                              },
                              child: Text(list[index].prdUnitName!,
                                  style: AppStyles.DEFAULT_16)),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Material(
              color: AppColors.White,
              child: InkWell(
                onTap: () {
                  Get.toNamed(AppRoutes.unit, arguments: 1);
                },
                child: Container(
                  padding: EdgeInsets.only(left: 12, right: 12),
                  child: Image.asset(
                    'assets/images/add_icon.png',
                    width: 24,
                  ),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  _renderInput2() {
    return Column(
      children: [
        ...List.generate(
          listInput2.length,
          (index) => WidgetInput(
            controller: listInput2[index].controller,
            focusNode: listInput2[index].focusNode,
            textAlign: TextAlign.right,
            hintText: index == 0
                ? 'Giá vốn'
                : index == 1
                    ? 'Giá sỉ'
                    : 'Giá lẻ',
            inputFormatters: [ThousandsFormatter(allowFraction: true)],
            keyboardType: TextInputType.number,
            onChanged: (e) {
              listInput2[index].controller.text = e;

              listInput2[index].controller.selection =
                  TextSelection.fromPosition(TextPosition(
                      offset: listInput2[index].controller.text.length));
            },
            onFieldSubmitted: (e) {
              listInput2[index].controller.text = e.replaceAll(',', '');
            },
            childLeft: Container(
              padding: EdgeInsets.only(right: 8),
              child: GunText(
                index == 0
                    ? 'Giá vốn'
                    : index == 1
                        ? 'Giá sỉ'
                        : 'Giá lẻ',
                maxLines: 1,
                style: AppStyles.DEFAULT_16.copyWith(color: Colors.black),
              ),
            ),
          ),
        )
      ],
    );
  }

  _renderCatalogue(List<CategoryData> list) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.only(bottom: 12.r, top: 12.r),
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 1, color: AppColors.Gray3))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Chọn danh mục', style: AppStyles.DEFAULT_16),
                    PopupMenuButton(
                      child: Row(
                        children: [
                          Text(
                              danhMuc != null
                                  ? danhMuc!.prdGroupName ?? ''
                                  : 'Chọn',
                              style: AppStyles.DEFAULT_16),
                          Image.asset(
                            './assets/images/expanded_icon.png',
                            height: 10.h,
                          ),
                        ],
                      ),
                      itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                        ...List.generate(
                          list.length,
                          (index) => PopupMenuItem(
                              onTap: () {
                                setState(() {
                                  danhMuc = list[index].danhMuc;
                                });
                              },
                              child: Text(list[index].danhMuc!.prdGroupName!,
                                  style: AppStyles.DEFAULT_16)),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Material(
              color: AppColors.White,
              child: InkWell(
                onTap: () {
                  Get.toNamed(AppRoutes.unit, arguments: 2);
                },
                child: Container(
                  padding: EdgeInsets.only(left: 12, right: 12),
                  child: Image.asset(
                    'assets/images/add_icon.png',
                    width: 24,
                  ),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  _renderProducer(List<ProducerResponse> list) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.only(bottom: 12.r, top: 12.r),
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 1, color: AppColors.Gray3))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Chọn nhà sản xuất', style: AppStyles.DEFAULT_16),
                    PopupMenuButton(
                      child: Row(
                        children: [
                          Text(nsx != null ? nsx!.prdManufName ?? '' : 'Chọn',
                              style: AppStyles.DEFAULT_16),
                          Image.asset(
                            './assets/images/expanded_icon.png',
                            height: 10.h,
                          ),
                        ],
                      ),
                      itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                        ...List.generate(
                          list.length,
                          (index) => PopupMenuItem(
                              onTap: () {
                                setState(() {
                                  nsx = list[index];
                                });
                              },
                              child: Text(list[index].prdManufName!,
                                  style: AppStyles.DEFAULT_16)),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Material(
              color: AppColors.White,
              child: InkWell(
                onTap: () {
                  Get.toNamed(AppRoutes.unit, arguments: 3);
                },
                child: Container(
                  padding: EdgeInsets.only(left: 12, right: 12),
                  child: Image.asset(
                    'assets/images/add_icon.png',
                    width: 24,
                  ),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  _renderInput3() {
    return Column(
      children: [
        ...List.generate(
          listInput3.length,
          (index) => WidgetInput(
            textAlign: TextAlign.right,
            childLeft: Container(
              padding: EdgeInsets.only(right: 8),
              child: GunText(
                index == 0 ? 'Định mức nhỏ nhất' : 'Định mức lớn nhất',
                maxLines: 1,
                style: AppStyles.DEFAULT_16.copyWith(color: Colors.black),
              ),
            ),
            controller: listInput3[index].controller,
            focusNode: listInput3[index].focusNode,
            keyboardType: TextInputType.number,
            hintText: index == 0 ? 'Định mức nhỏ nhất' : 'Định mức lớn nhất',
          ),
        )
      ],
    );
  }

  _renderCheckBox() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: CheckBoxCustom(
              textSpan1: 'Cho phép sửa giá khi bán?',
              isCheck: isEditPrice,
              onChanged: (e) {},
              onTap: () {
                setState(() {
                  isEditPrice = !isEditPrice;
                });
              }),
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: CheckBoxCustom(
              textSpan1: 'Cho phép bán âm?',
              isCheck: isSellNegative,
              onChanged: (e) {},
              onTap: () {
                setState(() {
                  isSellNegative = !isSellNegative;
                });
              }),
        ),
        Container(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: CheckBoxCustom(
                    textSpan1: 'Tích điểm',
                    isCheck: isTichDiem,
                    onChanged: (e) {},
                    onTap: () {
                      setState(() {
                        isTichDiem = !isTichDiem;
                      });
                    }),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: CheckBoxCustom(
                    textSpan1: 'Sản phẩm Topping?',
                    isCheck: isTopping,
                    onChanged: (e) {},
                    onTap: () {
                      setState(() {
                        isTopping = !isTopping;
                      });
                    }),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: CheckBoxCustom(
                    textSpan1: 'Nguyên liệu?',
                    isCheck: isNguyenLieu,
                    onChanged: (e) {},
                    onTap: () {
                      setState(() {
                        isNguyenLieu = !isNguyenLieu;
                      });
                    }),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: CheckBoxCustom(
                    textSpan1: 'In tem nhãn khi bán?',
                    isCheck: isInTem,
                    onChanged: (e) {},
                    onTap: () {
                      setState(() {
                        isInTem = !isInTem;
                      });
                    }),
              ),
            ],
          ),
        )
      ],
    );
  }

  _renderUnitConversion() {
    return Container(
      padding: EdgeInsets.only(top: 22.r, bottom: 10.r),
      decoration: BoxDecoration(
          border:
              Border(bottom: BorderSide(width: 1, color: Color(0xffE7E7E7)))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              child: Text('Chọn loại hàng hoá', style: AppStyles.DEFAULT_16)),
          Expanded(
            child: PopupMenuButton(
              child: Text(
                  donViQuyDoi != null ? donViQuyDoi!.prdUnitName ?? '' : 'Chọn',
                  textAlign: TextAlign.right,
                  style: AppStyles.DEFAULT_16),
              itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                ...List.generate(
                  2,
                  (index) => PopupMenuItem(
                      onTap: () {
                        setState(() {
                          donViQuyDoi = UnitResponse(
                              id: index,
                              prdUnitName: index == 0
                                  ? 'Hàng hóa không có đơn vị tính và quy đổi'
                                  : 'Hàng hoá có đơn vị tính và quy đổi');
                        });
                      },
                      child: Text(
                          index == 0
                              ? 'Hàng hóa không có đơn vị tính và quy đổi'
                              : 'Hàng hoá có đơn vị tính và quy đổi',
                          style: AppStyles.DEFAULT_16)),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  _renderListUnitConversion() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GunText(
                  'Đơn vị quy đổi',
                  style:
                      AppStyles.DEFAULT_18_BOLD.copyWith(color: AppColors.Blue),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      listUnitConversion.add(ItemUnitConversion(
                        ItemInputKhuVuc(
                            controller: TextEditingController(),
                            focusNode: FocusNode()),
                        ItemInputKhuVuc(
                            controller: TextEditingController(),
                            focusNode: FocusNode()),
                        ItemInputKhuVuc(
                            controller: TextEditingController(),
                            focusNode: FocusNode()),
                        ItemInputKhuVuc(
                            controller: TextEditingController(),
                            focusNode: FocusNode()),
                        ItemInputKhuVuc(
                            controller: TextEditingController(),
                            focusNode: FocusNode()),
                        ItemInputKhuVuc(
                            controller: TextEditingController(),
                            focusNode: FocusNode()),
                      ));
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    color: Colors.transparent,
                    child: Row(
                      children: [
                        GunText(
                          'Thêm đơn vị',
                          style: AppStyles.DEFAULT_18_BOLD
                              .copyWith(color: AppColors.Orange),
                        ),
                        Image.asset(
                          './assets/images/add_icon.png',
                          width: 20,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          ...List.generate(
              listUnitConversion.length,
              (index) => Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    padding: EdgeInsets.all(16),
                    color: AppColors.White,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Material(
                          color: AppColors.White,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                listUnitConversion.removeAt(index);
                              });
                            },
                            child: Container(
                                color: Colors.transparent,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10.r, vertical: 20),
                                child: Image.asset(
                                    './assets/images/delete_icon.png',
                                    width: 24.r)),
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: _renderInput(
                                        controller: listUnitConversion[index]
                                            .item
                                            .controller,
                                        keyboardType: TextInputType.text,
                                        focusNode: listUnitConversion[index]
                                            .item
                                            .focusNode,
                                        hintText: 'unit_name'.tr),
                                  ),
                                  Container(
                                    width: 20,
                                  ),
                                  Expanded(
                                    child: _renderInput(
                                        controller: listUnitConversion[index]
                                            .item1
                                            .controller,
                                        focusNode: listUnitConversion[index]
                                            .item1
                                            .focusNode,
                                        hintText: 'quantity'.tr,
                                        onChanged: (e) {
                                          setState(() {
                                            listInput2.forEach((e) {
                                              e.controller.text = e
                                                  .controller.text
                                                  .replaceAll(',', '');
                                            });
                                          });
                                          if (int.tryParse(
                                                      listUnitConversion[index]
                                                          .item1
                                                          .controller
                                                          .text) !=
                                                  null &&
                                              listInput2[0].controller.text !=
                                                  '') {
                                            setState(() {
                                              listUnitConversion[index]
                                                  .item2
                                                  .controller
                                                  .text = (int.parse(
                                                          listInput2[0]
                                                              .controller
                                                              .text) *
                                                      int.parse(
                                                          listUnitConversion[
                                                                  index]
                                                              .item1
                                                              .controller
                                                              .text))
                                                  .toString();
                                              if (listInput2[1]
                                                      .controller
                                                      .text !=
                                                  '') {
                                                listUnitConversion[index]
                                                    .item3
                                                    .controller
                                                    .text = (int.parse(
                                                            listInput2[1]
                                                                .controller
                                                                .text) *
                                                        int.parse(
                                                            listUnitConversion[
                                                                    index]
                                                                .item1
                                                                .controller
                                                                .text))
                                                    .toString();
                                              }
                                              if (listInput2[2]
                                                      .controller
                                                      .text !=
                                                  '') {
                                                listUnitConversion[index]
                                                    .item4
                                                    .controller
                                                    .text = (int.parse(
                                                            listInput2[2]
                                                                .controller
                                                                .text) *
                                                        int.parse(
                                                            listUnitConversion[
                                                                    index]
                                                                .item1
                                                                .controller
                                                                .text))
                                                    .toString();
                                              }
                                            });
                                          }
                                        }),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: AbsorbPointer(
                                      child: _renderInput(
                                          controller: listUnitConversion[index]
                                              .item2
                                              .controller,
                                          focusNode: listUnitConversion[index]
                                              .item2
                                              .focusNode,
                                          hintText: 'price_origin'.tr,
                                          inputFormatters: [
                                            ThousandsFormatter()
                                          ],
                                          onChanged: (e) {
                                            setState(() {
                                              listUnitConversion[index]
                                                  .item2
                                                  .controller
                                                  .text = e;
                                              listUnitConversion[index]
                                                      .item2
                                                      .controller
                                                      .text =
                                                  listUnitConversion[index]
                                                      .item2
                                                      .controller
                                                      .text
                                                      .replaceAll(',', '');
                                              listUnitConversion[index]
                                                      .item2
                                                      .controller
                                                      .selection =
                                                  TextSelection.fromPosition(
                                                      TextPosition(
                                                          offset:
                                                              listUnitConversion[
                                                                      index]
                                                                  .item2
                                                                  .controller
                                                                  .text
                                                                  .length));
                                            });
                                          }),
                                    ),
                                  ),
                                  Container(
                                    width: 20,
                                  ),
                                  Expanded(
                                    child: AbsorbPointer(
                                      child: _renderInput(
                                          controller: listUnitConversion[index]
                                              .item3
                                              .controller,
                                          focusNode: listUnitConversion[index]
                                              .item3
                                              .focusNode,
                                          hintText: 'partial'.tr,
                                          inputFormatters: [
                                            ThousandsFormatter()
                                          ],
                                          onChanged: (e) {
                                            setState(() {
                                              listUnitConversion[index]
                                                  .item3
                                                  .controller
                                                  .text = e;
                                              listUnitConversion[index]
                                                      .item3
                                                      .controller
                                                      .text =
                                                  listUnitConversion[index]
                                                      .item3
                                                      .controller
                                                      .text
                                                      .replaceAll(',', '');
                                              listUnitConversion[index]
                                                      .item3
                                                      .controller
                                                      .selection =
                                                  TextSelection.fromPosition(
                                                      TextPosition(
                                                          offset:
                                                              listUnitConversion[
                                                                      index]
                                                                  .item3
                                                                  .controller
                                                                  .text
                                                                  .length));
                                            });
                                          }),
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: AbsorbPointer(
                                      child: _renderInput(
                                          controller: listUnitConversion[index]
                                              .item4
                                              .controller,
                                          focusNode: listUnitConversion[index]
                                              .item4
                                              .focusNode,
                                          hintText: 'wholesale'.tr,
                                          inputFormatters: [
                                            ThousandsFormatter()
                                          ],
                                          onChanged: (e) {
                                            setState(() {
                                              listUnitConversion[index]
                                                  .item4
                                                  .controller
                                                  .text = e;
                                              listUnitConversion[index]
                                                      .item4
                                                      .controller
                                                      .text =
                                                  listUnitConversion[index]
                                                      .item4
                                                      .controller
                                                      .text
                                                      .replaceAll(',', '');
                                              listUnitConversion[index]
                                                      .item4
                                                      .controller
                                                      .selection =
                                                  TextSelection.fromPosition(
                                                      TextPosition(
                                                          offset:
                                                              listUnitConversion[
                                                                      index]
                                                                  .item4
                                                                  .controller
                                                                  .text
                                                                  .length));
                                            });
                                          }),
                                    ),
                                  ),
                                  Container(
                                    width: 20,
                                  ),
                                  Expanded(
                                    child: AbsorbPointer(
                                      child: _renderInput(
                                          controller: listUnitConversion[index]
                                              .item5
                                              .controller,
                                          focusNode: listUnitConversion[index]
                                              .item5
                                              .focusNode,
                                          hintText: 'sp.code'.tr),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ))
        ],
      ),
    );
  }

  _renderInput(
      {required TextEditingController controller,
      required FocusNode focusNode,
      String? hintText,
      TextInputType? keyboardType,
      ValueChanged<String>? onChanged,
      List<TextInputFormatter>? inputFormatters}) {
    return WidgetInput(
      keyboardType: keyboardType ?? TextInputType.number,
      controller: controller,
      focusNode: focusNode,
      hintText: hintText,
      onChanged: (e) {
        if (onChanged != null) {
          onChanged(e);
        }
      },
      inputFormatters: inputFormatters,
    );
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.all(30),
      child: WidgetButton(
        onTap: () async {
          if (key.currentState!.validate()) {
            setState(() {
              listInput2.forEach((e) {
                e.controller.text = e.controller.text.replaceAll(',', '');
              });
            });
            List<UnitConversionResponse> list = [];
            if (donViQuyDoi != null && donViQuyDoi!.id == 1) {
              setState(() {
                listUnitConversion.forEach((e) {
                  if (int.tryParse(e.item2.controller.text) != null &&
                      int.tryParse(e.item4.controller.text) != null &&
                      int.tryParse(e.item3.controller.text) != null &&
                      int.tryParse(e.item1.controller.text) != null) {
                    list.add(UnitConversionResponse(
                        prdCode: e.item5.controller.text,
                        prdOriginPrice: int.parse(e.item2.controller.text),
                        prdSellPrice2: int.parse(e.item4.controller.text),
                        prdSellPrice: int.parse(e.item3.controller.text),
                        number:int.parse(e.item1.controller.text)  ,
                        unitName: e.item.controller.text));
                  }
                });
              });
            }
            print(list);

            if (productController.idProduct != -1) {
              productController.detailProduct!.quyDoi!.forEach((e) {
                list.forEach((e1) {
                  setState(() {
                    e1.id = e.id;
                  });
                  print(e1.id);
                });
              });

              var result = await productController.productEdit(
                  id: productController.idProduct,
                  prdName: listInput1[0].controller.text,
                  prdCode: listInput1[2].controller.text,
                  prd_sls: listInput1[1].controller.text != ''
                      ? int.parse(listInput1[1].controller.text)
                      : null,
                  prd_image_url: image,
                  prd_edit_price: isEditPrice ? 1 : 0,
                  prd_print: isInTem ? 1 : 0,
                  prd_allownegative: isSellNegative ? 1 : 0,
                  prd_material: isNguyenLieu ? 1 : 0,
                  prdAdd: isTopping ? 1 : 0,
                  infor: '',
                  prd_origin_price: listInput2[0].controller.text != ''
                      ? int.parse(listInput2[0].controller.text)
                      : 0,
                  prd_sell_price: listInput2[1].controller.text != ''
                      ? int.parse(listInput2[1].controller.text)
                      : 0,
                  prd_sell_price2: listInput2[2].controller.text != ''
                      ? int.parse(listInput2[2].controller.text)
                      : 0,
                  prd_group_id: danhMuc != null ? danhMuc!.id! : null,
                  prd_manufacture_id: nsx != null ? nsx!.id! : null,
                  prd_unit_id: donViTinh != null ? donViTinh!.id! : null,
                  prd_new: 1,
                  prd_hot: 1,
                  prd_highlight: 1,
                  prd_max: listInput3[1].controller.text != '' &&
                          listInput3[1].controller.text != '0'
                      ? int.parse(listInput3[1].controller.text)
                      : null,
                  prd_min: listInput3[0].controller.text != '' &&
                          listInput3[0].controller.text != '0'
                      ? int.parse(listInput3[0].controller.text)
                      : null,
                  quyDoi: list);
              Get.dialog(DialogCustom(
                  title: 'Thông báo',
                  onTapOK: () {
                    Get.back();
                    Get.back();
                    Get.back();
                    _controller.loadProductList(null, '');
                  },
                  children: [
                    Text(result.message!, style: AppStyles.DEFAULT_16)
                  ]));
            } else {
              var result = await productController.productCreate(
                  prdName: listInput1[0].controller.text,
                  prdCode: listInput1[2].controller.text,
                  prd_sls: listInput1[1].controller.text != ''
                      ? int.parse(listInput1[1].controller.text)
                      : null,
                  prd_image_url: image,
                  prd_edit_price: isEditPrice ? 1 : 0,
                  prd_print: isInTem ? 1 : 0,
                  prd_allownegative: isSellNegative ? 1 : 0,
                  prd_material: isNguyenLieu ? 1 : 0,
                  prdAdd: isTopping ? 1 : 0,
                  infor: '',
                  prd_origin_price: listInput2[0].controller.text != ''
                      ? int.parse(listInput2[0].controller.text)
                      : 0,
                  prd_sell_price: listInput2[1].controller.text != ''
                      ? int.parse(listInput2[1].controller.text)
                      : 0,
                  prd_sell_price2: listInput2[2].controller.text != ''
                      ? int.parse(listInput2[2].controller.text)
                      : 0,
                  prd_group_id: danhMuc != null ? danhMuc!.id! : null,
                  prd_manufacture_id: nsx != null ? nsx!.id! : null,
                  prd_unit_id: donViTinh != null ? donViTinh!.id! : null,
                  prd_new: 1,
                  prd_hot: 1,
                  prd_highlight: 1,
                  prd_max: listInput3[1].controller.text != '' &&
                          listInput3[1].controller.text != '0'
                      ? int.parse(listInput3[1].controller.text)
                      : null,
                  prd_min: listInput3[0].controller.text != '' &&
                          listInput3[0].controller.text != '0'
                      ? int.parse(listInput3[0].controller.text)
                      : null,
                  quyDoi: list);
              Get.dialog(DialogCustom(
                  title: 'Thông báo',
                  onTapOK: () {
                    Get.back();
                    Get.back();
                    _controller.loadProductList(null, '');
                  },
                  children: [
                    Text(result.message!, style: AppStyles.DEFAULT_16)
                  ]));
            }
          }
        },
        isFullWidth: true,
        backgroundColor: AppColors.Blue,
        text: productController.idProduct != -1 ? 'Chỉnh sửa' : 'Tạo',
        textStyle: AppStyles.DEFAULT_18_BOLD.copyWith(color: AppColors.White),
      ),
    );
  }
}

class ItemUnitConversion {
  final ItemInputKhuVuc item;
  final ItemInputKhuVuc item1;
  final ItemInputKhuVuc item2;
  final ItemInputKhuVuc item3;
  final ItemInputKhuVuc item4;
  final ItemInputKhuVuc item5;

  ItemUnitConversion(
      this.item, this.item1, this.item2, this.item3, this.item4, this.item5);
}
