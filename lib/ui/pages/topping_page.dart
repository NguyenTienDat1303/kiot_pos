import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/controllers/order_create_controller.dart';
import 'package:kiot_pos/controllers/order_topping_controller.dart';
import 'package:kiot_pos/data/entities/response/order_detail_response.dart';
import 'package:kiot_pos/models/routes.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/res/values.dart';
import 'package:kiot_pos/ui/pages/order_topping_page.dart';
import 'package:kiot_pos/ui/widgets/widget_cached_image.dart';

class ToppingPage extends StatefulWidget {
  const ToppingPage({Key? key}) : super(key: key);

  @override
  State<ToppingPage> createState() => _ToppingPageState();
}

class _ToppingPageState extends State<ToppingPage>
    with TickerProviderStateMixin {
  OrderToppingController _controller = Get.find<OrderToppingController>();

  // TextEditingController _tuKhoaEditController = TextEditingController();

  late TabController _tabController;

  late ODProductData _productData;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 0), () {
      _controller.loadToppingList('');
    });
  }

  TextEditingController textController = TextEditingController();
  bool isSearch = false;
  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          backgroundColor: Colors.white,
          type: GunAppBarType.TOP,
          titles: Row(
            children: [
              isSearch
                  ? Expanded(
                      child: TextField(
                      controller: textController,
                      onChanged: (e) {
                        setState(() {});
                      },
                      onSubmitted: (e) {
                        _controller.loadToppingList(e);
                      },
                      autofocus: true,
                      decoration: InputDecoration(
                          isDense: true, border: InputBorder.none),
                    ))
                  : GunText(
                      'order_topping.all'.tr,
                      style: AppStyles.pageTitleStyle,
                    ),
            ],
          ),
          leadingColor: Colors.black,
          actions: [
            GunAction.icon(
              onTap: () {
                setState(() {
                  isSearch = !isSearch;
                  textController.clear();
                });
                if (isSearch == false) {
                  _controller.loadToppingList('');
                }
              },
              iconFactor: isSearch ? 0.4 : 0.7,
              icon: Image.asset(
                isSearch
                    ? './assets/images/close_icon.png'
                    : './assets/images/search_icon.png',
                color: AppColors.Orange,
              ),
            ),
            if (isSearch == false)
              GunAction.icon(
                onTap: () {
                  Get.toNamed(AppRoutes.topping_create);
                },
                icon: Image.asset('assets/images/add_icon.png'),
              )
          ],
        ),
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            Positioned.fill(
              child: SingleChildScrollView(
                padding:
                    EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 100),
                child: GetX<OrderToppingController>(
                  builder: (controller) {
                    return Column(
                      children: [
                        Container(
                          height: 0,
                          child: Text(controller.toppingList.length.toString()),
                        ),
                        Column(
                          children: [
                            ...List.generate(controller.toppingList.length,
                                (index) {
                              return OrderToppingWidget(
                                isStock: double.parse((controller
                                            .toppingList[index].prdSls) ??
                                        "0")
                                    .toInt()
                                    .toString(),
                                data: controller.toppingList[index],
                                // quantity: 0,
                              );
                            })
                          ],
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ],
        ));
  }
}
