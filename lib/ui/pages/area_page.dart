import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class AreaPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const AreaPage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<AreaPage> createState() => _AreaPageState();
}

class _AreaPageState extends State<AreaPage> {
  AreaController areaController = Get.find<AreaController>();
  AreaEditController areaEditController = Get.find<AreaEditController>();
  TextEditingController areaTextController = TextEditingController();
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 0), () {
      areaController.area();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetX<AreaController>(
      builder: (controller) {
        return GunPage(
            appBarBottom: GunAppBar(
              type: GunAppBarType.BOTTOM,
              height: 0,
            ),
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              title: 'home_drawer.area'.tr,
              titleStyle: AppStyles.pageTitleStyle,
              leading: [
                GunAction.icon(
                    onTap: () {
                      widget.homePageKey.currentState!.openDrawer();
                    },
                    icon: Image.asset('assets/images/drawer_menu.png'))
              ],
              actions: [
                GunAction.icon(
                  iconFactor: 0.5,
                  onTap: () {
                    
                    Get.toNamed(
                      AppRoutes.area_edit,
                    );
                  },
                  icon: Image.asset(
                    './assets/images/add_icon.png',
                  ),
                )
              ],
            ),
            child: _renderBody(controller.listArea, controller));
      },
    );
  }

  _renderBody(List<AreaData> list, AreaController controller) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          children: [
            ...List.generate(
                list.length,
                (index) => EditDeleteInfo(
                    onTap: () {
                      Get.toNamed(AppRoutes.area_detail,
                          arguments: [list[index], list[index].id]);
                    },
                    name: list[index].areaName!,
                    onDelete: () {
                      Get.dialog(DialogCustom(
                          title: 'Thông báo',
                          isTwoButton: true,
                          nameButtonLeft: 'Huỷ',
                          nameButtonRight: 'Xoá',
                          onTapLeft: () {
                            Get.back();
                          },
                          onTapRight: () {
                            Get.back();
                            controller.areaDelete(
                                id: list[index].id!,
                                callback: () {
                                  areaController.area();
                                });
                          },
                          children: [
                            Text('Bạn có chắc chắn muốnn xoá ?',
                                style: AppStyles.DEFAULT_16),
                          ]));
                    },
                    onEdit: () {
                      Get.toNamed(AppRoutes.area_edit,
                          arguments: list[index].id!);
                    })),
          ],
        ),
      ),
    );
  }
}
