import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderToppingPage extends StatefulWidget {
  const OrderToppingPage({
    Key? key,
  }) : super(key: key);

  @override
  State<OrderToppingPage> createState() => _OrderToppingPageState();
}

class _OrderToppingPageState extends State<OrderToppingPage>
    with TickerProviderStateMixin {
  OrderToppingController _controller = Get.find<OrderToppingController>();
  OrderCreateController _orderCreateController =
      Get.find<OrderCreateController>();
  TextEditingController _tuKhoaEditController = TextEditingController();

  late TabController _tabController;

  late ODProductData _productData;

  @override
  void initState() {
    super.initState();
    _productData = Get.arguments;
    _controller.loadToppingList(_tuKhoaEditController.text);
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          backgroundColor: Colors.white,
          type: GunAppBarType.TOP,
          title: 'order_topping.all'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
          actions: [
            GunAction.icon(
              onTap: () {},
              icon: Image.asset('assets/images/menu_search.png'),
            )
          ],
        ),
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            Positioned.fill(
              child: SingleChildScrollView(
                padding:
                    EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 100),
                child: GetX<OrderToppingController>(
                  builder: (controller) {
                    return Column(
                      children: [
                        Container(
                          height: 0,
                          child: Text(controller.toppingList.length.toString()),
                        ),
                        GetX<OrderCreateController>(
                          builder: (orderCreateController) {
                            return Column(
                              children: [
                                ...List.generate(controller.toppingList.length,
                                    (index) {
                                  var _thisProductData = orderCreateController
                                      .orderDetail
                                      .findProduct(_productData.sanPham!.id!,
                                          version: _productData.version);

                                  var _thisTopping = _thisProductData != null
                                      ? _thisProductData!.findTopping(
                                          controller.toppingList[index].id!)
                                      : null;
                                  return OrderToppingWidget(
                                    data: controller.toppingList[index],
                                    // quantity: 0,
                                    quantity: _thisTopping != null
                                        ? _thisTopping.quantityAdd!
                                        : 0,
                                    onAddTap: () {
                                      _orderCreateController.addTopping(
                                          _productData,
                                          controller.toppingList[index]);
                                    },
                                    onDeleteTap: () {
                                      _orderCreateController.deleteTopping(
                                          _productData,
                                          controller.toppingList[index]);
                                    },
                                  );
                                })
                              ],
                            );
                          },
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
            GetX<OrderCreateController>(
              builder: (controller) {
                var productData = controller.orderDetail.findProduct(
                    _productData.sanPham!.id!,
                    version: _productData.version);

                return GunInkwell(
                  onTap: () {
                    Get.back();
                  },
                  borderRadius: BorderRadius.circular(500),
                  margin: EdgeInsets.all(16),
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    color: AppColors.primary,
                  ),
                  child: Container(
                    width: double.infinity,
                    height: 40,
                    child: Row(children: [
                      Expanded(
                          child: Text(
                        'order_topping.total_topping'.trParams({
                          'count':
                              '${productData != null ? productData!.totalToppingQuantity : 0}'
                        }),
                        style: AppStyles.normalTextStyle
                            .copyWith(color: Colors.white),
                      )),
                      Expanded(
                          child: Text(
                        '${AppValue.APP_MONEY_FORMAT.format(productData != null ? productData!.totalToppingSellPrice : 0)}',
                        textAlign: TextAlign.end,
                        style: AppStyles.normalTextStyle
                            .copyWith(color: Colors.white),
                      ))
                    ]),
                  ),
                );
              },
            )
          ],
        ));
  }
}

class OrderToppingWidget extends StatelessWidget {
  final ToppingData data;
  final int quantity;
  final VoidCallback? onAddTap;
  final VoidCallback? onDeleteTap;
  final String? isStock;
  const OrderToppingWidget(
      {Key? key,
      required this.data,
      this.quantity = 0,
      this.onAddTap,
      this.onDeleteTap,
      this.isStock})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: quantity < 1 ? onAddTap : null,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        decoration: BoxDecoration(
            color: quantity > 0 ? Color(0XFFFFEBD4) : Colors.white,
            border: Border.all(color: Colors.grey.shade300, width: 1),
            borderRadius: BorderRadius.circular(5)),
        margin: EdgeInsets.only(bottom: 10),
        child: Container(
          padding: EdgeInsets.all(5),
          child: Row(
            children: [
              Container(
                  width: 45,
                  height: 45,
                  margin: EdgeInsets.only(right: 10),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(5.0),
                      child:
                          WidgetCachedImage(imageUrl: data.prdImageUrl ?? ""))),
              Expanded(
                  child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data.prdName!,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Container(
                      constraints: BoxConstraints(minHeight: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '${AppValue.APP_MONEY_FORMAT.format(data.prdSellPrice)}',
                            style: TextStyle(
                                color: Color(0XFFE27C04),
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                          if (quantity > 0)
                            Expanded(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                GunInkwell(
                                  onTap: onDeleteTap,
                                  padding: EdgeInsets.zero,
                                  child: Container(
                                      width: 30,
                                      height: 30,
                                      child: Image.asset(
                                          'assets/images/btn_delete.png')),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 15, right: 15),
                                  child: Text(
                                    '$quantity',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                                GunInkwell(
                                  onTap: onAddTap,
                                  padding: EdgeInsets.zero,
                                  child: Container(
                                      width: 30,
                                      height: 30,
                                      child: Image.asset(
                                          'assets/images/btn_add.png')),
                                )
                              ],
                            )),
                          if (isStock != null)
                            Expanded(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                GunText(
                                  'stock'.tr,
                                  style: AppStyles.DEFAULT_14
                                      .copyWith(color: Colors.black),
                                ),
                                GunText(
                                  isStock ?? '',
                                  style: AppStyles.DEFAULT_14
                                      .copyWith(color: Colors.blue),
                                )
                              ],
                            ))
                        ],
                      ),
                    )
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
