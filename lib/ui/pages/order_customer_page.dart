import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';

class OrderCustomerPage extends StatefulWidget {
  const OrderCustomerPage({
    Key? key,
  }) : super(key: key);

  @override
  State<OrderCustomerPage> createState() => _OrderCustomerPageState();
}

class _OrderCustomerPageState extends State<OrderCustomerPage> {
  CustomerController _controller = Get.find<CustomerController>();
  OrderCreateController _orderCreateController =
      Get.find<OrderCreateController>();

  TextEditingController textController = TextEditingController();
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 0), () {
      _controller.getListCustomer(tuKhoa: '');
    });
  }

  bool isSearch = false;
  @override
  Widget build(BuildContext context) {
    return GetX<CustomerController>(
      builder: (controller) {
        return GunPage(
            appBarBottom: GunAppBar(
              type: GunAppBarType.BOTTOM,
              height: 0,
            ),
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              titles: Row(
                children: [
                  isSearch
                      ? Expanded(
                          child: TextField(
                          controller: textController,
                          onChanged: (e) {
                            setState(() {});
                          },
                          onSubmitted: (e) {
                            controller.getListCustomer(tuKhoa: e);
                          },
                          autofocus: true,
                          decoration: InputDecoration(
                              isDense: true, border: InputBorder.none),
                        ))
                      : GunText(
                          'home_drawer.customer'.tr,
                          style: AppStyles.pageTitleStyle,
                        ),
                ],
              ),
              leadingColor: Colors.black,
              actions: [
                GunAction.icon(
                  onTap: () {
                    setState(() {
                      isSearch = !isSearch;
                      textController.clear();
                    });
                    if (isSearch == false) {
                      _controller.getListCustomer(tuKhoa: '');
                    }
                  },
                  iconFactor: isSearch ? 0.4 : 0.7,
                  icon: Image.asset(
                    isSearch
                        ? './assets/images/close_icon.png'
                        : './assets/images/search_icon.png',
                    color: AppColors.Orange,
                  ),
                ),
              ],
            ),
            child: _renderBody(controller.listCustomer));
      },
    );
  }

  _renderBody(List<CustomerResponse> list) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...List.generate(
              list.length,
              (index) => Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: GestureDetector(
                      onTap: () {
                        _orderCreateController.addCustomer(list[index]);
                        Get.back();
                      },
                      child: BaseInfoWidget(
                          title: list[index].customerName,
                          contextLeft1: 'customer.code'.tr,
                          contextLeft2: 'address'.tr,
                          contextLeft3: 'phone'.tr,
                          contextRight1: list[index].customerCode!,
                          contextRight2: list[index].customerAddr!,
                          contextRight3: list[index].customerPhone!),
                    ),
                  )),
        ],
      ),
    );
  }
}
