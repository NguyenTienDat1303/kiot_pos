import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/controllers/order_create_controller.dart';
import 'package:kiot_pos/controllers/order_create_product_controller.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/data/entities/response/common/product_data.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/res/values.dart';
import 'package:kiot_pos/ui/pages/order_create_product_page.dart';
import 'package:kiot_pos/ui/widgets/widget_cached_image.dart';

import '../../data/entities/response/order_detail_response.dart';

class StockingGoods extends StatefulWidget {
  const StockingGoods({Key? key}) : super(key: key);

  @override
  State<StockingGoods> createState() => _StockingGoodsState();
}

class _StockingGoodsState extends State<StockingGoods>
    with TickerProviderStateMixin {
  StockController stockController = Get.find<StockController>();
  OrderCreateProductController _controller =
      Get.find<OrderCreateProductController>();
  ProductController productController = Get.find<ProductController>();

  late TabController _tabController;

  late OrderDetailResponse _orderDetail;
  TextEditingController textController = TextEditingController();
  @override
  void initState() {
    super.initState();

    _controller.loadCategoryList(tuKhoa: '');
  }

  bool isSearch = false;

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          backgroundColor: Colors.white,
          type: GunAppBarType.TOP,
          titles: Row(
            children: [
              isSearch
                  ? Expanded(
                      child: TextField(
                      controller: textController,
                      onChanged: (e) {
                        setState(() {});
                      },
                      onSubmitted: (e) {
                        _controller.tuKhoa = e;
                        _controller.loadCategoryList(tuKhoa: '');
                      },
                      autofocus: true,
                      decoration: InputDecoration(
                          isDense: true, border: InputBorder.none),
                    ))
                  : GunText(
                      'list_goods'.tr,
                      style: AppStyles.pageTitleStyle,
                    ),
            ],
          ),
          leadingColor: Colors.black,
          actions: [
            GunAction.icon(
              onTap: () {
                setState(() {
                  isSearch = !isSearch;
                  textController.clear();
                });
                if (isSearch == false) {
                  _controller.tuKhoa = '';
                  _controller.loadCategoryList(tuKhoa: '');
                }
              },
              iconFactor: isSearch ? 0.4 : 0.7,
              icon: Image.asset(
                isSearch
                    ? './assets/images/close_icon.png'
                    : './assets/images/search_icon.png',
                color: AppColors.Orange,
              ),
            ),
          ],
        ),
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            Column(
              children: [
                GetX<OrderCreateProductController>(
                  builder: (controller) {
                    _tabController = TabController(
                      initialIndex: 0,
                      length: controller.categoryList.length,
                      vsync: this,
                    );
                    _tabController.addListener(
                      () => {
                        _controller.categorySelected =
                            controller.categoryList[_tabController.index]
                      },
                    );
                    return Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      margin: EdgeInsets.symmetric(horizontal: 16),
                      height: 40,
                      padding: EdgeInsets.symmetric(vertical: 3, horizontal: 8),
                      decoration: BoxDecoration(
                          color: Color(0XFFE7E7E7),
                          borderRadius: BorderRadius.circular(25.0)),
                      child: TabBar(
                          controller: _tabController,
                          isScrollable: true,
                          indicator: BoxDecoration(
                              color: AppColors.primary,
                              borderRadius: BorderRadius.circular(20.0)),
                          labelColor: Colors.white,
                          unselectedLabelColor: Colors.black,
                          tabs: List.generate(
                              controller.categoryList.length,
                              (index) => Tab(
                                    text: controller.categoryList[index]
                                        .danhMuc!.prdGroupName!.tr,
                                  ))),
                    );
                  },
                ),
                Expanded(
                    child: SingleChildScrollView(
                  padding: EdgeInsets.only(
                      left: 16, right: 16, top: 16, bottom: 100),
                  child: GetX<OrderCreateProductController>(
                    builder: (controller) {
                      return Column(
                        children: [
                          Container(
                            height: 0,
                            child:
                                Text(controller.productList.length.toString()),
                          ),
                          GetX<OrderCreateController>(
                            builder: (orderCreateController) {
                              return Column(
                                children: [
                                  ...List.generate(
                                      controller.productList.length, (index) {
                                    return CatalogueWidget(
                                      onTap: () {
                                        int count = 0;
                                        stockController.listStockCreate
                                            .forEach((e) {
                                          if (e.tenSP ==
                                              controller
                                                  .productList[index].prdName) {
                                            setState(() {
                                              count++;
                                            });
                                          }
                                        });
                                        if (count == 0) {
                                          if (Get.arguments == null) {
                                            stockController.listStockCreate
                                                .add(ChiTiet(
                                              id: controller
                                                  .productList[index].id,
                                              tenSP: controller
                                                  .productList[index].prdName,
                                              tonKho: double.parse(controller
                                                      .productList[index]
                                                      .prdSls!)
                                                  .toInt(),
                                              thucTe: 0,
                                              lech: -double.parse(controller
                                                      .productList[index]
                                                      .prdSls!)
                                                  .toInt(),
                                            ));
                                          } else {
                                            stockController
                                                .detailStock!.chiTiet!
                                                .add(ChiTiet(
                                              id: controller
                                                  .productList[index].id,
                                              tenSP: controller
                                                  .productList[index].prdName,
                                              tonKho: double.parse(controller
                                                      .productList[index]
                                                      .prdSls!)
                                                  .toInt(),
                                              thucTe: 0,
                                              lech: -double.parse(controller
                                                      .productList[index]
                                                      .prdSls!)
                                                  .toInt(),
                                            ));
                                          }
                                        }
                                        print(stockController.listStockCreate);
                                        Get.back();
                                      },
                                      data: controller.productList[index],
                                    );
                                  })
                                ],
                              );
                            },
                          ),
                        ],
                      );
                    },
                  ),
                ))
              ],
            ),
          ],
        ));
  }
}
