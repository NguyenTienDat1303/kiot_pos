import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class PrintBluetoothPrinterPage extends StatefulWidget {
  const PrintBluetoothPrinterPage({Key? key}) : super(key: key);

  @override
  State<PrintBluetoothPrinterPage> createState() =>
      _PrintBluetoothPrinterPageState();
}

class _PrintBluetoothPrinterPageState extends State<PrintBluetoothPrinterPage> {
  // PrintBluetoothPrinterController _controller =
  //     Get.find<PrintBluetoothPrinterController>();

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'print_bluetooth_printer.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
        ),
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: GetX<HomeController>(
            builder: (controller) {
              print("PrintBluetoothPrinterController");
              return Stack(
                children: [
                  // SingleChildScrollView(
                  //   child: Column(
                  //     children: List.generate(
                  //         controller.bluetoothPrinters.length,
                  //         (index) =>
                  //             Text(controller.bluetoothPrinters[index].name!)),
                  //   ),
                  // ),
                  // if (controller.scanLoading)
                  //   Positioned.fill(
                  //       child: Container(
                  //     // color: Colors.red,
                  //     child: Center(
                  //       child: CircularProgressIndicator(),
                  //     ),
                  //   ))
                ],
              );
            },
          ),
        ));
  }
}
