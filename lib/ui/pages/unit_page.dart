import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';

class UnitPage extends StatefulWidget {
  const UnitPage({Key? key}) : super(key: key);

  @override
  State<UnitPage> createState() => _UnitPageState();
}

class _UnitPageState extends State<UnitPage> {
  ProductController productController = Get.find<ProductController>();
  TextEditingController textController = TextEditingController();
  int type = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (Get.arguments != null) {
      setState(() {
        type = Get.arguments;
      });
      Future.delayed(Duration.zero, () {
        if (type == 1) {
          productController.unit();
        } else if (type == 2) {
          productController.loadCategoryList('');
        } else if (type == 3) {
          productController.producer();
        }
      });
    }
  }

  onAdd() {
    Get.dialog(DialogCustom(
        backDropTap: () {
          null;
        },
        title: type == 1
            ? 'Tạo mới đơn vị tính'
            : type == 2
                ? 'tạo mới danh mục'
                : 'Tạo mới nhà sản xuấy',
        isTwoButton: true,
        nameButtonLeft: 'Huỷ',
        nameButtonRight: 'save'.tr,
        onTapLeft: () {
          Get.back();
        },
        onTapRight: () async {
          Get.back();
          onSuccess(type);
        },
        children: [
          TextField(
            controller: textController,
            onChanged: (e) {
              setState(() {});
            },
            autofocus: true,
          )
        ]));
  }

  onSuccess(int type) {
    if (type == 1) {
      productController.unitCreate(textController.text, callback: () {
        productController.unit();
      });
    } else if (type == 2) {
      productController.catalogueCreate(
          tenDanhMuc: textController.text,
          callback: () {
            productController.loadCategoryList('');
          });
    } else if (type == 3) {
      productController.producerCreate(textController.text, callback: () {
        productController.producer();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetX<ProductController>(
      builder: (controller) {
        return GunPage(
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              leadingColor: Colors.black,
              decoration: BoxDecoration(color: AppColors.Background),
              title: type == 1
                  ? 'unit'.tr
                  : type == 2
                      ? 'catalogue_goods'.tr
                      : 'producer'.tr,
              titleStyle: AppStyles.pageTitleStyle,
              actions: [
                GunAction.icon(
                  iconFactor: 0.5,
                  onTap: onAdd,
                  icon: Image.asset(
                    './assets/images/add_icon.png',
                  ),
                )
              ],
            ),
            child: Container(
              color: AppColors.Background,
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                children: [
                  if (type == 1) _renderUnit(controller.listUnit, controller),
                  if (type == 2)
                    _renderCatalogue(controller.categoryList, controller),
                  if (type == 3)
                    _renderProducer(controller.listProducer, controller),
                ],
              ),
            ));
      },
    );
  }

  _renderUnit(List<UnitResponse> list, ProductController controller) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...List.generate(
              list.length,
              (index) => EditDeleteInfo(
                    name: list[index].prdUnitName!,
                    // onDelete: () {
                    //   Get.dialog(DialogCustom(
                    //       title: 'Thông báo',
                    //       isTwoButton: true,
                    //       nameButtonLeft: 'Huỷ',
                    //       nameButtonRight: 'Xoá',
                    //       onTapLeft: () {
                    //         Get.back();
                    //       },
                    //       onTapRight: () {
                    //         Get.back();
                    //       },
                    //       children: [
                    //         Text('Bạn có chắc chắn muốnn xoá ?',
                    //             style: AppStyles.DEFAULT_16),
                    //       ]));
                    // },
                  )),
        ],
      ),
    );
  }

  _renderCatalogue(List<CategoryData> list, ProductController controller) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...List.generate(
              list.length,
              (index) => EditDeleteInfo(
                    name: list[index].danhMuc!.prdGroupName!,
                    // onDelete: () {
                    //   Get.dialog(DialogCustom(
                    //       title: 'Thông báo',
                    //       isTwoButton: true,
                    //       nameButtonLeft: 'Huỷ',
                    //       nameButtonRight: 'Xoá',
                    //       onTapLeft: () {
                    //         Get.back();
                    //       },
                    //       onTapRight: () {
                    //         Get.back();
                    //       },
                    //       children: [
                    //         Text('Bạn có chắc chắn muốnn xoá ?',
                    //             style: AppStyles.DEFAULT_16),
                    //       ]));
                    // },
                  )),
        ],
      ),
    );
  }

  _renderProducer(List<ProducerResponse> list, ProductController controller) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...List.generate(
              list.length,
              (index) => EditDeleteInfo(
                    name: list[index].prdManufName!,
                    // onDelete: () {
                    //   Get.dialog(DialogCustom(
                    //       title: 'Thông báo',
                    //       isTwoButton: true,
                    //       nameButtonLeft: 'Huỷ',
                    //       nameButtonRight: 'Xoá',
                    //       onTapLeft: () {
                    //         Get.back();
                    //       },
                    //       onTapRight: () {
                    //         Get.back();
                    //       },
                    //       children: [
                    //         Text('Bạn có chắc chắn muốnn xoá ?',
                    //             style: AppStyles.DEFAULT_16),
                    //       ]));
                    // },
                  )),
        ],
      ),
    );
  }
}
