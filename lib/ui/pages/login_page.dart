import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginController _controller = Get.find<LoginController>();
  final _formKey = GlobalKey<FormState>();

  TextEditingController _storeEditController = TextEditingController();
  TextEditingController _usernameEditController = TextEditingController();
  TextEditingController _passwordEditController = TextEditingController();

  bool _passwordObscureText = true;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    await _controller.loadPrefs();
    // _storeEditController.text = _controller.storeName;
    // _usernameEditController.text = _controller.username;
    _usernameEditController.text = "nguyentiendat130397mobi@gmail.com";
    _passwordEditController.text = "admin123";
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
            type: GunAppBarType.TOP,
            height: 0,
            decoration: BoxDecoration(color: Colors.white)),
        appBarBottom: GunAppBar(
            type: GunAppBarType.BOTTOM,
            height: 0,
            decoration: BoxDecoration(color: Colors.white)),
        child: Stack(
          children: [
            Positioned.fill(
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Image.asset('assets/images/bg_login_bottom.png'))),
            Positioned.fill(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          Container(
                            width: double.infinity,
                            height: 200,
                            child: Image.asset(
                              'assets/images/login_bg.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                          Positioned(
                            top: 120.h,
                            left: 30.w,
                            child: Card(
                              semanticContainer: true,
                              clipBehavior: Clip.hardEdge,
                              elevation: 5,
                              child: Container(
                                height: 60.h,
                                width: 60.w,
                                child: Image.asset(
                                  'assets/images/logo.png',
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 40),
                      padding: EdgeInsets.symmetric(horizontal: 30),
                      child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              SizedBox(
                                height: 30,
                              ),
                              TextFormField(
                                  style: AppStyles.normalTextStyle,
                                  // textAlign: TextAlign.center,
                                  controller: _storeEditController,
                                  textAlignVertical: TextAlignVertical.center,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    // alignLabelWithHint: true,
                                    hintText: 'login.store'.tr,
                                    contentPadding: EdgeInsets.only(bottom: 10),
                                    prefixIconConstraints: BoxConstraints(
                                        minWidth: 0, minHeight: 0),
                                    prefixIcon: Container(
                                        margin: EdgeInsets.only(
                                            right: 5, bottom: 10),
                                        width: 20,
                                        child: Image.asset(
                                            'assets/images/login_store.png')),
                                    suffixIcon: Container(
                                      margin: EdgeInsets.only(bottom: 10),
                                      child: Text(
                                        ".kiotsoft.com",
                                        style: AppStyles.normalTextStyle
                                            .copyWith(color: AppColors.primary),
                                      ),
                                    ),
                                    suffixIconConstraints: BoxConstraints(
                                        minWidth: 0, minHeight: 10),
                                  )),
                              SizedBox(
                                height: 30,
                              ),
                              TextFormField(
                                  controller: _usernameEditController,
                                  autovalidateMode: AutovalidateMode.always,
                                  validator: FormBuilderValidators.required(
                                      errorText: "login.user_name_required".tr),
                                  style: AppStyles.normalTextStyle,
                                  // textAlign: TextAlign.center,
                                  textAlignVertical: TextAlignVertical.center,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    // alignLabelWithHint: true,
                                    hintText: 'login.user_name'.tr,
                                    contentPadding: EdgeInsets.only(bottom: 10),
                                    prefixIconConstraints: BoxConstraints(
                                        minWidth: 0, minHeight: 0),
                                    prefixIcon: Container(
                                        margin: EdgeInsets.only(
                                            right: 5, bottom: 10),
                                        width: 20,
                                        child: Image.asset(
                                            'assets/images/login_user_name.png')),
                                  )),
                              SizedBox(
                                height: 30,
                              ),
                              TextFormField(
                                  controller: _passwordEditController,
                                  autovalidateMode: AutovalidateMode.always,
                                  validator: FormBuilderValidators.required(
                                      errorText: "login.password_required".tr),
                                  obscureText: _passwordObscureText,
                                  style: AppStyles.normalTextStyle,
                                  // textAlign: TextAlign.center,
                                  textAlignVertical: TextAlignVertical.center,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    // alignLabelWithHint: true,
                                    hintText: 'login.password'.tr,
                                    contentPadding: EdgeInsets.only(bottom: 10),
                                    prefixIconConstraints: BoxConstraints(
                                        minWidth: 0, minHeight: 0),
                                    prefixIcon: Container(
                                        margin: EdgeInsets.only(
                                            right: 5, bottom: 10),
                                        width: 20,
                                        child: Image.asset(
                                            'assets/images/login_password.png')),
                                    suffixIcon: Container(
                                      width: 24,
                                      margin: EdgeInsets.only(bottom: 10),
                                      child: GunInkwell(
                                        onTap: () {
                                          setState(() {
                                            _passwordObscureText =
                                                !_passwordObscureText;
                                          });
                                        },
                                        padding: EdgeInsets.zero,
                                        child: Image.asset(_passwordObscureText
                                            ? 'assets/images/password_eye.png'
                                            : 'assets/images/password_eye_remove.png'),
                                      ),
                                    ),
                                    suffixIconConstraints: BoxConstraints(
                                        minWidth: 0, minHeight: 10),
                                  )),
                              Container(
                                alignment: Alignment.centerRight,
                                margin: EdgeInsets.symmetric(vertical: 15),
                                child: GunInkwell(
                                  onTap: () {
                                    Get.toNamed(AppRoutes.forgot_password);
                                  },
                                  child: Text(
                                    "login.forgot_password".tr,
                                    style: AppStyles.normalTextStyle
                                        .copyWith(color: AppColors.primary),
                                  ),
                                ),
                              ),
                              Container(
                                width: double.infinity,
                                child: TextButton(
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              AppColors.primary),
                                      shape: MaterialStateProperty.all(
                                          RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20),
                                      ))),
                                  onPressed: () async {
                                    // Validate returns true if the form is valid, or false otherwise.
                                    if (_formKey.currentState!.validate()) {
                                      var response = await _controller.login(
                                          _storeEditController.text,
                                          _usernameEditController.text,
                                          _passwordEditController.text);
                                      Get.offAllNamed(AppRoutes.home);
                                    } else {
                                      // ScaffoldMessenger.of(context)
                                      //     .showSnackBar(
                                      //   const SnackBar(content: Text('Ko dc')),
                                      // );
                                    }
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(3),
                                    child: Text(
                                      'login.title'.tr,
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 40,
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                        text: 'login.no_account'.tr + " ",
                                        style: AppStyles.normalTextStyle),
                                    TextSpan(
                                      text: 'login.register'.tr,
                                      style: AppStyles.normalTextStyle
                                          .copyWith(color: AppColors.primary),
                                      recognizer: new TapGestureRecognizer()
                                        ..onTap = () =>
                                            Get.toNamed(AppRoutes.register),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 100,
                              ),
                              Column(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(right: 10),
                                          width: 30,
                                          height: 30,
                                          child: Image.asset(
                                              'assets/images/login_hot_line.png')),
                                      Text(
                                        '0888.1900.36',
                                        style: TextStyle(
                                            color: AppColors.primary,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w700),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(right: 10),
                                          width: 30,
                                          height: 30,
                                          child: Image.asset(
                                              'assets/images/login_website.png')),
                                      Text(
                                        '0888.1900.36',
                                        style: TextStyle(
                                            color: AppColors.primary,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w700),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              )
                            ],
                          )),
                    )
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
