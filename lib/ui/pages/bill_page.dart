import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:get/get.dart';
import 'package:gun_base/res/values.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/ui/widgets/base_info_widget.dart';
import 'package:kiot_pos/ui/widgets/choose_time.dart';

class BillPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const BillPage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<BillPage> createState() => _BillPageState();
}

class _BillPageState extends State<BillPage> with TickerProviderStateMixin {
  OverviewCollectPayController overviewCollectPayController =
      Get.find<OverviewCollectPayController>();
  BillDetailController billDetailBinding = Get.find<BillDetailController>();

  late TabController _tabController;
  List<String> listTabBar = [
    'Tổng quan',
    'Phiếu thu',
    'Phiếu chi',
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 0), () {
      overviewCollectPayController.getOverviewCollectPay();
      overviewCollectPayController.getListCollect(
          '', null, null, null, null, 10);
      overviewCollectPayController.getListPay('', null, null, null, null, 10);
    });
    _tabController = TabController(
      initialIndex: 0,
      length: listTabBar.length,
      vsync: this,
    );
    _tabController.addListener(() {
      setState(() {
        indexTab = _tabController.index;
        if (indexTab == 0) {
          overviewCollectPayController.getOverviewCollectPay();
        } else if (indexTab == 1) {
          overviewCollectPayController.getListCollect(
              '', null, null, null, null, 10);
        } else {
          overviewCollectPayController.getListPay(
              '', null, null, null, null, 10);
        }
      });
    });
  }

  String start = AppValue.DATE_FORMAT.format(DateTime.now());
  String end = AppValue.DATE_FORMAT.format(DateTime.now());
  String start1 = AppValue.DATE_FORMAT.format(DateTime.now());
  String end1 = AppValue.DATE_FORMAT.format(DateTime.now());
  int indexTab = 0;
  @override
  Widget build(BuildContext context) {
    return GetX<OverviewCollectPayController>(
      builder: (controller) {
        return GunPage(
            appBarBottom: GunAppBar(
              type: GunAppBarType.BOTTOM,
              height: 0,
            ),
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              title: 'home_drawer.bill'.tr,
              titleStyle: AppStyles.pageTitleStyle,
              leading: [
                GunAction.icon(
                    onTap: () {
                      widget.homePageKey.currentState!.openDrawer();
                    },
                    icon: Image.asset('assets/images/drawer_menu.png'))
              ],
              actions: [
                if (indexTab != 0)
                  GunAction.icon(
                    iconFactor: 0.5,
                    onTap: () {
                      billDetailBinding.idBill = -1;
                      Get.toNamed(AppRoutes.bill_edit, arguments: indexTab - 1);
                    },
                    icon: Image.asset(
                      './assets/images/add_icon.png',
                    ),
                  )
              ],
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                children: [
                  _renderTabbar(),
                  Expanded(
                      child: indexTab == 0
                          ? _renderItem1(controller.overview)
                          : indexTab == 1
                              ? _renderItem2(controller.listCollect)
                              : _renderItem3(controller.listPay))
                ],
              ),
            ));
      },
    );
  }

  _renderTabbar() {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      margin: EdgeInsets.only(top: 12),
      height: 40,
      padding: EdgeInsets.symmetric(vertical: 3, horizontal: 8),
      decoration: BoxDecoration(
          color: Color(0XFFE7E7E7), borderRadius: BorderRadius.circular(25.0)),
      child: TabBar(
          controller: _tabController,
          isScrollable: true,
          indicator: BoxDecoration(
              color: AppColors.primary,
              borderRadius: BorderRadius.circular(20.0)),
          labelColor: Colors.white,
          unselectedLabelColor: Colors.black,
          tabs: List.generate(
              listTabBar.length,
              (index) => Tab(
                    text: listTabBar[index],
                  ))),
    );
  }

  _renderItem1(OverviewCollectPayResponse? data) {
    return data != null
        ? SingleChildScrollView(
            child: Column(children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                child: CellWhite(
                    padding: EdgeInsets.all(16),
                    child: Container(
                      child: Row(
                        children: [
                          Image.asset(
                            './assets/images/drawer7_tab1_icon1.png',
                            height: 30,
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(left: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  GunText(
                                    'Phiếu Thu',
                                    style: AppStyles.DEFAULT_16_BOLD
                                        .copyWith(color: AppColors.Blue),
                                  ),
                                  _renderItemTongQuan('Tổng phiếu thu',
                                      data.soPhieuThu! + ' phiếu'),
                                  _renderItemTongQuan(
                                      'Tổng thu',
                                      GunValue.format_money(
                                          double.parse(data.tongTienThu!)))
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                child: CellWhite(
                    padding: EdgeInsets.all(16),
                    child: Row(
                      children: [
                        Image.asset(
                          './assets/images/drawer7_tab1_icon2.png',
                          height: 30,
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                GunText(
                                  'Phiếu Chi',
                                  style: AppStyles.DEFAULT_16_BOLD
                                      .copyWith(color: AppColors.Orange),
                                ),
                                _renderItemTongQuan('Tổng phiếu chi',
                                    data.soPhieuChi! + ' phiếu'),
                                _renderItemTongQuan(
                                    'Tổng chi',
                                    GunValue.format_money(
                                        double.parse(data.tongTienChi ?? "0")))
                              ],
                            ),
                          ),
                        ),
                      ],
                    )),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                child: CellWhite(
                    padding: EdgeInsets.all(16),
                    child: Container(
                      child: Row(
                        children: [
                          Image.asset(
                            './assets/images/drawer7_tab1_icon3.png',
                            height: 30,
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(left: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  GunText(
                                    'Còn Lại',
                                    style: AppStyles.DEFAULT_16_BOLD
                                        .copyWith(color: AppColors.GREEN),
                                  ),
                                  _renderItemTongQuan(
                                      'Tổng còn',
                                      GunValue.format_money(double.parse(
                                          data.conLai!.toString()))),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                child: CellWhite(
                    padding: EdgeInsets.all(16),
                    child: Container(
                      child: Row(
                        children: [
                          Image.asset(
                            './assets/images/drawer7_tab1_icon4.png',
                            height: 30,
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(left: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  GunText(
                                    'Hình Thức Thanh Toán',
                                    style: AppStyles.DEFAULT_16_BOLD
                                        .copyWith(color: Colors.black),
                                  ),
                                  _renderItemTongQuan(
                                      'Tiền mặt',
                                      GunValue.format_money(
                                          double.parse(data.tienMat ?? "0"))),
                                  _renderItemTongQuan(
                                      'Chuyển khoản',
                                      data.chuyenKhoan != null
                                          ? GunValue.format_money(double.parse(
                                              data.chuyenKhoan ?? "0"))
                                          : ''),
                                  _renderItemTongQuan(
                                      'Thẻ',
                                      data.the != null
                                          ? GunValue.format_money(
                                              double.parse(data.the ?? "0"))
                                          : ''),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
              )
            ]),
          )
        : SizedBox.shrink();
  }

  _renderItem2(List<CollectResponse> list) {
    return SingleChildScrollView(
      child: Column(children: [
        Container(
            child: ChooseTime(
                indexTabBar: 1,
                onChangeFrom: (e) {
                  setState(() {
                    start = e;
                  });
                },
                onChangeTo: (e) {
                  setState(() {
                    end = e;
                  });
                  overviewCollectPayController.getListCollect(
                      '',
                      null,
                      null,
                      AppValue.formatSearchDay(start),
                      AppValue.formatSearchDay(end),
                      100);
                })),
        ...List.generate(
            list.length,
            (index) => Container(
                  margin: EdgeInsets.only(top: 16),
                  child: BaseInfoWidget(
                      onTap: () {
                        Get.toNamed(AppRoutes.bill_detail,
                            arguments: [0, list[index].phieuThu!.id]);
                      },
                      contextLeft1: 'collect.code'.tr,
                      contextLeft2: 'collect.user'.tr,
                      contextLeft3: 'total_money',
                      contextRight1: list[index].phieuThu!.receiptCode ?? '',
                      contextRight2: list[index].tenNguoiThu!,
                      contextRight3:
                          list[index].phieuThu!.totalMoney.toString()),
                ))
      ]),
    );
  }

  _renderItem3(List<PayResponse> list) {
    return SingleChildScrollView(
      child: Column(children: [
        Container(
            child: ChooseTime(
                indexTabBar: 2,
                onChangeFrom: (e) {
                  setState(() {
                    start1 = e;
                  });
                },
                onChangeTo: (e) {
                  setState(() {
                    end1 = e;
                  });
                  overviewCollectPayController.getListPay(
                      '',
                      null,
                      null,
                      AppValue.formatSearchDay(start1),
                      AppValue.formatSearchDay(end1),
                      100);
                })),
        ...List.generate(
            list.length,
            (index) => Container(
                  margin: EdgeInsets.only(top: 16),
                  child: BaseInfoWidget(
                      onTap: () {
                        Get.toNamed(AppRoutes.bill_detail,
                            arguments: [1, list[index].phieuChi!.id]);
                      },
                      contextLeft1: 'pay.code'.tr,
                      contextLeft2: 'pay.user'.tr,
                      contextLeft3: 'total_money'.tr,
                      contextRight1: list[index].phieuChi!.paymentCode ?? '',
                      contextRight2: list[index].tenNguoiChi!,
                      contextRight3:
                          list[index].phieuChi!.totalMoney.toString()),
                ))
      ]),
    );
  }

  _renderItemTongQuan(String title, String subTitle) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(title,
            style: AppStyles.DEFAULT_14
                .copyWith(color: AppColors.Gray1, fontWeight: FontWeight.bold)),
        Text(subTitle,
            style: AppStyles.DEFAULT_14.copyWith(color: AppColors.Gray1)),
      ]),
    );
  }
}
