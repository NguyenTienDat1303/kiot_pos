import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class PaymentPage extends StatefulWidget {
  const PaymentPage({
    Key? key,
  }) : super(key: key);

  @override
  State<PaymentPage> createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  OrderCreateController _controller = Get.find<OrderCreateController>();
  HomeController _homeController = Get.find<HomeController>();
  OrderDiscountRatio orderDiscountType = OrderDiscountRatio.vnd;

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'payment.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
        ),
        child: SingleChildScrollView(
            padding: EdgeInsets.all(16),
            child: GetX<OrderCreateController>(
              builder: (controller) {
                return Column(children: [
                  GestureDetector(
                    onTap: () {
                      Get.toNamed(AppRoutes.order_customer);
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          left: 4, right: 4, top: 0, bottom: 16),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border.all(color: AppColors.primary),
                          borderRadius: BorderRadius.circular(5)),
                      child: Row(children: [
                        Expanded(
                          child: Text(
                            controller.orderDetail.tenkhach != null
                                ? controller.orderDetail.tenkhach!.customerName!
                                : "order_create.customer".tr,
                            style: AppStyles.normalTextStyle.copyWith(
                                color: Color(0XFFBDBDBD),
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                        if (_homeController.authorization([
                          KiotVersion.coffee,
                          KiotVersion.karaoke,
                          KiotVersion.milktea,
                          KiotVersion.shop,
                          KiotVersion.enter
                        ], [
                          KiotPermisson.customer_create,
                        ]))
                          GunInkwell(
                            onTap: () {
                              Get.toNamed(AppRoutes.customer_create);
                            },
                            padding: EdgeInsets.zero,
                            child: Container(
                                width: 24,
                                child: Image.asset(
                                    'assets/images/order_create_customer_add.png')),
                          ),
                      ]),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(top: 10),
                    elevation: 5,
                    child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Text.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                            text: 'payment.total_money_product'
                                                    .tr +
                                                " ",
                                            style: AppStyles.normalTextStyle),
                                        TextSpan(
                                          text:
                                              '(${controller.orderDetail.tongSoLuong})',
                                          style: AppStyles.normalTextStyle
                                              .copyWith(
                                                  color: Color(0XFFE27C04)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tongTienHang ?? 0)}',
                                      style: AppStyles.normalTextStyle,
                                    ),
                                  ],
                                ))
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Text.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                            text:
                                                'payment.total_money_staff'.tr +
                                                    " ",
                                            style: AppStyles.normalTextStyle),
                                        TextSpan(
                                          text:
                                              '(${controller.orderDetail.nhanVien!.length})',
                                          style: AppStyles.normalTextStyle
                                              .copyWith(
                                                  color: Color(0XFFE27C04)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tienNhanVien ?? 0)}',
                                      style: AppStyles.normalTextStyle,
                                    ),
                                  ],
                                ))
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  'payment.total_money_hour'.tr,
                                  style: AppStyles.normalTextStyle,
                                )),
                                Expanded(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tienBan ?? 0)}',
                                      style: AppStyles.normalTextStyle,
                                    ),
                                  ],
                                ))
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  'payment.vat_money'.tr,
                                  style: AppStyles.normalTextStyle,
                                )),
                                Expanded(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tienVAT ?? 0)}',
                                      style: AppStyles.normalTextStyle,
                                    ),
                                  ],
                                ))
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  'payment.total_money'.tr,
                                  style: AppStyles.normalTextStyle,
                                )),
                                Expanded(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tongThanhToan ?? 0)}',
                                      style: AppStyles.normalTextStyle,
                                    ),
                                  ],
                                ))
                              ],
                            )
                          ],
                        )),
                  ),
                  if (_homeController.authorization([
                    KiotVersion.coffee,
                    KiotVersion.milktea,
                    KiotVersion.karaoke
                  ], []))
                    Card(
                      margin: EdgeInsets.only(top: 10),
                      elevation: 5,
                      child: Container(
                          padding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                    'payment.time_enter'.tr,
                                    style: AppStyles.normalTextStyle,
                                  )),
                                  Expanded(
                                      child: AbsorbPointer(
                                    absorbing: true,
                                    child: GunInkwell(
                                      onTap: () {
                                        ComponentUtils.showCupertinoDatePicker(
                                            context,
                                            mode: CupertinoDatePickerMode
                                                .dateAndTime,
                                            maximumDate: DateTime(2900),
                                            onSubmitted: (val) {});
                                      },
                                      padding: EdgeInsets.zero,
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: TextFormField(
                                              initialValue:
                                                  '${controller.orderDetail.donHang!.fromDate ?? ''}',
                                              textAlign: TextAlign.end,
                                              style: AppStyles.normalTextStyle,
                                              decoration: InputDecoration(
                                                  hintText:
                                                      '15:30 - 15/03/2022 ',
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.only(
                                                          bottom: 2)),
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 5),
                                              width: 24,
                                              child: Image.asset(
                                                  'assets/images/payment_calendar.png'))
                                        ],
                                      ),
                                    ),
                                  ))
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                    'payment.time_exit'.tr,
                                    style: AppStyles.normalTextStyle,
                                  )),
                                  Expanded(
                                      child: AbsorbPointer(
                                    absorbing: true,
                                    child: GunInkwell(
                                      onTap: () {
                                        ComponentUtils.showCupertinoDatePicker(
                                            context,
                                            mode: CupertinoDatePickerMode
                                                .dateAndTime,
                                            maximumDate: DateTime(2900),
                                            onSubmitted: (val) {});
                                      },
                                      padding: EdgeInsets.zero,
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: TextFormField(
                                              initialValue:
                                                  '${controller.orderDetail.donHang!.finishDate ?? ''}',
                                              textAlign: TextAlign.end,
                                              style: AppStyles.normalTextStyle,
                                              decoration: InputDecoration(
                                                  hintText: '',
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.only(
                                                          bottom: 2)),
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 5),
                                              width: 24,
                                              child: Image.asset(
                                                  'assets/images/payment_calendar.png'))
                                        ],
                                      ),
                                    ),
                                  ))
                                ],
                              )
                            ],
                          )),
                    ),
                  if (_homeController.authorization([
                    KiotVersion.shop,
                    KiotVersion.enter,
                  ], []))
                    Card(
                      margin: EdgeInsets.only(top: 10),
                      elevation: 5,
                      child: Container(
                          padding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                    'payment.time_sell'.tr,
                                    style: AppStyles.normalTextStyle,
                                  )),
                                  Expanded(
                                      child: AbsorbPointer(
                                    absorbing: true,
                                    child: GunInkwell(
                                      onTap: () {
                                        ComponentUtils.showCupertinoDatePicker(
                                            context,
                                            mode: CupertinoDatePickerMode
                                                .dateAndTime,
                                            maximumDate: DateTime(2900),
                                            onSubmitted: (val) {});
                                      },
                                      padding: EdgeInsets.zero,
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: TextFormField(
                                              initialValue:
                                                  '${controller.orderDetail.donHang!.sellDate != null ? AppValue.backendDateTimeToDateTome(controller.orderDetail.donHang!.sellDate!) : ''}',
                                              textAlign: TextAlign.end,
                                              style: AppStyles.normalTextStyle,
                                              decoration: InputDecoration(
                                                  hintText:
                                                      '15:30 - 15/03/2022 ',
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.only(
                                                          bottom: 2)),
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 5),
                                              width: 24,
                                              child: Image.asset(
                                                  'assets/images/payment_calendar.png'))
                                        ],
                                      ),
                                    ),
                                  ))
                                ],
                              ),
                            ],
                          )),
                    ),
                  if (_homeController.authorization([
                    KiotVersion.coffee,
                    KiotVersion.milktea,
                    KiotVersion.karaoke
                  ], []))
                    Card(
                        margin: EdgeInsets.only(top: 10),
                        elevation: 5,
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text.rich(
                                  TextSpan(
                                    children: [
                                      TextSpan(
                                          text: 'payment.change_room'.tr + " ",
                                          style: AppStyles.normalTextStyle),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                  child: GunInkwell(
                                onTap: () async {
                                  if (_controller.orderId != null) {
                                    try {
                                      TableData tableData = await Get.toNamed(
                                          AppRoutes.move_table);
                                      var response =
                                          await _controller.moveTable(
                                              _controller.tableData!,
                                              tableData,
                                              TableAction.change);
                                      Get.until((route) =>
                                          route.settings.name ==
                                          AppRoutes.home);
                                      Get.dialog(DialogCustom(
                                          title: "dialog.title_notify".tr,
                                          onTapOK: () {
                                            Get.back();
                                          },
                                          children: [
                                            Text("${response.message}")
                                          ]));
                                      ;
                                    } catch (e) {}
                                  }
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${_controller.tableData != null ? _controller.tableData!.ban!.tableName : ""}',
                                      style: AppStyles.normalTextStyle,
                                    ),
                                    Icon(Icons.arrow_drop_down)
                                  ],
                                ),
                              ))
                            ],
                          ),
                        )),
                  if (_homeController.authorization([
                    KiotVersion.coffee,
                    KiotVersion.karaoke,
                    KiotVersion.milktea,
                    KiotVersion.shop,
                    KiotVersion.enter
                  ], [
                    KiotPermisson.sell_coupon,
                    KiotPermisson.sell_discount,
                  ]))
                    Card(
                      margin: EdgeInsets.only(top: 10),
                      elevation: 5,
                      child: Container(
                          padding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Text.rich(
                                      TextSpan(
                                        children: [
                                          TextSpan(
                                              text:
                                                  'payment.total_money_discount'
                                                      .tr,
                                              style: AppStyles.normalTextStyle),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        '${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.donHang!.coupon ?? 0)}',
                                        style: AppStyles.normalTextStyle,
                                      ),
                                    ],
                                  ))
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                      child: Row(
                                    children: [
                                      Text(
                                        'payment.discount'.tr,
                                        style: AppStyles.normalTextStyle,
                                      ),
                                      Container(
                                        width: 80,
                                        height: 30,
                                        margin: EdgeInsets.only(left: 10),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            border: Border.all(
                                                width: 2,
                                                color: AppColors.primary)),
                                        child: Row(children: [
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  orderDiscountType =
                                                      OrderDiscountRatio.vnd;
                                                });
                                              },
                                              child: Container(
                                                alignment: Alignment.center,
                                                height: double.infinity,
                                                decoration: BoxDecoration(
                                                    color: orderDiscountType ==
                                                            OrderDiscountRatio
                                                                .vnd
                                                        ? AppColors.primary
                                                        : Colors.white),
                                                child: Text(
                                                  "VND",
                                                  style: AppStyles
                                                      .normalTextStyle
                                                      .copyWith(
                                                          color: orderDiscountType ==
                                                                  OrderDiscountRatio
                                                                      .vnd
                                                              ? Colors.white
                                                              : AppColors
                                                                  .primary),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  orderDiscountType =
                                                      OrderDiscountRatio
                                                          .percent;
                                                });
                                              },
                                              child: Container(
                                                alignment: Alignment.center,
                                                height: double.infinity,
                                                decoration: BoxDecoration(
                                                    color: orderDiscountType ==
                                                            OrderDiscountRatio
                                                                .percent
                                                        ? AppColors.primary
                                                        : Colors.white),
                                                child: Text(
                                                  "%",
                                                  style: AppStyles
                                                      .normalTextStyle
                                                      .copyWith(
                                                          color: orderDiscountType ==
                                                                  OrderDiscountRatio
                                                                      .percent
                                                              ? Colors.white
                                                              : AppColors
                                                                  .primary),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]),
                                      )
                                    ],
                                  )),
                                  Expanded(
                                      child: orderDiscountType ==
                                              OrderDiscountRatio.vnd
                                          ? TextFormField(
                                              key: ValueKey(1),
                                              // initialValue:
                                              //     '${controller.orderDetail.donHang!.coupon ?? 0}',
                                              onChanged: (val) {
                                                var coupon = 0;
                                                try {
                                                  coupon = int.parse(val);
                                                } catch (e) {}

                                                controller.orderDetail.donHang!
                                                    .coupon = coupon;
                                                controller.refreshOrderDetail();
                                              },
                                              // initialValue: AppValue.formatterVnd
                                              //     .format(
                                              //         '${controller.orderDetail.donHang!.coupon ?? 0}'),
                                              // inputFormatters: <
                                              //     TextInputFormatter>[
                                              //   AppValue.formatterVnd
                                              // ],
                                              keyboardType:
                                                  TextInputType.number,
                                              inputFormatters: <
                                                  TextInputFormatter>[
                                                FilteringTextInputFormatter
                                                    .digitsOnly
                                              ], // Only
                                              textAlign: TextAlign.end,
                                              style: AppStyles.normalTextStyle,
                                              decoration: InputDecoration(
                                                  hintText: '',
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.only(
                                                          bottom: 2)),
                                            )
                                          : TextFormField(
                                              key: ValueKey(2),
                                              // initialValue:
                                              //     ' ${controller.orderDetail.donHang!.coupon ?? '0'}',
                                              onChanged: (val) {
                                                var coupon = 0;
                                                try {
                                                  coupon = int.parse(val);
                                                  if (coupon <= 100) {
                                                    coupon = ((controller
                                                            .orderDetail
                                                            .donHang!
                                                            .coupon = controller
                                                                .orderDetail
                                                                .donHang!
                                                                .totalMoney! *
                                                            coupon) ~/
                                                        100);
                                                  }
                                                } catch (e) {}
                                                controller.orderDetail.donHang!
                                                    .coupon = coupon;
                                                controller.refreshOrderDetail();
                                              },
                                              keyboardType:
                                                  TextInputType.number,
                                              inputFormatters: <
                                                  TextInputFormatter>[
                                                FilteringTextInputFormatter
                                                    .digitsOnly
                                              ], // Only
                                              textAlign: TextAlign.end,
                                              style: AppStyles.normalTextStyle,
                                              decoration: InputDecoration(
                                                  hintText: '',
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.only(
                                                          bottom: 2)),
                                            ))
                                ],
                              )
                            ],
                          )),
                    ),
                  if (_homeController.authorization([
                    KiotVersion.coffee,
                    KiotVersion.karaoke,
                    KiotVersion.milktea,
                    KiotVersion.shop,
                    KiotVersion.enter
                  ], [
                    KiotPermisson.sell_vat,
                  ]))
                    Card(
                      margin: EdgeInsets.only(top: 10),
                      elevation: 5,
                      child: Container(
                          padding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                    'payment.vat'.tr,
                                    style: AppStyles.normalTextStyle,
                                  )),
                                  Expanded(
                                      child: GunInkwell(
                                    onTap: () {
                                      Get.bottomSheet(Container(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 30, horizontal: 10),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.only(
                                                topLeft:
                                                    const Radius.circular(15),
                                                topRight:
                                                    const Radius.circular(15),
                                              )),
                                          child: SingleChildScrollView(
                                              child: Column(
                                            children: List.generate(
                                                VatOrder.supported.length,
                                                (index) => GunInkwell(
                                                      onTap: () {
                                                        Get.back();
                                                        controller.orderDetail
                                                                .donHang!.vat =
                                                            VatOrder.supported[
                                                                index];
                                                        controller.syncData();
                                                      },
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                            border: Border(
                                                                bottom: BorderSide(
                                                                    width: 1,
                                                                    color: Colors
                                                                        .grey
                                                                        .shade300))),
                                                        width: double.infinity,
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 10,
                                                                right: 10,
                                                                top: 10,
                                                                bottom: 20),
                                                        child: Text(
                                                            '${VatOrder.supported[index]}%'
                                                                .tr),
                                                      ),
                                                    )),
                                          ))));
                                    },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          '${controller.orderDetail.donHang!.vat ?? 0}%',
                                          style: AppStyles.normalTextStyle,
                                        ),
                                        Icon(Icons.arrow_drop_down)
                                      ],
                                    ),
                                  ))
                                ],
                              ),
                            ],
                          )),
                    ),
                  Card(
                      margin: EdgeInsets.only(top: 10),
                      elevation: 5,
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Text.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                            text:
                                                'payment.customer_pay'.tr + " ",
                                            style: AppStyles.normalTextStyle
                                                .copyWith(
                                                    color: AppColors.primary,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Text(
                                  '${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tongThanhToan ?? 0)}',
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                      color: AppColors.primary),
                                ))
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Text.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                            text: 'payment.payment_method'.tr +
                                                " ",
                                            style: AppStyles.normalTextStyle),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: GunInkwell(
                                  padding: EdgeInsets.symmetric(),
                                  onTap: () {
                                    print("abc");
                                    Get.bottomSheet(Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 30, horizontal: 10),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.only(
                                              topLeft:
                                                  const Radius.circular(15),
                                              topRight:
                                                  const Radius.circular(15),
                                            )),
                                        child: SingleChildScrollView(
                                            child: Column(
                                          children: List.generate(
                                              PaymentMethod.supported.length,
                                              (index) => Material(
                                                    color: Colors.transparent,
                                                    child: InkWell(
                                                      onTap: () {
                                                        _controller
                                                                .orderDetail
                                                                .donHang!
                                                                .paymentMethod =
                                                            PaymentMethod
                                                                    .supported[
                                                                index];
                                                        _controller
                                                            .refreshOrderDetail();
                                                        Get.back();
                                                      },
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                            border: Border(
                                                                bottom: BorderSide(
                                                                    width: 1,
                                                                    color: Colors
                                                                        .grey
                                                                        .shade300))),
                                                        width: double.infinity,
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 10,
                                                                right: 10,
                                                                top: 10,
                                                                bottom: 20),
                                                        child: Text(
                                                            'payment_method.${PaymentMethod.supported[index]}'
                                                                .tr),
                                                      ),
                                                    ),
                                                  )),
                                        ))));
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        controller.orderDetail.donHang!
                                                    .paymentMethod !=
                                                null
                                            ? 'payment_method.${controller.orderDetail.donHang!.paymentMethod}'
                                                .tr
                                            : ''.tr,
                                        style: AppStyles.normalTextStyle,
                                      ),
                                      Icon(Icons.arrow_drop_down)
                                    ],
                                  ),
                                ))
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  'payment.customer_given'.tr,
                                  style: AppStyles.normalTextStyle,
                                )),
                                Expanded(
                                    child: TextFormField(
                                  initialValue: AppValue.formatterVnd.format(
                                      controller
                                          .orderDetail.donHang!.customerPay
                                          .toString()),
                                  inputFormatters: <TextInputFormatter>[
                                    AppValue.formatterVnd
                                  ],
                                  keyboardType: TextInputType.number,
                                  onChanged: (val) {
                                    var customerPay = 0;
                                    try {
                                      customerPay = int.parse(val);
                                    } catch (e) {}
                                    controller
                                            .orderDetail.donHang!.customerPay =
                                        AppValue.formatterVnd
                                            .getUnformattedValue()
                                            .toInt();
                                    controller.refreshOrderDetail();
                                  },
                                  textAlign: TextAlign.end,
                                  style: AppStyles.normalTextStyle,
                                  decoration: InputDecoration(
                                      hintText: '',
                                      isDense: true,
                                      contentPadding:
                                          EdgeInsets.only(bottom: 2)),
                                ))
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Text.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                            text:
                                                'payment.money_return'.tr + " ",
                                            style: AppStyles.normalTextStyle),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${AppValue.APP_MONEY_FORMAT.format((controller.orderDetail.donHang!.customerPay!.toDouble() - controller.orderDetail.tongThanhToan!) > 0 ? controller.orderDetail.donHang!.customerPay!.toDouble() - controller.orderDetail.tongThanhToan! : 0)}',
                                      style: AppStyles.normalTextStyle
                                          .copyWith(color: Color(0XFFE27C04)),
                                    ),
                                  ],
                                ))
                              ],
                            ),
                          ],
                        ),
                      )),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 40),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 1,
                            child: TextButton(
                                onPressed: () async {
                                  try {
                                    await _controller.syncData(
                                        orderStatus: OrderStatus.tempPayment);
                                    var printManagerController =
                                        Get.find<PrintManagerController>();
                                    printManagerController.printForm(
                                        PrintAction.payment_temp,
                                        tableData: _controller.tableData,
                                        loadTableResponse:
                                            _controller.orderDetail);
                                  } catch (e) {
                                    print(e);
                                  }
                                },
                                style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                    )),
                                    padding: MaterialStateProperty.all(
                                        EdgeInsets.symmetric(horizontal: 10)),
                                    backgroundColor: MaterialStateProperty.all(
                                        Color(0XFFD6E8FF))),
                                child: Text(
                                  'payment.print'.tr,
                                  style: TextStyle(
                                      color: AppColors.primary,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600),
                                )),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            flex: 1,
                            child: TextButton(
                                onPressed: () async {
                                  try {
                                    var result = await _controller.syncData(
                                        orderStatus: OrderStatus.payment);
                                    if (result == true) {
                                      var printManagerController =
                                          Get.find<PrintManagerController>();
                                      var printResult =
                                          await printManagerController
                                              .printForm(PrintAction.payment,
                                                  tableData:
                                                      _controller.tableData,
                                                  loadTableResponse:
                                                      _controller.orderDetail);
                                      if (printResult) {
                                        Get.until((route) =>
                                            route.settings.name ==
                                            AppRoutes.home);
                                        Get.dialog(DialogCustom(
                                            title: "dialog.title_notify".tr,
                                            children: [Text("Thành công")]));
                                      }
                                    }
                                  } catch (e) {
                                    print("ERROR $e");
                                  }
                                },
                                style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                    )),
                                    padding: MaterialStateProperty.all(
                                        EdgeInsets.symmetric(horizontal: 10)),
                                    backgroundColor: MaterialStateProperty.all(
                                        AppColors.primary)),
                                child: Text(
                                  'payment.title'.tr,
                                  style: TextStyle(
                                      color: AppColors.White,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600),
                                )),
                          )
                        ]),
                  )
                ]);
              },
            )));
  }
}
