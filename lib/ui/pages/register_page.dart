import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:webview_flutter/webview_flutter.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({
    Key? key,
  }) : super(key: key);

  @override
  State<RegisterPage> createState() => _KiotSettingPageState();
}

class _KiotSettingPageState extends State<RegisterPage> {
  late WebViewController _webViewController;
  final Set<Factory<OneSequenceGestureRecognizer>> gestureRecognizers = {
    Factory(() => EagerGestureRecognizer())
  };
  UniqueKey _key = UniqueKey();
  int _progress = 0;
  String _page = "";

  late String title;
  late String targetUrl = "https://kiotsoft.com/dangky/ad/dangky.php";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          leadingColor: Colors.black,
          behavior: GunBehavior.STACKED,
        ),
        child: Stack(
          children: [
            WebView(
              key: _key,
              gestureRecognizers: gestureRecognizers,
              initialUrl: AppStrings.kiot_register_url,
              onPageStarted: (page) {},
              onPageFinished: (page) {
                print("onPageFinished");
                print("page ${page}");
                if (page == AppStrings.kiot_login_url) {
                  Get.back();
                }
                if (_page == AppStrings.kiot_register_url ||
                    _page == AppStrings.kiot_login_url) {
                  Get.back();
                }
                _page = page;

                // if (page == AppStrings.kiot_login_url) {
                //   setState(() {
                //     _webViewController.runJavascriptReturningResult('''
                //       var email = document.getElementById("inputEmail3");
                //       var password = document.getElementById("inputPassword3");
                //        var button = document.getElementsByClassName("btn-login")[0];
                //       email.value = "nguyentiendat130397mobi@gmail.com";
                //       password.value = "admin123";
                //       button.click();
                //     ''');
                //   });
                // }
                // if (page == AppStrings.kiot_general_url) {
                //   Future.delayed(Duration(seconds: 0), () {
                //     _webViewController.loadUrl(targetUrl);
                //   });
                // }
                // if (page == targetUrl) {
                //   setState(() {
                //     _page = page;
                //   });
                // }
              },
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _webViewController = webViewController;
              },
              onProgress: (progress) {
                setState(() {
                  _progress = progress;
                });
              },
            ),
            // if (_progress != 100 || _page != targetUrl)
            //   Positioned.fill(
            //       child: Container(
            //     color: Colors.grey.shade300,
            //     child: Center(child: CircularProgressIndicator()),
            //   ))
          ],
        ));
  }
}
