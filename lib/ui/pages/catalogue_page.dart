import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/controllers/order_create_product_controller.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/models/routes.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/widgets/widget_dialog.dart';

class CataloguePage extends StatefulWidget {
  const CataloguePage({Key? key}) : super(key: key);

  @override
  State<CataloguePage> createState() => _CataloguePageState();
}

class _CataloguePageState extends State<CataloguePage> {
  TextEditingController catalogueController = TextEditingController();
  OrderCreateProductController _controller =
      Get.find<OrderCreateProductController>();
  TextEditingController textController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller.loadCategoryListNotAll();
  }

  bool isSearch = false;
  @override
  Widget build(BuildContext context) {
    return GetX<OrderCreateProductController>(
      builder: (controller) {
        return GunPage(
          appBarBottom: GunAppBar(
            type: GunAppBarType.BOTTOM,
            height: 0,
          ),
          appBarTop: GunAppBar(
            type: GunAppBarType.TOP,
            titles: Row(
              children: [
                isSearch
                    ? Expanded(
                        child: TextField(
                        controller: textController,
                        onChanged: (e) {
                          setState(() {});
                        },
                        onSubmitted: (e) {
                          _controller.loadCategoryList(tuKhoa: e);
                        },
                        autofocus: true,
                        decoration: InputDecoration(
                            isDense: true, border: InputBorder.none),
                      ))
                    : GunText(
                        'catalogue_goods'.tr,
                        style: AppStyles.pageTitleStyle,
                      ),
              ],
            ),
            leadingColor: Colors.black,
            actions: [
              GunAction.icon(
                onTap: () {
                  setState(() {
                    isSearch = !isSearch;
                    textController.clear();
                  });
                  if (isSearch == false) {
                    _controller.loadCategoryList(tuKhoa: '');
                  }
                },
                iconFactor: isSearch ? 0.4 : 0.7,
                icon: Image.asset(
                  isSearch
                      ? './assets/images/close_icon.png'
                      : './assets/images/search_icon.png',
                  color: AppColors.Orange,
                ),
              ),
              if (isSearch == false)
                GunAction.icon(
                  iconFactor: 0.5,
                  onTap: () {
                    Get.dialog(DialogCustom(
                        backDropTap: () {
                          null;
                        },
                        title: 'catalogue_create'.tr,
                        isTwoButton: true,
                        nameButtonLeft: 'Huỷ',
                        nameButtonRight: 'save'.tr,
                        onTapLeft: () {
                          Get.back();
                        },
                        onTapRight: () async {
                          Get.back();
                          var result = await _controller.catalogueCreate(
                              tenDanhMuc: catalogueController.text,
                              parentID: 1);
                          if (result.type == "success") {
                            Get.dialog(DialogCustom(
                                title: 'Thông báo',
                                onTapOK: () {
                                  Get.back();

                                  _controller.loadCategoryListNotAll();
                                },
                                children: [
                                  Text(result.message!,
                                      style: AppStyles.DEFAULT_16)
                                ]));
                          }
                        },
                        children: [
                          TextField(
                            controller: catalogueController,
                            onChanged: (e) {
                              setState(() {});
                            },
                            autofocus: true,
                          )
                        ]));
                  },
                  icon: Image.asset(
                    './assets/images/add_icon.png',
                  ),
                )
            ],
          ),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  ...List.generate(
                      controller.categoryList.length,
                      (index) => Card(
                          elevation: 2,
                          child: InkWell(
                              onTap: () {
                                Get.toNamed(AppRoutes.catalogue_detail,
                                    arguments: controller.categoryList[index]);
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 15, horizontal: 15),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        GunText(
                                          '${controller.categoryList[index].danhMuc!.prdGroupName!}'
                                              .tr,
                                          style: AppStyles.DEFAULT_16
                                              .copyWith(color: Colors.black),
                                        ),
                                      ],
                                    ),
                                    GunText(
                                      '${controller.categoryList[index].soLuongSanPham ?? '0'}  ' +
                                          'goods'.tr,
                                      style: AppStyles.DEFAULT_14
                                          .copyWith(color: AppColors.Blue),
                                    )
                                  ],
                                ),
                              ))))
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
