import 'dart:io';

import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

import 'package:flutter/material.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:gun_base/ui/widgets/gun_inkwell.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/res/values.dart';
import 'package:kiot_pos/ui/ui.dart';
import 'package:kiot_pos/ui/widgets/avatar_create.dart';

class EditUserPage extends StatefulWidget {
  const EditUserPage({Key? key}) : super(key: key);

  @override
  State<EditUserPage> createState() => _EditUserPageState();
}

class _EditUserPageState extends State<EditUserPage> {
  UserController _controller = Get.find<UserController>();
  TextEditingController userController = TextEditingController();
  FocusNode userFocusNode = FocusNode();
  TextEditingController emailController = TextEditingController();
  FocusNode emailFocusNode = FocusNode();
  File? image;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      _controller.getUserDetail();
    });
  }

  @override
  Widget build(BuildContext context) {
    final sizeHeight = MediaQuery.of(context).size.height;
    final sizeWidth = MediaQuery.of(context).size.width;
    return GetX<UserController>(
      builder: (controller) {
        if (controller.detailUser != null &&
            controller.detailUser!.userInfo != null) {
          userController.text =
              controller.detailUser!.userInfo!.displayName ?? '';
          emailController.text = controller.detailUser!.userInfo!.email ?? '';
        }
        return GunPage(
          appBarBottom: GunAppBar(
            type: GunAppBarType.BOTTOM,
            height: 0,
          ),
          appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              title: 'edit_user'.tr,
              titleStyle: AppStyles.pageTitleStyle,
              leadingColor: Colors.black),
          child: Container(
            color: AppColors.White,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  AvatarCreate(onChange: (e) {}),
                  _renderInput(),
                  _renderButton()
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget ChooseImage(IconData icon, String name, GestureTapCallback? onTap) {
    final sizeWidth = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: onTap,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              child: Icon(
                icon,
                size: sizeWidth * 0.15,
              ),
            ),
            Text(
              name,
              style: AppStyles.DEFAULT_16.copyWith(fontWeight: FontWeight.w400),
            )
          ],
        ),
      ),
    );
  }

  _renderInput() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Column(children: [
        Container(
          margin: EdgeInsets.only(top: 23),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (userController.text.isNotEmpty)
                Container(
                    margin: EdgeInsets.only(left: 44),
                    child: Text(
                      'Tên đăng nhập',
                      style:
                          AppStyles.DEFAULT_10.copyWith(color: AppColors.Gray1),
                    )),
              TextFormField(
                  controller: userController,
                  focusNode: userFocusNode,
                  validator: FormBuilderValidators.required(
                      errorText: "error_miss".tr),
                  style: AppStyles.normalTextStyle,
                  // textAlign: TextAlign.center,
                  textAlignVertical: TextAlignVertical.center,
                  decoration: InputDecoration(
                    isDense: true,
                    // alignLabelWithHint: true,
                    hintText: 'login.user_name'.tr,
                    contentPadding: EdgeInsets.only(bottom: 10),
                    prefixIconConstraints:
                        BoxConstraints(minWidth: 0, minHeight: 0),
                    prefixIcon: Container(
                        margin: EdgeInsets.only(right: 5, bottom: 10),
                        width: 20,
                        child:
                            Image.asset('assets/images/login_user_name.png')),
                  )),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (emailController.text.isNotEmpty)
                Container(
                    margin: EdgeInsets.only(left: 44),
                    child: Text(
                      'Gmail',
                      style:
                          AppStyles.DEFAULT_10.copyWith(color: AppColors.Gray1),
                    )),
              TextFormField(
                  controller: emailController,
                  validator: FormBuilderValidators.required(
                      errorText: "error_miss".tr),
                  focusNode: emailFocusNode,
                  style: AppStyles.normalTextStyle,
                  // textAlign: TextAlign.center,
                  textAlignVertical: TextAlignVertical.center,
                  decoration: InputDecoration(
                    isDense: true,
                    // alignLabelWithHint: true,
                    hintText: 'Email'.tr,
                    contentPadding: EdgeInsets.only(bottom: 10),
                    prefixIconConstraints:
                        BoxConstraints(minWidth: 0, minHeight: 0),
                    prefixIcon: Container(
                        margin: EdgeInsets.only(right: 5, bottom: 10),
                        width: 20,
                        child: Image.asset('assets/images/email_icon.png')),
                  )),
            ],
          ),
        ),
      ]),
    );
  }

  _renderButton() {
    final sizeHeight = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      margin: EdgeInsets.only(top: sizeHeight * 0.12),
      child: WidgetButton(
        onTap: () {
          _controller.userEdit(
              displayName: userController.text,
              email: emailController.text,
              id: 1,
              onTap: () {
                Get.back();
                _controller.getUserDetail();
              });
        },
        text: 'Lưu',
        isFullWidth: true,
        backgroundColor: AppColors.Blue,
      ),
    );
  }
}
