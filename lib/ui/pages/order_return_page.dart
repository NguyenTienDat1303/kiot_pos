import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/styles.dart';

class OrderReturnPage extends StatefulWidget {
  const OrderReturnPage({
    Key? key,
  }) : super(key: key);

  @override
  State<OrderReturnPage> createState() => _OrderReturnPageState();
}

class _OrderReturnPageState extends State<OrderReturnPage> {
  OrderCreateController _controller = Get.find<OrderCreateController>();
  TextEditingController textController = TextEditingController();
  bool paymentDetailVisiable = true;
  List<bool> listChecked = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (_controller.orderDetail.sanPhams != null) {
      _controller.orderDetail.sanPhams!.forEach((e) {
        listChecked.add(true);
        _controller.orderReturn.add(ReturnAddDto(
            id: e.sanPham!.id, quantity: e.quantityInitial.toInt()));
      });
      print(_controller.orderReturn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetX<OrderCreateController>(
      builder: (controller) {
        var data = controller.orderDetail;
        var sls = controller.orderReturn;
        textController.text = data.donHang!.notes ?? '';
        return GunPage(
            resizeToAvoidBottomInset: false,
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              title: 'order_return.title'.tr,
              titleStyle: AppStyles.pageTitleStyle,
              leadingColor: Colors.black,
            ),
            child: Column(
              children: [
                Expanded(
                    child: SingleChildScrollView(
                        padding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        child: Column(
                          children: [
                            ...List.generate(
                              data.sanPhams!.length,
                              (index) => OrderReturnProductWidget(
                                  isChecked: listChecked[index],
                                  onChanged: (e) {
                                    setState(() {
                                      listChecked[index] = !listChecked[index];
                                      if (listChecked[index]) {
                                        sls[index].quantity = data
                                            .sanPhams![index].quantityInitial
                                            .toInt();
                                      } else {
                                        sls[index].quantity = 0;
                                      }
                                    });
                                  },
                                  onAdd: () {
                                    if (sls[index].quantity! <
                                        data.sanPhams![index].quantityInitial) {
                                      setState(() {
                                        sls[index].quantity =
                                            sls[index].quantity! + 1;
                                      });
                                    }
                                  },
                                  onDelete: () {
                                    if (sls[index].quantity! > 1) {
                                      setState(() {
                                        sls[index].quantity =
                                            sls[index].quantity! - 1;
                                      });
                                    }
                                  },
                                  sls: sls[index].quantity ?? 0,
                                  name: data.sanPhams![index].sanPham!.prdName!,
                                  price: data.sanPhams![index].price!),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            TextFormField(
                              textAlign: TextAlign.start,
                              controller: textController,
                              maxLines: 2,
                              style: AppStyles.normalTextStyle,
                              decoration: InputDecoration(
                                  hintText: 'order_return.note'.tr,
                                  isDense: true,
                                  contentPadding: EdgeInsets.only(bottom: 2)),
                            ),
                          ],
                        ))),
                Material(
                  color: Color(0XFFFFEBD4),
                  child: InkWell(
                    child: Column(children: [
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        child: Row(children: [
                          Expanded(
                            child: RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                    text: "order_detail.customer".tr,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600)),
                              ]),
                            ),
                          ),
                          Expanded(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                  controller.orderDetail.tenkhach != null
                                      ? '${controller.orderDetail.tenkhach!.customerName ?? ''}-${controller.orderDetail.tenkhach!.id ?? ''}'
                                      : '',
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600)),
                            ],
                          ))
                        ]),
                      ),
                      // if (paymentDetailVisiable)
                      Column(
                        children: [
                          Column(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 2),
                                child: Row(children: [
                                  Expanded(
                                    child: Text("order_detail.money_product".tr,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400)),
                                  ),
                                  Expanded(
                                      child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                          AppValue.APP_MONEY_FORMAT
                                              .format(data.tongThanhToan),
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              color: AppColors.primary,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600)),
                                    ],
                                  ))
                                ]),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 2),
                                child: Row(children: [
                                  Expanded(
                                    child: Text("order_detail.discount".tr,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400)),
                                  ),
                                  Expanded(
                                      child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                          AppValue.APP_MONEY_FORMAT
                                              .format(data.tongChietKhau),
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              color: AppColors.primary,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400)),
                                    ],
                                  ))
                                ]),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 2),
                                child: Row(children: [
                                  Expanded(
                                    child: Text("order_detail.coupon".tr,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400)),
                                  ),
                                  Expanded(
                                      child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                          AppValue.APP_MONEY_FORMAT
                                              .format(data.donHang!.coupon),
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              color: AppColors.primary,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400)),
                                    ],
                                  ))
                                ]),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 2),
                                child: Row(children: [
                                  Expanded(
                                    child: Text("order_create.customer_pay".tr,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600)),
                                  ),
                                  Expanded(
                                      child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                          AppValue.APP_MONEY_FORMAT.format(
                                              (data.tongThanhToan ??
                                                  0 - data.tongChietKhau!)),
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              color: AppColors.primary,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600)),
                                    ],
                                  ))
                                ]),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 2),
                                child: Row(children: [
                                  Expanded(
                                    child: Text("order_detail.debt".tr,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                        )),
                                  ),
                                  Expanded(
                                      child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                          AppValue.APP_MONEY_FORMAT.format((0)),
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              color: AppColors.primary,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600)),
                                    ],
                                  ))
                                ]),
                              ),
                            ],
                          ),
                        ],
                      )
                    ]),
                  ),
                ),
                GunInkwell(
                  padding: EdgeInsets.zero,
                  decoration: BoxDecoration(
                    color: Color(0XFFE27C04),
                  ),
                  borderRadius: BorderRadius.zero,
                  onTap: () async {
                    print(_controller.orderReturn);
                    var retult = await controller.getReturn(
                        id: data.donHang!.id!,
                        sanPhams: _controller.orderReturn,
                        notes: textController.text);
                    if (retult.type == "success") {
                      Get.dialog(DialogCustom(
                          title: 'Thông báo',
                          onTapOK: () {
                            Get.back();
                            Get.back();
                            Get.back();
                          },
                          children: [
                            Text(retult.message!, style: AppStyles.DEFAULT_16)
                          ]));
                    }
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 56,
                    child: Text(
                      "order_return.confirm_return".tr,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w600),
                    ),
                  ),
                )
              ],
            ));
      },
    );
  }
}

class OrderReturnProductWidget extends StatelessWidget {
  String name;
  int price;
  GestureTapCallback? onDelete, onAdd;
  int sls;
  bool isChecked;
  ValueChanged<bool> onChanged;

  OrderReturnProductWidget(
      {Key? key,
      required this.name,
      required this.price,
      this.onDelete,
      this.onAdd,
      required this.sls,
      required this.isChecked,
      required this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: 10),
            width: 24,
            height: 24,
            child: Checkbox(
              value: isChecked,
              onChanged: (e) {
                onChanged(e!);
              },
              activeColor: AppColors.primary,
            ),
          ),
          Expanded(
              child: Container(
            margin: EdgeInsets.only(right: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: AppStyles.normalTextStyle
                      .copyWith(fontWeight: FontWeight.w600),
                ),
                Text(
                  "${AppValue.APP_MONEY_FORMAT.format(price)}",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                )
              ],
            ),
          )),
          GunInkwell(
            onTap: onDelete,
            padding: EdgeInsets.zero,
            child: Container(
                width: 30,
                height: 30,
                child: Image.asset('assets/images/btn_delete.png')),
          ),
          Container(
            margin: EdgeInsets.only(left: 15, right: 15),
            child: Text(
              sls.toString(),
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.w400),
            ),
          ),
          GunInkwell(
            onTap: onAdd,
            padding: EdgeInsets.zero,
            child: Container(
                width: 30,
                height: 30,
                child: Image.asset('assets/images/btn_add.png')),
          )
        ],
      ),
    );
  }
}
