import 'package:flutter/material.dart';
import 'package:gun_base/res/values.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class BillDetailPage extends StatefulWidget {
  const BillDetailPage({Key? key}) : super(key: key);

  @override
  State<BillDetailPage> createState() => _BillDetailPageState();
}

class _BillDetailPageState extends State<BillDetailPage> {
  BillDetailController billDetailController = Get.find<BillDetailController>();
  OverviewCollectPayController overviewCollectPayController =
      Get.find<OverviewCollectPayController>();
  int typeNavigation = -1;
  String title = '';
  int id = -1;
  List<UsersResponse> hinhThucThu = [
    UsersResponse(id: 3, displayName: 'Thu bán hàng'),
    UsersResponse(id: 9, displayName: '	Thu trả hàng'),
    UsersResponse(id: 11, displayName: 'Thu ngoài'),
    UsersResponse(id: 4, displayName: 'Thu khách lẻ'),
    UsersResponse(id: 5, displayName: 'Thu HĐGTGT'),
    UsersResponse(id: 6, displayName: 'Thu khác'),
    UsersResponse(id: 7, displayName: 'Thu văn phòng'),
    UsersResponse(id: 8, displayName: 'Thu công nợ'),
  ];

  List<UsersResponse> hinhThucChi = [
    UsersResponse(id: 2, displayName: 'Chi mua hàng'),
    UsersResponse(id: 13, displayName: 'Chi trả hàng'),
    UsersResponse(id: 3, displayName: 'Chi công nợ'),
    UsersResponse(id: 16, displayName: 'Chi ngoài'),
    UsersResponse(id: 4, displayName: 'Chi phí'),
    UsersResponse(id: 5, displayName: 'Tiền xăng'),
    UsersResponse(id: 6, displayName: 'Thuê xe và gửi hàng'),
    UsersResponse(id: 7, displayName: 'Tiền ứng'),
    UsersResponse(id: 9, displayName: 'Chi văn phòng'),
    UsersResponse(id: 10, displayName: 'Chi tài sản cố định'),
    UsersResponse(id: 8, displayName: 'Chi khác'),
  ];
  // 0 thu 1 chi
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (Get.arguments != null) {
      var argument = Get.arguments;
      setState(() {
        typeNavigation = argument[0];
        id = argument[1];
        if (typeNavigation == 0) {
          title = 'collect.detail'.tr;
          Future.delayed(Duration(seconds: 0), () {
            billDetailController.getDetailCollect(id);
          });
        } else {
          title = 'pay.detail'.tr;
          Future.delayed(Duration(seconds: 0), () {
            billDetailController.getDetailPay(id);
          });
        }
      });
    }
  }

  onDeleteCollect() {
    billDetailController.collectDelete(
        id: id,
        callback: () {
          Get.back();
          overviewCollectPayController.getListCollect(
              '', null, null, null, null, 10);
        });
  }

  onDeletePay() {
    billDetailController.payDelete(
        id: id,
        callback: () {
          Get.back();
          overviewCollectPayController.getListPay(
              '', null, null, null, null, 10);
        });
  }

  String form = '';
  @override
  Widget build(BuildContext context) {
    return GetX<BillDetailController>(
      builder: (controller) {
        if (typeNavigation == 0) {
          if (controller.detailCollect != null) {
            hinhThucThu.forEach((e) {
              if (controller.detailCollect!.phieuThu!.typeId == e.id) {
                form = e.displayName!;
              }
            });
          }
        } else {
          if (controller.detailPay != null) {
            hinhThucChi.forEach((e) {
              if (controller.detailPay!.phieuChi!.typeId == e.id) {
                form = e.displayName!;
              }
            });
          }
        }
        return GunPage(
            appBarBottom: GunAppBar(
              type: GunAppBarType.BOTTOM,
              height: 0,
            ),
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              title: title,
              titleStyle: AppStyles.pageTitleStyle,
              leadingColor: Colors.black,
              actions: [
                GunAction.icon(
                  onTap: () {
                    controller.idBill = id;
                    Get.toNamed(AppRoutes.bill_edit, arguments: typeNavigation);
                  },
                  icon:
                      Image.asset('./assets/images/edit_icon.png', width: 24.r),
                )
              ],
            ),
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      Expanded(
                          child: SingleChildScrollView(
                        child: typeNavigation == 0
                            ? _renderBodyCollect(controller.detailCollect)
                            : _renderBodyPay(controller.detailPay),
                      )),
                      Container(
                          padding: EdgeInsets.all(16), child: _renderButton()),
                    ],
                  ),
                ),
              ],
            ));
      },
    );
  }

  _renderBodyCollect(CollectResponse? data) {
    return data != null
        ? Column(
            children: [
              OneRow(
                  title: 'collect.code'.tr,
                  subtitle:
                      data.phieuThu != null ? data.phieuThu!.receiptCode! : ''),
              OneRow(
                  title: 'collect.pay_in'.tr,
                  subtitle: data.phieuXuat != null
                      ? data.phieuXuat!.output_code!
                      : ''),
              OneRow(title: 'customer'.tr, subtitle: data.khachHang!),
              OneRow(
                  title: 'collect.day'.tr,
                  subtitle: data.phieuThu!.receiptDate!),
              OneRow(title: 'collect.user'.tr, subtitle: data.tenNguoiThu!),
              OneRow(title: 'collect.form'.tr, subtitle: form),
              OneRow(title: 'note'.tr, subtitle: data.phieuThu!.notes!),
              OneRow(
                  title: 'total_money'.tr,
                  subtitle: GunValue.format_money(
                      double.parse(data.phieuThu!.totalMoney!.toString()))),
            ],
          )
        : SizedBox.shrink();
  }

  _renderBodyPay(PayResponse? data) {
    return data != null
        ? Column(
            children: [
              OneRow(
                  title: 'pay.code'.tr,
                  subtitle:
                      data.phieuChi != null ? data.phieuChi!.paymentCode! : ''),
              OneRow(
                  title: 'pay.pay_out'.tr,
                  subtitle: data.phieuNhap != null
                      ? data.phieuNhap!.input_code!
                      : ''),
              OneRow(title: 'nsx'.tr, subtitle: data.nhaCungCap!),
              OneRow(
                  title: 'pay.day'.tr, subtitle: data.phieuChi!.paymentDate!),
              OneRow(title: 'pay.user'.tr, subtitle: data.tenNguoiChi!),
              OneRow(title: 'pay.form'.tr, subtitle: ''),
              OneRow(title: 'note'.tr, subtitle: data.phieuChi!.notes!),
              OneRow(
                  title: 'total_money'.tr,
                  subtitle: GunValue.format_money(
                      double.parse(data.phieuChi!.totalMoney!.toString()))),
            ],
          )
        : SizedBox.shrink();
  }

  _renderButton() {
    return Container(
      child: Row(
        children: [
          ...List.generate(
              2,
              (index) => Expanded(
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: GestureDetector(
                              onTap: () {
                                if (index == 0) {
                                  Get.dialog(DialogCustom(
                                      title: 'Thông báo',
                                      isTwoButton: true,
                                      nameButtonLeft: 'Huỷ',
                                      nameButtonRight: 'Xoá',
                                      onTapLeft: () {
                                        Get.back();
                                      },
                                      onTapRight: () {
                                        Get.back();
                                        if (typeNavigation == 0) {
                                          onDeleteCollect();
                                        } else {
                                          onDeletePay();
                                        }
                                      },
                                      children: [
                                        Text('Bạn có chắc chắn muốnn xoá ?',
                                            style: AppStyles.DEFAULT_16),
                                      ]));
                                }
                                if (index == 1) {
                                  var printManagerController =
                                      Get.find<PrintManagerController>();
                                  if (typeNavigation == 0) {
                                    printManagerController.printForm(
                                        PrintAction.receipt_collect,
                                        collectResponse: billDetailController
                                            .detailCollect!);
                                  } else {
                                    printManagerController.printForm(
                                        PrintAction.receipt_pay,
                                        payResponse:
                                            billDetailController.detailPay!);
                                  }
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 11, horizontal: 20),
                                decoration: BoxDecoration(
                                    color: index == 0
                                        ? AppColors.Blue4
                                        : AppColors.Blue,
                                    borderRadius: BorderRadius.circular(100)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      index == 0 ? 'delete'.tr : 'print'.tr,
                                      style: AppStyles.DEFAULT_16_BOLD.copyWith(
                                          color: index == 0
                                              ? AppColors.Blue
                                              : AppColors.White),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ))
        ],
      ),
    );
  }
}
