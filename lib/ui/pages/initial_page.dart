import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/ui/widgets/qr_code_view.dart';

class InitialPage extends StatefulWidget {
  const InitialPage({Key? key}) : super(key: key);

  @override
  State<InitialPage> createState() => _InitialPageState();
}

class _InitialPageState extends State<InitialPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      // Get.until((route) => Get.currentRoute == AppRoutes.login);
      Get.offAllNamed(AppRoutes.splash);
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        child: Container(
      child: Image.asset('assets/images/bg_splash.png'),
    ));
  }
}
