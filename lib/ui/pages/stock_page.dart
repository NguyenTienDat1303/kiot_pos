import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/ui.dart';

class StockPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const StockPage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<StockPage> createState() => _StockPageState();
}

class _StockPageState extends State<StockPage> {
  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'home_drawer.stock'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leading: [
            GunAction.icon(
                onTap: () {
                  widget.homePageKey.currentState!.openDrawer();
                },
                icon: Image.asset('assets/images/drawer_menu.png'))
          ],
          
        ),
        child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: Column(
              children: [
                ...List.generate(
                    4,
                    (index) => Column(
                          children: [
                            SettingMenu(
                              imageAssetPath: index == 0
                                  ? 'assets/images/stock_icon1.png'
                                  : index == 1
                                      ? 'assets/images/stock_icon2.png'
                                      : index == 2
                                          ? 'assets/images/stock_icon3.png'
                                          : 'assets/images/stock_icon4.png',
                              title: index == 0
                                  ? 'catalogue_goods'.tr
                                  : index == 1
                                      ? 'list_goods'.tr
                                      : index == 2
                                          ? 'stock_goods'.tr
                                          : 'topping'.tr,
                              onTap: () {
                                if (index == 0) {
                                  Get.toNamed(AppRoutes.catalogue);
                                } else if (index == 1) {
                                  Get.toNamed(AppRoutes.goods_list);
                                } else if (index == 2) {
                                  Get.toNamed(AppRoutes.stocking);
                                } else {
                                  Get.toNamed(AppRoutes.topping);
                                }
                              },
                            ),
                          ],
                        ))
              ],
            )));
  }
}
