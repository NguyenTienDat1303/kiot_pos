import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:marquee/marquee.dart';

class StorePage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const StorePage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<StorePage> createState() => _StorePageState();
}

class _StorePageState extends State<StorePage> with TickerProviderStateMixin {
  StoreController _controller = Get.find<StoreController>();
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    _controller.loadStoreList();
    await _controller.loadAreaList();
    if (_controller.areaList.isNotEmpty) {
      _controller.areaSelected = _controller.areaList[0];
      await _controller.loadTableList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        backgroundColor: Color(0XFFF5F5F5),
        appBarTop: GunAppBar(
          backgroundColor: Colors.white,
          type: GunAppBarType.TOP,
          title: 'store.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leading: [
            GunAction.icon(
                onTap: () {
                  widget.homePageKey.currentState!.openDrawer();
                },
                icon: Image.asset('assets/images/drawer_menu.png'))
          ],
          actions: [
            GunAction.custom(
                onTap: () {
                  Get.bottomSheet(Container(
                    padding: EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(15),
                          topRight: const Radius.circular(15),
                        )),
                    child: SingleChildScrollView(child: GetX<StoreController>(
                      builder: (controller) {
                        return Column(
                            children: List.generate(
                                controller.storeList.length,
                                (index) => Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        onTap: () {
                                          Get.back();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      width: 1,
                                                      color: Colors
                                                          .grey.shade300))),
                                          width: double.infinity,
                                          padding: EdgeInsets.only(
                                              left: 10,
                                              right: 10,
                                              top: 10,
                                              bottom: 20),
                                          child: Text(controller
                                              .storeList[index].storeName!),
                                        ),
                                      ),
                                    )));
                      },
                    )),
                  ));
                },
                child: Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Row(
                    children: [
                      GetX<StoreController>(
                        builder: (controller) {
                          return Text(
                            '${controller.storeSelected != null ? controller.storeSelected!.storeName : ""}',
                            style: AppStyles.normalTextStyle.copyWith(
                                color: AppColors.primary,
                                fontWeight: FontWeight.w600),
                          );
                        },
                      ),
                      Icon(Icons.arrow_drop_down)
                    ],
                  ),
                ))
          ],
        ),
        child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 18, horizontal: 18),
            child: Column(
              children: [
                Card(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                    child: Column(children: [
                      Row(
                        children: [
                          Expanded(
                              child: Row(
                            children: [
                              Container(
                                  margin: EdgeInsets.only(right: 10),
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                      'assets/images/store_provisional.png')),
                              Expanded(
                                child: Text(
                                  'store.provisional'.tr,
                                  style: AppStyles.normalTextStyle
                                      .copyWith(fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          )),
                          Expanded(child: GetX<StoreController>(
                            builder: (controller) {
                              return Text(
                                '${AppValue.APP_MONEY_FORMAT.format(controller.tamTinh)}',
                                textAlign: TextAlign.end,
                                style: AppStyles.normalTextStyle.copyWith(
                                    color: AppColors.primary,
                                    fontWeight: FontWeight.w600),
                              );
                            },
                          ))
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Row(
                            children: [
                              Container(
                                  margin: EdgeInsets.only(right: 10),
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                      'assets/images/store_table.png')),
                              Expanded(
                                child: Text(
                                  'store.table_show'.tr,
                                  style: AppStyles.normalTextStyle
                                      .copyWith(fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          )),
                          Expanded(
                              child: GunInkwell(
                            padding: EdgeInsets.zero,
                            onTap: () {
                              Get.bottomSheet(Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 30, horizontal: 10),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                        topLeft: const Radius.circular(15),
                                        topRight: const Radius.circular(15),
                                      )),
                                  child: SingleChildScrollView(
                                    child: Column(
                                        children: List.generate(
                                            TableStatus.supported.length,
                                            (index) => Material(
                                                  color: Colors.transparent,
                                                  child: InkWell(
                                                    onTap: () {
                                                      _controller.tableStatus =
                                                          TableStatus
                                                              .supported[index];
                                                      _controller
                                                          .loadTableList();
                                                      Get.back();
                                                    },
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                          border: Border(
                                                              bottom: BorderSide(
                                                                  width: 1,
                                                                  color: Colors
                                                                      .grey
                                                                      .shade300))),
                                                      width: double.infinity,
                                                      padding: EdgeInsets.only(
                                                          left: 10,
                                                          right: 10,
                                                          top: 10,
                                                          bottom: 20),
                                                      child: Text(
                                                          'table_status.${TableStatus.supported[index]}'
                                                              .tr),
                                                    ),
                                                  ),
                                                ))),
                                  )));
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                GetX<StoreController>(
                                  builder: (controller) {
                                    return Text(
                                      'table_status.${controller.tableStatus}'
                                          .tr,
                                      style: AppStyles.normalTextStyle.copyWith(
                                          color: AppColors.primary,
                                          fontWeight: FontWeight.w600),
                                    );
                                  },
                                ),
                                Icon(Icons.arrow_drop_down)
                              ],
                            ),
                          ))
                        ],
                      )
                    ]),
                  ),
                ),
                GetX<StoreController>(
                  builder: (controller) {
                    _tabController = TabController(
                      initialIndex: 0,
                      length: controller.areaList.length,
                      vsync: this,
                    );
                    _tabController.addListener(
                      () => {
                        _controller.areaSelected =
                            controller.areaList[_tabController.index]
                      },
                    );
                    return Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 12),
                      height: 40,
                      padding: EdgeInsets.symmetric(vertical: 3, horizontal: 8),
                      decoration: BoxDecoration(
                          color: Color(0XFFE7E7E7),
                          borderRadius: BorderRadius.circular(25.0)),
                      child: TabBar(
                          controller: _tabController,
                          isScrollable: true,
                          indicator: BoxDecoration(
                              color: AppColors.primary,
                              borderRadius: BorderRadius.circular(20.0)),
                          labelColor: Colors.white,
                          unselectedLabelColor: Colors.black,
                          tabs: List.generate(
                              controller.areaList.length,
                              (index) => Tab(
                                    text: controller.areaList[index].areaName,
                                  ))),
                    );
                  },
                ),
                GetX<StoreController>(
                  builder: (controller) {
                    return GridView.builder(
                      itemCount: controller.tableList.length,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 4 / 4,
                          crossAxisCount: 3,
                          crossAxisSpacing: 5,
                          mainAxisSpacing: 5),
                      itemBuilder: (context, index) => StoreTable(
                        data: controller.tableList[index],
                        onTap: () async {
                          await Get.toNamed(AppRoutes.order_create, arguments: {
                            'table': controller.tableList[index],
                            'orderId': controller.tableList[index].donHangID
                          });
                          _controller.loadTableList();
                        },
                      ),
                    );
                  },
                )
              ],
            )));
  }
}

class StoreTable extends StatelessWidget {
  final TableData data;
  final VoidCallback? onTap;
  const StoreTable({Key? key, required this.data, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var bgColor = data.ban!.tableStatus == TableStatus.empty
        ? Color(0XFFEBF4FF)
        : AppColors.primary;
    var textColor = data.ban!.tableStatus == TableStatus.empty
        ? Colors.black
        : Colors.white;

    return GestureDetector(
      onTap: onTap,
      child: Card(
        color: bgColor,
        child: Container(
          child: Column(children: [
            Container(
              padding: EdgeInsets.all(6),
              child: Text(
                data.ban!.tableName!,
                style: TextStyle(
                  color: textColor,
                  fontSize: 16,
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 0.3,
              color: textColor,
            ),
            Container(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    if (data.ban!.tableStatus == TableStatus.empty)
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Text(
                          'store.ready'.tr,
                          style: TextStyle(
                            color: textColor,
                            fontSize: 12,
                          ),
                        ),
                      ),
                    if (data.ban!.tableStatus == TableStatus.used)
                      Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                  margin: EdgeInsets.only(right: 10),
                                  width: 14,
                                  height: 14,
                                  child: Image.asset(
                                    'assets/images/store_time.png',
                                    color: textColor,
                                  )),
                              Expanded(
                                child: Container(
                                  height: 20,
                                  child: Marquee(
                                    text: '${data.status}',
                                    style: TextStyle(
                                      color: textColor,
                                      fontSize: 10,
                                    ),
                                    scrollAxis: Axis.horizontal,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,

                                    blankSpace: 100.0,
                                    velocity: 50.0,
                                    pauseAfterRound: Duration(seconds: 1),
                                    // accelerationDuration: Duration(seconds: 1),
                                    // accelerationCurve: Curves.linear,
                                    // decelerationDuration:
                                    //     Duration(milliseconds: 500),
                                    // decelerationCurve: Curves.easeOut,
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                  margin: EdgeInsets.only(right: 10),
                                  width: 14,
                                  height: 14,
                                  child: Image.asset(
                                    'assets/images/store_cash.png',
                                    color: textColor,
                                  )),
                              Expanded(
                                child: Container(
                                  height: 20,
                                  child: Marquee(
                                    text:
                                        '${AppValue.APP_MONEY_FORMAT.format(double.parse(data.giaTien!))}',
                                    style: TextStyle(
                                      color: textColor,
                                      fontSize: 10,
                                    ),
                                    scrollAxis: Axis.horizontal,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,

                                    blankSpace: 50.0,
                                    velocity: 50.0,
                                    pauseAfterRound: Duration(seconds: 1),
                                    // accelerationDuration: Duration(seconds: 1),
                                    // accelerationCurve: Curves.linear,
                                    // decelerationDuration:
                                    //     Duration(milliseconds: 500),
                                    // decelerationCurve: Curves.easeOut,
                                  ),
                                ),
                              )
                              // Expanded(
                              //     child: Text(
                              //   // '${AppValue.APP_MONEY_FORMAT.format(double.parse(data.giaTien!))}',
                              //   '${AppValue.APP_MONEY_FORMAT.format(10000000000000000000.0)}',
                              //   style: TextStyle(
                              //     color: textColor,
                              //     fontSize: 10,
                              //   ),
                              // ))
                            ],
                          )
                        ],
                      ),
                  ],
                ))
          ]),
        ),
      ),
    );
  }
}
