import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';

class PrintPreviewPage extends StatefulWidget {
  const PrintPreviewPage({
    Key? key,
  }) : super(key: key);

  @override
  State<PrintPreviewPage> createState() => _PrintPreviewPageState();
}

class _PrintPreviewPageState extends State<PrintPreviewPage> {
  PrintManagerController _controller = Get.find<PrintManagerController>();

  List<Uint8List> imageList = [];

  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    _controller.loadConfigs();
    print("ready 1");
    print("PrintType.lan ${_controller.printType}");
    init();
    // _controller.printingPaperList.forEach((e) {

    // });
  }

  var error = false;

  init() async {
    await Future.delayed(
        Duration(
            seconds: _controller.printingPaperList.length * 2 > 3
                ? _controller.printingPaperList.length * 2
                : 3), () async {
      _controller.printingPaperList.forEach((e) {
        e.screenshotController.capture().then((image) async {
          if (image != null) {
            imageList.add(image);
            if (_controller.printType == PrintType.lan) {
              try {
                var printerConfig = (await _controller
                    .loadPrinterConfigByType(e.printFormat))[0];
                PrinterUtils.printLan(printerConfig.ipAddress!, 9100, [image]);
              } catch (e) {
                error = true;
                Get.back();
                Get.dialog(DialogCustom(
                    title: "dialog.title_error".tr,
                    onTapOK: () {
                      Get.back();
                    },
                    children: [
                      Text(
                          "Có lỗi xảy ra. Vui lòng kiểm tra lại cấu hình máy in!"),
                    ]));
              }
            }
            if (imageList.length == _controller.printingPaperList.length) {
              print("lenth ==${imageList.length}");
              // PrinterUtils.printLan("192.168.1.87", "9100", imageList);
              if (_controller.printType == PrintType.bluetooth) {
                var status = await Permission.location.request().isGranted;
                _controller.startScanBluetooth();
                if (status) {
                  Get.bottomSheet(
                      enableDrag: false,
                      isDismissible: false,
                      Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 30, horizontal: 10),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(15),
                                topRight: const Radius.circular(15),
                              )),
                          child: SingleChildScrollView(
                              child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      'print_preview.list_printer_bluetooth'.tr,
                                      style: AppStyles.pageTitleStyle,
                                    ),
                                  ),
                                  GunAction(
                                    onTap: () {},
                                    child: Container(
                                      width: 30,
                                      child: Image.asset(
                                          'assets/images/sync_icon.png'),
                                    ),
                                  )
                                ],
                              ),
                              GetX<PrintManagerController>(
                                builder: (controller) {
                                  return Column(
                                    children: List.generate(
                                        controller.bluetoothPrinters.length,
                                        (index) => GunInkwell(
                                              onTap: () async {
                                                Get.back();
                                                await PrinterUtils
                                                    .printBluetoothImage(
                                                        _controller
                                                            .bluetoothManager,
                                                        controller
                                                                .bluetoothPrinters[
                                                            index],
                                                        imageList);
                                                // Get.back();
                                              },
                                              padding: EdgeInsets.zero,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                    border: Border(
                                                        bottom: BorderSide(
                                                            width: 1,
                                                            color: Colors.grey
                                                                .shade300))),
                                                width: double.infinity,
                                                padding: EdgeInsets.only(
                                                    left: 10,
                                                    right: 10,
                                                    top: 10,
                                                    bottom: 20),
                                                child: Text(
                                                    '${controller.bluetoothPrinters[index].name}'
                                                        .tr),
                                              ),
                                            )),
                                  );
                                },
                              ),
                            ],
                          ))));
                }
              } else {
                print("lenth here");
                if (!error) Get.back();
                // Get.back();
              }
            }
          }
        });
      });

      // Get.until((route) => route.settings.name == AppRoutes.payment);
      // setState(() {
      //   isLoading = false;
      // });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        // onWillPop: Future<bool?>() => false,
        // appBarTop: GunAppBar(
        //   type: GunAppBarType.TOP,
        //   title: "Máy in đang sử lý vui lòng chờ!",
        //   leadingColor: Colors.black,
        //   leading: [],
        // ),
        child: Stack(
      children: [
        Container(
          width: double.infinity,
          // height: Get.height,
          child: SingleChildScrollView(
            child: GetX<PrintManagerController>(
              builder: (controller) {
                return Column(
                    children: List.generate(controller.printingPaperList.length,
                        (index) {
                  var printData = controller.printingPaperList[index];
                  print("printData.data ${printData.data}");

                  switch (printData.printFormat) {
                    case PrintFormat.invoice:
                      return Screenshot(
                        controller: printData.screenshotController,
                        child: HtmlWidget(
                          printData.data,
                          buildAsync: true,
                          customWidgetBuilder: (element) {
                            if (element.className.contains('table-order')) {
                              print("customWidgetBuilder  ${element}");
                              return Column(children: [
                                PrintOrderTableHeader(),
                                ...List.generate(
                                  printData.orderDetail!.sanPhams!.length,
                                  (index) {
                                    return PrintOrderTableRow(
                                      data: printData
                                          .orderDetail!.sanPhams![index],
                                    );
                                  },
                                ),
                              ]);
                            }
                          },
                          customStylesBuilder: (element) {
                            return {
                              'font-size': '16px !important;',
                            };
                          },
                        ),
                      );
                    case PrintFormat.kitchen:
                      return Screenshot(
                        controller: printData.screenshotController,
                        child: HtmlWidget(
                          printData.data,
                          buildAsync: true,
                          onLoadingBuilder:
                              (context, element, loadingProgress) {
                            print("loadingProgress $loadingProgress");
                          },
                          customWidgetBuilder: (element) {
                            if (element.className.contains('table-order')) {
                              print("customWidgetBuilder  ${element}");
                              return Column(children: [
                                PrintOrderTableHeader(),
                                ...List.generate(
                                  printData.orderDetail!.sanPhams!.length,
                                  (index) {
                                    return PrintOrderTableRow(
                                      data: printData
                                          .orderDetail!.sanPhams![index],
                                    );
                                  },
                                ),
                              ]);
                            }
                          },
                          customStylesBuilder: (element) {
                            return {
                              'font-size': '16px !important;',
                            };
                          },
                        ),
                      );
                    case PrintFormat.receipt_collect:
                      return Screenshot(
                        controller: printData.screenshotController,
                        child: HtmlWidget(
                          printData.data,
                          buildAsync: true,
                          customStylesBuilder: (element) {
                            return {
                              'font-size': '16px !important;',
                            };
                          },
                        ),
                      );
                    case PrintFormat.receipt_pay:
                      return Screenshot(
                        controller: printData.screenshotController,
                        child: HtmlWidget(
                          printData.data,
                          buildAsync: true,
                          customStylesBuilder: (element) {
                            return {
                              'font-size': '16px !important;',
                            };
                          },
                        ),
                      );
                    case PrintFormat.stamp:
                      return Screenshot(
                        controller: printData.screenshotController,
                        child: HtmlWidget(
                          printData.data,
                          buildAsync: true,
                          onLoadingBuilder:
                              (context, element, loadingProgress) {
                            print("loadingProgress $loadingProgress");
                          },
                          customStylesBuilder: (element) {
                            return {
                              'font-size': '16px !important;',
                            };
                          },
                        ),
                      );
                    default:
                      return Container(
                        child: Center(
                          child: Text("Bir şeyler ters gitti!"),
                        ),
                      );
                  }
                }));
              },
            ),
          ),
        ),
        if (isLoading)
          Positioned.fill(
              child: Container(
                  width: double.infinity,
                  height: Get.height,
                  alignment: Alignment.center,
                  color: Colors.white,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          width: 120,
                          child:
                              Image.asset("assets/images/kiot_icon_app.png")),
                      Container(
                          margin: EdgeInsets.symmetric(vertical: 5),
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator()),
                      Text(
                        "Máy in đang xử lý vui lòng chờ!",
                      )
                    ],
                  )))
      ],
    ));
  }
}

class PrintOrderTableHeader extends StatelessWidget {
  const PrintOrderTableHeader({Key? key}) : super(key: key);

  static final TextStyle textStyle =
      TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black, width: 2))),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 55,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'print_template.price_unit'.tr,
                      style: textStyle,
                    ),
                  ],
                )),
            Expanded(
                flex: 10,
                child: Text(
                  'print_template.quantity'.tr,
                  textAlign: TextAlign.end,
                  style: textStyle,
                )),
            Expanded(
                flex: 40,
                child: Text(
                  'print_template.money'.tr,
                  textAlign: TextAlign.end,
                  style: textStyle,
                )),
          ]),
    );
  }
}

class PrintOrderTableRow extends StatelessWidget {
  final ODProductData data;
  const PrintOrderTableRow({Key? key, required this.data}) : super(key: key);

  static final TextStyle textStyle =
      TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w400);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black, width: 1))),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 55,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data.sanPham!.prdName!,
                      style: textStyle,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    if (data.topping!.isNotEmpty)
                      Text(
                        "${data.topping!.map((e) => "${e.topping!.prdName}x${e.quantityAdd}").join(", ")},",
                        style: TextStyle(fontSize: 14),
                      ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            AppValue.APP_MONEY_FORMAT
                                .format(data.sauChietKhau ?? 0),
                            style: textStyle,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            AppValue.APP_MONEY_FORMAT
                                .format(data.truocChietKhau ?? 0),
                            style: textStyle.copyWith(
                              decoration: TextDecoration.lineThrough,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
            Expanded(
                flex: 10,
                child: Text(
                  data.quantity!.toString(),
                  textAlign: TextAlign.end,
                  style: textStyle,
                )),
            Expanded(
                flex: 40,
                child: Text(
                  AppValue.APP_MONEY_FORMAT.format(data.sauChietKhau),
                  textAlign: TextAlign.end,
                  style: textStyle,
                )),
          ]),
    );
  }
}
