import 'package:flutter/cupertino.dart';
import 'package:gun_base/gun_base.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return GunPage(child: Text("Profile"));
  }
}
