import 'package:flutter/material.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:get/get.dart';

class PolicyPage extends StatefulWidget {
  const PolicyPage({Key? key}) : super(key: key);

  @override
  State<PolicyPage> createState() => _PolicyPageState();
}

class _PolicyPageState extends State<PolicyPage> {
  String policy =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tincidunt blandit metus, in viverra lorem porta nec. Nam pretium lorem at sollicitudin imperdiet. Vivamus ornare velit et arcu dictum finibus. Nulla sit amet leo nec justo ornare lacinia sit amet id diam. Maecenas eget lacinia dolor. Nam fringilla turpis in tortor tincidunt, nec molestie ligula hendrerit. Maecenas gravida id eros dapibus sollicitudin. Vivamus lacinia imperdiet massa, in vulputate diam sollicitudin sed. Ut nec magna a magna rutrum ornare at at tellus. Maecenas auctor lectus sit amet arcu volutpat maximus. Suspendisse potenti. Phasellus at odio sed nunc ultricies congue. Duis sed nulla et odio rutrum porttitor. Sed id urna eget lectus ullamcorper sagittis sed nec risus. Fusce pharetra, velit non molestie sagittis, neque turpis feugiat nunc, et gravida lacus metus a leo.';
  @override
  Widget build(BuildContext context) {
    final sizeWidth = MediaQuery.of(context).size.width;
    return GunPage(
      appBarBottom: GunAppBar(
        type: GunAppBarType.BOTTOM,
        height: 0,
      ),
      appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'policy'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                "assets/images/policy.png",
                fit: BoxFit.cover,
              ),
              Text(
                'Điều khoản sử dụng',
                style: AppStyles.DEFAULT_18_BOLD.copyWith(
                  color: AppColors.Gray1,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Điều khoản sử dụng này áp dụng cho Dịch vụ phần mềm quản lý cửa hàng KiotPro, phiên bản chính thức chạy trên máy chủ",
                style: AppStyles.DEFAULT_16.copyWith(
                  color: AppColors.Gray1,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
