import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class MoveTablePage extends StatefulWidget {
  const MoveTablePage({
    Key? key,
  }) : super(key: key);

  @override
  State<MoveTablePage> createState() => _MoveTablePageState();
}

class _MoveTablePageState extends State<MoveTablePage>
    with TickerProviderStateMixin {
  MoveTableController _controller = Get.find<MoveTableController>();
  late TabController _tabController;

  // late int tableAction;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    await _controller.loadAreaList();
    if (_controller.areaList.isNotEmpty) {
      _controller.areaSelected = _controller.areaList[0];
      await _controller.loadTableList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        backgroundColor: Color(0XFFF5F5F5),
        appBarTop: GunAppBar(
          backgroundColor: Colors.white,
          type: GunAppBarType.TOP,
          title: 'move_table.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
          actions: [
            GunAction.custom(
                onTap: () {
                  _controller.loadStoreList();
                  Get.bottomSheet(Container(
                    padding: EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(15),
                          topRight: const Radius.circular(15),
                        )),
                    child:
                        SingleChildScrollView(child: GetX<MoveTableController>(
                      builder: (controller) {
                        return Column(
                            children: List.generate(
                                controller.storeList.length,
                                (index) => Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        onTap: () {},
                                        child: Container(
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      width: 1,
                                                      color: Colors
                                                          .grey.shade300))),
                                          width: double.infinity,
                                          padding: EdgeInsets.only(
                                              left: 10,
                                              right: 10,
                                              top: 10,
                                              bottom: 20),
                                          child: Text(controller
                                              .storeList[index].storeName!),
                                        ),
                                      ),
                                    )));
                      },
                    )),
                  ));
                },
                child: Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Row(
                    children: [
                      GetX<StoreController>(
                        builder: (controller) {
                          return Text(
                            '${controller.storeSelected != null ? controller.storeSelected!.storeName : ""}',
                            style: AppStyles.normalTextStyle.copyWith(
                                color: AppColors.primary,
                                fontWeight: FontWeight.w600),
                          );
                        },
                      ),
                      Icon(Icons.arrow_drop_down)
                    ],
                  ),
                ))
          ],
        ),
        child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 18, horizontal: 18),
            child: Column(
              children: [
                Card(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                    child: Column(children: [
                      Row(
                        children: [
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              width: 24,
                              height: 24,
                              child: Image.asset(
                                  'assets/images/store_provisional.png')),
                          Text(
                            'store.provisional'.tr,
                            style: AppStyles.normalTextStyle
                                .copyWith(fontWeight: FontWeight.w500),
                          ),
                          Expanded(child: GetX<MoveTableController>(
                            builder: (controller) {
                              return Text(
                                '${AppValue.APP_MONEY_FORMAT.format(controller.tamTinh)}',
                                textAlign: TextAlign.end,
                                style: AppStyles.normalTextStyle.copyWith(
                                    color: AppColors.primary,
                                    fontWeight: FontWeight.w600),
                              );
                            },
                          ))
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              width: 24,
                              height: 24,
                              child:
                                  Image.asset('assets/images/store_table.png')),
                          Text(
                            'store.table_show'.tr,
                            style: AppStyles.normalTextStyle
                                .copyWith(fontWeight: FontWeight.w500),
                          ),
                          Expanded(
                              child: GunInkwell(
                            padding: EdgeInsets.zero,
                            onTap: () {
                              Get.bottomSheet(Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 30, horizontal: 10),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                        topLeft: const Radius.circular(15),
                                        topRight: const Radius.circular(15),
                                      )),
                                  child: SingleChildScrollView(
                                    child: Column(
                                        children: List.generate(
                                            TableStatus.supported.length,
                                            (index) => Material(
                                                  color: Colors.transparent,
                                                  child: InkWell(
                                                    onTap: () {
                                                      _controller.tableStatus =
                                                          TableStatus
                                                              .supported[index];
                                                      _controller
                                                          .loadTableList();
                                                      Get.back();
                                                    },
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                          border: Border(
                                                              bottom: BorderSide(
                                                                  width: 1,
                                                                  color: Colors
                                                                      .grey
                                                                      .shade300))),
                                                      width: double.infinity,
                                                      padding: EdgeInsets.only(
                                                          left: 10,
                                                          right: 10,
                                                          top: 10,
                                                          bottom: 20),
                                                      child: Text(
                                                          'table_status.${TableStatus.supported[index]}'
                                                              .tr),
                                                    ),
                                                  ),
                                                ))),
                                  )));
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                GetX<MoveTableController>(
                                  builder: (controller) {
                                    return Text(
                                      'table_status.${controller.tableStatus}'
                                          .tr,
                                      style: AppStyles.normalTextStyle.copyWith(
                                          color: AppColors.primary,
                                          fontWeight: FontWeight.w600),
                                    );
                                  },
                                ),
                                Icon(Icons.arrow_drop_down)
                              ],
                            ),
                          ))
                        ],
                      )
                    ]),
                  ),
                ),
                GetX<MoveTableController>(
                  builder: (controller) {
                    _tabController = TabController(
                      initialIndex: 0,
                      length: controller.areaList.length,
                      vsync: this,
                    );
                    _tabController.addListener(
                      () => {
                        _controller.areaSelected =
                            controller.areaList[_tabController.index]
                      },
                    );
                    return Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 12),
                      height: 40,
                      padding: EdgeInsets.symmetric(vertical: 3, horizontal: 8),
                      decoration: BoxDecoration(
                          color: Color(0XFFE7E7E7),
                          borderRadius: BorderRadius.circular(25.0)),
                      child: TabBar(
                          controller: _tabController,
                          isScrollable: true,
                          indicator: BoxDecoration(
                              color: AppColors.primary,
                              borderRadius: BorderRadius.circular(20.0)),
                          labelColor: Colors.white,
                          unselectedLabelColor: Colors.black,
                          tabs: List.generate(
                              controller.areaList.length,
                              (index) => Tab(
                                    text: controller.areaList[index].areaName,
                                  ))),
                    );
                  },
                ),
                GetX<MoveTableController>(
                  builder: (controller) {
                    return GridView.builder(
                      itemCount: controller.tableList.length,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 4 / 4,
                          crossAxisCount: 3,
                          crossAxisSpacing: 5,
                          mainAxisSpacing: 5),
                      itemBuilder: (context, index) => StoreTable(
                        data: controller.tableList[index],
                        onTap: () async {
                          Get.back(result: controller.tableList[index]);
                        },
                      ),
                    );
                  },
                )
              ],
            )));
  }
}
