import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';

class OrderStaffPage extends StatefulWidget {
  const OrderStaffPage({
    Key? key,
  }) : super(key: key);

  @override
  State<OrderStaffPage> createState() => _OrderStaffPageState();
}

class _OrderStaffPageState extends State<OrderStaffPage> {
  StaffController staffController = Get.find<StaffController>();
  OrderCreateController _orderCreateController =
      Get.find<OrderCreateController>();

  TextEditingController textController = TextEditingController();
  bool isSearch = false;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 0), () {
      staffController.searchStaff(tuKhoa: '');
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetX<StaffController>(
      builder: (controller) {
        return GunPage(
            appBarBottom: GunAppBar(
              type: GunAppBarType.BOTTOM,
              height: 0,
            ),
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              titles: Row(
                children: [
                  isSearch
                      ? Expanded(
                          child: TextField(
                          controller: textController,
                          onChanged: (e) {
                            setState(() {});
                          },
                          onSubmitted: (e) {
                            controller.searchStaff(tuKhoa: e);
                          },
                          autofocus: true,
                          decoration: InputDecoration(
                              isDense: true, border: InputBorder.none),
                        ))
                      : GunText(
                          'home_drawer.staff'.tr,
                          style: AppStyles.pageTitleStyle,
                        ),
                ],
              ),
              leadingColor: Colors.black,
              actions: [
                GunAction.icon(
                  onTap: () {
                    setState(() {
                      isSearch = !isSearch;
                      textController.clear();
                    });
                    if (isSearch == false) {
                      staffController.searchStaff(tuKhoa: '');
                    }
                  },
                  iconFactor: isSearch ? 0.4 : 0.7,
                  icon: Image.asset(
                    isSearch
                        ? './assets/images/close_icon.png'
                        : './assets/images/search_icon.png',
                    color: AppColors.Orange,
                  ),
                ),
              ],
            ),
            child: _renderBody(controller.listStaff));
      },
    );
  }

  _renderBody(List<StaffDetailResponse> list) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...List.generate(
              list.length,
              (index) => Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: BaseInfoWidget(
                        onTap: () async {
                          await _orderCreateController
                              .addStaff(list[index].nhanVien!);
                          Get.back();
                        },
                        contextLeft1: 'staff.name'.tr,
                        contextLeft2: 'phone'.tr,
                        contextLeft3: 'staff.total_money'.tr,
                        contextRight1: list[index].nhanVien!.employeeName!,
                        contextRight2: list[index].nhanVien!.employeePhone!,
                        contextRight3: list[index].tongDoanhSo.toString()),
                  )),
        ],
      ),
    );
  }
}
