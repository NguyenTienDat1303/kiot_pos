import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/controllers/order_create_controller.dart';
import 'package:kiot_pos/controllers/order_create_product_controller.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/data/entities/response/common/product_data.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/res/values.dart';
import 'package:kiot_pos/ui/pages/order_create_product_page.dart';
import 'package:kiot_pos/ui/widgets/widget_cached_image.dart';

import '../../data/entities/response/order_detail_response.dart';

class GoodsListPage extends StatefulWidget {
  const GoodsListPage({Key? key}) : super(key: key);

  @override
  State<GoodsListPage> createState() => _GoodsListPageState();
}

class _GoodsListPageState extends State<GoodsListPage>
    with TickerProviderStateMixin {
  OrderCreateProductController _controller =
      Get.find<OrderCreateProductController>();
  ProductController productController = Get.find<ProductController>();

  late TabController _tabController;

  late OrderDetailResponse _orderDetail;
  TextEditingController textController = TextEditingController();
  @override
  void initState() {
    super.initState();

    _controller.loadCategoryList(tuKhoa: '');
  }

  bool isSearch = false;

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          backgroundColor: Colors.white,
          type: GunAppBarType.TOP,
          titles: Row(
            children: [
              isSearch
                  ? Expanded(
                      child: TextField(
                      controller: textController,
                      onChanged: (e) {
                        setState(() {});
                      },
                      onSubmitted: (e) {
                        _controller.tuKhoa = e;
                        _controller.loadCategoryList(tuKhoa: '');
                      },
                      autofocus: true,
                      decoration: InputDecoration(
                          isDense: true, border: InputBorder.none),
                    ))
                  : GunText(
                      'list_goods'.tr,
                      style: AppStyles.pageTitleStyle,
                    ),
            ],
          ),
          leadingColor: Colors.black,
          actions: [
            GunAction.icon(
              onTap: () {
                setState(() {
                  isSearch = !isSearch;
                  textController.clear();
                });
                if (isSearch == false) {
                  _controller.tuKhoa = '';
                  _controller.loadCategoryList(tuKhoa: '');
                }
              },
              iconFactor: isSearch ? 0.4 : 0.7,
              icon: Image.asset(
                isSearch
                    ? './assets/images/close_icon.png'
                    : './assets/images/search_icon.png',
                color: AppColors.Orange,
              ),
            ),
            if (isSearch == false)
              GunAction.icon(
                onTap: () {
                  productController.idProduct = -1;
                  productController.detailProduct = DetailProductResponse(
                      sanPham: DetailProductData(
                          prdOriginPrice: 0,
                          prdSellPrice: 0,
                          prdSellPrice2: 0));
                  Get.toNamed(AppRoutes.goods_edit);
                },
                icon: Image.asset('assets/images/add_icon.png'),
              )
          ],
        ),
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            Column(
              children: [
                GetX<OrderCreateProductController>(
                  builder: (controller) {
                    _tabController = TabController(
                      initialIndex: 0,
                      length: controller.categoryList.length,
                      vsync: this,
                    );
                    _tabController.addListener(
                      () => {
                        _controller.categorySelected =
                            controller.categoryList[_tabController.index]
                      },
                    );
                    return Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      margin: EdgeInsets.symmetric(horizontal: 16),
                      height: 40,
                      padding: EdgeInsets.symmetric(vertical: 3, horizontal: 8),
                      decoration: BoxDecoration(
                          color: Color(0XFFE7E7E7),
                          borderRadius: BorderRadius.circular(25.0)),
                      child: TabBar(
                          controller: _tabController,
                          isScrollable: true,
                          indicator: BoxDecoration(
                              color: AppColors.primary,
                              borderRadius: BorderRadius.circular(20.0)),
                          labelColor: Colors.white,
                          unselectedLabelColor: Colors.black,
                          tabs: List.generate(
                              controller.categoryList.length,
                              (index) => Tab(
                                    text: controller.categoryList[index]
                                        .danhMuc!.prdGroupName!.tr,
                                  ))),
                    );
                  },
                ),
                Expanded(
                    child: SingleChildScrollView(
                  padding: EdgeInsets.only(
                      left: 16, right: 16, top: 16, bottom: 100),
                  child: GetX<OrderCreateProductController>(
                    builder: (controller) {
                      return Column(
                        children: [
                          Container(
                            height: 0,
                            child:
                                Text(controller.productList.length.toString()),
                          ),
                          GetX<OrderCreateController>(
                            builder: (orderCreateController) {
                              return Column(
                                children: [
                                  ...List.generate(
                                      controller.productList.length, (index) {
                                    return ProductWidget(
                                      onTap: () {
                                        productController.idProduct =
                                            controller.productList[index].id!;
                                        Get.toNamed(AppRoutes.goods_detail);
                                      },
                                      data: controller.productList[index],
                                    );
                                  })
                                ],
                              );
                            },
                          ),
                        ],
                      );
                    },
                  ),
                ))
              ],
            ),
          ],
        ));
  }
}

class ProductWidget extends StatelessWidget {
  final ProductData data;
  final GestureTapCallback? onTap;
  const ProductWidget({
    Key? key,
    required this.data,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Material(
        color: AppColors.White,
        child: InkWell(
          onTap: onTap,
          child: AnimatedContainer(
            duration: Duration(milliseconds: 300),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey.shade300, width: 1),
                borderRadius: BorderRadius.circular(5)),
            child: Container(
              padding: EdgeInsets.all(5),
              child: Row(
                children: [
                  Container(
                      width: 45,
                      height: 45,
                      margin: EdgeInsets.only(right: 10),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: WidgetCachedImage(
                              imageUrl: data.prdImageUrl ?? ""))),
                  Expanded(
                      child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          data.prdName!,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Container(
                          constraints: BoxConstraints(minHeight: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                AppValue.APP_MONEY_FORMAT
                                    .format(data.prdSellPrice!),
                                style: TextStyle(
                                    color: Color(0XFFE27C04),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                '${'stock'.tr}${double.parse(data.prdSls ?? '0').toInt()}',
                                // +AppValue.APP_MONEY_FORMAT.format(data.prdSls!).toString(),
                                style: TextStyle(
                                    color: AppColors.Blue,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
