import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/data/entities/response/stock_detail_response.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/widgets/avatar_create.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/ui/widgets/stock_dialog.dart';

class StockingEditPage extends StatefulWidget {
  const StockingEditPage({Key? key}) : super(key: key);

  @override
  State<StockingEditPage> createState() => _StockingEditPageState();
}

class _StockingEditPageState extends State<StockingEditPage> {
  StockController stockController = Get.find<StockController>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(Duration.zero, () {
      stockController.getDetailStock(id: stockController.idStock);
    });
  }

  int count = 1;
  int thucTe = 0;
  int lech = 0;
  List<ChiTiet> list1 = [];
  List<dynamic> listStock = [];

  TextEditingController amountController = TextEditingController();
  FocusNode amountFocusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    return GetX<StockController>(
      builder: (controller) {
        list1 = controller.detailStock!.chiTiet!;
        listStock = json.decode(controller.detailStock!.kiemKe!.detailAdjust!);

        return GunPage(
          resizeToAvoidBottomInset: false,
          appBarBottom: GunAppBar(
            type: GunAppBarType.BOTTOM,
            height: 0,
          ),
          appBarTop: GunAppBar(
            type: GunAppBarType.TOP,
            title: controller.detailStock != null
                ? controller.detailStock!.kiemKe!.adjustCode
                : '',
            titleStyle: AppStyles.pageTitleStyle,
            leadingColor: Colors.black,
            actions: [
              GunAction.icon(
                onTap: () async {
                  if (controller.isCompleted == 0) {
                    await Get.toNamed(AppRoutes.stocking_goods, arguments: 1);
                    setState(() {});
                  }
                },
                icon: controller.isCompleted == 0
                    ? Image.asset(
                        './assets/images/add_icon.png',
                      )
                    : Container(width: 24),
              )
            ],
          ),
          child: Container(
            child: Column(
              children: [
                Expanded(
                    child: SingleChildScrollView(
                  child: _renderBody(controller.detailStock!.chiTiet!),
                )),
                _renderButton()
              ],
            ),
          ),
        );
      },
    );
  }

  _renderBody(List<ChiTiet> list) {
    print('object');
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    flex: 2,
                    child: GunText(
                      'Hàng kiểm',
                      style: AppStyles.DEFAULT_14,
                    )),
                Expanded(
                    flex: 1,
                    child: GunText(
                      'Tồn kho',
                      textAlign: TextAlign.center,
                      style: AppStyles.DEFAULT_14,
                    )),
                Expanded(
                    flex: 1,
                    child: GunText(
                      'Thực tế',
                      textAlign: TextAlign.center,
                      style: AppStyles.DEFAULT_14,
                    )),
                Expanded(
                    flex: 1,
                    child: GunText(
                      'Lệch',
                      textAlign: TextAlign.center,
                      style: AppStyles.DEFAULT_14,
                    )),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            child: Divider(
              color: AppColors.Gray4,
              height: 4,
            ),
          ),
          ...List.generate(
              list1.length,
              (index) => Material(
                    color: AppColors.White,
                    child: InkWell(
                      onTap: () async {
                        await showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return StatefulBuilder(
                              builder: (BuildContext context,
                                  void Function(void Function()) setState) {
                                return StockDialog(
                                  title: list1[index].tenSP!,
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                            child: GunText(
                                          'Tồn kho',
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14,
                                        )),
                                        Expanded(
                                            child: GunText(
                                          'Thực tế',
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14,
                                        )),
                                        Expanded(
                                            child: GunText(
                                          'Lệch',
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14,
                                        )),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                            child: GunText(
                                          list1[index].tonKho.toString(),
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14.copyWith(
                                              color: AppColors.Orange),
                                        )),
                                        Expanded(
                                            child: GunText(
                                          '-',
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14.copyWith(
                                              color: AppColors.Orange),
                                        )),
                                        Expanded(
                                            child: GunText(
                                          '-',
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14.copyWith(
                                              color: AppColors.Orange),
                                        )),
                                      ],
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 20),
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 30),
                                      child: Row(
                                        children: [
                                          Expanded(
                                              child: GunText(
                                            'Số lượng:',
                                            style: AppStyles.DEFAULT_14,
                                          )),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              GunInkwell(
                                                onTap: () {
                                                  print('object');
                                                  if (count > 1) {
                                                    setState(() {
                                                      count--;
                                                      amountController.text =
                                                          count.toString();
                                                    });
                                                  }
                                                },
                                                padding: EdgeInsets.zero,
                                                child: Container(
                                                    width: 30,
                                                    height: 30,
                                                    child: Image.asset(
                                                        'assets/images/btn_delete.png')),
                                              ),
                                              Container(
                                                  width: 40,
                                                  margin: EdgeInsets.only(
                                                      left: 15, right: 15),
                                                  child: WidgetInput(
                                                    inputFormatters: <
                                                        TextInputFormatter>[
                                                      FilteringTextInputFormatter
                                                          .digitsOnly,
                                                      NumericalRangeFormatter(
                                                          min: 1,
                                                          max: list1[index]
                                                              .tonKho!
                                                              .toDouble()),
                                                    ],
                                                    keyboardType: TextInputType
                                                        .numberWithOptions(
                                                            decimal: false,
                                                            signed: false),
                                                    focusNode: amountFocusNode,
                                                    textAlign: TextAlign.center,
                                                    controller:
                                                        amountController,
                                                    onChanged: (e) {
                                                      if (e.isNotEmpty) {
                                                        count = int.parse(e);
                                                      }
                                                      setState(() {});
                                                      amountController
                                                              .selection =
                                                          TextSelection.fromPosition(
                                                              TextPosition(
                                                                  offset: amountController
                                                                      .text
                                                                      .length));
                                                    },
                                                  )),
                                              GunInkwell(
                                                onTap: () {
                                                  setState(() {
                                                    count = count + 1;
                                                    amountController.text =
                                                        count.toString();
                                                  });
                                                },
                                                padding: EdgeInsets.zero,
                                                child: Container(
                                                    width: 30,
                                                    height: 30,
                                                    child: Image.asset(
                                                        'assets/images/btn_add.png')),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                  onTapLeft: () {
                                    Get.back();
                                    setState(() {
                                      list1[index].thucTe = count;
                                      list1[index].lech =
                                          count - list1[index].tonKho!;
                                      amountController.text = "";
                                    });
                                  },
                                  onTapRight: () {
                                    Get.back();
                                    setState(() {
                                      list1[index].thucTe =
                                          list1[index].thucTe! + count;
                                      list1[index].lech = list1[index].thucTe! -
                                          list1[index].tonKho!;
                                      amountController.text = "";
                                    });
                                  },
                                );
                              },
                            );
                          },
                        );
                        setState(() {});
                      },
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 30,
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                    flex: 2,
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 20),
                                      child: GunText(
                                        list1[index].tenSP!,
                                        style: AppStyles.DEFAULT_14,
                                      ),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 20),
                                      child: GunText(
                                        list1[index].tonKho!.toString(),
                                        textAlign: TextAlign.center,
                                        style: AppStyles.DEFAULT_14,
                                      ),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 20),
                                      child: GunText(
                                        list1[index].thucTe!.toString(),
                                        textAlign: TextAlign.center,
                                        style: AppStyles.DEFAULT_14,
                                      ),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 20),
                                      child: GunText(
                                        list1[index].lech!.toString(),
                                        textAlign: TextAlign.center,
                                        style: AppStyles.DEFAULT_14,
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          if (index != list1.length - 1)
                            Container(
                              width: double.infinity,
                              child: Divider(
                                color: AppColors.Gray4,
                                height: 4,
                              ),
                            ),
                        ],
                      ),
                    ),
                  ))
        ],
      ),
    );
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.all(30),
      child: WidgetButton(
        onTap: () {
          List<StockDto> list = [];
          setState(() {
            listStock.forEach((e) {
              list.add(StockDto(
                  id: e["id"],
                  quantity: e["quantity"],
                  inventory: e["inventory"]));
            });

            list.forEach((e) {
              list1.forEach((e1) {
                e.quantity = e1.thucTe;
              });
            });
          });

          if (list.isNotEmpty) {
            print('list ${list}');
            stockController.stockEdit(
                id: stockController.detailStock!.kiemKe!.ID!,
                adjustStatus: 1,
                data: list,
                callback: () {
                  Get.back();

                  stockController.getDetailStock(
                      id: stockController.detailStock!.kiemKe!.ID!);
                });
          }
        },
        isFullWidth: true,
        text: 'save'.tr,
      ),
    );
  }
}
