import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:rxdart/rxdart.dart';

class OrderCreateProductPage extends StatefulWidget {
  const OrderCreateProductPage({
    Key? key,
  }) : super(key: key);

  @override
  State<OrderCreateProductPage> createState() => _OrderCreateProductPageState();
}

class _OrderCreateProductPageState extends State<OrderCreateProductPage>
    with TickerProviderStateMixin {
  OrderCreateProductController _controller =
      Get.find<OrderCreateProductController>();
  OrderCreateController _orderCreateController =
      Get.find<OrderCreateController>();
  late TabController _tabController;

  late OrderDetailResponse _orderDetail;

  List<ProductData> productDataList = [];

  bool isSeachVisible = false;

  late int type;

  @override
  void initState() {
    super.initState();
    type = Get.arguments;
    // _orderDetail = _orderCreateController.orderDetail;
    _controller.loadCategoryList(tuKhoa: '');
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          backgroundColor: Colors.white,
          type: GunAppBarType.TOP,
          // title: 'order_create_product.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
          titles: Row(children: [
            isSeachVisible
                ? Expanded(
                    child: TextFormField(
                      autofocus: true,
                      decoration: InputDecoration(border: InputBorder.none),
                      onFieldSubmitted: (val) {
                        _controller.tuKhoa = val;
                        _controller.loadProductList(
                            _controller.categorySelected!.danhMuc!.id, val);
                      },
                    ),
                  )
                : Text('order_create_product.title'.tr)
          ]),
          actions: [
            GunAction.icon(
              onTap: () {
                setState(() {
                  isSeachVisible = !isSeachVisible;
                });
                if (isSeachVisible == false && _controller.tuKhoa.isNotEmpty) {
                  _controller.tuKhoa = "";
                  _controller.loadProductList(
                      _controller.categorySelected!.danhMuc!.id, "");
                }
              },
              icon: Image.asset(isSeachVisible
                  ? 'assets/images/close_icon.png'
                  : 'assets/images/menu_search.png'),
            ),
            if (!isSeachVisible)
              GunAction.icon(
                icon: Image.asset('assets/images/menu_add.png'),
                onTap: () async {
                  await Get.toNamed(
                    AppRoutes.goods_edit,
                  );
                },
              ),
          ],
        ),
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            Column(
              children: [
                GetX<OrderCreateProductController>(
                  builder: (controller) {
                    _tabController = TabController(
                      initialIndex: 0,
                      length: controller.categoryList.length,
                      vsync: this,
                    );
                    _tabController.addListener(
                      () => {
                        _controller.categorySelected =
                            controller.categoryList[_tabController.index]
                      },
                    );
                    return Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      margin: EdgeInsets.symmetric(horizontal: 16),
                      height: 40,
                      padding: EdgeInsets.symmetric(vertical: 3, horizontal: 8),
                      decoration: BoxDecoration(
                          color: Color(0XFFE7E7E7),
                          borderRadius: BorderRadius.circular(25.0)),
                      child: TabBar(
                          controller: _tabController,
                          isScrollable: true,
                          indicator: BoxDecoration(
                              color: AppColors.primary,
                              borderRadius: BorderRadius.circular(20.0)),
                          labelColor: Colors.white,
                          unselectedLabelColor: Colors.black,
                          tabs: List.generate(
                              controller.categoryList.length,
                              (index) => Tab(
                                    text: controller.categoryList[index]
                                        .danhMuc!.prdGroupName!.tr,
                                  ))),
                    );
                  },
                ),
                Expanded(
                    child: SingleChildScrollView(
                  padding: EdgeInsets.only(
                      left: 16, right: 16, top: 16, bottom: 100),
                  child: GetX<OrderCreateProductController>(
                    builder: (controller) {
                      return Column(
                        children: [
                          Container(
                            height: 0,
                            child:
                                Text(controller.productList.length.toString()),
                          ),
                          ...List.generate(controller.productList.length,
                              (index) {
                            var productData = productDataList.firstWhereOrNull(
                                (e) =>
                                    e.id == controller.productList[index].id);
                            TextEditingController quantityEditControler =
                                new TextEditingController();
                            if (productData != null) {
                              quantityEditControler = TextEditingController
                                  .fromValue(new TextEditingValue(
                                      text: productData.selectedQuantity
                                          .toString(),
                                      selection: new TextSelection.collapsed(
                                          offset: productData.selectedQuantity
                                              .toString()
                                              .length)));
                              quantityEditControler.addListener(() {
                                //  setState(() { name = c.text;});
                              });
                            }

                            return OrderCreateProductWidget(
                              data: controller.productList[index],
                              quantity: productData != null
                                  ? productData.selectedQuantity
                                  : 0,
                              quantityEditController: quantityEditControler,
                              onChanged: (val) {
                                print("onChanged $val");
                                if (val.isNotEmpty) {
                                  productData!.selectedQuantity =
                                      int.parse(val);
                                }
                                setState(() {});
                                quantityEditControler.selection =
                                    TextSelection.fromPosition(TextPosition(
                                        offset:
                                            quantityEditControler.text.length));
                              },
                              onAddTap: () {
                                setState(() {
                                  if (productData != null) {
                                    productData.selectedQuantity += 1;
                                  } else {
                                    controller.productList[index]
                                        .selectedQuantity = 1;
                                    productDataList
                                        .add(controller.productList[index]);
                                  }
                                });
                              },
                              onDeleteTap: () {
                                setState(() {
                                  if (productData != null) {
                                    if (productData.selectedQuantity > 1) {
                                      productData.selectedQuantity -= 1;
                                    } else {
                                      productDataList.remove(productData);
                                    }
                                  }
                                  quantityEditControler.text =
                                      productData!.selectedQuantity.toString();
                                });
                              },
                            );
                          })
                        ],
                      );
                    },
                  ),
                ))
              ],
            ),
            GunInkwell(
              onTap: () {
                _orderCreateController.addProductList(type, productDataList);
                Get.back();
              },
              borderRadius: BorderRadius.circular(500),
              margin: EdgeInsets.all(16),
              padding: EdgeInsets.symmetric(horizontal: 16),
              decoration: BoxDecoration(
                color: AppColors.primary,
              ),
              child: Container(
                width: double.infinity,
                height: 40,
                child: Row(children: [
                  Expanded(
                      child: Text(
                    'order_create_product.total_product'.trParams({
                      'productCount':
                          '${productDataList.fold<int>(0, (previousValue, e) => previousValue + e.selectedQuantity)}'
                    }),
                    style:
                        AppStyles.normalTextStyle.copyWith(color: Colors.white),
                  )),
                  // Expanded(
                  //     child: Text(
                  //   '${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.productListTotalPrice)}',
                  //   textAlign: TextAlign.end,
                  //   style: AppStyles.normalTextStyle
                  //       .copyWith(color: Colors.white),
                  // ))
                ]),
              ),
            )
          ],
        ));
  }
}

class OrderCreateProductWidget extends StatefulWidget {
  final ProductData data;
  final int quantity;
  final TextEditingController quantityEditController;
  final VoidCallback? onAddTap;
  final VoidCallback? onDeleteTap;
  final ValueChanged<String> onChanged;
  const OrderCreateProductWidget(
      {Key? key,
      required this.data,
      this.quantity = 0,
      this.onAddTap,
      this.onDeleteTap,
      required this.onChanged,
      required this.quantityEditController})
      : super(key: key);

  @override
  State<OrderCreateProductWidget> createState() =>
      _OrderCreateProductWidgetState();
}

class _OrderCreateProductWidgetState extends State<OrderCreateProductWidget> {
  final _searchTerms = BehaviorSubject<String>();
  final _results = BehaviorSubject<String>();
  @override
  void initState() {
    super.initState();
    // _results.addStream(_searchTerms
    //     .debounce((_) => TimerStream(true, Duration(milliseconds: 500)))
    //     .switchMap((query) async* {
    //   if (query.length > 0) widget.onChanged(query);
    // }));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.quantity < 1 ? widget.onAddTap : null,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        decoration: BoxDecoration(
            color: widget.quantity > 0 ? Color(0XFFFFEBD4) : Colors.white,
            border: Border.all(color: Colors.grey.shade300, width: 1),
            borderRadius: BorderRadius.circular(5)),
        margin: EdgeInsets.only(bottom: 10),
        child: Container(
          padding: EdgeInsets.all(5),
          child: Row(
            children: [
              Container(
                  width: 45,
                  height: 45,
                  margin: EdgeInsets.only(right: 10),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(5.0),
                      child: WidgetCachedImage(
                          imageUrl: widget.data.prdImageUrl ?? ""))),
              Expanded(
                  child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.data.prdName!,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Container(
                      constraints: BoxConstraints(minHeight: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            AppValue.APP_MONEY_FORMAT
                                .format(widget.data.prdSellPrice!),
                            style: TextStyle(
                                color: Color(0XFFE27C04),
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                          if (widget.quantity > 0)
                            Expanded(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                GunInkwell(
                                  onTap: widget.onDeleteTap,
                                  padding: EdgeInsets.zero,
                                  child: Container(
                                      width: 30,
                                      height: 30,
                                      child: Image.asset(
                                          'assets/images/btn_delete.png')),
                                ),
                                // Container(
                                //   margin: EdgeInsets.only(left: 15, right: 15),
                                //   child: Text(
                                //     '$quantity',
                                //     style: TextStyle(
                                //         color: Colors.black,
                                //         fontSize: 14,
                                //         fontWeight: FontWeight.w400),
                                //   ),
                                // ),
                                Container(
                                  width: 50,
                                  margin: EdgeInsets.only(left: 15, right: 15),
                                  child: TextField(
                                    controller: widget.quantityEditController,
                                    // onChanged: (val) {
                                    //   _searchTerms.add(val);
                                    // },
                                    onChanged: widget.onChanged,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        isDense: true),
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly,
                                      NumericalRangeFormatter(
                                          min: 1, max: 100000),
                                    ],
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: false, signed: false),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                GunInkwell(
                                  onTap: widget.onAddTap,
                                  padding: EdgeInsets.zero,
                                  child: Container(
                                      width: 30,
                                      height: 30,
                                      child: Image.asset(
                                          'assets/images/btn_add.png')),
                                )
                              ],
                            ))
                        ],
                      ),
                    )
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
