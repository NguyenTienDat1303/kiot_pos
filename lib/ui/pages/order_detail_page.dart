import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderDetailPage extends StatefulWidget {
  const OrderDetailPage({
    Key? key,
  }) : super(key: key);

  @override
  State<OrderDetailPage> createState() => _OrderDetailPageState();
}

class _OrderDetailPageState extends State<OrderDetailPage> {
  OrderCreateController _controller = Get.find<OrderCreateController>();

  OrderTypeLoad _type = OrderTypeLoad.order_edit;
  late int? _tableId;
  late int? _orderId;

  @override
  void initState() {
    super.initState();
    _orderId = Get.arguments["orderId"];
    _controller.orderId = _orderId;
    // _controller.getOrderDetail(_orderId);
    // _controller.getOrderDetail(255);
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'order_detail.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
          actions: [
            GunAction.icon(
              icon: Image.asset("assets/images/edit_icon.png"),
              onTap: () {
                Get.toNamed(AppRoutes.order_create, arguments: {
                  'type': OrderTypeLoad.order_edit,
                  'orderId': _orderId
                });
              },
            )
          ],
        ),
        child: SingleChildScrollView(
            child: Column(
          children: [
            GetX<OrderCreateController>(
              builder: (controller) {
                return Column(
                  children: [
                    if (controller.orderDetail.donHang != null)
                      Container(
                        color: Colors.white,
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                        child: Column(children: [
                          Row(
                            children: [
                              Expanded(
                                  child: Text(
                                'order_detail.code'.tr,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.primary),
                              )),
                              Expanded(
                                  child: Text(
                                '${controller.orderDetail.donHang!.outputCode}'
                                    .tr,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ))
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: Text(
                                'order_detail.customer'.tr,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.primary),
                              )),
                              Expanded(
                                  child: Text(
                                '${controller.orderDetail.tenkhach != null ? controller.orderDetail.tenkhach!.customerName : ""}'
                                    .tr,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ))
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: Text(
                                'order_detail.sell_date'.tr,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.primary),
                              )),
                              Expanded(
                                  child: Text(
                                '${controller.orderDetail.donHang!.sellDate}'
                                    .tr,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ))
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: Text(
                                'order_detail.sell_staff'.tr,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.primary),
                              )),
                              Expanded(
                                  child: Text(
                                '${controller.orderDetail.nguoiThu != null ? controller.orderDetail.nguoiThu!.displayName : ""}'
                                    .tr,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ))
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: Text(
                                'order_detail.quantity'.tr,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.primary),
                              )),
                              Expanded(
                                  child: Text(
                                '${controller.orderDetail.donHang!.totalQuantity}'
                                    .tr,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ))
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: Text(
                                'order_detail.total_money'.tr,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.primary),
                              )),
                              Expanded(
                                  child: Text(
                                '${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tongThanhToan ?? 0)}',
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ))
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: Text(
                                'order_detail.total_debt'.tr,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.primary),
                              )),
                              Expanded(
                                  child: Text(
                                '${AppValue.APP_MONEY_FORMAT.format(0)}',
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ))
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ]),
                      ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              margin: EdgeInsets.symmetric(vertical: 20),
                              child: Text(
                                'order_detail.product_list'.tr,
                                style: AppStyles.normalTextStyle.copyWith(
                                    color: AppColors.primary,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            ...List.generate(
                                controller.orderDetail.sanPhams!.length,
                                (index) {
                              var productData =
                                  controller.orderDetail.sanPhams![index];
                              return OrderItemWidget(
                                data: productData,
                              );
                            }),
                            if (controller.orderDetail.khuyenMai != null &&
                                controller.orderDetail.khuyenMai!.isNotEmpty)
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.symmetric(vertical: 20),
                                child: Text(
                                  'order_create.promotion'.tr,
                                  style: AppStyles.normalTextStyle.copyWith(
                                      color: AppColors.primary,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ...List.generate(
                                controller.orderDetail.khuyenMai!.length,
                                (index) {
                              var productData =
                                  controller.orderDetail.khuyenMai![index];
                              return OrderItemWidget(
                                data: productData,
                              );
                            }),
                            SizedBox(
                              height: 30,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 40),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: TextButton(
                                          onPressed: () {
                                            Get.toNamed(AppRoutes.order_return);
                                          },
                                          style: ButtonStyle(
                                              shape: MaterialStateProperty.all<
                                                      RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(18.0),
                                              )),
                                              padding:
                                                  MaterialStateProperty.all(
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10)),
                                              backgroundColor:
                                                  MaterialStateProperty.all(
                                                      Color(0XFFD6E8FF))),
                                          child: Text(
                                            'order_detail.return'.tr,
                                            style: TextStyle(
                                                color: AppColors.primary,
                                                fontSize: 18,
                                                fontWeight: FontWeight.w600),
                                          )),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: TextButton(
                                          onPressed: () {
                                            // var printManagerController = Get
                                            //     .find<PrintManagerController>();
                                            // printManagerController.printForm(
                                            //     PrintAction.payment,
                                            //     tableData:
                                            //         _controller.tableData,
                                            //     orderDetail:
                                            //         _controller.orderDetail);
                                          },
                                          style: ButtonStyle(
                                              shape: MaterialStateProperty.all<
                                                      RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(18.0),
                                              )),
                                              padding:
                                                  MaterialStateProperty.all(
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10)),
                                              backgroundColor:
                                                  MaterialStateProperty.all(
                                                      AppColors.primary)),
                                          child: Text(
                                            'order_detail.print'.tr,
                                            style: TextStyle(
                                                color: AppColors.White,
                                                fontSize: 18,
                                                fontWeight: FontWeight.w600),
                                          )),
                                    )
                                  ]),
                            )
                          ]),
                    ),
                  ],
                );
              },
            ),
          ],
        )));
  }
}
