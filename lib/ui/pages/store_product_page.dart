import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class StoreProductPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const StoreProductPage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<StoreProductPage> createState() => _StoreProductPageState();
}

class _StoreProductPageState extends State<StoreProductPage> {
  @override
  Widget build(BuildContext context) {
    return GunPage(appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'store_product.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
        ),child: Text("StoreProduct"));
  }
}
