import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/controllers/controllers.dart';
import 'package:kiot_pos/controllers/order_create_product_controller.dart';
import 'package:kiot_pos/data/entities/response/category_response.dart';
import 'package:kiot_pos/data/entities/response/unit_response.dart';
import 'package:kiot_pos/models/routes.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/pages/area_edit_page.dart';
import 'package:kiot_pos/ui/pages/goods_edit_page.dart';
import 'package:kiot_pos/ui/widgets/avatar_create.dart';
import 'package:kiot_pos/ui/widgets/check_box.dart';
import 'package:kiot_pos/ui/widgets/widget_button.dart';
import 'package:kiot_pos/ui/widgets/widget_dialog.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/ui/widgets/widget_input.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ToppingCreatePage extends StatefulWidget {
  const ToppingCreatePage({Key? key}) : super(key: key);

  @override
  State<ToppingCreatePage> createState() => _ToppingCreatePageState();
}

class _ToppingCreatePageState extends State<ToppingCreatePage> {
  OrderToppingController orderToppingController =
      Get.find<OrderToppingController>();
  OrderCreateProductController _controller =
      Get.find<OrderCreateProductController>();
  ProductController productController = Get.find<ProductController>();
  GlobalKey<FormState> key = GlobalKey();
  List<ItemInputKhuVuc> listInput1 = [];
  List<ItemInputKhuVuc> listInput2 = [];
  List<ItemInputKhuVuc> listInput3 = [];
  bool isEditPrice = false;
  bool isSellNegative = false;
  bool isTichDiem = false;
  bool isTopping = false;
  bool isNguyenLieu = false;
  bool isInTem = false;
  List<ItemUnitConversion> listUnitConversion = [];
  UnitResponse? donViTinh;
  ProducerResponse? nsx;
  UsersResponse? user;
  CategoryDetail? danhMuc;
  UnitResponse? donViQuyDoi;
  File? image;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      productController.unit();
      productController.producer();
      productController.loadCategoryList('');
    });
    for (var i = 0; i < 3; i++) {
      listInput1.add(ItemInputKhuVuc(
          controller: TextEditingController(), focusNode: FocusNode()));
      listInput2.add(ItemInputKhuVuc(
          controller: TextEditingController(), focusNode: FocusNode()));
    }
    for (var i = 0; i < 2; i++) {
      listInput3.add(ItemInputKhuVuc(
          controller: TextEditingController(), focusNode: FocusNode()));
    }
    if (productController.idProduct == -1) {
      for (var i = 0; i < 1; i++) {
        listUnitConversion.add(ItemUnitConversion(
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(), focusNode: FocusNode())));
      }
    } else {
      if (productController.detailProduct!.quyDoi != null) {
        var data = productController.detailProduct!;
        for (var i = 0; i < data.quyDoi!.length; i++) {
          listUnitConversion.add(ItemUnitConversion(
            ItemInputKhuVuc(
                controller:
                    TextEditingController(text: data.quyDoi![i].unitName),
                focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(
                    text: data.quyDoi![i].number.toString()),
                focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(
                    text: data.quyDoi![i].prdOriginPrice.toString()),
                focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(
                    text: data.quyDoi![i].prdSellPrice.toString()),
                focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller: TextEditingController(
                    text: data.quyDoi![i].prdSellPrice2.toString()),
                focusNode: FocusNode()),
            ItemInputKhuVuc(
                controller:
                    TextEditingController(text: data.quyDoi![i].prdCode),
                focusNode: FocusNode()),
          ));
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
      appBarBottom: GunAppBar(
        type: GunAppBarType.BOTTOM,
        height: 0,
      ),
      appBarTop: GunAppBar(
        type: GunAppBarType.TOP,
        title: productController.detailProduct != null
            ? productController.detailProduct!.sanPham!.prdName
            : 'Tạo sản phẩm',
        titleStyle: AppStyles.pageTitleStyle,
        leadingColor: Colors.black,
        actions: [
          GunAction.icon(
              icon: Container(
            width: 24,
          ))
        ],
      ),
      child: Container(
        child: Column(
          children: [Expanded(child: _renderBody())],
        ),
      ),
    );
  }

  _renderBody() {
    return GetX<ProductController>(
      builder: (controller) {
        return Container(
          color: AppColors.Background,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  color: AppColors.White,
                  child: Form(
                    key: key,
                    child: Column(
                      children: [
                        _renderAvatar(),
                        _renderInput1(),
                        _renderUnit(controller.listUnit),
                        _renderInput2(),
                        _renderCatalogue(controller.categoryList),
                        _renderProducer(controller.listProducer),
                        _renderInput3(),
                        _renderCheckBox(controller.detailProduct != null),
                      ],
                    ),
                  ),
                ),
                _renderButton()
              ],
            ),
          ),
        );
      },
    );
  }

  _renderAvatar() {
    return AvatarCreate(
        image: image,
        onChange: (e) {
          setState(() {
            image = e;
          });
        });
  }

  _renderInput1() {
    return Column(
      children: [
        ...List.generate(
          listInput1.length,
          (index) => WidgetInput(
            controller: listInput1[index].controller,
            focusNode: listInput1[index].focusNode,
            keyboardType:
                index == 1 ? TextInputType.number : TextInputType.text,
            validator: index != 2
                ? FormBuilderValidators.required(errorText: 'error_miss'.tr)
                : null,
            hintText: index == 0
                ? 'Tên hàng hóa'
                : index == 1
                    ? 'Số lượng'
                    : 'Mã SP (Nếu không nhập, hệ thống sẽ tự sinh)',
          ),
        )
      ],
    );
  }

  _renderUnit(List<UnitResponse> list) {
    return Row(
      children: [
        Expanded(
          child: Container(
            padding: EdgeInsets.only(top: 24.r, bottom: 12.r),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(width: 1, color: AppColors.Gray3))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Đơn vị tính', style: AppStyles.DEFAULT_16),
                PopupMenuButton(
                  child: Row(
                    children: [
                      Text(
                          donViTinh != null
                              ? donViTinh!.prdUnitName ?? ''
                              : 'Chọn',
                          style: AppStyles.DEFAULT_16),
                      Image.asset(
                        './assets/images/expanded_icon.png',
                        height: 10.h,
                      ),
                    ],
                  ),
                  itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                    ...List.generate(
                      list.length,
                      (index) => PopupMenuItem(
                          onTap: () {
                            setState(() {
                              donViTinh = list[index];
                            });
                          },
                          child: Text(list[index].prdUnitName!,
                              style: AppStyles.DEFAULT_16)),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
        Material(
          color: AppColors.White,
          child: InkWell(
            onTap: () {
              Get.toNamed(AppRoutes.unit, arguments: 1);
            },
            child: Container(
              padding:
                  EdgeInsets.only(top: 24.r, bottom: 12.r, left: 12, right: 12),
              child: Image.asset(
                'assets/images/add_icon.png',
                width: 24,
              ),
            ),
          ),
        )
      ],
    );
  }

  _renderInput2() {
    return Column(
      children: [
        ...List.generate(
          listInput2.length,
          (index) => WidgetInput(
            controller: listInput2[index].controller,
            focusNode: listInput2[index].focusNode,
            keyboardType: TextInputType.number,
            validator:
                FormBuilderValidators.required(errorText: 'error_miss'.tr),
            hintText: index == 0
                ? 'Giá vốn'
                : index == 1
                    ? 'Giá bán lẻ'
                    : 'Giá bán sỉ',
          ),
        )
      ],
    );
  }

  _renderCatalogue(List<CategoryData> list) {
    return Row(
      children: [
        Expanded(
          child: Container(
            padding: EdgeInsets.only(top: 24.r, bottom: 12.r),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(width: 1, color: AppColors.Gray3))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Chọn danh mục', style: AppStyles.DEFAULT_16),
                PopupMenuButton(
                  child: Row(
                    children: [
                      Text(
                          danhMuc != null
                              ? danhMuc!.prdGroupName ?? ''
                              : 'Chọn',
                          style: AppStyles.DEFAULT_16),
                      Image.asset(
                        './assets/images/expanded_icon.png',
                        height: 10.h,
                      ),
                    ],
                  ),
                  itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                    ...List.generate(
                      list.length,
                      (index) => PopupMenuItem(
                          onTap: () {
                            setState(() {
                              danhMuc = list[index].danhMuc;
                            });
                          },
                          child: Text(list[index].danhMuc!.prdGroupName!,
                              style: AppStyles.DEFAULT_16)),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        Material(
          color: AppColors.White,
          child: InkWell(
            onTap: () {
              Get.toNamed(AppRoutes.unit, arguments: 2);
            },
            child: Container(
              padding:
                  EdgeInsets.only(top: 24.r, bottom: 12.r, left: 12, right: 12),
              child: Image.asset(
                'assets/images/add_icon.png',
                width: 24,
              ),
            ),
          ),
        )
      ],
    );
  }

  _renderProducer(List<ProducerResponse> list) {
    return Row(
      children: [
        Expanded(
          child: Container(
            padding: EdgeInsets.only(top: 24.r, bottom: 12.r),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(width: 1, color: AppColors.Gray3))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Chọn nhà sản xuất', style: AppStyles.DEFAULT_16),
                PopupMenuButton(
                  child: Row(
                    children: [
                      Text(nsx != null ? nsx!.prdManufName ?? '' : 'Chọn',
                          style: AppStyles.DEFAULT_16),
                      Image.asset(
                        './assets/images/expanded_icon.png',
                        height: 10.h,
                      ),
                    ],
                  ),
                  itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                    ...List.generate(
                      list.length,
                      (index) => PopupMenuItem(
                          onTap: () {
                            setState(() {
                              nsx = list[index];
                            });
                          },
                          child: Text(list[index].prdManufName!,
                              style: AppStyles.DEFAULT_16)),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
        Material(
          color: AppColors.White,
          child: InkWell(
            onTap: () {
              Get.toNamed(AppRoutes.unit, arguments: 3);
            },
            child: Container(
              padding:
                  EdgeInsets.only(top: 24.r, bottom: 12.r, left: 12, right: 12),
              child: Image.asset(
                'assets/images/add_icon.png',
                width: 24,
              ),
            ),
          ),
        )
      ],
    );
  }

  _renderInput3() {
    return Column(
      children: [
        ...List.generate(
          listInput3.length,
          (index) => WidgetInput(
            controller: listInput3[index].controller,
            focusNode: listInput3[index].focusNode,
            keyboardType: TextInputType.number,
            validator:
                FormBuilderValidators.required(errorText: 'error_miss'.tr),
            hintText: index == 0 ? 'Định mức lớn nhất' : 'Dịnh mức nhỏ nhất',
          ),
        )
      ],
    );
  }

  _renderCheckBox(bool isEdit) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: CheckBoxCustom(
              textSpan1: 'Cho phép sửa giá khi bán?',
              isCheck: isEditPrice,
              onChanged: (e) {},
              onTap: () {
                setState(() {
                  isEditPrice = !isEditPrice;
                });
              }),
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: CheckBoxCustom(
              textSpan1: 'Cho phép bán âm?',
              isCheck: isSellNegative,
              onChanged: (e) {},
              onTap: () {
                setState(() {
                  isSellNegative = !isSellNegative;
                });
              }),
        ),
        if (isEdit == false)
          Container(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: CheckBoxCustom(
                      textSpan1: 'Tích điểm',
                      isCheck: isTichDiem,
                      onChanged: (e) {},
                      onTap: () {
                        setState(() {
                          isTichDiem = !isTichDiem;
                        });
                      }),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: CheckBoxCustom(
                      textSpan1: 'Nguyên liệu?',
                      isCheck: isNguyenLieu,
                      onChanged: (e) {},
                      onTap: () {
                        setState(() {
                          isNguyenLieu = !isNguyenLieu;
                        });
                      }),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: CheckBoxCustom(
                      textSpan1: 'In tem nhãn khi bán?',
                      isCheck: isInTem,
                      onChanged: (e) {},
                      onTap: () {
                        setState(() {
                          isInTem = !isInTem;
                        });
                      }),
                ),
              ],
            ),
          )
      ],
    );
  }

  _renderInput({
    required TextEditingController controller,
    required FocusNode focusNode,
    String? hintText,
  }) {
    return Expanded(
      child: WidgetInput(
        keyboardType: TextInputType.number,
        controller: controller,
        focusNode: focusNode,
        hintText: hintText,
        validator: FormBuilderValidators.required(errorText: 'error_miss'.tr),
        onChanged: (e) {
          setState(() {});
        },
      ),
    );
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.all(30),
      child: WidgetButton(
        onTap: () async {
          if (key.currentState!.validate()) {
            if (int.tryParse(listInput1[1].controller.text) != null &&
                int.tryParse(listInput2[0].controller.text) != null &&
                int.tryParse(listInput2[1].controller.text) != null &&
                int.tryParse(listInput2[2].controller.text) != null &&
                int.tryParse(listInput3[0].controller.text) != null &&
                int.tryParse(listInput3[1].controller.text) != null) {
              var result = await productController.productCreate(
                  prdName: listInput1[0].controller.text,
                  prdCode: listInput1[2].controller.text,
                  prd_sls: int.parse(listInput1[1].controller.text),
                  prd_image_url: image,
                  prd_edit_price: isEditPrice ? 1 : 0,
                  prd_print: isInTem ? 1 : 0,
                  prd_allownegative: isSellNegative ? 1 : 0,
                  prd_material: isNguyenLieu ? 1 : 0,
                  prdAdd: 1,
                  infor: '',
                  prd_origin_price: int.parse(listInput2[0].controller.text),
                  prd_sell_price: int.parse(listInput2[1].controller.text),
                  prd_sell_price2: int.parse(listInput2[2].controller.text),
                  prd_group_id: danhMuc!.id!,
                  prd_manufacture_id: nsx!.id!,
                  prd_unit_id: donViTinh!.id!,
                  prd_new: 1,
                  prd_hot: 1,
                  prd_highlight: 1,
                  prd_max: int.parse(listInput3[0].controller.text),
                  prd_min: int.parse(listInput3[1].controller.text),
                  quyDoi: []);
              Get.dialog(DialogCustom(
                  title: 'Thông báo',
                  onTapOK: () {
                    Get.back();
                    Get.back();
                    orderToppingController.loadToppingList('');
                  },
                  children: [
                    Text(result.message!, style: AppStyles.DEFAULT_16)
                  ]));
            }
          }
        },
        isFullWidth: true,
        backgroundColor: AppColors.Blue,
        text: productController.idProduct != -1 ? 'Chỉnh sửa' : 'Tạo',
        textStyle: AppStyles.DEFAULT_18_BOLD.copyWith(color: AppColors.White),
      ),
    );
  }
}
