import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/ui/widgets/base_info_widget.dart';

class StaffPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const StaffPage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<StaffPage> createState() => _StaffPageState();
}

class _StaffPageState extends State<StaffPage> {
  TextEditingController textController = TextEditingController();
  StaffController staffController = Get.find<StaffController>();
  bool isSearch = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 0), () {
      staffController.searchStaff(tuKhoa: '');
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetX<StaffController>(
      builder: (controller) {
        return GunPage(
            appBarBottom: GunAppBar(
              type: GunAppBarType.BOTTOM,
              height: 0,
            ),
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              titles: Row(
                children: [
                  isSearch
                      ? Expanded(
                          child: TextField(
                          controller: textController,
                          onChanged: (e) {
                            setState(() {});
                          },
                          onSubmitted: (e) {
                            controller.searchStaff(tuKhoa: e);
                          },
                          autofocus: true,
                          decoration: InputDecoration(
                              isDense: true, border: InputBorder.none),
                        ))
                      : GunText(
                          'home_drawer.staff'.tr,
                          style: AppStyles.pageTitleStyle,
                        ),
                ],
              ),
              leading: [
                GunAction.icon(
                    iconFactor: 0.5,
                    onTap: () {
                      widget.homePageKey.currentState!.openDrawer();
                    },
                    icon: Image.asset('assets/images/drawer_menu.png'))
              ],
              actions: [
                GunAction.icon(
                  onTap: () {
                    setState(() {
                      isSearch = !isSearch;
                      textController.clear();
                    });
                    if (isSearch == false) {
                      staffController.searchStaff(tuKhoa: '');
                    }
                  },
                  iconFactor: isSearch ? 0.4 : 0.7,
                  icon: Image.asset(
                    isSearch
                        ? './assets/images/close_icon.png'
                        : './assets/images/search_icon.png',
                    color: AppColors.Orange,
                  ),
                ),
                if (isSearch == false)
                  GunAction.icon(
                    iconFactor: 0.5,
                    onTap: () {
                      Get.toNamed(AppRoutes.staff_edit);
                    },
                    icon: Image.asset(
                      './assets/images/add_icon.png',
                    ),
                  )
              ],
            ),
            child: _renderBody(controller.listStaff));
      },
    );
  }

  _renderBody(List<StaffDetailResponse> list) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...List.generate(
              list.length,
              (index) => Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: BaseInfoWidget(
                        onTap: () {
                          
                          Get.toNamed(AppRoutes.detail_staff,
                              arguments: list[index].nhanVien!.ID);
                        },
                        contextLeft1: 'staff.name'.tr,
                        contextLeft2: 'phone'.tr,
                        contextLeft3: 'staff.total_money'.tr,
                        contextRight1: list[index].nhanVien!.employeeName!,
                        contextRight2: list[index].nhanVien!.employeePhone!,
                        contextRight3: list[index].tongDoanhSo.toString()),
                  )),
        ],
      ),
    );
  }
}
