import 'dart:convert';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/data/entities/response/stock_detail_response.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/widgets/avatar_create.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/ui/widgets/stock_dialog.dart';
import 'package:permission_handler/permission_handler.dart';

class StockingCreatePage extends StatefulWidget {
  const StockingCreatePage({Key? key}) : super(key: key);

  @override
  State<StockingCreatePage> createState() => _StockingCreatePageState();
}

class _StockingCreatePageState extends State<StockingCreatePage>
    with TickerProviderStateMixin {
  StockController stockController = Get.find<StockController>();
  ProductController productController = Get.find<ProductController>();
  TextEditingController amountController = TextEditingController();
  FocusNode amountFocusNode = FocusNode();
  late TabController _tabController;
  int initialIndex = 0;
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    stockController.listStockCreate = [];
  }

  List<String> listCatalogue = ["Tất cả", "Khớp", "Lệch", "Chưa kiểm"];
  int count = 1;
  int thucTe = 0;
  int lech = 0;
  List<ChiTiet> list = [];
  List<StockDto> listStock = [];

  @override
  Widget build(BuildContext context) {
    return GetX<StockController>(
      builder: (controller) {
        print('objahsvdhjasvdhect ${controller.listStockCreate}');
        return GunPage(
          resizeToAvoidBottomInset: false,
          appBarBottom: GunAppBar(
            type: GunAppBarType.BOTTOM,
            height: 0,
          ),
          appBarTop: GunAppBar(
            type: GunAppBarType.TOP,
            title: 'Tạo phiếu kiểm',
            titleStyle: AppStyles.pageTitleStyle,
            leadingColor: Colors.black,
            actions: [
              GunAction.icon(
                onTap: () async {
                  await Get.toNamed(AppRoutes.stocking_goods);
                  setState(() {});
                },
                icon: Image.asset('assets/images/add_icon.png'),
              ),
              GunAction.icon(
                onTap: () async {
                  Map<Permission, PermissionStatus> statuses =
                      await [Permission.camera].request();
                  print(statuses[Permission.camera] ==
                      PermissionStatus.permanentlyDenied);
                  if (statuses[Permission.camera] ==
                      PermissionStatus.permanentlyDenied) {
                    AppSettings.openAppSettings();
                  } else {
                    var result = await Get.toNamed(AppRoutes.qr_code);

                    var response =
                        await stockController.getDetailProduct(result);
                    print(double.parse(response.sanPham!.prdSls!).toInt());
                    stockController.listStockCreate.add(ChiTiet(
                        id: response.sanPham!.id,
                        tenSP: response.sanPham!.prdName,
                        tonKho: double.parse(response.sanPham!.prdSls!).toInt(),
                        thucTe: 0,
                        lech:
                            -double.parse(response.sanPham!.prdSls!).toInt()));
                    stockController.onFresh();
                  }
                },
                icon: Image.asset('assets/images/menu_qr.png'),
              )
            ],
          ),
          child: Container(
            child: Column(
              children: [
                Expanded(
                    child: SingleChildScrollView(
                  child: _renderBody(controller.listStockCreate),
                )),
                _renderButton()
              ],
            ),
          ),
        );
      },
    );
  }

  _renderBody(List<ChiTiet> list12) {
    list = list12;
    _tabController = TabController(
      initialIndex: initialIndex,
      length: listCatalogue.length,
      vsync: this,
    );
    _tabController.addListener(
      () {
        if (_tabController.index == 0) {
          list = list12;
        } else if (_tabController.index == 1) {
          list = list12.where((e) => e.tonKho == e.thucTe).toList();
        } else if (_tabController.index == 2) {
          list = list12.where((e) => e.lech != 0).toList();
        } else if (_tabController.index == 3) {
          list = list12.where((e) => e.thucTe == 0).toList();
        }
      },
    );
    print('object');
    return Container(
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: 16),
            height: 40,
            padding: EdgeInsets.symmetric(vertical: 3, horizontal: 8),
            decoration: BoxDecoration(
                color: Color(0XFFE7E7E7),
                borderRadius: BorderRadius.circular(25.0)),
            child: TabBar(
                controller: _tabController,
                isScrollable: true,
                indicator: BoxDecoration(
                    color: AppColors.primary,
                    borderRadius: BorderRadius.circular(20.0)),
                labelColor: Colors.white,
                unselectedLabelColor: Colors.black,
                tabs: List.generate(listCatalogue.length,
                    (index) => Tab(text: listCatalogue[index]))),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    flex: 2,
                    child: GunText(
                      'Hàng kiểm',
                      style: AppStyles.DEFAULT_14,
                    )),
                Expanded(
                    flex: 1,
                    child: GunText(
                      'Tồn kho',
                      textAlign: TextAlign.center,
                      style: AppStyles.DEFAULT_14,
                    )),
                Expanded(
                    flex: 1,
                    child: GunText(
                      'Thực tế',
                      textAlign: TextAlign.center,
                      style: AppStyles.DEFAULT_14,
                    )),
                Expanded(
                    flex: 1,
                    child: GunText(
                      'Lệch',
                      textAlign: TextAlign.center,
                      style: AppStyles.DEFAULT_14,
                    )),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            child: Divider(
              color: AppColors.Gray4,
              height: 4,
            ),
          ),
          ...List.generate(
              list.length,
              (index) => Material(
                    color: AppColors.White,
                    child: InkWell(
                      onTap: () async {
                        await showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return StatefulBuilder(
                              builder: (BuildContext context,
                                  void Function(void Function()) setState) {
                                return StockDialog(
                                  title: list[index].tenSP!,
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                            child: GunText(
                                          'Tồn kho',
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14,
                                        )),
                                        Expanded(
                                            child: GunText(
                                          'Thực tế',
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14,
                                        )),
                                        Expanded(
                                            child: GunText(
                                          'Lệch',
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14,
                                        )),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                            child: GunText(
                                          list[index].tonKho.toString(),
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14.copyWith(
                                              color: AppColors.Orange),
                                        )),
                                        Expanded(
                                            child: GunText(
                                          '-',
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14.copyWith(
                                              color: AppColors.Orange),
                                        )),
                                        Expanded(
                                            child: GunText(
                                          '-',
                                          textAlign: TextAlign.center,
                                          style: AppStyles.DEFAULT_14.copyWith(
                                              color: AppColors.Orange),
                                        )),
                                      ],
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 20),
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 30),
                                      child: Row(
                                        children: [
                                          Expanded(
                                              child: GunText(
                                            'Số lượng:',
                                            style: AppStyles.DEFAULT_14,
                                          )),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              GunInkwell(
                                                onTap: () {
                                                  print('object');
                                                  if (count > 1) {
                                                    setState(() {
                                                      count--;
                                                      amountController.text =
                                                          count.toString();
                                                    });
                                                  }
                                                },
                                                padding: EdgeInsets.zero,
                                                child: Container(
                                                    width: 30,
                                                    height: 30,
                                                    child: Image.asset(
                                                        'assets/images/btn_delete.png')),
                                              ),
                                              Container(
                                                  width: 40,
                                                  margin: EdgeInsets.only(
                                                      left: 15, right: 15),
                                                  child: WidgetInput(
                                                    inputFormatters: <
                                                        TextInputFormatter>[
                                                      FilteringTextInputFormatter
                                                          .digitsOnly,
                                                      NumericalRangeFormatter(
                                                          min: 1,
                                                          max: list[index]
                                                              .tonKho!
                                                              .toDouble()),
                                                    ],
                                                    keyboardType: TextInputType
                                                        .numberWithOptions(
                                                            decimal: false,
                                                            signed: false),
                                                    focusNode: amountFocusNode,
                                                    textAlign: TextAlign.center,
                                                    controller:
                                                        amountController,
                                                    onChanged: (e) {
                                                      if (e.isNotEmpty) {
                                                        count = int.parse(e);
                                                      }
                                                      setState(() {});
                                                      amountController
                                                              .selection =
                                                          TextSelection.fromPosition(
                                                              TextPosition(
                                                                  offset: amountController
                                                                      .text
                                                                      .length));
                                                    },
                                                  )),
                                              GunInkwell(
                                                onTap: () {
                                                  setState(() {
                                                    count = count + 1;
                                                    amountController.text =
                                                        count.toString();
                                                  });
                                                },
                                                padding: EdgeInsets.zero,
                                                child: Container(
                                                    width: 30,
                                                    height: 30,
                                                    child: Image.asset(
                                                        'assets/images/btn_add.png')),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                  onTapLeft: () {
                                    Get.back();
                                    setState(() {
                                      list[index].thucTe = count;
                                      list[index].lech =
                                          count - list[index].tonKho!;
                                      amountController.text = "";
                                    });
                                  },
                                  onTapRight: () {
                                    Get.back();
                                    setState(() {
                                      list[index].thucTe =
                                          list[index].thucTe! + count;
                                      list[index].lech = list[index].thucTe! -
                                          list[index].tonKho!;
                                      amountController.text = "";
                                    });
                                  },
                                );
                              },
                            );
                          },
                        );
                        setState(() {});
                      },
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 30,
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                    flex: 2,
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 20),
                                      child: GunText(
                                        list[index].tenSP!,
                                        style: AppStyles.DEFAULT_14,
                                      ),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 20),
                                      child: GunText(
                                        list[index].tonKho!.toString(),
                                        textAlign: TextAlign.center,
                                        style: AppStyles.DEFAULT_14,
                                      ),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 20),
                                      child: GunText(
                                        list[index].thucTe!.toString(),
                                        textAlign: TextAlign.center,
                                        style: AppStyles.DEFAULT_14,
                                      ),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 20),
                                      child: GunText(
                                        list[index].lech!.toString(),
                                        textAlign: TextAlign.center,
                                        style: AppStyles.DEFAULT_14,
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          if (index != list.length - 1)
                            Container(
                              width: double.infinity,
                              child: Divider(
                                color: AppColors.Gray4,
                                height: 4,
                              ),
                            ),
                        ],
                      ),
                    ),
                  ))
        ],
      ),
    );
  }

  _renderButton() {
    return Row(
      children: [
        ...List.generate(
          2,
          (index) => Expanded(
            child: Container(
              padding: EdgeInsets.all(8),
              child: WidgetButton(
                onTap: () {
                  List<StockDto> list = [];
                  stockController.listStockCreate.forEach((e) {
                    list.add(StockDto(
                        id: e.id, inventory: e.tonKho, quantity: e.thucTe));
                  });
                  print(list);
                  if (list.isNotEmpty) {
                    print('list ${list}');
                    stockController.stockCreate(
                        adjustStatus: index,
                        data: list,
                        callback: () {
                          Get.back();

                          stockController.getStockSearch(tuKhoa: '');
                        },
                        notes: '');
                  }
                },
                isFullWidth: true,
                text: index == 0 ? 'Lưu tạm' : 'Hoàn thành'.tr,
                textStyle: AppStyles.DEFAULT_18_BOLD.copyWith(
                    color: index == 0 ? AppColors.Blue : AppColors.White),
                backgroundColor: index == 0 ? AppColors.Blue4 : AppColors.Blue,
              ),
            ),
          ),
        )
      ],
    );
  }
}
