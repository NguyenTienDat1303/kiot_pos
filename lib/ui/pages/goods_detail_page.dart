import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GoodsDetailPage extends StatefulWidget {
  const GoodsDetailPage({Key? key}) : super(key: key);

  @override
  State<GoodsDetailPage> createState() => _GoodsDetailPageState();
}

class _GoodsDetailPageState extends State<GoodsDetailPage> {
  OrderCreateProductController _controller =
      Get.find<OrderCreateProductController>();
  ProductController productController = Get.find<ProductController>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      productController
          .getDetailProduct(productController.idProduct.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetX<ProductController>(
      builder: (controller) {
        return GunPage(
          appBarBottom: GunAppBar(
            type: GunAppBarType.BOTTOM,
            height: 0,
          ),
          appBarTop: GunAppBar(
            type: GunAppBarType.TOP,
            title: controller.detailProduct != null
                ? controller.detailProduct!.sanPham!.prdName
                : '',
            titleStyle: AppStyles.pageTitleStyle,
            leadingColor: Colors.black,
            actions: [
              GunAction.icon(
                onTap: () {
                  Get.toNamed(AppRoutes.goods_edit);
                },
                icon: Image.asset('./assets/images/edit_icon.png', width: 24.r),
              )
            ],
          ),
          child: _renderBody(controller.detailProduct),
        );
      },
    );
  }

  _renderBody(DetailProductResponse? data) {
    return data != null
        ? Container(
            color: AppColors.Background,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    color: AppColors.White,
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      children: [
                        Container(
                            width: 0.3.sw,
                            height: 0.3.sw,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10000),
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10000),
                              child: data.sanPham!.prdImageUrl != null &&
                                      data.sanPham!.prdImageUrl != ''
                                  ? CachedNetworkImage(
                                      imageUrl: data.sanPham!.prdImageUrl!,
                                      fit: BoxFit.cover,
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                    )
                                  : Image.asset('./assets/images/logo.png'),
                            )),
                        OneRow(
                            title: 'product.name'.tr,
                            subtitle: data.sanPham!.prdName ?? ''),
                        OneRow(
                            title: 'stock.store'.tr,
                            subtitle: double.parse(data.sanPham!.prdSls ?? '0')
                                .toInt()
                                .toString()),
                        OneRow(
                            title: 'product.code'.tr,
                            subtitle: data.sanPham!.prdCode ?? ''),
                        OneRow(
                            title: 'unit'.tr,
                            subtitle: data.sanPham!.unitName ?? ''),
                        OneRow(
                            title: 'price_origin'.tr,
                            subtitle: AppValue.APP_MONEY_FORMAT
                                .format(data.sanPham!.prdOriginPrice ?? 0)),
                        OneRow(
                            title: 'partial'.tr,
                            subtitle: AppValue.APP_MONEY_FORMAT
                                .format(data.sanPham!.prdSellPrice ?? 0)),
                        OneRow(
                            title: 'wholesale'.tr,
                            subtitle: AppValue.APP_MONEY_FORMAT
                                .format(data.sanPham!.prdSellPrice2)),
                        OneRow(
                            title: 'catalogue'.tr,
                            subtitle: data.danhMuc ?? ''),
                        OneRow(
                            title: 'nsx'.tr, subtitle: data.nhaSanXuat ?? ''),
                        OneRow(
                            title: 'product.min'.tr,
                            subtitle: data.sanPham!.prdMin.toString()),
                        OneRow(
                            title: 'product.max'.tr,
                            subtitle: data.sanPham!.prdMax.toString()),
                        OneRow(
                            title: 'is_edit_price_nevigate'.tr,
                            subtitle: data.sanPham!.prdEditPrice == 0
                                ? 'no'.tr
                                : 'yes'.tr),
                        OneRow(
                            title: 'is_sell_nevigate'.tr,
                            subtitle: data.sanPham!.prdAllownegative == 0
                                ? 'no'.tr
                                : 'yes'.tr),
                      ],
                    ),
                  ),
                  if (data.quyDoi != null && data.quyDoi!.isNotEmpty)
                    _renderUnitConversion(data.quyDoi!),
                  _renderButton()
                ],
              ),
            ),
          )
        : SizedBox.shrink();
  }

  _renderUnitConversion(List<UnitConversionResponse>? list) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: GunText(
              'unit_convertion'.tr,
              style: AppStyles.DEFAULT_18_BOLD.copyWith(color: AppColors.Blue),
            ),
          ),
          if (list != null)
            ...List.generate(
                list.length,
                (index) => Container(
                      color: AppColors.White,
                      padding: EdgeInsets.all(12),
                      child: Column(
                        children: [
                          OneRow(
                              title: 'unit_name'.tr,
                              subtitle: list[index].unitName!),
                          OneRow(
                              title: 'quantity'.tr,
                              subtitle: list[index].number.toString()),
                          OneRow(
                              title: 'price_origin'.tr,
                              subtitle: AppValue.APP_MONEY_FORMAT
                                  .format(list[index].prdOriginPrice!)),
                          OneRow(
                              title: 'partial'.tr,
                              subtitle: AppValue.APP_MONEY_FORMAT
                                  .format(list[index].prdSellPrice!)),
                          OneRow(
                              title: 'wholesale'.tr,
                              subtitle: AppValue.APP_MONEY_FORMAT
                                  .format(list[index].prdSellPrice2!)),
                          OneRow(
                              title: 'sp.code'.tr,
                              subtitle: list[index].prdCode!)
                        ],
                      ),
                    )),
        ],
      ),
    );
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.all(30),
      child: WidgetButton(
        text: 'product.delete'.tr,
        isFullWidth: true,
        onTap: () {
          Get.dialog(DialogCustom(
              title: 'Thông báo',
              isTwoButton: true,
              nameButtonLeft: 'Huỷ',
              nameButtonRight: 'Xoá',
              onTapLeft: () {
                Get.back();
              },
              onTapRight: () {
                Get.back();
                productController.productDelete(productController.idProduct,
                    callback: () {
                  Get.back();
                  _controller.loadProductList(null, '');
                });
              },
              children: [
                Text('Bạn có chắc chắn muốnn xoá ?',
                    style: AppStyles.DEFAULT_16),
              ]));
        },
      ),
    );
  }
}
