import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class PrinterCreatePage extends StatefulWidget {
  const PrinterCreatePage({Key? key}) : super(key: key);

  @override
  State<PrinterCreatePage> createState() => _PrinterCreatePageState();
}

class _PrinterCreatePageState extends State<PrinterCreatePage> {
  PrinterCreateController _controller = Get.find<PrinterCreateController>();
  final _formKey = GlobalKey<FormState>();

  TextEditingController _descriptionEditController = TextEditingController();
  TextEditingController _kieuInEditController = TextEditingController();
  TextEditingController _khoGiayEditController = TextEditingController();
  TextEditingController _ipEditController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'printer_create.title_add'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
        ),
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            TextFormField(
                                controller: _descriptionEditController,
                                onChanged: (val) {
                                  _controller.description = val;
                                },
                                autovalidateMode: AutovalidateMode.always,
                                validator: FormBuilderValidators.required(
                                    errorText:
                                        "printer_create.description_required"
                                            .tr),
                                style: AppStyles.normalTextStyle,
                                // textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.center,
                                decoration: InputDecoration(
                                  isDense: true,
                                  // alignLabelWithHint: true,
                                  hintText: 'printer_create.description'.tr,
                                  contentPadding: EdgeInsets.only(bottom: 10),
                                  prefixIconConstraints:
                                      BoxConstraints(minWidth: 0, minHeight: 0),
                                )),
                            SizedBox(
                              height: 30,
                            ),
                            GestureDetector(
                              onTap: () {
                                Get.bottomSheet(Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 30, horizontal: 10),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                        topLeft: const Radius.circular(15),
                                        topRight: const Radius.circular(15),
                                      )),
                                  child: SingleChildScrollView(
                                      child: Column(
                                          children: List.generate(
                                              PrintFormat.supported.length,
                                              (index) => Material(
                                                    color: Colors.transparent,
                                                    child: InkWell(
                                                      onTap: () {
                                                        _controller.kieuIn =
                                                            PrintFormat
                                                                    .supported[
                                                                index];
                                                        _kieuInEditController
                                                                .text =
                                                            "print_format.${_controller.kieuIn}"
                                                                .tr;
                                                        Get.back();
                                                      },
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                            border: Border(
                                                                bottom: BorderSide(
                                                                    width: 1,
                                                                    color: Colors
                                                                        .grey
                                                                        .shade300))),
                                                        width: double.infinity,
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 10,
                                                                right: 10,
                                                                top: 10,
                                                                bottom: 20),
                                                        child: Text(
                                                            "print_format.${PrintFormat.supported[index]}"
                                                                .tr),
                                                      ),
                                                    ),
                                                  )))),
                                ));
                              },
                              child: AbsorbPointer(
                                absorbing: true,
                                child: TextFormField(
                                    enabled: false,
                                    controller: _kieuInEditController,
                                    onChanged: (val) {},
                                    autovalidateMode: AutovalidateMode.always,
                                    validator: FormBuilderValidators.required(
                                        errorText:
                                            "printer_create.kieu_in_required"
                                                .tr),
                                    style: AppStyles.normalTextStyle,
                                    // textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: InputDecoration(
                                      isDense: true,
                                      // alignLabelWithHint: true,
                                      hintText: 'printer_create.kieu_in'.tr,
                                      contentPadding:
                                          EdgeInsets.only(bottom: 10),
                                      prefixIconConstraints: BoxConstraints(
                                          minWidth: 0, minHeight: 0),
                                    )),
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            GestureDetector(
                              onTap: () {
                                Get.bottomSheet(Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 30, horizontal: 10),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                        topLeft: const Radius.circular(15),
                                        topRight: const Radius.circular(15),
                                      )),
                                  child: SingleChildScrollView(
                                      child: Column(
                                          children: List.generate(
                                              PrintrPaperSize.supported.length,
                                              (index) => Material(
                                                    color: Colors.transparent,
                                                    child: InkWell(
                                                      onTap: () {
                                                        Get.back();
                                                        _controller.khoGiay =
                                                            PrintrPaperSize
                                                                    .supported[
                                                                index];
                                                        _khoGiayEditController
                                                                .text =
                                                            "printr_paper_size.${_controller.khoGiay}"
                                                                .tr;
                                                      },
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                            border: Border(
                                                                bottom: BorderSide(
                                                                    width: 1,
                                                                    color: Colors
                                                                        .grey
                                                                        .shade300))),
                                                        width: double.infinity,
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 10,
                                                                right: 10,
                                                                top: 10,
                                                                bottom: 20),
                                                        child: Text(
                                                            "printr_paper_size.${PrintrPaperSize.supported[index]}"
                                                                .tr),
                                                      ),
                                                    ),
                                                  )))),
                                ));
                              },
                              child: AbsorbPointer(
                                absorbing: true,
                                child: TextFormField(
                                    enabled: false,
                                    controller: _khoGiayEditController,
                                    onChanged: (val) {},
                                    autovalidateMode: AutovalidateMode.always,
                                    validator: FormBuilderValidators.required(
                                        errorText:
                                            "printer_create.kho_giay_required"
                                                .tr),
                                    style: AppStyles.normalTextStyle,
                                    // textAlign: TextAlign.center,
                                    textAlignVertical: TextAlignVertical.center,
                                    decoration: InputDecoration(
                                      isDense: true,
                                      // alignLabelWithHint: true,
                                      hintText: 'printer_create.kho_giay'.tr,
                                      contentPadding:
                                          EdgeInsets.only(bottom: 10),
                                    )),
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            TextFormField(
                                controller: _ipEditController,
                                onChanged: (val) {
                                  _controller.ip = val;
                                },
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: false, signed: false),
                                autovalidateMode: AutovalidateMode.always,
                                validator: FormBuilderValidators.required(
                                    errorText: "printer_create.ip_required".tr),
                                style: AppStyles.normalTextStyle,
                                // textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.center,
                                decoration: InputDecoration(
                                  isDense: true,
                                  // alignLabelWithHint: true,
                                  hintText: 'printer_create.ip'.tr,
                                  contentPadding: EdgeInsets.only(bottom: 10),
                                )),
                            SizedBox(
                              height: 30,
                            ),
                            Container(
                              width: double.infinity,
                              child: TextButton(
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        AppColors.primary),
                                    shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20),
                                    ))),
                                onPressed: () async {
                                  // Validate returns true if the form is valid, or false otherwise.
                                  if (_formKey.currentState!.validate()) {
                                    var response =
                                        await _controller.printCreate();
                                    Get.back();
                                    Get.dialog(DialogCustom(
                                        title: "dialog.title_notify".tr,
                                        children: [
                                          Text(
                                            "${response.message}",
                                            textAlign: TextAlign.center,
                                          )
                                        ]));
                                  } else {
                                    // ScaffoldMessenger.of(context)
                                    //     .showSnackBar(
                                    //   const SnackBar(content: Text('Ko dc')),
                                    // );
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.all(3),
                                  child: Text(
                                    'printer_create.submit_add'.tr,
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}
