import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/data/entities/response/category_response.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/res.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/ui.dart';
import 'package:get/get.dart';

class CatalogueDetailPage extends StatefulWidget {
  const CatalogueDetailPage({Key? key}) : super(key: key);

  @override
  State<CatalogueDetailPage> createState() => _CatalogueDetailPageState();
}

class _CatalogueDetailPageState extends State<CatalogueDetailPage> {
  OrderCreateProductController _controller =
      Get.find<OrderCreateProductController>();
  TextEditingController textController = TextEditingController();
  CatalogueController catalogueController = Get.find<CatalogueController>();
  CategoryData? data;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (Get.arguments != null) {
      setState(() {
        data = Get.arguments;
      });
    }
  }

  onDelete() {
    Get.dialog(DialogCustom(
        title: 'Thông báo',
        isTwoButton: true,
        nameButtonLeft: 'Huỷ',
        nameButtonRight: 'Xoá',
        onTapLeft: () {
          Get.back();
        },
        onTapRight: () {
          Get.back();
          catalogueController.catalogueDelete(
              danhMucID: data!.danhMuc!.id!,
              callback: () {
                Get.back();
                _controller.loadCategoryListNotAll();
              });
        },
        children: [
          Text('Bạn có chắc chắn muốnn xoá ?', style: AppStyles.DEFAULT_16),
        ]));
  }

  onEdit() {
    setState(() {
      textController.text = data!.danhMuc!.prdGroupName!;
    });
    Get.dialog(DialogCustom(
        backDropTap: () {
          null;
        },
        title: 'Sửa danh mục hàng hóa',
        isTwoButton: true,
        nameButtonLeft: 'Huỷ',
        nameButtonRight: 'save'.tr,
        onTapLeft: () {
          Get.back();
        },
        onTapRight: () async {
          Get.back();
          var result = await catalogueController.catalogueEdit(
              tenDanhMuc: textController.text, danhMucID: data!.danhMuc!.id!);
          if (result.type == "success") {
            Get.dialog(DialogCustom(
                title: 'Thông báo',
                onTapOK: () {
                  Get.back();
                  Get.back();

                  _controller.loadCategoryListNotAll();
                },
                children: [
                  Text(result.message!, style: AppStyles.DEFAULT_16)
                ]));
          }
        },
        children: [
          TextField(
            controller: textController,
            onChanged: (e) {
              setState(() {});
            },
            autofocus: true,
          )
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
      appBarBottom: GunAppBar(
        type: GunAppBarType.BOTTOM,
        height: 0,
        decoration: BoxDecoration(color: AppColors.Background),
      ),
      appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'Chi tiết danh mục hàng hóa',
          titleStyle: AppStyles.pageTitleStyle,
          decoration: BoxDecoration(color: AppColors.Background),
          leadingColor: Colors.black),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(color: AppColors.Background),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (data != null)
                Container(
                  child: CellWhite(
                      child: Column(
                    children: [
                      GunText(
                        data!.danhMuc!.prdGroupName!,
                        style: AppStyles.DEFAULT_16_BOLD
                            .copyWith(color: AppColors.Gray1),
                      ),
                      GunText(
                        data!.soLuongSanPham! + ' ' + 'goods'.tr,
                        style: AppStyles.DEFAULT_16_BOLD
                            .copyWith(color: AppColors.Blue),
                      ),
                      Row(
                        children: [
                          ...List.generate(
                              2,
                              (index) => Expanded(
                                    child: Container(
                                      padding: EdgeInsets.all(16),
                                      child: WidgetButton(
                                        onTap: () {
                                          if (index == 0) {
                                            onDelete();
                                          } else {
                                            onEdit();
                                          }
                                        },
                                        text: index == 0
                                            ? 'delete'.tr
                                            : 'Chỉnh sửa'.tr,
                                        backgroundColor: index == 0
                                            ? AppColors.Blue4
                                            : AppColors.Blue,
                                        textStyle: AppStyles.DEFAULT_18_BOLD
                                            .copyWith(
                                                color: index == 0
                                                    ? AppColors.Blue
                                                    : AppColors.White),
                                        isFullWidth: true,
                                      ),
                                    ),
                                  ))
                        ],
                      )
                    ],
                  )),
                ),
              if (data != null)
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: GunText(
                    'Hàng hóa thuộc danh mục',
                    style: AppStyles.DEFAULT_16_BOLD
                        .copyWith(color: AppColors.Blue),
                  ),
                ),
              if (data != null)
                ...List.generate(data!.cacSanPham!.length,
                    (index) => CatalogueWidget(data: data!.cacSanPham![index]))
            ],
          ),
        ),
      ),
    );
  }
}

class CatalogueWidget extends StatelessWidget {
  final ProductData data;
  final GestureTapCallback? onTap;
  const CatalogueWidget({
    Key? key,
    required this.data,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Material(
        color: AppColors.White,
        child: InkWell(
          onTap: onTap,
          child: AnimatedContainer(
            duration: Duration(milliseconds: 300),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey.shade300, width: 1),
                borderRadius: BorderRadius.circular(5)),
            child: Container(
              padding: EdgeInsets.all(5),
              child: Row(
                children: [
                  Container(
                      width: 45,
                      height: 45,
                      margin: EdgeInsets.only(right: 10),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: WidgetCachedImage(
                              imageUrl: data.prdImageUrl ?? ""))),
                  Expanded(
                      child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          data.prdName!,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Container(
                          constraints: BoxConstraints(minHeight: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                AppValue.APP_MONEY_FORMAT
                                    .format(data.prdSellPrice!),
                                style: TextStyle(
                                    color: Color(0XFFE27C04),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                '${'stock'.tr}${double.parse(data.prdSls ?? '0').toInt()}',
                                // +AppValue.APP_MONEY_FORMAT.format(data.prdSls!).toString(),
                                style: TextStyle(
                                    color: AppColors.Blue,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
