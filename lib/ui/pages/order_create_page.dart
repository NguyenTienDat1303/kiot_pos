import 'package:app_settings/app_settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:permission_handler/permission_handler.dart';

class OrderCreatePage extends StatefulWidget {
  final GlobalKey<ScaffoldState>? homePageKey;
  const OrderCreatePage({
    Key? key,
    this.homePageKey,
  }) : super(key: key);

  @override
  State<OrderCreatePage> createState() => _OrderCreatePageState();
}

class _OrderCreatePageState extends State<OrderCreatePage> {
  OrderCreateController _controller = Get.find<OrderCreateController>();
  HomeController _homeController = Get.find<HomeController>();
  bool paymentDetailVisiable = false;

  OrderTypeLoad _type = OrderTypeLoad.payment;
  TableData? _tableData;
  int? _orderId;

  List<String> orderMenuList = ["order.move_table".tr, "order.print_stamp".tr];

  @override
  void initState() {
    super.initState();
    if (widget.homePageKey == null) {}
    _controller.tableData = Get.arguments["table"];
    _orderId = Get.arguments["orderId"];
    _controller.loadTable();
    // _controller.getOrderDetail(255);
  }

  String getTitle() {
    return "abc";
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: _type == OrderTypeLoad.payment
              ? 'order_create.title_3_4'.tr
              : 'order.title_edit'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
          leading: widget.homePageKey != null
              ? [
                  GunAction.icon(
                      onTap: () {
                        widget.homePageKey!.currentState!.openDrawer();
                      },
                      icon: Image.asset('assets/images/drawer_menu.png'))
                ]
              : null,
          actions: [
            if (_homeController.authorization([
              KiotVersion.karaoke,
            ], [
              KiotPermisson.pay_collect_view,
            ]))
              GunAction.icon(
                icon: Image.asset('assets/images/order_create_staff.png'),
                onTap: () {
                  Get.toNamed(AppRoutes.order_staff);
                },
              ),
            GunAction.icon(
              icon: Image.asset('assets/images/order_create_gift.png'),
              onTap: () async {
                await Get.toNamed(AppRoutes.order_create_product,
                    arguments: OrderProductSelectType.gift);
                _controller.syncData();
              },
            ),
            GunAction.icon(
              icon: Image.asset('assets/images/menu_qr.png'),
              onTap: () async {
                Map<Permission, PermissionStatus> statuses =
                    await [Permission.camera].request();
                print(statuses[Permission.camera] ==
                    PermissionStatus.permanentlyDenied);
                if (statuses[Permission.camera] ==
                    PermissionStatus.permanentlyDenied) {
                  AppSettings.openAppSettings();
                } else {
                  var result = await Get.toNamed(AppRoutes.qr_code);
                  if (result != null) {
                    await _controller.addProductFromQr(result);
                    await _controller.syncData();
                  }
                }
              },
            ),
            GunAction.icon(
              icon: Image.asset('assets/images/menu_add.png'),
              onTap: () async {
                await Get.toNamed(AppRoutes.order_create_product,
                    arguments: OrderProductSelectType.normal);
                _controller.syncData();
              },
            ),
          ],
        ),
        appBarBottom: GunAppBar(
          type: GunAppBarType.BOTTOM,
          height: 0,
          // backgroundColor: Color(0XFFE27C04),
        ),
        child: Stack(
          children: [
            Positioned.fill(
              child: Column(children: [
                Expanded(
                    child: SingleChildScrollView(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    children: [
                      GetX<OrderCreateController>(
                        builder: (controller) {
                          return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Get.toNamed(AppRoutes.order_customer);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        left: 4, right: 4, top: 0, bottom: 16),
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: AppColors.primary),
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Row(children: [
                                      Expanded(
                                        child: Text(
                                          controller.orderDetail.tenkhach !=
                                                  null
                                              ? controller.orderDetail.tenkhach!
                                                  .customerName!
                                              : "order_create.customer".tr,
                                          style: AppStyles.normalTextStyle
                                              .copyWith(
                                                  color: Color(0XFFBDBDBD),
                                                  fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      if (_homeController.authorization([
                                        KiotVersion.coffee,
                                        KiotVersion.karaoke,
                                        KiotVersion.milktea,
                                        KiotVersion.shop,
                                        KiotVersion.enter
                                      ], [
                                        KiotPermisson.customer_create,
                                      ]))
                                        GunInkwell(
                                          onTap: () {
                                            Get.toNamed(
                                                AppRoutes.customer_create);
                                          },
                                          padding: EdgeInsets.zero,
                                          child: Container(
                                              width: 24,
                                              child: Image.asset(
                                                  'assets/images/order_create_customer_add.png')),
                                        ),
                                    ]),
                                  ),
                                ),
                                ...List.generate(
                                    controller.orderDetail.sanPhams!.length,
                                    (index) {
                                  var productData =
                                      controller.orderDetail.sanPhams![index];
                                  return OrderItemWidget(
                                    data: productData,
                                    onTap: () {
                                      Get.bottomSheet(
                                        Container(
                                          height: Get.height * 80 / 100,
                                          child: OrderProductBottomSheet(
                                            data: productData,
                                            onToppingTap: () async {
                                              Get.back();
                                              var result = await Get.toNamed(
                                                  AppRoutes.order_topping,
                                                  arguments: productData);
                                              _controller.syncData();
                                            },
                                            onSubmit: (productData) {
                                              controller.updateProductAt(
                                                  OrderProductSelectType.normal,
                                                  index,
                                                  productData);
                                              Get.back();
                                            },
                                          ),
                                        ),
                                        isScrollControlled: true,
                                      );
                                    },
                                    onDeleteTap: () {
                                      controller.removeProductAt(
                                          OrderProductSelectType.normal, index);
                                    },
                                    onCopyTap: (context) {
                                      controller.copyProduct(productData);
                                    },
                                  );
                                }),
                                if (controller
                                    .orderDetail.khuyenMai!.isNotEmpty)
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    margin: EdgeInsets.symmetric(vertical: 20),
                                    child: Text(
                                      'order_create.promotion'.tr,
                                      style: AppStyles.normalTextStyle.copyWith(
                                          color: AppColors.primary,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ...List.generate(
                                    controller.orderDetail.khuyenMai!.length,
                                    (index) {
                                  var productData =
                                      controller.orderDetail.khuyenMai![index];
                                  return OrderItemWidget(
                                    data: productData,
                                    onTap: () {
                                      Get.bottomSheet(
                                        OrderGiftProductBottomSheet(
                                          data: productData,
                                          onSubmit: (productData) {
                                            controller.updateProductAt(
                                                OrderProductSelectType.gift,
                                                index,
                                                productData);
                                            Get.back();
                                          },
                                        ),
                                        isScrollControlled: false,
                                      );
                                    },
                                    onDeleteTap: () {
                                      controller.removeProductAt(
                                          OrderProductSelectType.gift, index);
                                    },
                                    onCopyTap: (context) {
                                      controller.copyProduct(productData);
                                    },
                                  );
                                }),
                                if (controller.orderDetail.nhanVien!.isNotEmpty)
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    margin: EdgeInsets.symmetric(vertical: 20),
                                    child: Text(
                                      'order_create.staff'.tr,
                                      style: AppStyles.normalTextStyle.copyWith(
                                          color: AppColors.primary,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                Column(
                                  children: List.generate(
                                      controller.orderDetail.nhanVien!.length,
                                      (index) => Container(
                                            margin: EdgeInsets.only(bottom: 10),
                                            child: OrderStaffWidget(
                                              data: controller
                                                  .orderDetail.nhanVien![index],
                                              onDeleteTap: () {
                                                _controller.deleteStaff(
                                                    controller.orderDetail
                                                        .nhanVien![index]);
                                              },
                                              onEnterTap: () {
                                                ComponentUtils
                                                    .showCupertinoDatePicker(
                                                        context,
                                                        mode:
                                                            CupertinoDatePickerMode
                                                                .dateAndTime,
                                                        maximumDate:
                                                            DateTime(2900),
                                                        onSubmitted:
                                                            (val) async {
                                                  print("teststart $val");
                                                  await _controller
                                                      .changeStartStaff(
                                                          controller.orderDetail
                                                              .nhanVien![index],
                                                          val);
                                                });
                                              },
                                              onStopTap: () {
                                                _controller.stopStaff(controller
                                                    .orderDetail
                                                    .nhanVien![index]);
                                              },
                                            ),
                                          )),
                                )
                              ]);
                        },
                      ),
                    ],
                  ),
                )),
                GetX<OrderCreateController>(
                  builder: (controller) {
                    return Column(
                      children: [
                        Container(
                            height: 0,
                            child: Text("${controller.orderDetail}")),
                        if (_type == OrderTypeLoad.payment)
                          Column(
                            children: [
                              Material(
                                color: Color(0XFFFFEBD4),
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      paymentDetailVisiable =
                                          !paymentDetailVisiable;
                                    });
                                  },
                                  child: Column(children: [
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 8),
                                      child: Row(children: [
                                        Expanded(
                                          child: RichText(
                                            text: TextSpan(children: [
                                              TextSpan(
                                                  text:
                                                      "order_create.total_money"
                                                          .tr,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                              TextSpan(
                                                  text:
                                                      " (${controller.orderDetail.tongSoLuong})",
                                                  style: TextStyle(
                                                      color: Color(0XFFE27C04),
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                            ]),
                                          ),
                                        ),
                                        Expanded(
                                            child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Text(
                                                "${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tongThanhToan ?? 0)}",
                                                textAlign: TextAlign.end,
                                                style: TextStyle(
                                                    color: Color(0XFFE27C04),
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                            Icon(
                                              Icons.arrow_drop_down,
                                              color: Color(0XFFE27C04),
                                            )
                                          ],
                                        ))
                                      ]),
                                    ),
                                    if (paymentDetailVisiable)
                                      Column(
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 2),
                                                child: Row(children: [
                                                  Expanded(
                                                    child: Text(
                                                        "order_create.total_discount"
                                                            .tr,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400)),
                                                  ),
                                                  Expanded(
                                                      child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                          "- ${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tongChietKhau ?? 0)}",
                                                          textAlign:
                                                              TextAlign.end,
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .primary,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600)),
                                                      Icon(
                                                        Icons.arrow_drop_down,
                                                        color:
                                                            Color(0XFFE27C04),
                                                      )
                                                    ],
                                                  ))
                                                ]),
                                              ),
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 2),
                                                child: Row(children: [
                                                  Expanded(
                                                    child: Text(
                                                        "order_create.vat".tr,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600)),
                                                  ),
                                                  Expanded(
                                                      child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                          "${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tienVAT ?? 0)}",
                                                          textAlign:
                                                              TextAlign.end,
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .primary,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400)),
                                                      Icon(
                                                        Icons.arrow_drop_down,
                                                        color:
                                                            Color(0XFFE27C04),
                                                      )
                                                    ],
                                                  ))
                                                ]),
                                              ),
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 2),
                                                child: Row(children: [
                                                  Expanded(
                                                    child: Text(
                                                        "order_create.customer_pay"
                                                            .tr,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600)),
                                                  ),
                                                  Expanded(
                                                      child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                          "${AppValue.APP_MONEY_FORMAT.format(controller.orderDetail.tongThanhToan ?? 0)}",
                                                          textAlign:
                                                              TextAlign.end,
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .primary,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600)),
                                                      Icon(
                                                        Icons.arrow_drop_down,
                                                        color:
                                                            Color(0XFFE27C04),
                                                      )
                                                    ],
                                                  ))
                                                ]),
                                              ),
                                            ],
                                          ),
                                        ],
                                      )
                                  ]),
                                ),
                              ),
                              Container(
                                color: Color(0XFFE27C04),
                                height: 56,
                                child: Row(children: [
                                  Expanded(
                                      flex: 2,
                                      child: GunInkwell(
                                        padding: EdgeInsets.zero,
                                        onTap: () {
                                          Get.bottomSheet(Container(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 30, horizontal: 10),
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    topLeft:
                                                        const Radius.circular(
                                                            15),
                                                    topRight:
                                                        const Radius.circular(
                                                            15),
                                                  )),
                                              child: SingleChildScrollView(
                                                  child: Column(
                                                children: List.generate(
                                                    orderMenuList.length,
                                                    (index) => GunInkwell(
                                                          onTap: () async {
                                                            Get.back();
                                                            if (index == 0) {
                                                              if (_controller
                                                                      .orderId !=
                                                                  null) {
                                                                try {
                                                                  TableData
                                                                      tableData =
                                                                      await Get.toNamed(
                                                                          AppRoutes
                                                                              .move_table);
                                                                  var response = await _controller.moveTable(
                                                                      _controller
                                                                          .tableData!,
                                                                      tableData,
                                                                      TableAction
                                                                          .move);
                                                                  Get.until((route) =>
                                                                      route
                                                                          .settings
                                                                          .name ==
                                                                      AppRoutes
                                                                          .home);
                                                                  Get.dialog(
                                                                      DialogCustom(
                                                                          title:
                                                                              "dialog.title_notify".tr,
                                                                          onTapOK: () {
                                                                            Get.back();
                                                                          },
                                                                          children: [
                                                                        Text(
                                                                            "${response.message}")
                                                                      ]));
                                                                  ;
                                                                } catch (e) {}
                                                              }
                                                            }
                                                            if (index == 1) {
                                                              var printManagerController =
                                                                  Get.find<
                                                                      PrintManagerController>();
                                                              printManagerController.printForm(
                                                                  PrintAction
                                                                      .stamp,
                                                                  tableData:
                                                                      _controller
                                                                          .tableData,
                                                                  loadTableResponse:
                                                                      _controller
                                                                          .orderDetail);
                                                            }
                                                          },
                                                          padding:
                                                              EdgeInsets.zero,
                                                          child: Container(
                                                            decoration: BoxDecoration(
                                                                border: Border(
                                                                    bottom: BorderSide(
                                                                        width:
                                                                            1,
                                                                        color: Colors
                                                                            .grey
                                                                            .shade300))),
                                                            width:
                                                                double.infinity,
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 10,
                                                                    right: 10,
                                                                    top: 10,
                                                                    bottom: 20),
                                                            child: Text(
                                                                orderMenuList[
                                                                    index]),
                                                          ),
                                                        )),
                                              ))));
                                        },
                                        child: Container(
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  right: BorderSide(
                                                      width: .6,
                                                      color: Colors.white))),
                                          child: Stack(
                                            alignment:
                                                AlignmentDirectional.center,
                                            children: [
                                              Positioned.fill(
                                                child: Center(
                                                  child: Container(
                                                      width: 24,
                                                      child: Image.asset(
                                                          'assets/images/order_create_invoice.png')),
                                                ),
                                              ),
                                              Positioned(
                                                top: 8,
                                                right: 20,
                                                child: Container(
                                                  width: 18,
                                                  height: 18,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                      color: AppColors.primary,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              500)),
                                                  child: Text(
                                                    "1",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )),
                                  if (_homeController.authorization(
                                      [KiotVersion.shop, KiotVersion.enter],
                                      []))
                                    Expanded(
                                      flex: 4,
                                      child: GunInkwell(
                                        padding: EdgeInsets.zero,
                                        onTap: () {
                                          Get.offAllNamed(AppRoutes.home);
                                        },
                                        child: Container(
                                          alignment: Alignment.center,
                                          height: double.infinity,
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  right: BorderSide(
                                                      width: .6,
                                                      color: Colors.white))),
                                          child: Text(
                                            "order_create.create_new"
                                                .tr
                                                .toUpperCase(),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ),
                                    ),
                                  if (_homeController.authorization([
                                    KiotVersion.coffee,
                                    KiotVersion.milktea,
                                    KiotVersion.karaoke
                                  ], []))
                                    Expanded(
                                      flex: 4,
                                      child: GunInkwell(
                                        padding: EdgeInsets.zero,
                                        onTap: () {
                                          var printManagerController = Get.find<
                                              PrintManagerController>();
                                          printManagerController.printForm(
                                              PrintAction.kitchen,
                                              tableData: _controller.tableData,
                                              loadTableResponse:
                                                  _controller.orderDetail);
                                        },
                                        child: Container(
                                          alignment: Alignment.center,
                                          height: double.infinity,
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  right: BorderSide(
                                                      width: .6,
                                                      color: Colors.white))),
                                          child: Text(
                                            "order_create.notify_kitchen"
                                                .tr
                                                .toUpperCase(),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ),
                                    ),
                                  Expanded(
                                      flex: 4,
                                      child: GunInkwell(
                                        padding: EdgeInsets.zero,
                                        onTap: () async {
                                          await Get.toNamed(AppRoutes.payment);
                                          _controller.syncData();
                                        },
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: Text(
                                            "order_create.payment"
                                                .tr
                                                .toUpperCase(),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ))
                                ]),
                              )
                            ],
                          ),
                        if (_type == OrderTypeLoad.order_edit)
                          Column(
                            children: [
                              Material(
                                color: Color(0XFFFFEBD4),
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      paymentDetailVisiable =
                                          !paymentDetailVisiable;
                                    });
                                  },
                                  child: Column(children: [
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 8),
                                      child: Row(children: [
                                        Expanded(
                                          child: RichText(
                                            text: TextSpan(children: [
                                              TextSpan(
                                                  text: "order.code".tr,
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                  )),
                                            ]),
                                          ),
                                        ),
                                        Expanded(
                                            child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Text(
                                                "${controller.orderDetail.donHang!.outputCode}",
                                                textAlign: TextAlign.end,
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                            Icon(
                                              Icons.arrow_drop_down,
                                              color: Color(0XFFE27C04),
                                            )
                                          ],
                                        ))
                                      ]),
                                    ),
                                    if (paymentDetailVisiable)
                                      Column(
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 2),
                                                child: Row(children: [
                                                  Expanded(
                                                    child: Text(
                                                        "order_detail.money_product"
                                                            .tr,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400)),
                                                  ),
                                                  Expanded(
                                                      child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                          AppValue
                                                              .APP_MONEY_FORMAT
                                                              .format(_controller
                                                                      .orderDetail
                                                                      .donHang!
                                                                      .totalMoney ??
                                                                  0),
                                                          textAlign:
                                                              TextAlign.end,
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .primary,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600)),
                                                    ],
                                                  ))
                                                ]),
                                              ),
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 2),
                                                child: Row(children: [
                                                  Expanded(
                                                    child: Text(
                                                        "order_detail.discount"
                                                            .tr,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400)),
                                                  ),
                                                  Expanded(
                                                      child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                          AppValue
                                                              .APP_MONEY_FORMAT
                                                              .format(_controller
                                                                      .orderDetail
                                                                      .tongChietKhau ??
                                                                  0),
                                                          textAlign:
                                                              TextAlign.end,
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .primary,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400)),
                                                    ],
                                                  ))
                                                ]),
                                              ),
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 2),
                                                child: Row(children: [
                                                  Expanded(
                                                    child: Text(
                                                        "order_detail.coupon"
                                                            .tr,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400)),
                                                  ),
                                                  Expanded(
                                                      child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                          AppValue
                                                              .APP_MONEY_FORMAT
                                                              .format(_controller
                                                                      .orderDetail
                                                                      .donHang!
                                                                      .coupon ??
                                                                  0),
                                                          textAlign:
                                                              TextAlign.end,
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .primary,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400)),
                                                    ],
                                                  ))
                                                ]),
                                              ),
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 2),
                                                child: Row(children: [
                                                  Expanded(
                                                    child: Text(
                                                        "order_create.customer_pay"
                                                            .tr,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600)),
                                                  ),
                                                  Expanded(
                                                      child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                          AppValue
                                                              .APP_MONEY_FORMAT
                                                              .format(((_controller
                                                                          .orderDetail
                                                                          .donHang!
                                                                          .totalMoney ??
                                                                      0) -
                                                                  (_controller
                                                                          .orderDetail
                                                                          .tongChietKhau ??
                                                                      0))),
                                                          textAlign:
                                                              TextAlign.end,
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .primary,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600)),
                                                    ],
                                                  ))
                                                ]),
                                              ),
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 2),
                                                child: Row(children: [
                                                  Expanded(
                                                    child: Text(
                                                        "order_detail.debt".tr,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 14,
                                                        )),
                                                  ),
                                                  Expanded(
                                                      child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                          AppValue
                                                              .APP_MONEY_FORMAT
                                                              .format((0)),
                                                          textAlign:
                                                              TextAlign.end,
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .primary,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600)),
                                                    ],
                                                  ))
                                                ]),
                                              ),
                                            ],
                                          ),
                                        ],
                                      )
                                  ]),
                                ),
                              ),
                              GunInkwell(
                                onTap: () {
                                  try {
                                    _controller.syncData(
                                        orderStatus: OrderStatus.payment);
                                    Get.back();
                                  } catch (e) {}
                                },
                                padding: EdgeInsets.zero,
                                child: Container(
                                  color: Color(0XFFE27C04),
                                  height: 56,
                                  alignment: Alignment.center,
                                  child: Text("order.save".tr,
                                      style: TextStyle(color: Colors.white)),
                                ),
                              )
                            ],
                          )
                      ],
                    );
                  },
                )
              ]),
            )
          ],
        ));
  }
}

class OrderItemWidget extends StatelessWidget {
  final ODProductData data;
  final VoidCallback? onTap;
  final VoidCallback? onDeleteTap;
  final SlidableActionCallback? onCopyTap;
  const OrderItemWidget(
      {Key? key,
      required this.data,
      this.onTap,
      this.onCopyTap,
      this.onDeleteTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Slidable(
          // Specify a key if the Slidable is dismissible.
          key: key,
          closeOnScroll: false,
          enabled: onCopyTap != null,
          endActionPane: ActionPane(
            closeThreshold: .2,
            extentRatio: .17,
            motion: ScrollMotion(),
            children: [
              SlidableAction(
                onPressed: onCopyTap,
                autoClose: true,
                borderRadius: BorderRadius.circular(5),
                backgroundColor: AppColors.primary,
                foregroundColor: Colors.white,
                icon: Icons.copy,
                spacing: 1,
              )
            ],
          ),

          // The child of the Slidable is what the user sees when the
          // component is not dragged.
          child: Card(
            elevation: 3,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
              child: Row(children: [
                if (onDeleteTap != null)
                  GunInkwell(
                      onTap: onDeleteTap,
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Container(
                          width: 24,
                          height: 24,
                          child: Image.asset(
                              'assets/images/order_create_delete.png'))),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data.sanPham!.prdName!,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    RichText(
                      text: TextSpan(children: [
                        TextSpan(
                            text:
                                "${AppValue.APP_MONEY_FORMAT.format(data.price ?? 0)} x ",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                        TextSpan(
                            text: "${data.quantity}",
                            style: TextStyle(
                                color: Color(0XFFE27C04),
                                fontSize: 14,
                                fontWeight: FontWeight.w400))
                      ]),
                    ),
                    if (data.topping != null)
                      Wrap(
                        children: List.generate(
                          data.topping!.length,
                          (index) => RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                  text:
                                      "${data.topping![index].topping!.prdName}(${AppValue.APP_MONEY_FORMAT.format(data.topping![index].priceAdd)}) x ",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: "${data.topping![index].quantityAdd}, ",
                                  style: TextStyle(
                                      color: Color(0XFFE27C04),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400)),
                            ]),
                          ),
                        ),
                      ),
                  ],
                )),
                Text(
                  "${AppValue.APP_MONEY_FORMAT.format(data.sauChietKhau ?? 0)}",
                  style: TextStyle(
                      color: Color(0XFFE27C04),
                      fontSize: 14,
                      fontWeight: FontWeight.w600),
                ),
              ]),
            ),
          )),
    );
  }
}

class OrderProductBottomSheet extends StatefulWidget {
  final ODProductData data;
  final VoidCallback? onToppingTap;
  final ValueChanged<ODProductData> onSubmit;

  const OrderProductBottomSheet(
      {Key? key, required this.data, this.onToppingTap, required this.onSubmit})
      : super(key: key);

  @override
  State<OrderProductBottomSheet> createState() =>
      _OrderProductBottomSheetState();
}

class _OrderProductBottomSheetState extends State<OrderProductBottomSheet> {
  HomeController _homeController = Get.find<HomeController>();
  TextEditingController _unitPriceEditController = TextEditingController();
  TextEditingController _percentDiscountRatioEditController =
      TextEditingController();
  TextEditingController _vndDiscountRatioEditController =
      TextEditingController();
  TextEditingController _quantityEditController = TextEditingController();

  TextEditingController _noteEditController = TextEditingController();

  OrderDiscountRatio? _discountRatio = OrderDiscountRatio.percent;
  bool _noIce = true;
  bool _noSugar = true;
  bool _test = true;

  late ODProductData _productData;

  @override
  void initState() {
    super.initState();
    _productData = widget.data.copyWith();
    if (_productData.discount != 0) {
      _discountRatio = OrderDiscountRatio.percent;
    } else {
      _discountRatio = OrderDiscountRatio.vnd;
    }

    print("_productData.price ${_productData.discount}");

    _quantityEditController.text = _productData.quantity!.toString();
  }

  @override
  Widget build(BuildContext context) {
    return KeyBoarDismisserWidget(
      child: Container(
        // constraints: BoxConstraints(minHeight: Get.height * 0.7),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(15),
              topRight: const Radius.circular(15),
            )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
              child: Text(
                widget.data.sanPham!.prdName!,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              ),
            ),
            Container(
              width: double.infinity,
              height: 1,
              color: Colors.grey.shade300,
            ),
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(16.0),
                child: Column(children: [
                  Row(
                    children: [
                      Expanded(
                          flex: 1, child: Text('order_create.unit_price'.tr)),
                      Expanded(
                          flex: 1,
                          child: TextFormField(
                            initialValue: AppValue.formatterVnd.format(
                                _productData.price != null
                                    ? _productData.price.toString()
                                    : "0"),
                            onChanged: (e) {
                              _productData.price = AppValue.formatterVnd
                                  .getUnformattedValue()
                                  .toInt();
                            },
                            inputFormatters: <TextInputFormatter>[
                              AppValue.formatterVnd
                            ],
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.end,
                            style: AppStyles.normalTextStyle.copyWith(
                                color: Color(0XFFE27C04),
                                fontWeight: FontWeight.w600),
                            decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.zero,
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade300),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade300),
                              ),
                            ),
                          ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  if (_homeController.authorization([
                    KiotVersion.coffee,
                    KiotVersion.karaoke,
                    KiotVersion.milktea,
                    KiotVersion.shop,
                    KiotVersion.enter
                  ], [
                    KiotPermisson.sell_coupon,
                    KiotPermisson.sell_discount,
                  ]))
                    Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: Text('order_create.discount_type'.tr)),
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: [
                                  Radio<OrderDiscountRatio>(
                                      value: OrderDiscountRatio.percent,
                                      groupValue: _discountRatio,
                                      onChanged: (val) {
                                        setState(() {
                                          _productData.discount = 0;
                                          _productData.discountVnd = 0;
                                          _discountRatio = val;
                                        });
                                      }),
                                  Expanded(
                                    child: Text('order_create.percent'.tr),
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: [
                                  Radio<OrderDiscountRatio>(
                                      value: OrderDiscountRatio.vnd,
                                      groupValue: _discountRatio,
                                      onChanged: (val) {
                                        setState(() {
                                          _productData.discount = 0;
                                          _productData.discountVnd = 0;
                                          _discountRatio = val;
                                        });
                                      }),
                                  Expanded(child: Text('order_create.vnd'.tr))
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        if (_discountRatio == OrderDiscountRatio.percent)
                          Row(
                            children: [
                              Expanded(
                                  flex: 1,
                                  child:
                                      Text('order_create.discount_ratio'.tr)),
                              Expanded(
                                  flex: 1,
                                  child: TextFormField(
                                    key: ValueKey(100),
                                    initialValue:
                                        _productData.discount.toString(),
                                    // controller:
                                    //     _percentDiscountRatioEditController,
                                    onChanged: (e) {
                                      try {
                                        _productData.discount = int.parse(e);
                                      } catch (e) {}
                                    },
                                    inputFormatters: <TextInputFormatter>[
                                      NumericalRangeFormatter(min: 0, max: 100)
                                    ],
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.end,
                                    style: AppStyles.normalTextStyle.copyWith(
                                        color: Color(0XFFE27C04),
                                        fontWeight: FontWeight.w600),
                                    decoration: InputDecoration(
                                      isDense: true,
                                      contentPadding: EdgeInsets.zero,
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.grey.shade300),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.grey.shade300),
                                      ),
                                    ),
                                  ))
                            ],
                          ),
                        if (_discountRatio == OrderDiscountRatio.vnd)
                          Row(
                            children: [
                              Expanded(
                                  flex: 1,
                                  child: Text('order_create.discount_vnd'.tr)),
                              Expanded(
                                  flex: 1,
                                  child: TextFormField(
                                    key: ValueKey(200),
                                    initialValue: AppValue.formatterVnd.format(
                                        _productData.discountVnd.toString()),
                                    onChanged: (e) {
                                      _productData.discountVnd = AppValue
                                          .formatterVnd
                                          .getUnformattedValue()
                                          .toInt();
                                    },
                                    inputFormatters: <TextInputFormatter>[
                                      AppValue.formatterVnd
                                    ],
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.end,
                                    style: AppStyles.normalTextStyle.copyWith(
                                        color: Color(0XFFE27C04),
                                        fontWeight: FontWeight.w600),
                                    decoration: InputDecoration(
                                      isDense: true,
                                      contentPadding: EdgeInsets.zero,
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.grey.shade300),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.grey.shade300),
                                      ),
                                    ),
                                  ))
                            ],
                          ),
                      ],
                    ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Expanded(flex: 1, child: Text('order_create.vat'.tr)),
                      Expanded(
                          child: GunInkwell(
                        onTap: () {
                          Get.bottomSheet(Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 30, horizontal: 10),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    topLeft: const Radius.circular(15),
                                    topRight: const Radius.circular(15),
                                  )),
                              child: SingleChildScrollView(
                                  child: Column(
                                children: List.generate(
                                    VatOrder.supported.length,
                                    (index) => GunInkwell(
                                          onTap: () {
                                            Get.back();
                                            _productData.vat =
                                                VatOrder.supported[index];
                                            setState(() {});
                                            // controller
                                            //     .refreshOrderDetail();
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        width: 1,
                                                        color: Colors
                                                            .grey.shade300))),
                                            width: double.infinity,
                                            padding: EdgeInsets.only(
                                                left: 10,
                                                right: 10,
                                                top: 10,
                                                bottom: 20),
                                            child: Text(
                                                '${VatOrder.supported[index]}%'
                                                    .tr),
                                          ),
                                        )),
                              ))));
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              '${_productData.vat ?? 0}%',
                              style: AppStyles.normalTextStyle,
                            ),
                            Icon(Icons.arrow_drop_down)
                          ],
                        ),
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Expanded(
                          flex: 1, child: Text('order_create.quantity'.tr)),
                      Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GunInkwell(
                                onTap: () {
                                  setState(() {
                                    int quantity = _productData.quantity!;
                                    if (_productData.quantity! > 0)
                                      _productData.quantity = quantity - 1;
                                    _quantityEditController.text =
                                        _productData.quantity!.toString();
                                  });
                                },
                                padding: EdgeInsets.zero,
                                child: Container(
                                    width: 30,
                                    height: 30,
                                    child: Image.asset(
                                        'assets/images/btn_delete.png')),
                              ),
                              // Container(
                              //   margin: EdgeInsets.only(left: 15, right: 15),
                              //   child: Text(
                              //     '${_productData.quantity}',
                              //     style: TextStyle(
                              //         color: Colors.black,
                              //         fontSize: 14,
                              //         fontWeight: FontWeight.w400),
                              //   ),
                              // ),
                              Container(
                                width: 50,
                                margin: EdgeInsets.only(left: 15, right: 15),
                                child: TextFormField(
                                  controller: _quantityEditController,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly,
                                    NumericalRangeFormatter(
                                        min: 0, max: 100000),
                                  ],
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: false, signed: false),
                                  textAlign: TextAlign.center,
                                  onChanged: (val) {
                                    _productData.quantity = int.parse(val);
                                  },
                                ),
                              ),
                              GunInkwell(
                                onTap: () {
                                  setState(() {
                                    int quantity = _productData.quantity!;
                                    _productData.quantity = quantity + 1;
                                    _quantityEditController.text =
                                        _productData.quantity!.toString();
                                  });
                                },
                                padding: EdgeInsets.zero,
                                child: Container(
                                    width: 30,
                                    height: 30,
                                    child: Image.asset(
                                        'assets/images/btn_add.png')),
                              )
                            ],
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Expanded(
                          flex: 1,
                          child: Row(
                            children: [
                              Checkbox(
                                checkColor: Colors.white,
                                fillColor: MaterialStateProperty.all(
                                    AppColors.primary),
                                value: _productData.cc2 == "1" ? true : false,
                                onChanged: (bool? value) {
                                  setState(() {
                                    _productData.cc2 =
                                        _productData.cc2 == "1" ? "0" : "1";
                                  });
                                },
                              ),
                              Text(
                                'order_create.no_sugar'.tr,
                                textAlign: TextAlign.start,
                              )
                            ],
                          )),
                      Expanded(
                          flex: 1,
                          child: Row(
                            children: [
                              Checkbox(
                                checkColor: Colors.white,
                                fillColor: MaterialStateProperty.all(
                                    AppColors.primary),
                                value: _productData.cc1 == "1" ? true : false,
                                onChanged: (bool? value) {
                                  setState(() {
                                    _productData.cc1 =
                                        _productData.cc1 == "1" ? "0" : "1";
                                  });
                                },
                              ),
                              Text(
                                'order_create.no_ice'.tr,
                                textAlign: TextAlign.start,
                              )
                            ],
                          ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'order_create.note'.tr,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: TextFormField(
                      controller: _noteEditController,
                      onChanged: (val) {
                        _productData.note = val;
                      },
                      minLines: 4,
                      maxLines: 4,
                      keyboardType: TextInputType.text,
                      textAlign: TextAlign.start,
                      style: AppStyles.normalTextStyle.copyWith(),
                      decoration: InputDecoration(
                        hintText: 'order_create.note_hint'.tr,
                        hintStyle: AppStyles.normalTextStyle
                            .copyWith(color: Colors.grey.shade300),
                        isDense: true,
                        contentPadding: EdgeInsets.zero,
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade300),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade300),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 40),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          if (_homeController.authorization([
                            KiotVersion.coffee,
                            KiotVersion.karaoke,
                            KiotVersion.milktea,
                          ], [
                            KiotPermisson.customer_create,
                          ]))
                            Expanded(
                              flex: 1,
                              child: TextButton(
                                  onPressed: widget.onToppingTap,
                                  style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                      )),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.symmetric(horizontal: 10)),
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              Color(0XFFD6E8FF))),
                                  child: Text(
                                    'order_create.topping'.tr,
                                    style: TextStyle(
                                        color: AppColors.primary,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600),
                                  )),
                            ),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            flex: 1,
                            child: TextButton(
                                onPressed: () {
                                  widget.onSubmit(_productData);
                                },
                                style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                    )),
                                    padding: MaterialStateProperty.all(
                                        EdgeInsets.symmetric(horizontal: 10)),
                                    backgroundColor: MaterialStateProperty.all(
                                        AppColors.primary)),
                                child: Text(
                                  'order_create.apply'.tr,
                                  style: TextStyle(
                                      color: AppColors.White,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600),
                                )),
                          )
                        ]),
                  )
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class OrderGiftProductBottomSheet extends StatefulWidget {
  final ODProductData data;

  final ValueChanged<ODProductData> onSubmit;

  const OrderGiftProductBottomSheet(
      {Key? key, required this.data, required this.onSubmit})
      : super(key: key);

  @override
  State<OrderGiftProductBottomSheet> createState() =>
      _OrderGiftProductBottomSheetState();
}

class _OrderGiftProductBottomSheetState
    extends State<OrderGiftProductBottomSheet> {
  HomeController _homeController = Get.find<HomeController>();

  TextEditingController _quantityEditController = TextEditingController();

  late ODProductData _productData;

  @override
  void initState() {
    super.initState();
    _productData = widget.data.copyWith();
    print("_productData.price ${widget.data.price}");
    print("_productData.price ${_productData.price}");
    // _unitPriceEditController.text = _productData.price!;
    // _percentDiscountRatioEditController.text = _productData.discount.toString();
    // _vndDiscountRatioEditController.text = _productData.discountVnd.toString();
    _quantityEditController.text = _productData.quantity!.toString();

    print("OrderProductBottomSheet initState");
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        KeyBoarDismisserWidget(
          child: Container(
            // constraints: BoxConstraints(minHeight: Get.height * 0.7),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(15),
                  topRight: const Radius.circular(15),
                )),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                  child: Text(
                    widget.data.sanPham!.prdName!,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 1,
                  color: Colors.grey.shade300,
                ),
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Column(children: [
                    Row(
                      children: [
                        Expanded(
                            flex: 1, child: Text('order_create.quantity'.tr)),
                        Expanded(
                            flex: 1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                GunInkwell(
                                  onTap: () {
                                    setState(() {
                                      int quantity = _productData.quantity!;
                                      if (_productData.quantity! > 0)
                                        _productData.quantity = quantity - 1;
                                      _quantityEditController.text =
                                          _productData.quantity!.toString();
                                    });
                                  },
                                  padding: EdgeInsets.zero,
                                  child: Container(
                                      width: 30,
                                      height: 30,
                                      child: Image.asset(
                                          'assets/images/btn_delete.png')),
                                ),
                                // Container(
                                //   margin: EdgeInsets.only(left: 15, right: 15),
                                //   child: Text(
                                //     '${_productData.quantity}',
                                //     style: TextStyle(
                                //         color: Colors.black,
                                //         fontSize: 14,
                                //         fontWeight: FontWeight.w400),
                                //   ),
                                // ),
                                Container(
                                  width: 50,
                                  margin: EdgeInsets.only(left: 15, right: 15),
                                  child: TextFormField(
                                    controller: _quantityEditController,
                                    decoration: InputDecoration(
                                        border: InputBorder.none),
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly,
                                      NumericalRangeFormatter(
                                          min: 0, max: 1000),
                                    ],
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: false, signed: false),
                                    textAlign: TextAlign.center,
                                    onChanged: (val) {
                                      _productData.quantity = int.parse(val);
                                    },
                                  ),
                                ),
                                GunInkwell(
                                  onTap: () {
                                    setState(() {
                                      int quantity = _productData.quantity!;
                                      _productData.quantity = quantity + 1;
                                      _quantityEditController.text =
                                          _productData.quantity!.toString();
                                    });
                                  },
                                  padding: EdgeInsets.zero,
                                  child: Container(
                                      width: 30,
                                      height: 30,
                                      child: Image.asset(
                                          'assets/images/btn_add.png')),
                                )
                              ],
                            )),
                      ],
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 40),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              flex: 1,
                              child: TextButton(
                                  onPressed: () {
                                    widget.onSubmit(_productData);
                                  },
                                  style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                      )),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.symmetric(horizontal: 10)),
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              AppColors.primary)),
                                  child: Text(
                                    'order_create.apply'.tr,
                                    style: TextStyle(
                                        color: AppColors.White,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600),
                                  )),
                            )
                          ]),
                    )
                  ]),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class OrderStaffWidget extends StatelessWidget {
  final ODStaffData data;
  final VoidCallback? onDeleteTap;
  final VoidCallback? onEnterTap;
  final VoidCallback? onStopTap;

  const OrderStaffWidget(
      {Key? key,
      required this.data,
      this.onDeleteTap,
      this.onEnterTap,
      this.onStopTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      key: Key(
          "${data.nhanVien!.id!.toString()}${DateTime.now().microsecondsSinceEpoch}"),
      elevation: 3,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 3.0, vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GunInkwell(
                onTap: onDeleteTap,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Container(
                    width: 24,
                    height: 24,
                    child:
                        Image.asset('assets/images/order_create_delete.png'))),
            Expanded(
                child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        child: Text(
                      "${data.nhanVien!.employee_name}",
                      style: AppStyles.normalTextStyle
                          .copyWith(fontWeight: FontWeight.w600),
                    )),
                    Text(
                      "${AppValue.APP_MONEY_FORMAT.format(data.totalPrice ?? 0)} ",
                      style: TextStyle(
                          color: Color(0XFFE27C04),
                          fontSize: 14,
                          fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                        child: AbsorbPointer(
                      absorbing: true,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'order_create.unit_price'.tr,
                            style: TextStyle(
                                fontSize: 10, fontWeight: FontWeight.w400),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          TextFormField(
                            initialValue:
                                "${AppValue.APP_MONEY_FORMAT.format(data.price)} ",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w400),
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.only(bottom: 2)),
                          ),
                        ],
                      ),
                    )),
                    SizedBox(
                      width: 30,
                    ),
                    Expanded(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'order_create.total_minutes'.tr,
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "${data.time}",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400),
                        )
                      ],
                    ))
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  children: [
                    Expanded(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'order_create.time_enter'.tr,
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        GunInkwell(
                          onTap: onEnterTap,
                          padding: EdgeInsets.zero,
                          child: AbsorbPointer(
                            absorbing: true,
                            child: Row(
                              children: [
                                Expanded(
                                  child: TextFormField(
                                    initialValue: data.start ?? "",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                    decoration: InputDecoration(
                                        isDense: true,
                                        contentPadding:
                                            EdgeInsets.only(bottom: 2)),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    )),
                    SizedBox(
                      width: 30,
                    ),
                    Expanded(
                        child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'order_create.time_leave'.tr,
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.w400),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                data.end ?? "",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.w400),
                              )
                            ],
                          ),
                        ),
                        if (data.end == null)
                          GunInkwell(
                            onTap: onStopTap,
                            child: Container(
                                width: 24,
                                child: Image.asset(
                                    'assets/images/order_staff_stop.png')),
                          )
                      ],
                    )),
                  ],
                ),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
