import 'package:flutter/material.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/controllers/stock_controller.dart';
import 'package:kiot_pos/data/entities/response/stock_search_response.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/res.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/ui/ui.dart';

class StockingPage extends StatefulWidget {
  const StockingPage({Key? key}) : super(key: key);

  @override
  State<StockingPage> createState() => _StockingPageState();
}

class _StockingPageState extends State<StockingPage> {
  StockController stockController = Get.find<StockController>();
  TextEditingController textController = TextEditingController();
  bool isSearch = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      stockController.getStockSearch(tuKhoa: '');
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetX<StockController>(
      builder: (controller) {
        return GunPage(
          appBarBottom: GunAppBar(
            type: GunAppBarType.BOTTOM,
            height: 0,
          ),
          appBarTop: GunAppBar(
            type: GunAppBarType.TOP,
            titles: Row(
              children: [
                isSearch
                    ? Expanded(
                        child: TextField(
                        controller: textController,
                        onChanged: (e) {
                          setState(() {});
                        },
                        onSubmitted: (e) {
                          controller.getStockSearch(tuKhoa: e);
                        },
                        autofocus: true,
                        decoration: InputDecoration(
                            isDense: true, border: InputBorder.none),
                      ))
                    : GunText(
                        'Kiểm kê'.tr,
                        style: AppStyles.pageTitleStyle,
                      ),
              ],
            ),
            leadingColor: Colors.black,
            actions: [
              GunAction.icon(
                onTap: () {
                  setState(() {
                    isSearch = !isSearch;
                    textController.clear();
                  });
                  if (isSearch == false) {
                    controller.getStockSearch(tuKhoa: '');
                  }
                },
                iconFactor: isSearch ? 0.4 : 0.7,
                icon: Image.asset(
                  isSearch
                      ? './assets/images/close_icon.png'
                      : './assets/images/search_icon.png',
                  color: AppColors.Orange,
                ),
              ),
              if (isSearch == false)
                GunAction.icon(
                  onTap: () {
                    Get.toNamed(AppRoutes.stocking_create);
                  },
                  icon: Image.asset('assets/images/add_icon.png'),
                )
            ],
          ),
          child: Container(
            color: AppColors.Background,
            child: Column(
              children: [Expanded(child: _renderBody(controller.listStock))],
            ),
          ),
        );
      },
    );
  }

  _renderBody(List<StockSearchResponse> list) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 16),
          ),
          ...List.generate(
              list.length,
              (index) => Material(
                    color: AppColors.White,
                    child: InkWell(
                      onTap: () {
                        stockController.idStock = list[index].item!.ID!;
                        Get.toNamed(AppRoutes.stocking_detail);
                      },
                      child: Container(
                        padding: EdgeInsets.all(12),
                        margin:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        child: Column(
                          children: [
                            OneRow(
                                title: 'Mã phiếu kiểm',
                                subtitle: list[index].item!.adjustCode ?? ''),
                            OneRow(
                                title: 'Trạng thái',
                                subtitle: list[index].item!.adjustStatus == 0
                                    ? 'Lưu tạm'
                                    : 'Hoàn thành'),
                            OneRow(
                                title: 'Ngày kiểm',
                                subtitle: list[index].item!.adjustDate!),
                            OneRow(
                                title: 'Số lượng kiểm',
                                subtitle:
                                    list[index].item!.totalQuantity.toString()),
                            OneRow(
                                title: 'Số lượng chênh lệch',
                                subtitle: list[index]
                                    .item!
                                    .totalDifferent
                                    .toString()),
                          ],
                        ),
                      ),
                    ),
                  ))
        ],
      ),
    );
  }
}
