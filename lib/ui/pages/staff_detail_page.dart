import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kiot_pos/ui/widgets/base_info_widget.dart';

class StaffDetailPage extends StatefulWidget {
  const StaffDetailPage({Key? key}) : super(key: key);

  @override
  State<StaffDetailPage> createState() => _StaffDetailPageState();
}

class _StaffDetailPageState extends State<StaffDetailPage> {
  StaffController staffController = Get.find<StaffController>();
  StaffDetailController staffDetailController =
      Get.find<StaffDetailController>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (Get.arguments != null) {
      Future.delayed(Duration(seconds: 0), () {
        staffDetailController.getDetailStaff(id: Get.arguments);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetX<StaffDetailController>(
      builder: (controller) {
        
        return controller.detailStaff != null &&
                controller.detailStaff!.nhanVien != null
            ? GunPage(
                appBarBottom: GunAppBar(
                  type: GunAppBarType.BOTTOM,
                  height: 0,
                ),
                appBarTop: GunAppBar(
                  type: GunAppBarType.TOP,
                  title: controller.detailStaff!.nhanVien!.employeeName,
                  titleStyle: AppStyles.pageTitleStyle,
                  leadingColor: Colors.black,
                  actions: [
                    GunAction.icon(
                      onTap: () {
                        Get.toNamed(AppRoutes.staff_edit,
                            arguments: Get.arguments);
                      },
                      icon: Image.asset('./assets/images/edit_icon.png',
                          width: 24.r),
                    )
                  ],
                ),
                child: Container(
                  child: Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          children: [
                            Expanded(
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    OneRow(
                                        title: 'staff.code'.tr,
                                        subtitle: controller.detailStaff!
                                            .nhanVien!.employeeCode!),
                                    OneRow(
                                        title: 'staff.name'.tr,
                                        subtitle: controller.detailStaff!
                                            .nhanVien!.employeeName!),
                                    OneRow(
                                        title: 'phone'.tr,
                                        subtitle: controller.detailStaff!
                                            .nhanVien!.employeePhone!),
                                    OneRow(
                                        title: 'staff.unit_price'.tr,
                                        subtitle: AppValue.formatterVnd.format(
                                            controller.detailStaff!.nhanVien!
                                                .employeePrice!
                                                .toString())),
                                    OneRow(
                                        title: 'staff.total_money'.tr,
                                        subtitle: controller
                                            .detailStaff!.tongDoanhSo!
                                            .toString()),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Positioned.fill(
                          child: Align(
                        alignment: Alignment.bottomCenter,
                        child: _renderButton(),
                      ))
                    ],
                  ),
                ))
            : SizedBox.shrink();
      },
    );
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
      child: WidgetButton(
        onTap: () async {
          Get.dialog(DialogCustom(
              title: 'Thông báo',
              isTwoButton: true,
              nameButtonLeft: 'Huỷ',
              nameButtonRight: 'Xoá',
              onTapLeft: () {
                Get.back();
              },
              onTapRight: () async {
                Get.back();
                if (Get.arguments != null) {
                  var result = await staffDetailController.staffDelete(
                    id: Get.arguments,
                  );
                  if (result.type == "success") {
                    Get.dialog(DialogCustom(
                        title: 'Thông báo',
                        onTapOK: () {
                          Get.back();
                          Get.back();
                          staffController.searchStaff(tuKhoa: '');
                        },
                        children: [
                          Text(result.message!, style: AppStyles.DEFAULT_16)
                        ]));
                  }
                }
              },
              children: [
                Text('Bạn có chắc chắn muốnn xoá ?',
                    style: AppStyles.DEFAULT_16),
              ]));
        },
        text: 'delete'.tr,
        isFullWidth: true,
      ),
    );
  }
}
