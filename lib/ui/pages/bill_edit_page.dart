import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:gun_base/res/res.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/data/entities/response/unit_response.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/colors.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/ui.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BillEditPage extends StatefulWidget {
  const BillEditPage({Key? key}) : super(key: key);

  @override
  State<BillEditPage> createState() => _BillEditPageState();
}

class _BillEditPageState extends State<BillEditPage> {
  OverviewCollectPayController overviewCollectPayController =
      Get.find<OverviewCollectPayController>();
  BillDetailController billDetailController = Get.find<BillDetailController>();
  CustomerController customerController = Get.find<CustomerController>();
  StaffController staffController = Get.find<StaffController>();
  List<ItemInputKhuVuc> listInput = [];
  List<UsersResponse> hinhThucThu = [
    UsersResponse(id: 3, displayName: 'Thu bán hàng'),
    UsersResponse(id: 9, displayName: '	Thu trả hàng'),
    UsersResponse(id: 11, displayName: 'Thu ngoài'),
    UsersResponse(id: 4, displayName: 'Thu khách lẻ'),
    UsersResponse(id: 5, displayName: 'Thu HĐGTGT'),
    UsersResponse(id: 6, displayName: 'Thu khác'),
    UsersResponse(id: 7, displayName: 'Thu văn phòng'),
    UsersResponse(id: 8, displayName: 'Thu công nợ'),
  ];

  List<UsersResponse> hinhThucChi = [
    UsersResponse(id: 2, displayName: 'Chi mua hàng'),
    UsersResponse(id: 13, displayName: 'Chi trả hàng'),
    UsersResponse(id: 3, displayName: 'Chi công nợ'),
    UsersResponse(id: 16, displayName: 'Chi ngoài'),
    UsersResponse(id: 4, displayName: 'Chi phí'),
    UsersResponse(id: 5, displayName: 'Tiền xăng'),
    UsersResponse(id: 6, displayName: 'Thuê xe và gửi hàng'),
    UsersResponse(id: 7, displayName: 'Tiền ứng'),
    UsersResponse(id: 9, displayName: 'Chi văn phòng'),
    UsersResponse(id: 10, displayName: 'Chi tài sản cố định'),
    UsersResponse(id: 8, displayName: 'Chi khác'),
  ];
  String day = '';
  Future onDayPicker(BuildContext context) async {
    final initialDate = DateTime.now();
    final newDay = await showDatePicker(
        context: context,
        initialDate: initialDate,
        initialEntryMode: DatePickerEntryMode.calendarOnly,
        builder: (context, child) {
          return Theme(
              data: ThemeData.light().copyWith(
                colorScheme: ColorScheme.light(
                  primary: AppColors.Blue,
                  onPrimary: Colors.white,
                  surface: AppColors.Blue,
                  onSurface: AppColors.Gray1,
                ),
              ),
              child: child!);
        },
        firstDate: DateTime(DateTime.now().year - 20),
        lastDate: initialDate);
    if (newDay == null) return;
    setState(() {
      day = GunValue.DATE_FORMAT.format(newDay);
      listInput[1].controller.text = day;
    });
  }

  int typeNavigation = -1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (Get.arguments != null) {
      setState(() {
        typeNavigation = Get.arguments;
      });
    }
    Future.delayed(Duration.zero, () {
      staffController.searchStaff(tuKhoa: '');
      customerController.getListCustomer(tuKhoa: '');
      billDetailController.users();
      billDetailController.producer();
    });
    for (var i = 0; i < 6; i++) {
      listInput.add(ItemInputKhuVuc(
          controller: TextEditingController(), focusNode: FocusNode()));
    }
    if (billDetailController.idBill == -1) {
      if (typeNavigation == 0) {
        setState(() {
          title = 'Tạo phiếu thu';
        });
      } else {
        setState(() {
          title = 'Tạo phiếu chi';
        });
      }
    } else {
      setState(() {
        title = 'Chỉnh sửa'.tr;
      });
    }
    if (billDetailController.idBill != -1) {
      if (typeNavigation == 0) {
        if (billDetailController.detailCollect != null) {
          title =
              billDetailController.detailCollect!.phieuThu!.receiptCode ?? '';
          listInput[0].controller.text =
              billDetailController.detailCollect!.khachHang ?? '';
          customerId = billDetailController.detailCollect!.phieuThu!.customerId;

          listInput[1].controller.text =
              billDetailController.detailCollect!.phieuThu!.receiptDate ?? '';
          listInput[2].controller.text =
              billDetailController.detailCollect!.tenNguoiThu ?? '';
          userId = billDetailController.detailCollect!.phieuThu!.receiptFor;

          hinhThucThu.forEach((e) {
            if (billDetailController.detailCollect!.phieuThu!.typeId == e.id) {
              listInput[3].controller.text = e.displayName!;
              typeId = e.id;
            }
          });
          listInput[4].controller.text =
              billDetailController.detailCollect!.phieuThu!.notes ?? '';
          listInput[5].controller.text =
              (billDetailController.detailCollect!.phieuThu!.totalMoney ?? 0)
                  .toString();
        }
      } else {
        if (billDetailController.detailPay != null) {
          title = billDetailController.detailPay!.phieuChi!.paymentCode!;
          listInput[0].controller.text =
              billDetailController.detailPay!.nhaCungCap ?? '';
          customerId = billDetailController.detailPay!.phieuChi!.customerId;

          listInput[1].controller.text =
              billDetailController.detailPay!.phieuChi!.paymentDate ?? '';
          listInput[2].controller.text =
              billDetailController.detailPay!.tenNguoiChi ?? '';
          userId = billDetailController.detailPay!.phieuChi!.paymentFor;

          hinhThucChi.forEach((e) {
            if (billDetailController.detailPay!.phieuChi!.typeId == e.id) {
              listInput[3].controller.text = e.displayName!;
              typeId = e.id;
            }
          });
          listInput[4].controller.text =
              billDetailController.detailPay!.phieuChi!.notes ?? '';
          listInput[5].controller.text =
              (billDetailController.detailPay!.phieuChi!.totalMoney ?? 0)
                  .toString();
        }
      }
    }
  }

  String title = '';
  int? customerId;
  int? userId;
  int? typeId;

  onEditCollect() {
    billDetailController.collectEdit(
        id: billDetailController.idBill,
        receiptFor: userId!,
        totalMoney: int.parse(listInput[5].controller.text),
        receiptDate: listInput[1].controller.text,
        notes: listInput[4].controller.text,
        receiptMethod: 1,
        typeId: typeId!,
        customerId: customerId!,
        callback: () {
          Get.back();
          Get.back();
          overviewCollectPayController.getListCollect(
              '', null, null, null, null, 10);
        });
  }

  onEditPay() {
    billDetailController.payEdit(
        id: billDetailController.idBill,
        paymentFor: userId!,
        totalMoney: int.parse(listInput[5].controller.text),
        paymentDate: listInput[1].controller.text,
        notes: listInput[4].controller.text,
        paymentMethod: 1,
        typeId: typeId!,
        customerId: customerId!,
        callback: () {
          Get.back();
          Get.back();
          overviewCollectPayController.getListPay(
              '', null, null, null, null, 10);
        });
  }

  onCreateCollect() {
    billDetailController.collectCreate(
        receiptFor: userId!,
        totalMoney: int.parse(listInput[5].controller.text),
        receiptDate: listInput[1].controller.text,
        notes: listInput[4].controller.text,
        receiptMethod: 1,
        typeId: typeId!,
        customerId: customerId!,
        callback: () {
          Get.back();
          overviewCollectPayController.getListCollect(
              '', null, null, null, null, 10);
        });
  }

  onCreatePay() {
    billDetailController.payCreate(
        paymentFor: userId!,
        totalMoney: int.parse(listInput[5].controller.text),
        paymentDate: listInput[1].controller.text,
        notes: listInput[4].controller.text,
        paymentMethod: 1,
        typeId: typeId!,
        customerId: customerId!,
        callback: () {
          Get.back();
          overviewCollectPayController.getListPay(
              '', null, null, null, null, 10);
        });
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        resizeToAvoidBottomInset: false,
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: title,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
          actions: [
            GunAction.icon(
              icon: Container(width: 24),
            )
          ],
        ),
        child: Stack(
          children: [
            Column(
              children: [Expanded(child: _renderBody()), _renderButton()],
            ),
          ],
        ));
  }

  _renderBody() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        children: [
          typeNavigation == 0
              ? GetX<CustomerController>(
                  builder: (controller) {
                    return WidgetDropdown(
                      controller: listInput[0].controller,
                      focusNode: listInput[0].focusNode,
                      list: controller.listCustomer,
                      hintText: 'customer.name'.tr,
                      isAutoComplete: true,
                      builder: (int id, CustomerResponse item,
                          CustomerResponse? selected) {
                        return GunText(
                          item.customerName!,
                          style: AppStyles.DEFAULT_16
                              .copyWith(color: Colors.black),
                        );
                      },
                      onTap: (int id, CustomerResponse? selected) {
                        setState(() {
                          listInput[0].controller.text =
                              selected!.customerName!;
                          customerId = selected.id;
                        });
                      },
                    );
                  },
                )
              : GetX<BillDetailController>(
                  builder: (controller) {
                    return WidgetDropdown(
                      controller: listInput[0].controller,
                      focusNode: listInput[0].focusNode,
                      list: controller.listProducer,
                      hintText: 'nsx'.tr,
                      isAutoComplete: true,
                      builder: (int id, ProducerResponse item,
                          ProducerResponse? selected) {
                        return GunText(
                          item.prdManufName!,
                          style: AppStyles.DEFAULT_16
                              .copyWith(color: Colors.black),
                        );
                      },
                      onTap: (int id, ProducerResponse? selected) {
                        setState(() {
                          listInput[0].controller.text =
                              selected!.prdManufName!;
                          customerId = selected.id;
                        });
                      },
                    );
                  },
                ),
          Material(
            color: Colors.white,
            child: InkWell(
              onTap: () {
                onDayPicker(context);
              },
              child: AbsorbPointer(
                child: WidgetInput(
                  controller: listInput[1].controller,
                  focusNode: listInput[1].focusNode,
                  hintText:
                      typeNavigation == 0 ? 'collect.day'.tr : 'pay.day'.tr,
                  onChanged: (e) {},
                  childRight: Image.asset(
                    './assets/images/calender_icon.png',
                    height: 25.h,
                    color: AppColors.Orange,
                  ),
                ),
              ),
            ),
          ),
          GetX<BillDetailController>(
            builder: (controller) {
              return WidgetDropdown(
                controller: listInput[2].controller,
                focusNode: listInput[2].focusNode,
                list: controller.listUser,
                hintText:
                    typeNavigation == 0 ? 'collect.user'.tr : 'pay.user'.tr,
                isAutoComplete: true,
                builder: (int id, UsersResponse item, UsersResponse? selected) {
                  return GunText(
                    item.displayName!,
                    style: AppStyles.DEFAULT_16.copyWith(color: Colors.black),
                  );
                },
                onTap: (int id, UsersResponse? selected) {
                  setState(() {
                    listInput[2].controller.text = selected!.displayName!;
                    userId = selected.id;
                  });
                },
              );
            },
          ),
          WidgetDropdown(
            controller: listInput[3].controller,
            focusNode: listInput[3].focusNode,
            list: typeNavigation == 0 ? hinhThucThu : hinhThucChi,
            hintText: typeNavigation == 0 ? 'collect.form'.tr : 'pay.form'.tr,
            isAutoComplete: true,
            builder: (int id, UsersResponse item, UsersResponse? selected) {
              return GunText(
                item.displayName!,
                style: AppStyles.DEFAULT_16.copyWith(color: Colors.black),
              );
            },
            onTap: (int id, UsersResponse? selected) {
              setState(() {
                listInput[3].controller.text = selected!.displayName!;
                typeId = selected.id;
              });
            },
          ),
          WidgetInput(
            controller: listInput[4].controller,
            focusNode: listInput[4].focusNode,
            validator:
                FormBuilderValidators.required(errorText: 'error_miss'.tr),
            hintText: 'note'.tr,
          ),
          WidgetInput(
            controller: listInput[5].controller,
            focusNode: listInput[5].focusNode,
            keyboardType: TextInputType.number,
            validator:
                FormBuilderValidators.required(errorText: 'error_miss'.tr),
            hintText: 'total_money'.tr,
          ),
        ],
      ),
    );
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
      child: WidgetButton(
        text: 'save'.tr,
        isFullWidth: true,
        onTap: () {
          if (billDetailController.idBill == -1) {
            if (typeNavigation == 0) {
              onCreateCollect();
            } else {
              onCreatePay();
            }
          } else {
            if (typeNavigation == 0) {
              onEditCollect();
            } else {
              onEditPay();
            }
          }
        },
      ),
    );
  }
}
