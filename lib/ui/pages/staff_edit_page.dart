import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/controllers/controllers.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/res/values.dart';
import 'package:kiot_pos/ui/pages/area_edit_page.dart';
import 'package:kiot_pos/ui/ui.dart';
import 'package:kiot_pos/ui/widgets/widget_input.dart';

class StaffEditPage extends StatefulWidget {
  const StaffEditPage({Key? key}) : super(key: key);

  @override
  State<StaffEditPage> createState() => _StaffEditPageState();
}

class _StaffEditPageState extends State<StaffEditPage> {
  List<ItemInputKhuVuc> listInput = [];
  StaffController staffController = Get.find<StaffController>();
  StaffDetailController staffDetailController =
      Get.find<StaffDetailController>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for (int i = 0; i < 4; i++) {
      listInput.add(ItemInputKhuVuc(
          controller: TextEditingController(), focusNode: FocusNode()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetX<StaffDetailController>(
      builder: (controller) {
        if (controller.detailStaff != null &&
            controller.detailStaff!.nhanVien != null) {
          listInput[0].controller.text =
              controller.detailStaff!.nhanVien!.employeeName!;
          listInput[1].controller.text =
              controller.detailStaff!.nhanVien!.employeeCode!;

          listInput[2].controller.text =
              controller.detailStaff!.nhanVien!.employeePhone!;

          listInput[3].controller.text = AppValue.formatterVnd.format(
              controller.detailStaff!.nhanVien!.employeePrice.toString());
        }
        return GunPage(
            appBarBottom: GunAppBar(
              type: GunAppBarType.BOTTOM,
              height: 0,
            ),
            appBarTop: GunAppBar(
                type: GunAppBarType.TOP,
                title: Get.arguments != null
                    ? controller.detailStaff!.nhanVien!.employeeName
                    : 'staff.add'.tr,
                titleStyle: AppStyles.pageTitleStyle,
                leadingColor: Colors.black),
            child: Stack(
              children: [
                Column(
                  children: [
                    Expanded(
                        child: SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          children: [
                            ...List.generate(
                              listInput.length,
                              (index) => WidgetInput(
                                controller: listInput[index].controller,
                                focusNode: listInput[index].focusNode,
                                keyboardType: index == 3 || index == 2
                                    ? TextInputType.number
                                    : TextInputType.text,
                                validator: FormBuilderValidators.required(
                                    errorText: 'error_miss'.tr),
                                hintText: index == 0
                                    ? 'staff.name'.tr
                                    : index == 1
                                        ? 'staff.code'.tr
                                        : index == 2
                                            ? 'phone'.tr
                                            : 'staff.unit_price'.tr,
                                inputFormatters: index == 3
                                    ? <TextInputFormatter>[
                                        AppValue.formatterVnd
                                      ]
                                    : null,
                                onChanged: (e) {},
                                textAlign: TextAlign.right,
                                childLeft: Container(
                                  padding: EdgeInsets.only(right: 8),
                                  child: GunText(
                                    index == 0
                                        ? 'staff.name'.tr
                                        : index == 1
                                            ? 'staff.code'.tr
                                            : index == 2
                                                ? 'phone'.tr
                                                : 'staff.unit_price'.tr,
                                    maxLines: 1,
                                    style: AppStyles.DEFAULT_16
                                        .copyWith(color: Colors.black),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ))
                  ],
                ),
                Positioned.fill(
                    child: Align(
                  alignment: Alignment.bottomCenter,
                  child: _renderButton(),
                ))
              ],
            ));
      },
    );
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
      child: WidgetButton(
        onTap: () async {
          setState(() {
            listInput[3].controller.text =
                AppValue.formatterVnd.getUnformattedValue().toInt().toString();
          });
          if (Get.arguments != null) {
            var result = await staffDetailController.staffEdit(
                id: Get.arguments,
                employeeCode: listInput[1].controller.text,
                employeeName: listInput[0].controller.text,
                employeePhone: listInput[2].controller.text,
                employeePrice: listInput[3].controller.text);
            if (result.type == "success") {
              Get.dialog(DialogCustom(
                  title: 'Thông báo',
                  onTapOK: () {
                    Get.back();
                    Get.back();
                    staffDetailController.getDetailStaff(id: Get.arguments);
                  },
                  children: [
                    Text(result.message!, style: AppStyles.DEFAULT_16)
                  ]));
            }
          } else {
            var result = await staffDetailController.staffCreate(
                employeeCode: listInput[1].controller.text,
                employeeName: listInput[0].controller.text,
                employeePhone: listInput[2].controller.text,
                employeePrice: listInput[3].controller.text);
            if (result.type == "success") {
              Get.dialog(DialogCustom(
                  title: 'Thông báo',
                  onTapOK: () {
                    Get.back();
                    Get.back();
                    staffController.searchStaff(tuKhoa: '');
                  },
                  children: [
                    Text(result.message!, style: AppStyles.DEFAULT_16)
                  ]));
            }
          }
        },
        text: 'save'.tr,
        isFullWidth: true,
      ),
    );
  }
}
