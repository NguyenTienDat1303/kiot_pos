import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/data/entities/response/stock_detail_response.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/widgets/avatar_create.dart';
import 'package:get/get.dart';

class StockingDetailPage extends StatefulWidget {
  const StockingDetailPage({Key? key}) : super(key: key);

  @override
  State<StockingDetailPage> createState() => _StockingDetailPageState();
}

class _StockingDetailPageState extends State<StockingDetailPage> {
  StockController stockController = Get.find<StockController>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(Duration.zero, () {
      stockController.getDetailStock(id: stockController.idStock);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetX<StockController>(
      builder: (controller) {
        return GunPage(
          resizeToAvoidBottomInset: false,
          appBarBottom: GunAppBar(
            type: GunAppBarType.BOTTOM,
            height: 0,
          ),
          appBarTop: GunAppBar(
            type: GunAppBarType.TOP,
            title: controller.detailStock != null
                ? controller.detailStock!.kiemKe!.adjustCode
                : '',
            titleStyle: AppStyles.pageTitleStyle,
            leadingColor: Colors.black,
            actions: [
              GunAction.icon(
                icon: Container(width: 24),
              )
            ],
          ),
          child: Container(
            child: Column(
              children: [
                Expanded(
                    child: SingleChildScrollView(
                  child: _renderBody(controller.detailStock),
                )),
                _renderButton()
              ],
            ),
          ),
        );
      },
    );
  }

  _renderBody(StockDetailResponse? data) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: data != null
          ? Column(
              children: [
                OneRow(
                    title: 'Mã phiếu kiểm', subtitle: data.kiemKe!.adjustCode!),
                OneRow(
                    title: 'Trạng thái',
                    subtitle: data.kiemKe!.adjustStatus == 0
                        ? 'Lưu tạm'
                        : 'Hoàn thành'),
                OneRow(title: 'Người kiểm', subtitle: data.nguoiKiem ?? ''),
                OneRow(title: 'Ngày kiểm', subtitle: data.kiemKe!.adjustDate!),
                OneRow(
                    title: 'Số lượng kiểm',
                    subtitle: data.soLuongKiem!.toString()),
                OneRow(title: 'Lệch âm', subtitle: data.lechAm!.toString()),
                OneRow(
                    title: 'Lệch dương', subtitle: data.lechDuong!.toString()),
                OneRow(
                    title: 'Số lượng chênh lệch',
                    subtitle: data.kiemKe!.totalDifferent!.toString()),
                OneRow(title: 'Ghi chú', subtitle: data.kiemKe!.notes!),
                Material(
                  color: AppColors.White,
                  child: InkWell(
                    onTap: () {
                      stockController.isCompleted = data.kiemKe!.adjustStatus!;
                      Get.toNamed(AppRoutes.stocking_detail_item);
                    },
                    child: Row(
                      children: [
                        Expanded(
                            child: Container(
                          padding: EdgeInsets.symmetric(vertical: 5),
                          child: GunText(
                            'Chi tiết hàng kiểm',
                            style: AppStyles.DEFAULT_14
                                .copyWith(color: AppColors.Blue),
                          ),
                        )),
                        Icon(
                          Icons.chevron_right,
                          size: 30,
                        )
                      ],
                    ),
                  ),
                )
              ],
            )
          : SizedBox.shrink(),
    );
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.all(30),
      child: WidgetButton(
        onTap: () {
          Get.dialog(DialogCustom(
              title: 'Thông báo',
              isTwoButton: true,
              nameButtonLeft: 'Huỷ',
              nameButtonRight: 'Xoá',
              onTapLeft: () {
                Get.back();
              },
              onTapRight: () async {
                Get.back();
                var result = await stockController.stockDelete(
                    id: stockController.idStock);
                Get.dialog(DialogCustom(
                    title: 'Thông báo',
                    onTapOK: () {
                      Get.back();
                      Get.back();

                      stockController.getStockSearch(tuKhoa: '');
                    },
                    children: [
                      Text(result.message!, style: AppStyles.DEFAULT_16)
                    ]));
              },
              children: [
                Text('Bạn có chắc chắn muốn xoá ?',
                    style: AppStyles.DEFAULT_16),
              ]));
        },
        isFullWidth: true,
        text: 'delete'.tr,
      ),
    );
  }
}
