import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  ForgotPasswordController _controller = Get.find<ForgotPasswordController>();
  final _formKey = GlobalKey<FormState>();

  TextEditingController _infoEditController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'forgot_password.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
        ),
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            SizedBox(
                              height: 30,
                            ),
                            TextFormField(
                                controller: _infoEditController,
                                autovalidateMode: AutovalidateMode.always,
                                validator: FormBuilderValidators.required(
                                    errorText: "login.user_name_required".tr),
                                style: AppStyles.normalTextStyle,
                                // textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.center,
                                decoration: InputDecoration(
                                  isDense: true,
                                  // alignLabelWithHint: true,
                                  hintText: 'login.user_name'.tr,
                                  contentPadding: EdgeInsets.only(bottom: 10),
                                  prefixIconConstraints:
                                      BoxConstraints(minWidth: 0, minHeight: 0),
                                  prefixIcon: Container(
                                      margin:
                                          EdgeInsets.only(right: 5, bottom: 10),
                                      width: 20,
                                      child: Image.asset(
                                          'assets/images/login_user_name.png')),
                                )),
                            SizedBox(
                              height: 30,
                            ),
                            Container(
                              width: double.infinity,
                              child: TextButton(
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        AppColors.primary),
                                    shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20),
                                    ))),
                                onPressed: () async {
                                  // Validate returns true if the form is valid, or false otherwise.
                                  if (_formKey.currentState!.validate()) {
                                    var response =
                                        await _controller.forgotPassword(
                                      _infoEditController.text,
                                    );
                                    Get.back();
                                    Get.dialog(DialogCustom(
                                        title: "dialog.title_notify".tr,
                                        children: [
                                          Text("${response.message}")
                                        ]));
                                  } else {
                                    // ScaffoldMessenger.of(context)
                                    //     .showSnackBar(
                                    //   const SnackBar(content: Text('Ko dc')),
                                    // );
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.all(3),
                                  child: Text(
                                    'forgot_password.submit'.tr,
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )),
                  )
                ],
              ),
            ),
          ],
        ));
  }

  // _renderInput() {
  //   return Container(
  //     padding: EdgeInsets.symmetric(horizontal: 30.w),
  //     child: Column(
  //       children: [
  //         Container(
  //           child: InputCustomBorderBottom(
  //             controller: emailController,
  //             focusNode: emailFocusNode,
  //             left: Image.asset(
  //               './assets/shop_icon.png',
  //               height: 24.r,
  //             ),
  //             right: Text(
  //               '.kiotsoft.com',
  //               style: AppStyle.DEFAULT_16.copyWith(color: AppColors.Blue),
  //             ),
  //             hintText: 'Tên cửa hàng',
  //             onChanged: (e) {
  //               setState(() {});
  //             },
  //           ),
  //         ),
  //         Container(
  //           margin: EdgeInsets.only(top: 10.r),
  //           child: InputCustomBorderBottom(
  //             controller: userController,
  //             focusNode: userFocusNode,
  //             left: Image.asset(
  //               './assets/user_icon.png',
  //               height: 24.h,
  //             ),
  //             hintText: 'Tên đăng nhập',
  //             onChanged: (e) {
  //               setState(() {});
  //             },
  //           ),
  //         ),
  //         Container(
  //           margin: EdgeInsets.only(top: 10.h),
  //           child: InputCustomBorderBottom(
  //             controller: passController,
  //             focusNode: passFocusNode,
  //             left: Image.asset(
  //               './assets/pass_icon.png',
  //               height: 24.r,
  //             ),
  //             obscureText: isVisible,
  //             right: GestureDetector(
  //               onTap: onVisible,
  //               child: Image.asset(
  //                   isVisible
  //                       ? './assets/visible_off.png'
  //                       : './assets/visible_icon.png',
  //                   height: 24.r,
  //                   color: AppColors.Gray4),
  //             ),
  //             hintText: 'Mật khẩu',
  //             onChanged: (e) {
  //               setState(() {});
  //             },
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }

  // _renderButton() {
  //   return Container(
  //     padding: EdgeInsets.symmetric(horizontal: 30.w),
  //     child: Column(
  //       children: [
  //         Container(
  //           margin: EdgeInsets.only(top: 20.r),
  //           alignment: Alignment.centerRight,
  //           child: GestureDetector(
  //             onTap: onForget,
  //             child: Text(
  //               'Quên mật khẩu ?',
  //               style: AppStyle.DEFAULT_16.copyWith(color: AppColors.Blue),
  //             ),
  //           ),
  //         ),
  //         Container(
  //           margin: EdgeInsets.only(top: 20.h),
  //           child: ButtonCustom(
  //             onTap: onForgotPassword,
  //             text: 'Đăng nhập',
  //             isFullWidth: true,
  //             backgroundColor: AppColors.Blue,
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // _renderInfo() {
  //   return Column(
  //     children: [
  //       Container(
  //         margin: EdgeInsets.only(top: 30.h),
  //         child: Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Column(
  //               children: [
  //                 Image.asset('./assets/contact_icon.png', height: 26.h),
  //                 Container(
  //                     margin: EdgeInsets.only(top: 10.h),
  //                     child: Image.asset('./assets/website.png', height: 26.h)),
  //               ],
  //             ),
  //             Container(
  //               margin: EdgeInsets.only(left: 10.w),
  //               child: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.start,
  //                   children: [
  //                     Text(
  //                       '0888.1900.36',
  //                       style: AppStyle.DEFAULT_20_BOLD.copyWith(
  //                           color: AppColors.Blue, fontWeight: FontWeight.w700),
  //                     ),
  //                     Container(
  //                       margin: EdgeInsets.only(top: 10.h),
  //                       child: Text(
  //                         'KIOTSOFT.COM',
  //                         style: AppStyle.DEFAULT_20_BOLD.copyWith(
  //                             color: AppColors.Blue,
  //                             fontWeight: FontWeight.w700),
  //                       ),
  //                     )
  //                   ]),
  //             ),
  //           ],
  //         ),
  //       ),
  //       Container(height: 20)
  //     ],
  //   );
  // }

  // _renderBackground() {
  //   return Positioned.fill(
  //       child: Align(
  //     // alignment: Alignment.bottomCenter,
  //     child: Column(
  //       children: [
  //         Expanded(child: Container()),
  //         Image.asset(
  //           './assets/background.png',
  //           width: 1.sw,
  //         ),
  //       ],
  //     ),
  //   ));
  // }
}
