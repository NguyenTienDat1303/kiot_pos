import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:gun_base/res/res.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomerCreatePage extends StatefulWidget {
  const CustomerCreatePage({Key? key}) : super(key: key);

  @override
  State<CustomerCreatePage> createState() => _CustomerCreatePageState();
}

class _CustomerCreatePageState extends State<CustomerCreatePage> {
  CustomerController customerController = Get.find<CustomerController>();
  TextEditingController nameController = TextEditingController();
  FocusNode nameFocusNode = FocusNode();
  TextEditingController dayController = TextEditingController();
  FocusNode dayFocusNode = FocusNode();
  TextEditingController phoneController = TextEditingController();
  FocusNode phoneFocusNode = FocusNode();
  TextEditingController emailController = TextEditingController();
  FocusNode emailFocusNode = FocusNode();
  TextEditingController addressController = TextEditingController();
  FocusNode addressFocusNode = FocusNode();
  TextEditingController codeController = TextEditingController();
  FocusNode codeFocus = FocusNode();
  TextEditingController debtController = TextEditingController();
  FocusNode debtFocus = FocusNode();
  TextEditingController groupCustomerController = TextEditingController();
  FocusNode groupCustomerFocus = FocusNode();
  GlobalKey<FormState> key = GlobalKey();
  CustomerCreateController customerCreateController =
      Get.find<CustomerCreateController>();

  List<String> listGender = ['male'.tr, 'female'.tr, 'other'.tr];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 0), () {
      customerCreateController.getGroupCustomer();
    });
  }

  String day = '';
  Future onDayPicker(BuildContext context) async {
    final initialDate = DateTime(2002, 1);
    final newDay = await showDatePicker(
        context: context,
        initialDate: initialDate,
        initialEntryMode: DatePickerEntryMode.calendarOnly,
        builder: (context, child) {
          return Theme(
              data: ThemeData.light().copyWith(
                colorScheme: ColorScheme.light(
                  primary: AppColors.Blue,
                  onPrimary: Colors.white,
                  surface: AppColors.Blue,
                  onSurface: AppColors.Gray1,
                ),
              ),
              child: child!);
        },
        firstDate: DateTime(DateTime.now().year - 100),
        lastDate: DateTime(2002, 1));
    if (newDay == null) return;
    setState(() {
      day = GunValue.DATE_FORMAT.format(newDay);
      dayController.text = day;
    });
  }

  int indexGender = 0;
  GroupCustomerResponse? groupCustomer;
  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarBottom: GunAppBar(
          type: GunAppBarType.BOTTOM,
          height: 0,
        ),
        appBarTop: GunAppBar(
            type: GunAppBarType.TOP,
            title: 'customer.add'.tr,
            titleStyle: AppStyles.pageTitleStyle,
            leadingColor: Colors.black),
        child: Column(
          children: [
            Expanded(child: _renderBody()),
          ],
        ));
  }

  _renderBody() {
    return SingleChildScrollView(
        child: Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Form(
        key: key,
        child: Column(
          children: [
            WidgetInput(
              controller: nameController,
              focusNode: nameFocusNode,
              validator:
                  FormBuilderValidators.required(errorText: 'error_miss'.tr),
              hintText: 'customer.name'.tr,
            ),
            GetX<CustomerCreateController>(
              builder: (controller) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    WidgetDropdown(
                      controller: groupCustomerController,
                      focusNode: groupCustomerFocus,
                      list: controller.listGroupCustomer,
                      hintText: 'customer.group'.tr,
                      isAutoComplete: true,
                      builder: (int id, GroupCustomerResponse item,
                          GroupCustomerResponse? selected) {
                        return GunText(
                          item.customerGroupName!,
                          style: AppStyles.DEFAULT_16
                              .copyWith(color: Colors.black),
                        );
                      },
                      onTap: (int id, GroupCustomerResponse? selected) {
                        setState(() {
                          groupCustomer = selected;
                          groupCustomerController.text =
                              selected!.customerGroupName!;
                        });
                      },
                    ),
                  ],
                );
              },
            ),
            WidgetInput(
              controller: codeController,
              focusNode: codeFocus,
              hintText: 'customer.code'.tr,
              onChanged: (e) {},
            ),
            Material(
              color: Colors.white,
              child: InkWell(
                onTap: () {
                  onDayPicker(context);
                },
                child: AbsorbPointer(
                  child: WidgetInput(
                    controller: dayController,
                    focusNode: dayFocusNode,
                    hintText: 'birthday'.tr,
                    onChanged: (e) {},
                    childRight: Image.asset(
                      './assets/images/calender_icon.png',
                      height: 25.h,
                      color: AppColors.Orange,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      'Giới tính',
                      style: AppStyles.DEFAULT_16.copyWith(
                        color: AppColors.Gray1,
                      ),
                    ),
                  ),
                  ...List.generate(
                      listGender.length,
                      (index) => Container(
                            margin: EdgeInsets.only(left: 20.r),
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  indexGender = index;
                                });
                                print(indexGender);
                              },
                              child: Row(children: [
                                Container(
                                  padding: EdgeInsets.all(3.r).r,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          width: 1, color: AppColors.Blue)),
                                  child: CircleAvatar(
                                    backgroundColor: indexGender == index
                                        ? AppColors.Blue
                                        : Colors.white,
                                    radius: 4,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 10.r),
                                  child: Text(
                                    listGender[index],
                                    style: AppStyles.DEFAULT_16.copyWith(
                                      color: AppColors.Gray1,
                                    ),
                                  ),
                                )
                              ]),
                            ),
                          )),
                ],
              ),
            ),
            WidgetInput(
              controller: phoneController,
              focusNode: phoneFocusNode,
              keyboardType: TextInputType.number,
              hintText: 'phone'.tr,
              onFieldSubmitted: (e) {
                FocusScope.of(context).autofocus(emailFocusNode);
              },
            ),
            WidgetInput(
              controller: emailController,
              focusNode: emailFocusNode,
              keyboardType: TextInputType.emailAddress,
              hintText: 'email'.tr,
              onFieldSubmitted: (e) {
                FocusScope.of(context).autofocus(addressFocusNode);
              },
            ),
            WidgetInput(
              controller: addressController,
              focusNode: addressFocusNode,
              hintText: 'address'.tr,
              onFieldSubmitted: (e) {
                FocusScope.of(context).autofocus(debtFocus);
              },
            ),
            WidgetInput(
              controller: debtController,
              focusNode: debtFocus,
              keyboardType: TextInputType.number,
              hintText: 'customer.debt'.tr,
            ),
            _renderButton()
          ],
        ),
      ),
    ));
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.all(30),
      child: WidgetButton(
        text: 'save'.tr,
        isFullWidth: true,
        onTap: () {
          if (key.currentState!.validate()) {
            customerCreateController.createCustomer(
                customerName: nameController.text,
                customerPhone:
                    phoneController.text != '' ? phoneController.text : null,
                customerEmail:
                    emailController.text != '' ? emailController.text : null,
                customerAddr: addressController.text != ''
                    ? addressController.text
                    : null,
                notes: null,
                customerBirthday:
                    dayController.text != '' ? dayController.text : null,
                customerGender: 1,
                customerGroup:
                    groupCustomer != null ? groupCustomer!.id! : null,
                customerCoin: debtController.text != ''
                    ? int.parse(debtController.text)
                    : null,
                callback: () {
                  Get.back();
                  customerController.getListCustomer(tuKhoa: '');
                });
          }
        },
      ),
    );
  }
}
