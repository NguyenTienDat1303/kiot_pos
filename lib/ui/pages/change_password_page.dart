import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  State<ChangePasswordPage> createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  ChangePasswordController _controller = Get.find<ChangePasswordController>();
  final _formKey = GlobalKey<FormState>();

  TextEditingController _passwordEditController = TextEditingController();
  TextEditingController _newPasswordEditController = TextEditingController();
  TextEditingController _newRePasswordEditController = TextEditingController();

  bool _passwordObscureText = true;
  bool _newPasswordObscureText = true;
  bool _newRePasswordObscureText = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'change_password.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leadingColor: Colors.black,
        ),
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            TextFormField(
                                controller: _passwordEditController,
                                autovalidateMode: AutovalidateMode.always,
                                validator: FormBuilderValidators.required(
                                    errorText:
                                        "change_password.password_required".tr),
                                obscureText: _passwordObscureText,
                                style: AppStyles.normalTextStyle,
                                // textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.center,
                                decoration: InputDecoration(
                                  isDense: true,
                                  // alignLabelWithHint: true,
                                  hintText: 'change_password.password'.tr,
                                  contentPadding: EdgeInsets.only(bottom: 10),
                                  prefixIconConstraints:
                                      BoxConstraints(minWidth: 0, minHeight: 0),
                                  prefixIcon: Container(
                                      margin:
                                          EdgeInsets.only(right: 5, bottom: 10),
                                      width: 20,
                                      child: Image.asset(
                                          'assets/images/login_password.png')),
                                  suffixIcon: Container(
                                    width: 24,
                                    margin: EdgeInsets.only(bottom: 10),
                                    child: GunInkwell(
                                      onTap: () {
                                        setState(() {
                                          _passwordObscureText =
                                              !_passwordObscureText;
                                        });
                                      },
                                      padding: EdgeInsets.zero,
                                      child: Image.asset(_passwordObscureText
                                          ? 'assets/images/password_eye.png'
                                          : 'assets/images/password_eye_remove.png'),
                                    ),
                                  ),
                                  suffixIconConstraints: BoxConstraints(
                                      minWidth: 0, minHeight: 10),
                                )),
                            SizedBox(
                              height: 30,
                            ),
                            TextFormField(
                                controller: _newPasswordEditController,
                                autovalidateMode: AutovalidateMode.always,
                                validator: FormBuilderValidators.required(
                                    errorText:
                                        "forgot_password.new_password_required"
                                            .tr),
                                obscureText: _newPasswordObscureText,
                                style: AppStyles.normalTextStyle,
                                // textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.center,
                                decoration: InputDecoration(
                                  isDense: true,
                                  // alignLabelWithHint: true,
                                  hintText: 'change_password.new_password'.tr,
                                  contentPadding: EdgeInsets.only(bottom: 10),
                                  prefixIconConstraints:
                                      BoxConstraints(minWidth: 0, minHeight: 0),
                                  prefixIcon: Container(
                                      margin:
                                          EdgeInsets.only(right: 5, bottom: 10),
                                      width: 20,
                                      child: Image.asset(
                                          'assets/images/login_password.png')),
                                  suffixIcon: Container(
                                    width: 24,
                                    margin: EdgeInsets.only(bottom: 10),
                                    child: GunInkwell(
                                      onTap: () {
                                        setState(() {
                                          _newPasswordObscureText =
                                              !_newPasswordObscureText;
                                        });
                                      },
                                      padding: EdgeInsets.zero,
                                      child: Image.asset(_newPasswordObscureText
                                          ? 'assets/images/password_eye.png'
                                          : 'assets/images/password_eye_remove.png'),
                                    ),
                                  ),
                                  suffixIconConstraints: BoxConstraints(
                                      minWidth: 0, minHeight: 10),
                                )),
                            SizedBox(
                              height: 30,
                            ),
                            TextFormField(
                                controller: _newRePasswordEditController,
                                autovalidateMode: AutovalidateMode.always,
                                validator: FormBuilderValidators.required(
                                    errorText:
                                        "forgot_password.new_re_password_required"
                                            .tr),
                                obscureText: _newRePasswordObscureText,
                                style: AppStyles.normalTextStyle,
                                // textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.center,
                                decoration: InputDecoration(
                                  isDense: true,
                                  // alignLabelWithHint: true,
                                  hintText:
                                      'change_password.new_re_password'.tr,
                                  contentPadding: EdgeInsets.only(bottom: 10),
                                  prefixIconConstraints:
                                      BoxConstraints(minWidth: 0, minHeight: 0),
                                  prefixIcon: Container(
                                      margin:
                                          EdgeInsets.only(right: 5, bottom: 10),
                                      width: 20,
                                      child: Image.asset(
                                          'assets/images/login_password.png')),
                                  suffixIcon: Container(
                                    width: 24,
                                    margin: EdgeInsets.only(bottom: 10),
                                    child: GunInkwell(
                                      onTap: () {
                                        setState(() {
                                          _newRePasswordObscureText =
                                              !_newRePasswordObscureText;
                                        });
                                      },
                                      padding: EdgeInsets.zero,
                                      child: Image.asset(_newRePasswordObscureText
                                          ? 'assets/images/password_eye.png'
                                          : 'assets/images/password_eye_remove.png'),
                                    ),
                                  ),
                                  suffixIconConstraints: BoxConstraints(
                                      minWidth: 0, minHeight: 10),
                                )),
                            SizedBox(
                              height: 30,
                            ),
                            Container(
                              width: double.infinity,
                              child: TextButton(
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        AppColors.primary),
                                    shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20),
                                    ))),
                                onPressed: () async {
                                  // Validate returns true if the form is valid, or false otherwise.
                                  if (_formKey.currentState!.validate()) {
                                    var response =
                                        await _controller.changePassword(
                                      _passwordEditController.text,
                                      _newPasswordEditController.text,
                                      _newRePasswordEditController.text,
                                    );
                                    Get.back();
                                    Get.dialog(DialogCustom(
                                        title: "dialog.title_notify".tr,
                                        children: [
                                          Text(
                                            "${response.message}",
                                            textAlign: TextAlign.center,
                                          )
                                        ]));
                                  } else {
                                    // ScaffoldMessenger.of(context)
                                    //     .showSnackBar(
                                    //   const SnackBar(content: Text('Ko dc')),
                                    // );
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.all(3),
                                  child: Text(
                                    'change_password.submit'.tr,
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}
