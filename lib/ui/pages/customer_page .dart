import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/ui/widgets/base_info_widget.dart';

class CustomerPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const CustomerPage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<CustomerPage> createState() => _CustomerPageState();
}

class _CustomerPageState extends State<CustomerPage> {
  CustomerController customerController = Get.find<CustomerController>();
  HomeController homeController = Get.find<HomeController>();

  TextEditingController textController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Get.put(HomeController());
    Future.delayed(Duration(seconds: 0), () {
      customerController.getListCustomer(tuKhoa: '');
    });
  }

  bool isSearch = false;
  @override
  Widget build(BuildContext context) {
    return GetX<CustomerController>(
      builder: (controller) {
        return GunPage(
            appBarBottom: GunAppBar(
              type: GunAppBarType.BOTTOM,
              height: 0,
            ),
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              titles: Row(
                children: [
                  isSearch
                      ? Expanded(
                          child: TextField(
                          controller: textController,
                          onChanged: (e) {
                            setState(() {});
                          },
                          onSubmitted: (e) {
                            controller.getListCustomer(tuKhoa: e);
                          },
                          autofocus: true,
                          decoration: InputDecoration(
                              isDense: true, border: InputBorder.none),
                        ))
                      : GunText(
                          'home_drawer.customer'.tr,
                          style: AppStyles.pageTitleStyle,
                        ),
                ],
              ),
              leading: [
                GunAction.icon(
                    iconFactor: 0.5,
                    onTap: () {
                      widget.homePageKey.currentState!.openDrawer();
                    },
                    icon: Image.asset('assets/images/drawer_menu.png'))
              ],
              actions: [
                GunAction.icon(
                  onTap: () {
                    setState(() {
                      isSearch = !isSearch;
                      textController.clear();
                    });
                    if (isSearch == false) {
                      customerController.getListCustomer(tuKhoa: '');
                    }
                  },
                  iconFactor: isSearch ? 0.4 : 0.7,
                  icon: Image.asset(
                    isSearch
                        ? './assets/images/close_icon.png'
                        : './assets/images/search_icon.png',
                    color: AppColors.Orange,
                  ),
                ),
                if (isSearch == false)
                  GunAction.icon(
                    iconFactor: 0.5,
                    onTap: () {
                      Get.toNamed(AppRoutes.customer_create);
                    },
                    icon: Image.asset(
                      './assets/images/add_icon.png',
                    ),
                  ),
              ],
            ),
            child: _renderBody(controller.listCustomer));
      },
    );
  }

  _renderBody(List<CustomerResponse> list) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...List.generate(
              list.length,
              (index) => Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: BaseInfoWidget(
                        title: list[index].customerName,
                        contextLeft1: 'customer.code'.tr,
                        contextLeft2: 'address'.tr,
                        contextLeft3: 'phone'.tr,
                        contextRight1: list[index].customerCode!,
                        contextRight2: list[index].customerAddr!,
                        contextRight3: list[index].customerPhone!),
                  )),
        ],
      ),
    );
  }
}
