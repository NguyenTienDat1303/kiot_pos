import 'package:flutter/material.dart';

import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kiot_pos/ui/widgets/widget_dialog.dart';

class AreaEditPage extends StatefulWidget {
  const AreaEditPage({Key? key}) : super(key: key);

  @override
  State<AreaEditPage> createState() => _AreaEditPageState();
}

class _AreaEditPageState extends State<AreaEditPage> {
  AreaEditController areaEditController = Get.find<AreaEditController>();
  AreaController areaController = Get.find<AreaController>();
  List<ItemPriceKhuVuc> listInput2 = [];
  List<ItemInputKhuVuc> listInput1 = [];
  GlobalKey<FormState> key = GlobalKey();

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 0), () {
      areaEditController.loadStoreList();
    });
    if (Get.arguments != null) {
      areaEditController.getDetailArea(Get.arguments);
    }
    for (var i = 0; i < 2; i++) {
      listInput1.add(ItemInputKhuVuc(
          controller: TextEditingController(), focusNode: FocusNode()));
    }
    for (var i = 0; i < 3; i++) {
      listInput2.add(ItemPriceKhuVuc(
        fromController: TextEditingController(
            text: i == 0
                ? '0'
                : i == 1
                    ? '6'
                    : '18'),
        toController: TextEditingController(
            text: i == 0
                ? '6'
                : i == 1
                    ? '18'
                    : '24'),
        priceController: TextEditingController(text: '0'),
        fromFocus: FocusNode(),
        toFocus: FocusNode(),
        priceFocus: FocusNode(),
      ));
    }
  }

  onTao() {
    if (store != null) {
      areaEditController.areaCreate(
          areaName: listInput1[0].controller.text,
          numberTable: int.parse(listInput1[1].controller.text),
          from1: 0,
          to1: int.parse(listInput2[0].toController.text),
          area_price1: int.parse(listInput2[0].priceController.text),
          from2: int.parse(listInput2[1].fromController.text),
          to2: int.parse(listInput2[1].toController.text),
          area_price2: int.parse(listInput2[1].priceController.text),
          from3: int.parse(listInput2[2].fromController.text),
          to3: 24,
          area_price3: int.parse(listInput2[2].priceController.text),
          storeId: store!.id!,
          callback: () {
            Get.back();
            areaController.area();
          });
    }
  }

  onSua() {
    if (Get.arguments != null && store != null) {
      areaEditController.areaEdit(
          id: Get.arguments!,
          areaName: listInput1[0].controller.text,
          numberTable: int.parse(listInput1[1].controller.text),
          from1: 0,
          to1: int.parse(listInput2[0].toController.text),
          area_price1: int.parse(listInput2[0].priceController.text),
          from2: int.parse(listInput2[1].fromController.text),
          to2: int.parse(listInput2[1].toController.text),
          area_price2: int.parse(listInput2[1].priceController.text),
          from3: int.parse(listInput2[2].fromController.text),
          to3: 24,
          area_price3: int.parse(listInput2[2].priceController.text),
          storeId: store!.id!,
          callback: () {
            Get.back();
            areaController.area();
          });
    }
  }

  StoreData? store;

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarBottom: GunAppBar(
          type: GunAppBarType.BOTTOM,
          height: 0,
        ),
        appBarTop: GunAppBar(
            decoration: BoxDecoration(color: AppColors.Background),
            type: GunAppBarType.TOP,
            title: Get.arguments != null ? 'area.edit'.tr : 'area.create'.tr,
            titleStyle: AppStyles.pageTitleStyle,
            leadingColor: Colors.black),
        child: Column(
          children: [Expanded(child: _renderBody())],
        ));
  }

  _renderBody() {
    return GetX<AreaEditController>(
      builder: (controller) {
        if (controller.detailArea != null &&
            controller.detailArea!.khuVuc != null &&
            Get.arguments != null) {
          var data = controller.detailArea!;
          listInput1[0].controller.text = data.khuVuc!.areaName!;
          listInput1[1].controller.text = '${data.khuVuc!.numberTable}';
          listInput2[0].fromController.text = '${data.khuVuc!.from1}';
          listInput2[1].fromController.text = '${data.khuVuc!.from2}';
          listInput2[2].fromController.text = '${data.khuVuc!.from3}';
          listInput2[0].toController.text = '${data.khuVuc!.to1}';
          listInput2[1].toController.text = '${data.khuVuc!.to2}';
          listInput2[2].toController.text = '${data.khuVuc!.to3}';
          listInput2[0].priceController.text = '${data.khuVuc!.areaPrice1}';
          listInput2[1].priceController.text = '${data.khuVuc!.areaPrice2}';
          listInput2[2].priceController.text = '${data.khuVuc!.areaPrice3}';
          store = data.kho;
        }
        return SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 30),
            color: Colors.white,
            child: Form(
              key: key,
              child: Column(
                children: [
                  _renderInput1(),
                  _renderChooseKhuVuc(controller.storeList),
                  _renderInput2(),
                  _renderButton()
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  _renderInput1() {
    return Column(
      children: [
        ...List.generate(
            listInput1.length,
            (index) => _renderInput(
                  keyboardType:
                      index != 0 ? TextInputType.number : TextInputType.text,
                  controller: listInput1[index].controller,
                  focusNode: listInput1[index].focusNode,
                  textAlign: TextAlign.left,
                  hintText: index == 0 ? 'Tên khu vực' : 'Tổng số phòng bàn',
                )),
      ],
    );
  }

  _renderInput2() {
    return Column(
      children: [
        ...List.generate(
            listInput2.length,
            (index) => _renderInputPrice(
                fromController: listInput2[index].fromController,
                toController: listInput2[index].toController,
                priceController: listInput2[index].priceController,
                fromFocus: listInput2[index].fromFocus,
                toFocus: listInput2[index].toFocus,
                priceFocus: listInput2[index].priceFocus,
                hintText:
                    'input_unit_price'.trParams({'index': '${index + 1}'}),
                index: index)),
      ],
    );
  }

  _renderChooseKhuVuc(List<StoreData> list) {
    return Container(
      padding: EdgeInsets.only(top: 22.r, bottom: 10.r),
      decoration: BoxDecoration(
          border:
              Border(bottom: BorderSide(width: 1, color: Color(0xffE7E7E7)))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('Chọn chi nhánh', style: AppStyles.DEFAULT_16),
          PopupMenuButton(
            child: Text(store != null ? store!.storeName ?? '' : 'Chọn kho',
                style: AppStyles.DEFAULT_16),
            itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              ...List.generate(
                list.length,
                (index) => PopupMenuItem(
                    onTap: () {
                      setState(() {
                        store = list[index];
                      });

                      print(store);
                    },
                    child: Text(list[index].storeName!,
                        style: AppStyles.DEFAULT_16)),
              )
            ],
          )
        ],
      ),
    );
  }

  _renderInputPrice(
      {required TextEditingController fromController,
      required TextEditingController toController,
      required TextEditingController priceController,
      required FocusNode fromFocus,
      required FocusNode toFocus,
      required FocusNode priceFocus,
      required String hintText,
      required int index}) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(
        margin: EdgeInsets.only(top: index == 0 ? 14.r : 24.r),
        child: GunText(
          'unit_price'.trParams({'index': (index + 1).toString()}),
          style: AppStyles.DEFAULT_16_BOLD.copyWith(color: AppColors.Blue),
        ),
      ),
      Container(
        child: Row(children: [
          ...List.generate(
            2,
            (index1) => Expanded(
                child: Row(
              children: [
                Text(index1 == 0 ? 'from'.tr : 'to'.tr,
                    style: AppStyles.DEFAULT_16),
                (index != 0 && index1 == 0) || (index != 2 && index1 == 1)
                    ? Expanded(
                        child: _renderInput(
                            textAlign: TextAlign.right,
                            controller:
                                index1 == 0 ? fromController : toController,
                            focusNode: index1 == 0 ? fromFocus : toFocus,
                            hintText: ''))
                    : Expanded(
                        child: AbsorbPointer(
                            child: _renderInput(
                                textAlign: TextAlign.right,
                                controller:
                                    index1 == 0 ? fromController : toController,
                                focusNode: index1 == 0 ? fromFocus : toFocus,
                                hintText: '')),
                      ),
                Text('h',
                    style:
                        AppStyles.DEFAULT_16.copyWith(color: AppColors.Blue)),
                if (index1 == 0)
                  Container(
                    width: 20.r,
                  ),
              ],
            )),
          ),
        ]),
      ),
      _renderInput(
          controller: priceController,
          focusNode: priceFocus,
          hintText: hintText,
          textAlign: TextAlign.left)
    ]);
  }

  _renderInput({
    required TextEditingController controller,
    required FocusNode focusNode,
    String? hintText,
    required TextAlign textAlign,
    TextInputType? keyboardType,
  }) {
    return WidgetInput(
      textAlign: textAlign,
      keyboardType: keyboardType ?? TextInputType.number,
      controller: controller,
      focusNode: focusNode,
      hintText: hintText,
      validator: FormBuilderValidators.required(errorText: 'error_miss'.tr),
    );
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30),
      child: WidgetButton(
        onTap: () {
          if (key.currentState!.validate()) {
            if (store == null) {
              Get.dialog(DialogCustom(
                  title: 'Thông báo',
                  onTapOK: () {
                    Get.back();
                  },
                  children: [
                    Text('area.store'.tr, style: AppStyles.DEFAULT_16)
                  ]));
            } else {
              if (Get.arguments != null) {
                onSua();
              } else {
                onTao();
              }
            }
          }
        },
        text: Get.arguments != null ? 'save'.tr : 'create'.tr,
        isFullWidth: true,
      ),
    );
  }
}

class ItemPriceKhuVuc {
  final TextEditingController fromController;
  final TextEditingController toController;
  final TextEditingController priceController;
  final FocusNode fromFocus;
  final FocusNode toFocus;
  final FocusNode priceFocus;

  ItemPriceKhuVuc({
    required this.fromController,
    required this.toController,
    required this.priceController,
    required this.fromFocus,
    required this.toFocus,
    required this.priceFocus,
  });
}

class ItemInputKhuVuc {
  final TextEditingController controller;
  final FocusNode focusNode;

  ItemInputKhuVuc({required this.controller, required this.focusNode});
}
