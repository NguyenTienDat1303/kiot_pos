import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeController _controller = Get.find<HomeController>();
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  Map<String, Widget> _drawerNavigator = {};

  @override
  void initState() {
    super.initState();
    _controller.loadPermissonList();

    _controller.getUserDetail();
    _drawerNavigator = {
      AppRoutes.store: StorePage(
        homePageKey: _key,
      ),
      AppRoutes.order_create: OrderCreatePage(
        homePageKey: _key,
      ),
      AppRoutes.overview: OverviewPage(
        homePageKey: _key,
      ),
      AppRoutes.customer: CustomerPage(
        homePageKey: _key,
      ),
      AppRoutes.staff: StaffPage(
        homePageKey: _key,
      ),
      AppRoutes.stock: StockPage(
        homePageKey: _key,
      ),
      AppRoutes.area: AreaPage(
        homePageKey: _key,
      ),
      AppRoutes.order: OrderPage(
        homePageKey: _key,
      ),
      AppRoutes.bill: BillPage(
        homePageKey: _key,
      ),
      AppRoutes.sync: SyncPage(
        homePageKey: _key,
      ),
      AppRoutes.setting: SettingPage(
        homePageKey: _key,
      ),
    };
  }

  drawerMenuOnClicked(String name) {
    setState(() {
      _controller.drawName = name;
      _key.currentState!.closeDrawer();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetX<HomeController>(
      builder: (controller) {
        if (_controller.drawName == "") {
          if (controller.authorization(
              [KiotVersion.coffee, KiotVersion.milktea, KiotVersion.karaoke],
              [])) {
            _controller.drawName = AppRoutes.store;
          }
          if (controller
              .authorization([KiotVersion.shop, KiotVersion.enter], [])) {
            _controller.drawName = AppRoutes.order_create;
          }
        }

        return GunPage(
          keyScaffold: _key,
          // appBarTop: GunAppBar(
          //   type: GunAppBarType.TOP,
          //   title: '$controller.drawName.title'.tr,
          //   titleStyle: AppStyles.pageTitleStyle,
          //   leading: [
          //     GunAction.icon(
          //         onTap: () {
          //           _key.currentState!.openDrawer();
          //         },
          //         icon: Image.asset('assets/images/drawer_menu.png'))
          //   ],
          //   actions: [
          //     if (controller.drawName == AppRoutes.store)
          //       GunAction.custom(onTap: () {}, child: Text("Cơ sở"))
          //   ],
          // ),
          child: _drawerNavigator[controller.drawName] ?? SizedBox(),
          drawer: Drawer(
            // width: MediaQuery.of(context).size.width * 90 / 100,
            child: SingleChildScrollView(
                child: Column(
              children: [
                Container(
                    width: double.infinity,
                    padding: EdgeInsets.only(
                        top: 42, bottom: 16, left: 16, right: 16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FractionallySizedBox(
                          widthFactor: .25,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(500),
                            child:
                                Image.asset('assets/images/kiot_icon_app.png'),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          controller.detailUser != null
                              ? controller.detailUser!.userInfo!.displayName ??
                                  ''
                              : '',
                          style: AppStyles.normalTextStyle,
                        ),
                        SizedBox(
                          height: 3,
                        ),
                        // Text(
                        //   "membership_when".trParams({'date': ' 25/03/2023'}),
                        //   style: TextStyle(
                        //     fontSize: 12,
                        //     color: Colors.red,
                        //   ),
                        // )
                      ],
                    )),
                Container(
                  width: double.infinity,
                  height: 0.5,
                  color: Colors.grey.shade300,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 8),
                        child: HomeDrawerMenu(
                          imageAsset: 'assets/images/home_drawer_store.png',
                          title: 'home_drawer.store'.tr,
                          onTap: () {
                            if (_controller.authorization([
                              KiotVersion.coffee,
                              KiotVersion.karaoke,
                              KiotVersion.milktea,
                            ], [])) {
                              drawerMenuOnClicked(AppRoutes.store);
                            }
                            if (_controller.authorization(
                                [KiotVersion.shop, KiotVersion.enter], [])) {
                              drawerMenuOnClicked(AppRoutes.order_create);
                            }
                          },
                          isSelected: controller.drawName == AppRoutes.store,
                        ),
                      ),
                      if (_controller.authorization([
                        KiotVersion.coffee,
                        KiotVersion.karaoke,
                        KiotVersion.milktea,
                        KiotVersion.shop,
                        KiotVersion.enter
                      ], [
                        KiotPermisson.overview,
                      ]))
                        Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: HomeDrawerMenu(
                            imageAsset:
                                'assets/images/home_drawer_overview.png',
                            title: 'home_drawer.overview'.tr,
                            onTap: () =>
                                drawerMenuOnClicked(AppRoutes.overview),
                            isSelected:
                                controller.drawName == AppRoutes.overview,
                          ),
                        ),
                      if (_controller.authorization([
                        KiotVersion.coffee,
                        KiotVersion.karaoke,
                        KiotVersion.milktea,
                        KiotVersion.shop,
                        KiotVersion.enter
                      ], [
                        KiotPermisson.customer,
                      ]))
                        Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: HomeDrawerMenu(
                            imageAsset:
                                'assets/images/home_drawer_customer.png',
                            title: 'home_drawer.customer'.tr,
                            onTap: () =>
                                drawerMenuOnClicked(AppRoutes.customer),
                            isSelected:
                                controller.drawName == AppRoutes.customer,
                          ),
                        ),
                      if (_controller.authorization([
                        KiotVersion.karaoke,
                      ], [
                        KiotPermisson.staff_management,
                      ]))
                        Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: HomeDrawerMenu(
                            imageAsset: 'assets/images/home_drawer_staff.png',
                            title: 'home_drawer.staff'.tr,
                            onTap: () => drawerMenuOnClicked(AppRoutes.staff),
                            isSelected: controller.drawName == AppRoutes.staff,
                          ),
                        ),

                      if (_controller.authorization([
                        KiotVersion.coffee,
                        KiotVersion.karaoke,
                        KiotVersion.milktea,
                        KiotVersion.shop,
                        KiotVersion.enter
                      ], [
                        KiotPermisson.product,
                      ]))
                        Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: HomeDrawerMenu(
                            imageAsset: 'assets/images/home_drawer_stock.png',
                            title: 'home_drawer.stock'.tr,
                            onTap: () => drawerMenuOnClicked(AppRoutes.stock),
                            isSelected: controller.drawName == AppRoutes.stock,
                          ),
                        ),

                      if (_controller.authorization([
                        KiotVersion.coffee,
                        KiotVersion.karaoke,
                        KiotVersion.milktea,
                      ], []))
                        Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: HomeDrawerMenu(
                            imageAsset: 'assets/images/home_drawer_area.png',
                            title: 'home_drawer.area'.tr,
                            onTap: () => drawerMenuOnClicked(AppRoutes.area),
                            isSelected: controller.drawName == AppRoutes.area,
                          ),
                        ),

                      if (_controller.authorization([
                        KiotVersion.coffee,
                        KiotVersion.karaoke,
                        KiotVersion.milktea,
                        KiotVersion.shop,
                        KiotVersion.enter
                      ], [
                        KiotPermisson.order,
                      ]))
                        Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: HomeDrawerMenu(
                            imageAsset: 'assets/images/home_drawer_order.png',
                            title: 'home_drawer.order'.tr,
                            onTap: () => drawerMenuOnClicked(AppRoutes.order),
                            isSelected: controller.drawName == AppRoutes.order,
                          ),
                        ),

                      if (_controller.authorization([
                        KiotVersion.coffee,
                        KiotVersion.karaoke,
                        KiotVersion.milktea,
                        KiotVersion.shop,
                        KiotVersion.enter
                      ], [
                        KiotPermisson.pay_collect_view,
                      ]))
                        Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: HomeDrawerMenu(
                            imageAsset: 'assets/images/home_drawer_bill.png',
                            title: 'home_drawer.bill'.tr,
                            onTap: () => drawerMenuOnClicked(AppRoutes.bill),
                            isSelected: controller.drawName == AppRoutes.bill,
                          ),
                        ),

                      // HomeDrawerMenu(
                      //   imageAsset: 'assets/images/home_drawer_sync.png',
                      //   title: 'home_drawer.sync'.tr,
                      //   onTap: () => drawerMenuOnClicked(AppRoutes.sync),
                      //   isSelected: controller.drawName == AppRoutes.sync,
                      // ),
                      // SizedBox(
                      //   height: 8,
                      // ),
                      Container(
                        margin: EdgeInsets.only(bottom: 8),
                        child: HomeDrawerMenu(
                          imageAsset: 'assets/images/home_drawer_setting.png',
                          title: 'home_drawer.setting'.tr,
                          onTap: () => drawerMenuOnClicked(AppRoutes.setting),
                          isSelected: controller.drawName == AppRoutes.setting,
                        ),
                      ),
                      SizedBox(
                        height: 46,
                      ),
                      Container(
                        width: double.infinity,
                        child: TextButton(
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Color(0XFFD6E8FF)),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                ))),
                            onPressed: () {
                              Get.dialog(DialogCustom(
                                title: 'logout.title'.tr,
                                children: [Text("logout.message".tr)],
                                isTwoButton: true,
                                nameButtonLeft: 'logout.negative'.tr,
                                onTapLeft: () {
                                  Get.back();
                                },
                                nameButtonRight: 'logout.positive'.tr,
                                onTapRight: () {
                                  Get.offAllNamed(AppRoutes.login);
                                },
                              ));
                            },
                            child: Text(
                              'home_drawer.logout'.tr,
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Color(0XFF205BA1),
                                  fontWeight: FontWeight.w600),
                            )),
                      )
                    ],
                  ),
                )
              ],
            )),
          ),
        );
      },
    );
  }
}

class HomeDrawerMenu extends StatelessWidget {
  final String imageAsset;
  final String title;
  final VoidCallback? onTap;
  final bool isSelected;
  const HomeDrawerMenu(
      {Key? key,
      required this.imageAsset,
      required this.title,
      this.onTap,
      this.isSelected = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var bgColor = isSelected ? Color(0XFFD6E8FF) : Colors.white;
    var textColor = isSelected ? Color(0XFF205BA1) : Colors.black;
    return GunInkwell(
      padding: EdgeInsets.zero,
      onTap: onTap,
      decoration:
          BoxDecoration(color: bgColor, borderRadius: BorderRadius.circular(5)),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8),
        height: 35,
        child: Row(
          children: [
            FractionallySizedBox(
                heightFactor: .6,
                child: Image.asset(
                  imageAsset,
                  color: textColor,
                )),
            Container(
                margin: EdgeInsets.only(left: 10),
                child: Text(title, style: TextStyle(color: textColor)))
          ],
        ),
      ),
    );
  }
}
