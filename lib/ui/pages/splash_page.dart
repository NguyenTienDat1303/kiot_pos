import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  SplashController _controller = Get.find<SplashController>();
  @override
  void initState() {
    super.initState();
    // Future.delayed(Duration(seconds: 1), () {
    //   // Get.until((route) => Get.currentRoute == AppRoutes.login);
    //   // Get.offAllNamed(AppRoutes.login);

    // });
    init();
  }

  void init() async {
    await _controller.initDatabase();
    // Get.offAllNamed(AppRoutes.login);
    Future.delayed(Duration(seconds: 1), () async {
      var destinationRoute = await _controller.findNavigationTo();
      Get.offAllNamed(destinationRoute);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GunPage(
        child: Container(
      child: Image.asset('assets/images/bg_splash.png'),
    ));
  }
}
