import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gun_base/ui/ui.dart';
import 'package:kiot_pos/data/entities/response/stock_detail_response.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:kiot_pos/res/styles.dart';
import 'package:kiot_pos/ui/widgets/avatar_create.dart';
import 'package:get/get.dart';

class StockingDetailItemPage extends StatefulWidget {
  const StockingDetailItemPage({Key? key}) : super(key: key);

  @override
  State<StockingDetailItemPage> createState() => _StockingDetailItemPageState();
}

class _StockingDetailItemPageState extends State<StockingDetailItemPage> {
  StockController stockController = Get.find<StockController>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(Duration.zero, () {
      stockController.getDetailStock(id: stockController.idStock);
    });
  }

  bool isEdit = false;
  @override
  Widget build(BuildContext context) {
    return GetX<StockController>(
      builder: (controller) {
        return GunPage(
          resizeToAvoidBottomInset: false,
          appBarBottom: GunAppBar(
            type: GunAppBarType.BOTTOM,
            height: 0,
          ),
          appBarTop: GunAppBar(
            type: GunAppBarType.TOP,
            title: controller.detailStock != null
                ? controller.detailStock!.kiemKe!.adjustCode
                : '',
            titleStyle: AppStyles.pageTitleStyle,
            leadingColor: Colors.black,
            actions: [
              if (controller.isCompleted == 0)
                GunAction.icon(
                  iconFactor: 0.5,
                  onTap: () {
                    if (controller.isCompleted == 0) {
                      Get.toNamed(AppRoutes.stocking_edit);
                    } else {
                      null;
                    }
                  },
                  icon: controller.isCompleted == 0
                      ? Image.asset(
                          './assets/images/edit_icon.png',
                        )
                      : Container(
                          width: 24,
                        ),
                )
            ],
          ),
          child: Container(
            child: Column(
              children: [
                Expanded(
                    child: SingleChildScrollView(
                  child: _renderBody(controller.detailStock!.chiTiet!),
                )),
                _renderButton()
              ],
            ),
          ),
        );
      },
    );
  }

  _renderBody(List<ChiTiet> list) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    flex: 2,
                    child: GunText(
                      'Hàng kiểm',
                      style: AppStyles.DEFAULT_14,
                    )),
                Expanded(
                    flex: 1,
                    child: GunText(
                      'Tồn kho',
                      textAlign: TextAlign.center,
                      style: AppStyles.DEFAULT_14,
                    )),
                Expanded(
                    flex: 1,
                    child: GunText(
                      'Thực tế',
                      textAlign: TextAlign.center,
                      style: AppStyles.DEFAULT_14,
                    )),
                Expanded(
                    flex: 1,
                    child: GunText(
                      'Lệch',
                      textAlign: TextAlign.center,
                      style: AppStyles.DEFAULT_14,
                    )),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            child: Divider(
              color: AppColors.Gray4,
              height: 4,
            ),
          ),
          ...List.generate(
              list.length,
              (index) => Column(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: 30,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                flex: 2,
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 20),
                                  child: GunText(
                                    list[index].tenSP!,
                                    style: AppStyles.DEFAULT_14,
                                  ),
                                )),
                            Expanded(
                                flex: 1,
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 20),
                                  child: GunText(
                                    list[index].tonKho!.toString(),
                                    textAlign: TextAlign.center,
                                    style: AppStyles.DEFAULT_14,
                                  ),
                                )),
                            Expanded(
                                flex: 1,
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 20),
                                  child: GunText(
                                    list[index].thucTe!.toString(),
                                    textAlign: TextAlign.center,
                                    style: AppStyles.DEFAULT_14,
                                  ),
                                )),
                            Expanded(
                                flex: 1,
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 20),
                                  child: GunText(
                                    list[index].lech!.toString(),
                                    textAlign: TextAlign.center,
                                    style: AppStyles.DEFAULT_14,
                                  ),
                                )),
                          ],
                        ),
                      ),
                      if (index != list.length - 1)
                        Container(
                          width: double.infinity,
                          child: Divider(
                            color: AppColors.Gray4,
                            height: 4,
                          ),
                        ),
                    ],
                  ))
        ],
      ),
    );
  }

  _renderButton() {
    return Container(
      padding: EdgeInsets.all(30),
      child: WidgetButton(
        onTap: () {
          Get.dialog(DialogCustom(
              title: 'Thông báo',
              isTwoButton: true,
              nameButtonLeft: 'Huỷ',
              nameButtonRight: 'Xoá',
              onTapLeft: () {
                Get.back();
              },
              onTapRight: () {
                Get.back();
                //  stockController
              },
              children: [
                Text('Bạn có chắc chắn muốnn xoá ?',
                    style: AppStyles.DEFAULT_16),
              ]));
        },
        isFullWidth: true,
        text: 'delete'.tr,
      ),
    );
  }
}
