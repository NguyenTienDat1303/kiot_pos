import 'package:flutter/material.dart';
import 'package:gun_base/gun_base.dart';
import 'package:gun_base/res/values.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/ui/widgets/choose_time.dart';

import '../../data/entities/response/order_response.dart';

class OrderPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const OrderPage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<OrderPage> createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  OrderController orderController = Get.find<OrderController>();

  TextEditingController textController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 0), () {
      orderController.getListOrder('', null, null, null, null, null, 100);
    });
  }

  String start = AppValue.DATE_FORMAT.format(DateTime.now());
  String end = AppValue.DATE_FORMAT.format(DateTime.now());

  bool isSearch = false;
  @override
  Widget build(BuildContext context) {
    return GetX<OrderController>(
      builder: (controller) {
        return GunPage(
            appBarBottom: GunAppBar(
              type: GunAppBarType.BOTTOM,
              height: 0,
            ),
            appBarTop: GunAppBar(
              type: GunAppBarType.TOP,
              titles: Row(
                children: [
                  isSearch
                      ? Expanded(
                          child: TextField(
                          controller: textController,
                          onChanged: (e) {
                            setState(() {});
                          },
                          onSubmitted: (e) {
                            controller.getListOrder(
                                e, null, null, null, null, null, 10);
                          },
                          autofocus: true,
                          decoration: InputDecoration(
                              isDense: true, border: InputBorder.none),
                        ))
                      : GunText(
                          'home_drawer.order'.tr,
                          style: AppStyles.pageTitleStyle,
                        ),
                ],
              ),
              leading: [
                GunAction.icon(
                    iconFactor: 0.5,
                    onTap: () {
                      widget.homePageKey.currentState!.openDrawer();
                    },
                    icon: Image.asset('assets/images/drawer_menu.png'))
              ],
              actions: [
                GunAction.icon(
                  onTap: () {
                    setState(() {
                      isSearch = !isSearch;
                      textController.clear();
                    });
                    if (isSearch == false) {
                      controller.getListOrder(
                          '', null, null, null, null, null, 10);
                    }
                  },
                  iconFactor: isSearch ? 0.4 : 0.7,
                  icon: Image.asset(
                    isSearch
                        ? './assets/images/close_icon.png'
                        : './assets/images/search_icon.png',
                    color: AppColors.Orange,
                  ),
                ),
              ],
            ),
            child: _renderBody(controller.listOrder));
      },
    );
  }

  _renderBody(List<OrderResponse> list) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: ChooseTime(
                  indexTabBar: 1,
                  onChangeFrom: (e) {
                    setState(() {
                      start = e;
                    });
                  },
                  onChangeTo: (e) {
                    setState(() {
                      end = e;
                    });
                    orderController.getListOrder(
                        '',
                        null,
                        null,
                        null,
                        AppValue.formatSearchDay(start),
                        AppValue.formatSearchDay(end),
                        100);
                  })),
          ...List.generate(
              list.length,
              (index) => GestureDetector(
                    onTap: () {
                      Get.toNamed(AppRoutes.order_detail,
                          arguments: {'orderId': list[index].id});
                    },
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                      child: BaseInfoWidget(
                          contextLeft1: 'order.code'.tr,
                          contextLeft2: 'sell_date'.tr,
                          contextLeft3: 'total_money'.tr,
                          contextRight1: list[index].outputCode!,
                          contextRight2: list[index].sellDate!,
                          contextRight3: GunValue.format_money(double.parse(
                              list[index].totalMoney!.toString()))),
                    ),
                  )),
        ],
      ),
    );
    ;
  }
}
