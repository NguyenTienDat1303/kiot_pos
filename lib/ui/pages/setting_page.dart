import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gun_base/gun_base.dart';
import 'package:kiot_pos/kiot_pos.dart';

class SettingPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> homePageKey;
  const SettingPage({Key? key, required this.homePageKey}) : super(key: key);

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  HomeController _homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return GunPage(
        appBarTop: GunAppBar(
          type: GunAppBarType.TOP,
          title: 'setting.title'.tr,
          titleStyle: AppStyles.pageTitleStyle,
          leading: [
            GunAction.icon(
                onTap: () {
                  widget.homePageKey.currentState!.openDrawer();
                },
                icon: Image.asset('assets/images/drawer_menu.png'))
          ],
        ),
        child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: SettingMenu(
                    imageAssetPath: 'assets/images/setting_profile_edit.png',
                    title: 'setting.profile_edit'.tr,
                    onTap: () {
                      Get.toNamed(AppRoutes.edit_user);
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: SettingMenu(
                    imageAssetPath: 'assets/images/setting_config.png',
                    title: 'setting.config'.tr,
                    onTap: () {
                      Get.toNamed(AppRoutes.config);
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: SettingMenu(
                    imageAssetPath: 'assets/images/setting_general.png',
                    title: 'setting.general'.tr,
                    onTap: () {
                      Get.toNamed(AppRoutes.kiot_setting, arguments: {
                        "title": "general.title".tr,
                        "targetUrl": AppStrings.kiot_general_url
                      });
                    },
                  ),
                ),
                // Container(
                //   padding: EdgeInsets.only(bottom: 15),
                //   child: SettingMenu(
                //     imageAssetPath: 'assets/images/setting_stock.png',
                //     title: 'setting.stock'.tr,
                //     onTap: () {
                //       Get.toNamed(AppRoutes.kiot_setting, arguments: {
                //         "title": "stock.title".tr,
                //         "targetUrl": AppStrings.kiot_stock_url
                //       });
                //     },
                //   ),
                // ),
                // Container(
                //   padding: EdgeInsets.only(bottom: 15),
                //   child: SettingMenu(
                //     imageAssetPath: 'assets/images/setting_customer.png',
                //     title: 'setting.customer'.tr,
                //     onTap: () {
                //       Get.toNamed(AppRoutes.kiot_setting, arguments: {
                //         "title": "customer.title".tr,
                //         "targetUrl": AppStrings.kiot_customer_url
                //       });
                //     },
                //   ),
                // ),
                // Container(
                //   padding: EdgeInsets.only(bottom: 15),
                //   child: SettingMenu(
                //     imageAssetPath: 'assets/images/setting_input.png',
                //     title: 'setting.input'.tr,
                //     onTap: () {
                //       Get.toNamed(AppRoutes.kiot_setting, arguments: {
                //         "title": "input.title".tr,
                //         "targetUrl": AppStrings.kiot_input_url
                //       });
                //     },
                //   ),
                // ),
                // Container(
                //   padding: EdgeInsets.only(bottom: 15),
                //   child: SettingMenu(
                //     imageAssetPath: 'assets/images/setting_profit.png',
                //     title: 'setting.profit'.tr,
                //     onTap: () {
                //       Get.toNamed(AppRoutes.kiot_setting, arguments: {
                //         "title": "profit.title".tr,
                //         "targetUrl": AppStrings.kiot_profit_url
                //       });
                //     },
                //   ),
                // ),
                // if (_homeController.authorization([
                //   KiotVersion.coffee,
                //   KiotVersion.karaoke,
                //   KiotVersion.milktea,
                //   KiotVersion.shop,
                //   KiotVersion.enter
                // ], [
                //   KiotPermisson.overview,
                // ]))
                //   Container(
                //     padding: EdgeInsets.only(bottom: 15),
                //     child: SettingMenu(
                //       imageAssetPath: 'assets/images/setting_setting.png',
                //       title: 'setting.setting'.tr,
                //       onTap: () {
                //         Get.toNamed(AppRoutes.kiot_setting, arguments: {
                //           "title": "setting.title".tr,
                //           "targetUrl": AppStrings.kiot_setting_url
                //         });
                //       },
                //     ),
                //   ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: SettingMenu(
                    imageAssetPath: 'assets/images/setting_policy_term.png',
                    title: 'setting.policy_term'.tr,
                    onTap: () {
                      print('jkasgdkjsa');

                      Get.toNamed(AppRoutes.policy);
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: SettingMenu(
                    imageAssetPath: 'assets/images/setting_change_password.png',
                    title: 'setting.change_password'.tr,
                    onTap: () {
                      Get.toNamed(AppRoutes.change_password);
                    },
                  ),
                ),
              ],
            )));
  }
}

class SettingMenu extends StatelessWidget {
  final String imageAssetPath;
  final String title;
  final VoidCallback? onTap;
  const SettingMenu(
      {Key? key, required this.imageAssetPath, required this.title, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          child: Row(children: [
            Container(
                margin: EdgeInsets.only(right: 15),
                width: 24,
                height: 24,
                child: Image.asset(imageAssetPath)),
            Text(
              title,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            )
          ]),
        ),
      ),
    );
  }
}
