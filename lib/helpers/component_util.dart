import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ComponentUtils {
  static void showCupertinoDatePicker(BuildContext context,
      {String? title,
      required ValueChanged<DateTime> onSubmitted,
      CupertinoDatePickerMode mode = CupertinoDatePickerMode.date,
      VoidCallback? onDelete,
      bool use24hFormat = true,
      DateTime? minimumDate,
      DateTime? initialDateTime,
      DateTime? maximumDate}) {
    if (minimumDate == null) minimumDate = DateTime.now();
    DateTime datetime = minimumDate;
    FocusScope.of(context).unfocus();
    showCupertinoModalPopup(
        context: context,
        builder: (_) => Wrap(
              children: [
                Container(
                  color: Color.fromARGB(255, 255, 255, 255),
                  child: Column(
                    children: [
                      Container(
                        height: 300,
                        child: Column(
                          children: [
                            Material(
                              child: Container(
                                color: Colors.grey.shade100,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 20),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        '${title ?? ''}',
                                        style:
                                            TextStyle(color: Colors.lightBlue),
                                      ),
                                    ),
                                    RichText(
                                      text: new TextSpan(
                                        children: [
                                          if (onDelete != null)
                                            new TextSpan(
                                              text:
                                                  'cupertino_picker.delete'.tr,
                                              style:
                                                  TextStyle(color: Colors.red),
                                              recognizer:
                                                  new TapGestureRecognizer()
                                                    ..onTap = () {
                                                      onDelete();
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                            ),
                                          if (onDelete != null)
                                            new TextSpan(
                                              text: ' | ',
                                              style: TextStyle(
                                                  color: Colors.lightBlue),
                                            ),
                                          new TextSpan(
                                              text:
                                                  'cupertino_picker.select'.tr,
                                              style: TextStyle(
                                                  color: Colors.lightBlue),
                                              recognizer:
                                                  new TapGestureRecognizer()
                                                    ..onTap = () {
                                                      onSubmitted(datetime);
                                                      Navigator.of(context)
                                                          .pop();
                                                    }),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: CupertinoDatePicker(
                                initialDateTime: initialDateTime ?? minimumDate,
                                use24hFormat: use24hFormat,
                                mode: mode,
                                onDateTimeChanged: (value) {
                                  datetime = value;
                                },
                                minimumDate: minimumDate,
                                maximumDate: maximumDate,
                              ),
                            ),
                          ],
                        ),
                      ),

                      // Close the modal
                    ],
                  ),
                ),
              ],
            ));
  }
}
