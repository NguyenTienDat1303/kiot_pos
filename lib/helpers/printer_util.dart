import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart'
    as EosBluetoothPrinter;
import 'package:image/image.dart' as ImagePackage;
import 'package:flutter/material.dart' hide Image;
import 'dart:ui' as ui;
import 'package:esc_pos_printer/esc_pos_printer.dart' as EosPrinter;
import 'package:kiot_pos/kiot_pos.dart';

class PrinterUtils {
  Map<int, int> paperSize = {
    PrintrPaperSize.mm55: 370,
    PrintrPaperSize.mm80: 572,
  };

  static Future<void> printBluetoothImage(
      EosBluetoothPrinter.PrinterBluetoothManager bluetoothManager,
      EosBluetoothPrinter.PrinterBluetooth printer,
      List<Uint8List> imageList) async {
    bluetoothManager.selectPrinter(printer);

    const PaperSize paper = PaperSize.mm58;
    final profile = await CapabilityProfile.load();
    profile.getCodePageId("CP1258");

    final Generator ticket = Generator(paper, profile);

    List<int> bytes = [];
    imageList.forEach((e) async {
      final ImagePackage.Image? image = ImagePackage.decodeImage(e);
      final ImagePackage.Image thumbnail =
          ImagePackage.copyResize(image!, width: 390);
      bytes += ticket.image(thumbnail);
      ticket.feed(3);
      ticket.cut();
    });
    final EosBluetoothPrinter.PosPrintResult res =
        await bluetoothManager.printTicket(bytes);

    // ticket.feed(4);
  }

  static Future<void> printLanImage(
      String printerIP, String printerPort, List<Uint8List> imageList) async {
    const PaperSize paper = PaperSize.mm80;
    final profile = await CapabilityProfile.load();
    final printer = EosPrinter.NetworkPrinter(paper, profile);

    final EosPrinter.PosPrintResult res = await printer.connect(printerIP,
        port: int.parse(printerPort), timeout: Duration(seconds: 5));

    if (res == EosPrinter.PosPrintResult.success) {
      imageList.forEach((e) {
        final ImagePackage.Image? printImage = ImagePackage.decodeImage(e);
        printer.image(printImage!);
        printer.feed(1);
        printer.cut();
      });

      printer.disconnect();
    } else {
      Get.snackbar("Lỗi", res.msg, backgroundColor: Colors.red);
      print(res.msg);
    }
  }

  static Future<void> printLan(
      String printerIP, int port, List<Uint8List> imageList) async {
    const PaperSize paper = PaperSize.mm80;
    final profile = await CapabilityProfile.load();
    final printer = EosPrinter.NetworkPrinter(paper, profile);

    // // printer.setGlobalCodeTable('PC1258');
    // // printer.printCodeTable();
    final EosPrinter.PosPrintResult res = await printer.connect(
        printerIP.trim(),
        port: port,
        timeout: Duration(seconds: 5));

    // print("printLan");

    if (res == EosPrinter.PosPrintResult.success) {
      print("PRINTER: EosPrinter.PosPrintResult.success");
      // final ImagePackage.Image? image = ImagePackage.decodeImage(imageBytes);
      // final ImagePackage.Image thumbnail =
      //     ImagePackage.copyResize(image!, width: 572);
      // print("thumbnail + ${thumbnail.height}");
      // printer.image(thumbnail!);

      // //   //   printer.text('Bầu trời trong xanh thăm thẳm không một gợn mây',
      // //   // styles: PosStyles(codeTable: '35'));

      // //   Uint8List encoded = await CharsetConverter.encode(
      // //       "windows-1258", "Bầu trời trong xanh thăm thẳm không một gợn mây.");
      // //   printer.textEncoded(encoded, styles: PosStyles(codeTable: 'CP1258'));
      // //   // printer.printCodeTable(codeTable: 'PC1258');
      // print("PRINTER: END");
      // printer.qrcode("gunnyga", size: QRSize.Size6);
      // printer.feed(1);
      // printer.cut();
      // printer.disconnect();
      // print("PRINTER: disconnect");
      imageList.forEach((e) {
        final ImagePackage.Image? image = ImagePackage.decodeImage(e);
        final ImagePackage.Image thumbnail =
            ImagePackage.copyResize(image!, width: 572);
        print("thumbnail + ${thumbnail.height}");
        printer.image(thumbnail);
        printer.feed(1);
        printer.cut();
      });
      printer.disconnect(delayMs: 100);
    } else {
      Get.snackbar("Lỗi", res.msg, backgroundColor: Colors.red);
      print("PRINTER ${res.msg}");
    }

    await Future.delayed(Duration(seconds: 10), () {
      return true;
    });
  }

  static Future<Uint8List> generateBytesImage(
    Uint8List imageBytes,
  ) async {
    final ui.ImmutableBuffer buffer =
        await ui.ImmutableBuffer.fromUint8List(imageBytes);
    final ui.ImageDescriptor descriptor =
        await ui.ImageDescriptor.encoded(buffer);
    int height = descriptor.height;

    ui.Codec codec = await ui.instantiateImageCodec(imageBytes,
        targetWidth: 500, allowUpscaling: true);
    ui.FrameInfo frame = await codec.getNextFrame();
    var fromImage = frame.image;

    ui.PictureRecorder recorder = new ui.PictureRecorder();
    Canvas canvas = Canvas(
        recorder,
        Rect.fromLTWH(
            0, 0, fromImage.width.toDouble(), fromImage.height.toDouble()));
    var ratio = fromImage.width / Get.width;
    print('height: ${height}');
    print('fromImage.width: ${fromImage.width}');
    print('fromImage.height: ${fromImage.height}');
    print('Get.width: ${Get.width}');
    print('Get.height ratio: ${fromImage.height.toDouble() / ratio}');
    print('Get.width: ${Get.width}');
    print('Get.height: ${Get.height}');

    paintImage(
        canvas: canvas,
        rect:
            Rect.fromLTWH(0, 0, Get.width, fromImage.height.toDouble() / ratio),
        image: fromImage,
        // fit: BoxFit.fitWidth,
        repeat: ImageRepeat.noRepeat,
        scale: 1.0,
        isAntiAlias: true,
        alignment: Alignment.center,
        flipHorizontally: false,
        filterQuality: FilterQuality.high);
    // tp.layout(minWidth: Get.width, maxWidth: Get.width);
    // tp.paint(canvas, const Offset(0.0, 0.0));
    var picture = recorder.endRecording();
    final pngBytes = await picture.toImage(
      fromImage.width,
      (fromImage.height.toDouble() / ratio).toInt(), // decrease padding
    );
    final byteData = await pngBytes.toByteData(format: ui.ImageByteFormat.png);
    var result = byteData!.buffer.asUint8List();
    return result;
  }
}
