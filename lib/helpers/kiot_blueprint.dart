import 'package:intl/intl.dart';
import 'package:kiot_pos/kiot_pos.dart';

class KiotBlueprint {
  static String getInvoicePrintData(
      String blueprint, LoadTableResponse orderDetail) {
    print("total prince ${orderDetail.donHang!.totalPrice}");
    return blueprint
        .replaceAll("\r", "")
        .replaceAll("\n", "")
        .replaceAll("\t", "")
        .replaceAll("{Ma_Don_Hang}", "${orderDetail.donHang!.outputCode}")
        .replaceAll("{Ngay_Xuat}", "${orderDetail.donHang!.sellDate}")
        .replaceAll("{Khach_Hang}",
            "${orderDetail.tenkhach != null ? orderDetail.tenkhach!.customerName : ""}")
        .replaceAll("{DC_Khach_Hang}", "")
        .replaceAll("{DT_Khach_Hang}", "")
        .replaceAll("{Gio_Vao}", "${orderDetail.donHang!.fromDate}")
        .replaceAll("{Gio_Ra}", "${orderDetail.donHang!.finishDate}")
        .replaceAll("{Tien_Hang}",
            "${AppValue.APP_MONEY_FORMAT.format(orderDetail.donHang!.totalPrice ?? 0)}")
        .replaceAll(
            "{Tien_Gio}", "${AppValue.APP_MONEY_FORMAT.format(orderDetail.donHang!.totalTablePrice ?? 0)}")
        .replaceAll("{Tien_Nhan_Vien}",
            "${AppValue.APP_MONEY_FORMAT.format(orderDetail.donHang!.totalEmployeePrice ?? 0)}")
        .replaceAll("{Tien_Khuyen_Mai}",
            "${AppValue.APP_MONEY_FORMAT.format(orderDetail.donHang!.coupon ?? 0)}")
        .replaceAll(
            "{Thanh_Toan_Diem}", "${AppValue.APP_MONEY_FORMAT.format(0)}")
        .replaceAll("{Tong_Tien}",
            "${AppValue.APP_MONEY_FORMAT.format(orderDetail.tongThanhToan ?? 0)}")
        .replaceAll("{Con_No}", "${AppValue.APP_MONEY_FORMAT.format(0)}")
        .replaceAll(
            "{Chi_Tiet_San_Pham3}", """<table class="table-order"></table>""");
  }

  static String getKitchenPrintData(
    String blueprint,
    TableData tableData,
    LoadTableResponse orderDetail,
  ) {
    // var ngayXuat = orderDetail.donHang!.sellDate ?? "";
    // try {
    //   DateFormat(AppValue.backend_datetime_format).format(DateTime.now());
    //   DateTime tempDate = new DateFormat(AppValue.backend_datetime_format)
    //       .parse(orderDetail.donHang!.sellDate!);
    //   ngayXuat = DateFormat(AppValue.short_datetime_format).format(tempDate);
    // } catch (e) {}

    return blueprint
        .replaceAll("\r", "")
        .replaceAll("\n", "")
        .replaceAll("\t", "")
        .replaceAll("{Ma_Don_Hang}", "${orderDetail.donHang!.outputCode}")
        .replaceAll("{Ngay_Xuat}", "${orderDetail.donHang!.sellDate}")
        .replaceAll("{Khach_Hang}",
            "${orderDetail.tenkhach != null ? orderDetail.tenkhach!.customerName : ""}")
        .replaceAll("{Phuc_Vu}",
            "${orderDetail.nhanVien!.map((e) => "${e.nhanVien!.employee_name}").join(", ")}")
        .replaceAll("{Ban}", "Bàn ${tableData.ban!.tableName}")
        .replaceAll("{Ghi_Chu}", "")
        .replaceAll(
            "{Chi_Tiet_San_Pham2}", """<table class="table-order"></table>""");
  }

  static String getReceiptCollectPrintData(
      String blueprint, CollectResponse collectResponse) {
    // var ngayXuat = orderDetail.donHang!.sellDate ?? "";
    // try {
    //   DateFormat(AppValue.backend_datetime_format).format(DateTime.now());
    //   DateTime tempDate = new DateFormat(AppValue.backend_datetime_format)
    //       .parse(orderDetail.donHang!.sellDate!);
    //   ngayXuat = DateFormat(AppValue.short_datetime_format).format(tempDate);
    // } catch (e) {}

    return blueprint
        .replaceAll("\r", "")
        .replaceAll("\n", "")
        .replaceAll("\t", "")
        .replaceAll(
            "{Ma_Phieu_Thu}", "${collectResponse.phieuThu!.receiptCode}")
        .replaceAll("{Ngay_Thu}",
            "${AppValue.backendDateTimeToDateTome(collectResponse.phieuThu!.receiptDate!)}")
        .replaceAll("{Thu_Ngan}", "${collectResponse.tenNguoiThu}")
        .replaceAll("{Hinh_Thuc_Thu}", "${collectResponse.phieuThu!.typeId}")
        .replaceAll("{Tong_Tien}",
            "${AppValue.APP_MONEY_FORMAT.format(collectResponse.phieuThu!.totalMoney ?? 0)}")
        .replaceAll("{So_Tien_Bang_Chu}",
            "${NumberToTextUtil.ChuyenSo(collectResponse.phieuThu!.totalMoney!.toString())}")
        .replaceAll("{Ghi_Chu}", "${collectResponse.phieuThu!.notes}");
  }

  static String getReceiptPayPrintData(
      String blueprint, PayResponse payResponse) {
    // var ngayXuat = orderDetail.donHang!.sellDate ?? "";
    // try {
    //   DateFormat(AppValue.backend_datetime_format).format(DateTime.now());
    //   DateTime tempDate = new DateFormat(AppValue.backend_datetime_format)
    //       .parse(orderDetail.donHang!.sellDate!);
    //   ngayXuat = DateFormat(AppValue.short_datetime_format).format(tempDate);
    // } catch (e) {}

    return blueprint
        .replaceAll("\r", "")
        .replaceAll("\n", "")
        .replaceAll("\t", "")
        .replaceAll("{Ma_Phieu_Chi}", "${payResponse.phieuChi!.paymentCode}")
        .replaceAll("{Ngay_Chi}",
            "${AppValue.backendDateTimeToDateTome(payResponse.phieuChi!.paymentDate!)}")
        .replaceAll("{Thu_Ngan}", "${payResponse.tenNguoiChi}")
        .replaceAll("{Hinh_Thuc_Chi}", "${payResponse.phieuChi!.typeId}")
        .replaceAll("{Tong_Tien}",
            "${AppValue.APP_MONEY_FORMAT.format(payResponse.phieuChi!.totalMoney ?? 0)}")
        .replaceAll("{So_Tien_Bang_Chu}",
            "${NumberToTextUtil.ChuyenSo(payResponse.phieuChi!.totalMoney!.toString())}")
        .replaceAll("{Ghi_Chu}", "${payResponse.phieuChi!.notes}");
  }

  static String getStampPrintData(String blueprint, TableData tableData,
      LoadTableResponse orderDetail, ODProductData productData) {
    var ngayXuat = orderDetail.donHang!.sellDate ?? "";
    try {
      DateFormat(AppValue.backend_datetime_format).format(DateTime.now());
      DateTime tempDate = new DateFormat(AppValue.backend_datetime_format)
          .parse(orderDetail.donHang!.sellDate!);
      ngayXuat = DateFormat(AppValue.short_datetime_format).format(tempDate);
    } catch (e) {}

    return blueprint
        .replaceAll("\r", "")
        .replaceAll("\n", "")
        .replaceAll("\t", "")
        .replaceAll("{Ban}", "Bàn ${tableData.ban!.tableName}")
        .replaceAll("{Ten_San_Pham}", "${productData.sanPham!.prdName}")
        .replaceAll("{Them}",
            "${productData.topping!.map((e) => "${e.topping!.prdName}x${e.quantityAdd}").join(", ")}")
        .replaceAll("{Chon}", "")
        .replaceAll("{Ngay_Xuat}", "${ngayXuat}")
        .replaceAll("{Gia}",
            "${AppValue.APP_MONEY_FORMAT.format(productData.sauChietKhau ?? 0)}");
  }
}
