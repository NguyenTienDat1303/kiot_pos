import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderCreateProductBinding extends Bindings {
  @override
  void dependencies() {
    // Get.lazyPut<OrderCreateProductController>(
    //   () => OrderCreateProductController(),
    //   fenix: true
    // );
    Get.put(OrderCreateProductController(), permanent: false);
  }
}
