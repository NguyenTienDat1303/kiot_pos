import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class CustomerCreateBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CustomerCreateController>(() => CustomerCreateController(),
        fenix: true);
  }
}
