import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class UserBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UserController>(() => UserController(), fenix: true);
  }
}
