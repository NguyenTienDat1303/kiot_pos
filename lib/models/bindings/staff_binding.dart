import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class StaffBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StaffController>(() => StaffController(),
        fenix: true);
  }
}
