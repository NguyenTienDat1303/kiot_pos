import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class ProductBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ProductController(), permanent: false);
  }
}
