import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class ConfigBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ConfigController>(() => ConfigController(), fenix: true);
    // Get.put(ConfigController(), permanent: false);
  }
}
