import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OverviewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OverviewController>(
      () => OverviewController(),
      fenix: true
    );
  }
}
