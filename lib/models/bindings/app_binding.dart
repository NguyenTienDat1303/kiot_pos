import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() async {
    Get.put<AppController>(AppController(), permanent: true);
  }
}
