import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class AreaEditBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AreaEditController(), permanent: false);
  }
}
