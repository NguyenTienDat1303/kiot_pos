import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class SplashBinding extends Bindings {
  @override
  void dependencies() {
    // Get.lazyPut<SplashController>(
    //   () => SplashController(),
    //   fenix: true
    // );
    Get.put(SplashController(), permanent: false);
  }
}
