import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderReturnBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OrderReturnController>(() => OrderReturnController(),
        fenix: true);
    // Get.put(OrderReturnController(), permanent: false);
  }
}
