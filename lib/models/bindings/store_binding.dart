import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class StoreBinding extends Bindings {
  @override
  void dependencies() {
    // Get.lazyPut<StoreController>(
    //   () => StoreController(),
    //   fenix: true
    // );
    Get.put(StoreController(), permanent: false);
  }
}
