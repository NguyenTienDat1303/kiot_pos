import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class PrinterCreateBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PrinterCreateController>(
      () => PrinterCreateController(),
      fenix: true
    );
  }
}
