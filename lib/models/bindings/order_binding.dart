import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderBinding extends Bindings {
  @override
  void dependencies() {
   
    Get.put(OrderController(), permanent: false);
  }
}