import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class BillDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BillDetailController>(() => BillDetailController(), fenix: true);
  }
}
