import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class ForgotPasswordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ForgotPasswordController>(
      () => ForgotPasswordController(),
      fenix: true
    );
  }
}
