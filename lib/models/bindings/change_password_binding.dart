import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class ChangePasswordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ChangePasswordController>(
      () => ChangePasswordController(),
      fenix: true
    );
  }
}
