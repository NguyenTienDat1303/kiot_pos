import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class AreaBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AreaController>(() => AreaController(), fenix: true);
  }
}
