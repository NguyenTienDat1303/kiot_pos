import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class StaffDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StaffDetailController>(() => StaffDetailController(),
        fenix: true);
  }
}
