import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class CatalogueBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CatalogueController>(() => CatalogueController(), fenix: true);
  }
}
