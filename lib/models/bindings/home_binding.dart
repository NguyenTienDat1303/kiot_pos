import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    // Get.lazyPut<HomeController>(
    //   () => HomeController(),
    //   fenix: true
    // );
    Get.put(HomeController(), permanent: false);
    Get.put(PrintManagerController(), permanent: false);
  }
}
