import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderCreateBinding extends Bindings {
  @override
  void dependencies() {
    // Get.lazyPut<OrderCreateController>(
    //   () => OrderCreateController(),
    //   fenix: true
    // );
    Get.put(OrderCreateController(), permanent: false);
  }
}
