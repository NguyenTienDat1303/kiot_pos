import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class MoveTableBinding extends Bindings {
  @override
  void dependencies() {
    // Get.lazyPut<StoreController>(
    //   () => StoreController(),
    //   fenix: true
    // );
    Get.put(MoveTableController(), permanent: false);
  }
}
