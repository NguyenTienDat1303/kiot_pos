import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OrderToppingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OrderToppingController>(() => OrderToppingController(),
        fenix: true);
    // Get.put(OrderToppingController(), permanent: false);
  }
}
