import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginController>(
      () => LoginController(),
      fenix: true
    );
  }
}
