import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class OverviewCollectPayBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OverviewCollectPayController>(
        () => OverviewCollectPayController(),
        fenix: true);
  }
}
