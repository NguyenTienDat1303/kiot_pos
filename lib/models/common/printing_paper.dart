import 'package:kiot_pos/data/data.dart';
import 'package:screenshot/screenshot.dart';

class PrintingPaper {
  final String data;
  final int printFormat;
  final LoadTableResponse? orderDetail;
  final ODProductData? productData;
  ScreenshotController screenshotController = ScreenshotController();

  PrintingPaper(this.data, this.printFormat,
      {this.orderDetail, this.productData});
}
