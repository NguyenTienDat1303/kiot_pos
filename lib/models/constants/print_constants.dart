class PrintType {
  static int none = 0;
  static int bluetooth = 1;
  static int lan = 2;
  static int usb = 2;
  static List<int> supported = [bluetooth, lan];
}

class PrintName {
  static String invoice = "invoice";
  static String kitchen = "kitchen";
  static String receipt_pay = "receipt_pay";
  static String receipt_collect = "receipt_collect";
  static String stamp = "stamp";
}

class PrintrPaperSize {
  static int mm55 = 0;
  static int mm80 = 1;
  static List<int> supported = [mm55, mm80];
}

class PrintFormat {
  static const int invoice = 1;
  static const int kitchen = 2;
  static const int receipt_pay = 3;
  static const int receipt_collect = 4;
  static const int stamp = 5;
  static List<int> supported = [
    invoice,
    kitchen,
    receipt_pay,
    receipt_collect,
    stamp
  ];
}

enum PrintAction {
  payment,
  payment_temp,
  kitchen,
  receipt_pay,
  receipt_collect,
  stamp
}
