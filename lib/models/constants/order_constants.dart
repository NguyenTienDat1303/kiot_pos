enum OrderDiscountRatio { percent, vnd }

enum OrderTypeLoad { payment, order_edit }

class OrderStatus {
  static const int save = 0;
  static const int payment = 1;
   static const int tempPayment = 2;
}

class OrderProductSelectType {
  static const int gift = 0;
  static const int normal = 1;
}

class PaymentMethod {
  static const int cash = 1;
  static const int card = 2;
  static const int bank_transfer = 3;

  static const supported = [cash, card, bank_transfer];
}

class VatOrder {
  static const int vat0 = 0;
  static const int vat5 = 5;
  static const int vat8 = 8;
  static const int vat10 = 10;

  static const supported = [vat0, vat5, vat8, vat10];
}
