class TableStatus {
  static int? all = null;
  static int used = 1;
  static int empty = 0;

  static List<int?> supported = [all, used, empty];
}

class TableAction {
  static int move = 1;
  static int change = 2;

  static List<int?> supported = [move, change];
}
