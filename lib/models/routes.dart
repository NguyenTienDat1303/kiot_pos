import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class AppRoutes {
  AppRoutes();

  static const initial = '/initial';
  static const splash = '/splash';
  static const login = '/login';
  static const register = '/register';
  static const forgot_password = '/forgot_password';
  static const home = '/home';

  static const store = 'store';
  static const overview = 'overview';
  static const customer = 'customer';
  static const staff = 'staff';
  static const stock = 'stock';
  static const area = 'area';
  static const order = 'order';
  static const bill = 'bill';
  static const sync = 'sync';
  static const setting = 'setting';

  static const order_create = '/order_create';
  static const order_create_product = '/order_create_product';
  static const order_topping = '/order_topping';
  static const order_customer = '/order_customer';
  static const order_staff = '/order_staff';
  static const payment = '/payment';
  static const move_table = '/move_table';
  static const print_preview = '/print_preview';

  static const order_detail = '/order_detail';
  static const order_return = '/order_return';

  static const config = '/config';
  static const kiot_setting = '/kiot_setting';

  static const customer_create = '/customer_create';
  static const area_detail = '/area_detail';
  static const area_edit = '/area_edit';
  static const print_bluetooth_printer = '/print_bluetooth_printer';
  static const detail_staff = '/detail_staff';
  static const staff_edit = '/staff_edit';
  static const bill_detail = '/bill_detail';
  static const bill_edit = '/bill_edit';
  static const catalogue = '/catalogue';
  static const catalogue_detail = '/catalogue_detail';
  static const goods_list = '/list_product';
  static const goods_detail = '/detail_product';
  static const goods_edit = '/product_edit';
  static const topping = '/topping';
  static const unit = '/unit';
  static const stocking = '/stocking';
  static const stocking_detail = '/stocking_detail';
  static const stocking_detail_item = '/stocking_detail_item';
  static const stocking_edit = '/stocking_edit';
  static const stocking_create = '/stocking_create';
  static const stocking_goods = '/stocking_goods';
  static const qr_code = '/qr_code';
  static const policy = '/policy';
  static const edit_user = '/edit_user';
  static const topping_create = '/topping_create';
  static const change_password = '/change_password';
  static const printer_create = '/printer_create';

  List<GetPage> pages = [
    GetPage(name: initial, page: () => InitialPage()),
    GetPage(
        name: splash, page: () => SplashPage(), bindings: [SplashBinding()]),
    GetPage(
        name: login,
        page: () => LoginPage(),
        bindings: [LoginBinding()],
        transition: Transition.fadeIn,
        transitionDuration: Duration(seconds: 1)),
    GetPage(
      name: register,
      page: () => RegisterPage(),
    ),
    GetPage(
      name: forgot_password,
      page: () => ForgotPasswordPage(),
      bindings: [ForgotPasswordBinding()],
    ),
    GetPage(
      name: home,
      page: () => HomePage(),
      bindings: [
        HomeBinding(),
        OrderCreateBinding(),
        StoreBinding(),
        OverviewBinding(),
        CustomerBinding(),
        StaffBinding(),
        AreaBinding(),
        AreaEditBinding(),
        OverviewCollectPayBinding(),
        OrderBinding(),
        BillDetailBinding()
      ],
    ),
    GetPage(
      name: order_create,
      page: () => OrderCreatePage(),
      bindings: [
        OrderCreateBinding(),
      ],
    ),
    GetPage(
      name: order_create_product,
      page: () => OrderCreateProductPage(),
      bindings: [
        OrderCreateProductBinding(),
      ],
    ),
    GetPage(
      name: order_topping,
      page: () => OrderToppingPage(),
      bindings: [
        OrderToppingBinding(),
      ],
    ),
    GetPage(
      name: order_customer,
      page: () => OrderCustomerPage(),
      bindings: [
        CustomerBinding(),
      ],
    ),
    GetPage(
      name: order_staff,
      page: () => OrderStaffPage(),
      bindings: [
        StaffBinding(),
      ],
    ),
    GetPage(
      name: move_table,
      page: () => MoveTablePage(),
      bindings: [
        MoveTableBinding(),
      ],
    ),
    GetPage(
      name: payment,
      page: () => PaymentPage(),
    ),
    GetPage(
      name: order_detail,
      page: () => OrderDetailPage(),
      bindings: [
        OrderCreateBinding(),
      ],
    ),
    GetPage(
      name: print_preview,
      page: () => PrintPreviewPage(),
      bindings: [
        // OrderCreateBinding(),
      ],
    ),
    GetPage(
      name: order_return,
      page: () => OrderReturnPage(),
      bindings: [
        OrderCreateBinding(),
      ],
    ),
    GetPage(
      name: config,
      page: () => ConfigPage(),
      bindings: [
        ConfigBinding(),
      ],
    ),
    GetPage(
      name: kiot_setting,
      page: () => KiotSettingPage(),
    ),
    GetPage(
      name: print_bluetooth_printer,
      page: () => PrintBluetoothPrinterPage(),
      bindings: [PrintBluetoothPrinterBinding()],
    ),
    GetPage(
      name: customer_create,
      page: () => CustomerCreatePage(),
      bindings: [CustomerCreateBinding(), CustomerBinding()],
    ),
    GetPage(
      name: area_detail,
      page: () => AreaDetailPage(),
      bindings: [AreaDetailBinding()],
    ),
    GetPage(
      name: area_edit,
      page: () => AreaEditPage(),
      bindings: [AreaEditBinding(), AreaBinding()],
    ),
    GetPage(
      name: detail_staff,
      page: () => StaffDetailPage(),
      bindings: [StaffDetailBinding()],
    ),
    GetPage(
      name: staff_edit,
      page: () => StaffEditPage(),
      bindings: [StaffDetailBinding()],
    ),
    GetPage(
      name: bill_detail,
      page: () => BillDetailPage(),
      bindings: [BillDetailBinding(), OverviewCollectPayBinding()],
    ),
    GetPage(
      name: bill_edit,
      page: () => BillEditPage(),
      bindings: [
        BillDetailBinding(),
        OverviewCollectPayBinding(),
        CustomerBinding(),
        StaffBinding()
      ],
    ),
    GetPage(
      name: catalogue,
      page: () => CataloguePage(),
      bindings: [OrderCreateProductBinding(), OrderCreateBinding()],
    ),
    GetPage(
      name: catalogue_detail,
      page: () => CatalogueDetailPage(),
      bindings: [
        CatalogueBinding(),
        OrderCreateProductBinding(),
        OrderCreateBinding()
      ],
    ),
    GetPage(
      name: goods_list,
      page: () => GoodsListPage(),
      bindings: [
        OrderCreateProductBinding(),
        OrderCreateBinding(),
        ProductBinding()
      ],
    ),
    GetPage(
      name: goods_detail,
      page: () => GoodsDetailPage(),
      bindings: [ProductBinding()],
    ),
    GetPage(
      name: goods_edit,
      page: () => GoodsEditPage(),
      bindings: [ProductBinding()],
    ),
    GetPage(
      name: topping,
      page: () => ToppingPage(),
      bindings: [OrderToppingBinding()],
    ),
    GetPage(
      name: topping,
      page: () => ToppingPage(),
      bindings: [OrderToppingBinding()],
    ),
    GetPage(
      name: stocking,
      page: () => StockingPage(),
      bindings: [StockBinding()],
    ),
    GetPage(
      name: stocking_detail,
      page: () => StockingDetailPage(),
      bindings: [StockBinding()],
    ),
    GetPage(
      name: stocking_detail_item,
      page: () => StockingDetailItemPage(),
      bindings: [StockBinding()],
    ),
    GetPage(
      name: stocking_edit,
      page: () => StockingEditPage(),
      bindings: [StockBinding()],
    ),
    GetPage(
      name: stocking_create,
      page: () => StockingCreatePage(),
      bindings: [
        StockBinding(),
        ProductBinding(),
      ],
    ),
    GetPage(
      name: stocking_goods,
      page: () => StockingGoods(),
      bindings: [
        OrderCreateProductBinding(),
        OrderCreateBinding(),
        ProductBinding(),
        StockBinding()
      ],
    ),
    GetPage(
      name: qr_code,
      page: () => QrCodePage(),
      bindings: [],
    ),
    GetPage(
      name: policy,
      page: () => PolicyPage(),
      bindings: [],
    ),
    GetPage(
      name: edit_user,
      page: () => EditUserPage(),
      bindings: [UserBinding()],
    ),
    GetPage(
      name: unit,
      page: () => UnitPage(),
      bindings: [ProductBinding()],
    ),
    GetPage(
      name: topping_create,
      page: () => ToppingCreatePage(),
      bindings: [ProductBinding(), OrderCreateProductBinding()],
    ),
    GetPage(
      name: change_password,
      page: () => ChangePasswordPage(),
      bindings: [ChangePasswordBinding()],
    ),
    GetPage(
      name: printer_create,
      page: () => PrinterCreatePage(),
      bindings: [PrinterCreateBinding()],
    ),
  ];
}
