import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:intl/intl.dart';

class AppValue {
  AppValue._();
  static final APP_MONEY_FORMAT =
      new NumberFormat.currency(locale: 'eu', symbol: 'đ ', decimalDigits: 0);
  static final App_money_format = NumberFormat('#,###');

  static final backend_datetime_format = "yyyy-MM-dd HH:mm:ss";
  static final datetime_format = "dd-MM-yyyy HH:mm:ss";
  static final short_datetime_format = "dd/MM HH:mm";
  static final DATE_FORMAT = DateFormat('dd/MM/yyyy');

  static final CurrencyTextInputFormatter formatterVnd =
      CurrencyTextInputFormatter(
    locale: 'vi',
    decimalDigits: 0,
    symbol: '₫',
  );

  static String backendDateTimeToDateTome(String backendDateTime) {
    DateFormat(AppValue.backend_datetime_format).format(DateTime.now());
    DateTime tempDate =
        new DateFormat(AppValue.backend_datetime_format).parse(backendDateTime);
    return DateFormat(AppValue.datetime_format).format(tempDate);
  }

  static formatSearchDay(String day) {
    List<String> list = day.split('/');
    String day1 = list[2] + '/' + list[1] + '/' + list[0];
    return day1;
  }
}
