import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppStyles {
  static TextStyle pageTitleStyle =
      TextStyle(fontSize: 20.sp, height: 1.2, fontWeight: FontWeight.normal);

  static TextStyle normalTextStyle =
      TextStyle(fontSize: 16.sp, height: 1.2, fontWeight: FontWeight.normal);

  static TextStyle DEFAULT_14 =
      TextStyle(fontSize: 14.sp, height: 1.2, fontWeight: FontWeight.normal);
  static TextStyle DEFAULT_12 =
      TextStyle(fontSize: 12.sp, height: 1.2, fontWeight: FontWeight.normal);
  static TextStyle DEFAULT_10 =
      TextStyle(fontSize: 10.sp, height: 1.2, fontWeight: FontWeight.normal);
  static TextStyle DEFAULT_16 =
      TextStyle(fontSize: 16.sp, height: 1.2, fontWeight: FontWeight.normal);
  static TextStyle DEFAULT_16_BOLD =
      TextStyle(fontSize: 16.sp, height: 1.2, fontWeight: FontWeight.w600);
  static TextStyle DEFAULT_18_BOLD =
      TextStyle(fontSize: 18.sp, height: 1.2, fontWeight: FontWeight.w600);

  static TextStyle DEFAULT_20_BOLD =
      TextStyle(fontSize: 20.sp, height: 1.2, fontWeight: FontWeight.w600);
  static TextStyle DEFAULT_24_BOLD =
      TextStyle(fontSize: 24.sp, height: 1.2, fontWeight: FontWeight.w500);
}
