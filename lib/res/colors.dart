import 'package:flutter/material.dart';

class AppColors {
  AppColors._();
  static final primary = Color(0xff205BA1);
  static final bg_login = Color(0xff205BA1);
  static final Blue = Color(0xff205BA1);
  static final White = Color(0xffFFFFFF);
  static final Gray7 = Color(0xffF9F9F9);
  static final Orange = Color(0xffE27C04);
  static final Blue2 = Color(0xff3981D8);
  static final Blue3 = Color(0xff7BABE5);
  static final Blue4 = Color(0xffD6E8FF);
  static final Orange1 = Color(0xffFB9E31);
  static final Orange4 = Color(0xffFFEBD5);
  static final Gray1 = Color(0xff333333);
  static final Gray3 = Color(0xff828282);
  static final Gray4 = Color(0xffBDBDBD2);
  static final Gray5 = Color(0xffE0E0E0);

  static final RED = Color(0xffEB5757);
  static final GREEN = Color(0xff219653);
  static final Background = Color(0xffF5F5F5);
  
}
