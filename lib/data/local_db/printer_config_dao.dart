import 'package:floor/floor.dart';
import 'package:kiot_pos/kiot_pos.dart';

@dao
abstract class PrinterConfigDao {
  @Query('SELECT * FROM PrinterConfig')
  Future<List<PrinterConfig>> findAll();

  @Query('SELECT * FROM PrinterConfig WHERE formatId = :formatId')
  Future<List<PrinterConfig>> findByFormat(int formatId);

  @Query('SELECT * FROM PrinterConfig WHERE id = :id')
  Stream<PrinterConfig?> findById(int id);

  @insert
  Future<void> add(PrinterConfig printerConfig);

  @update
  Future<void> edit(PrinterConfig task);
}
