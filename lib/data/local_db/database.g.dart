// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  PrinterConfigDao? _printerConfigDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `PrinterConfig` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `type` INTEGER, `name` TEXT, `order` INTEGER, `paperSize` INTEGER, `formatId` INTEGER, `ipAddress` TEXT, `port` TEXT)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  PrinterConfigDao get printerConfigDao {
    return _printerConfigDaoInstance ??=
        _$PrinterConfigDao(database, changeListener);
  }
}

class _$PrinterConfigDao extends PrinterConfigDao {
  _$PrinterConfigDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _printerConfigInsertionAdapter = InsertionAdapter(
            database,
            'PrinterConfig',
            (PrinterConfig item) => <String, Object?>{
                  'id': item.id,
                  'type': item.type,
                  'name': item.name,
                  'order': item.order,
                  'paperSize': item.paperSize,
                  'formatId': item.formatId,
                  'ipAddress': item.ipAddress,
                  'port': item.port
                },
            changeListener),
        _printerConfigUpdateAdapter = UpdateAdapter(
            database,
            'PrinterConfig',
            ['id'],
            (PrinterConfig item) => <String, Object?>{
                  'id': item.id,
                  'type': item.type,
                  'name': item.name,
                  'order': item.order,
                  'paperSize': item.paperSize,
                  'formatId': item.formatId,
                  'ipAddress': item.ipAddress,
                  'port': item.port
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<PrinterConfig> _printerConfigInsertionAdapter;

  final UpdateAdapter<PrinterConfig> _printerConfigUpdateAdapter;

  @override
  Future<List<PrinterConfig>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM PrinterConfig',
        mapper: (Map<String, Object?> row) => PrinterConfig(
            row['id'] as int?,
            row['type'] as int?,
            row['name'] as String?,
            row['order'] as int?,
            row['paperSize'] as int?,
            row['formatId'] as int?,
            row['ipAddress'] as String?,
            row['port'] as String?));
  }

  @override
  Future<List<PrinterConfig>> findByFormat(int formatId) async {
    return _queryAdapter.queryList(
        'SELECT * FROM PrinterConfig WHERE formatId = ?1',
        mapper: (Map<String, Object?> row) => PrinterConfig(
            row['id'] as int?,
            row['type'] as int?,
            row['name'] as String?,
            row['order'] as int?,
            row['paperSize'] as int?,
            row['formatId'] as int?,
            row['ipAddress'] as String?,
            row['port'] as String?),
        arguments: [formatId]);
  }

  @override
  Stream<PrinterConfig?> findById(int id) {
    return _queryAdapter.queryStream(
        'SELECT * FROM PrinterConfig WHERE id = ?1',
        mapper: (Map<String, Object?> row) => PrinterConfig(
            row['id'] as int?,
            row['type'] as int?,
            row['name'] as String?,
            row['order'] as int?,
            row['paperSize'] as int?,
            row['formatId'] as int?,
            row['ipAddress'] as String?,
            row['port'] as String?),
        arguments: [id],
        queryableName: 'PrinterConfig',
        isView: false);
  }

  @override
  Future<void> add(PrinterConfig printerConfig) async {
    await _printerConfigInsertionAdapter.insert(
        printerConfig, OnConflictStrategy.abort);
  }

  @override
  Future<void> edit(PrinterConfig task) async {
    await _printerConfigUpdateAdapter.update(task, OnConflictStrategy.abort);
  }
}
