import 'dart:async';

import 'package:floor/floor.dart';
import 'package:kiot_pos/kiot_pos.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [PrinterConfig])
abstract class AppDatabase extends FloorDatabase {
  PrinterConfigDao get printerConfigDao;
}