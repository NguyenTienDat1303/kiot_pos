// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_add_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailAddDto _$DetailAddDtoFromJson(Map<String, dynamic> json) => DetailAddDto(
      idAdd: json['id_add'] as int?,
      quantityAdd: json['quantity_add'] as int?,
      priceAdd: json['price_add'] as int?,
    );

Map<String, dynamic> _$DetailAddDtoToJson(DetailAddDto instance) =>
    <String, dynamic>{
      'id_add': instance.idAdd,
      'quantity_add': instance.quantityAdd,
      'price_add': instance.priceAdd,
    };

ReturnAddDto _$ReturnAddDtoFromJson(Map<String, dynamic> json) => ReturnAddDto(
      id: json['id'] as int?,
      quantity: json['quantity'] as int?,
    );

Map<String, dynamic> _$ReturnAddDtoToJson(ReturnAddDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'quantity': instance.quantity,
    };
