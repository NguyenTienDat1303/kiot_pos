import 'package:json_annotation/json_annotation.dart';
import 'package:kiot_pos/kiot_pos.dart';

part 'detail_order_dto.g.dart';

@JsonSerializable()
class DetailOrderDto {
  DetailOrderDto({
    this.id,
    this.quantity,
    this.price,
    this.cc1,
    this.cc2,
    this.discount,
    this.discountVnd,
    this.note,
    this.print = 0,
    this.printCheck = 0,
    this.promotion = 0,
    this.seq = 1,
    this.printed = 0,
    this.vat,
    this.detailAdd,
  });
  String? id;
  int? quantity;
  int? price;
  String? cc1;
  String? cc2;
  int? discount;
  @JsonKey(name: 'discount_vnd')
  int? discountVnd;
  String? note;
  int? print;
  @JsonKey(name: 'print_check')
  int? printCheck;
  int? promotion;
  int? seq;
  int? printed;

  int? vat;
  @JsonKey(name: 'detail_add')
  List<DetailAddDto>? detailAdd;

  factory DetailOrderDto.fromJson(Map<String, dynamic> json) =>
      _$DetailOrderDtoFromJson(json);

  Map<String, dynamic> toJson() => _$DetailOrderDtoToJson(this);
}
