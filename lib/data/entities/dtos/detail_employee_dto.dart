import 'package:json_annotation/json_annotation.dart';

part 'detail_employee_dto.g.dart';

@JsonSerializable()
class DetailEmployeeDto {
  DetailEmployeeDto({this.id,this.start, this.end});
  @JsonKey(name: 'id')
  String? id;
  @JsonKey(name: 'start')
  String? start;
  @JsonKey(name: 'end')
  String? end;

  factory DetailEmployeeDto.fromJson(Map<String, dynamic> json) =>
      _$DetailEmployeeDtoFromJson(json);

  Map<String, dynamic> toJson() => _$DetailEmployeeDtoToJson(this);
}
