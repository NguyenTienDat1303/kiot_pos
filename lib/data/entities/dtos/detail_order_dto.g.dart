// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_order_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailOrderDto _$DetailOrderDtoFromJson(Map<String, dynamic> json) =>
    DetailOrderDto(
      id: json['id'] as String?,
      quantity: json['quantity'] as int?,
      price: json['price'] as int?,
      cc1: json['cc1'] as String?,
      cc2: json['cc2'] as String?,
      discount: json['discount'] as int?,
      discountVnd: json['discount_vnd'] as int?,
      note: json['note'] as String?,
      print: json['print'] as int? ?? 0,
      printCheck: json['print_check'] as int? ?? 0,
      promotion: json['promotion'] as int? ?? 0,
      seq: json['seq'] as int? ?? 1,
      printed: json['printed'] as int? ?? 0,
      vat: json['vat'] as int?,
      detailAdd: (json['detail_add'] as List<dynamic>?)
          ?.map((e) => DetailAddDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DetailOrderDtoToJson(DetailOrderDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'quantity': instance.quantity,
      'price': instance.price,
      'cc1': instance.cc1,
      'cc2': instance.cc2,
      'discount': instance.discount,
      'discount_vnd': instance.discountVnd,
      'note': instance.note,
      'print': instance.print,
      'print_check': instance.printCheck,
      'promotion': instance.promotion,
      'seq': instance.seq,
      'printed': instance.printed,
      'vat': instance.vat,
      'detail_add': instance.detailAdd,
    };
