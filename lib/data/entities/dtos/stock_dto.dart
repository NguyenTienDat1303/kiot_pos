import 'package:json_annotation/json_annotation.dart';

part 'stock_dto.g.dart';

@JsonSerializable()
class StockDto {
  StockDto({
    this.id,
    this.inventory,
    this.quantity,
  });

  int? id;
  int? inventory;
  int? quantity;

  factory StockDto.fromJson(Map<String, dynamic> json) =>
      _$StockDtoFromJson(json);

  Map<String, dynamic> toJson() => _$StockDtoToJson(this);

  @override
  String toString() =>
      'StockDto(id: $id, inventory: $inventory, quantity: $quantity)';
}
