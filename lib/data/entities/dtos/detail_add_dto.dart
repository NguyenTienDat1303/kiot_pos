import 'package:json_annotation/json_annotation.dart';

part 'detail_add_dto.g.dart';

@JsonSerializable()
class DetailAddDto {
  DetailAddDto({
    this.idAdd,
    this.quantityAdd,
    this.priceAdd,
  });
  @JsonKey(name: 'id_add')
  int? idAdd;
  @JsonKey(name: 'quantity_add')
  int? quantityAdd;
  @JsonKey(name: 'price_add')
  int? priceAdd;

  factory DetailAddDto.fromJson(Map<String, dynamic> json) =>
      _$DetailAddDtoFromJson(json);

  Map<String, dynamic> toJson() => _$DetailAddDtoToJson(this);
}

@JsonSerializable()
class ReturnAddDto {
  ReturnAddDto({
    this.id,
    this.quantity,
  });
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'quantity')
  int? quantity;

  factory ReturnAddDto.fromJson(Map<String, dynamic> json) =>
      _$ReturnAddDtoFromJson(json);

  Map<String, dynamic> toJson() => _$ReturnAddDtoToJson(this);

  @override
  String toString() => 'ReturnAddDto(id: $id, quantity: $quantity)';
}
