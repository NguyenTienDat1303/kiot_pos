// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_employee_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailEmployeeDto _$DetailEmployeeDtoFromJson(Map<String, dynamic> json) =>
    DetailEmployeeDto(
      id: json['id'] as String?,
      start: json['start'] as String?,
      end: json['end'] as String?,
    );

Map<String, dynamic> _$DetailEmployeeDtoToJson(DetailEmployeeDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'start': instance.start,
      'end': instance.end,
    };
