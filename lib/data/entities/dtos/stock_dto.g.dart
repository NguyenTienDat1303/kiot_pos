// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stock_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StockDto _$StockDtoFromJson(Map<String, dynamic> json) => StockDto(
      id: json['id'] as int?,
      inventory: json['inventory'] as int?,
      quantity: json['quantity'] as int?,
    );

Map<String, dynamic> _$StockDtoToJson(StockDto instance) => <String, dynamic>{
      'id': instance.id,
      'inventory': instance.inventory,
      'quantity': instance.quantity,
    };
