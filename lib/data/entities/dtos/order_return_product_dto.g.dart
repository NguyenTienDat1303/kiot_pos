// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_return_product_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderReturnProductDto _$OrderReturnProductDtoFromJson(
        Map<String, dynamic> json) =>
    OrderReturnProductDto(
      id: json['id'] as int?,
      quantity: json['quantity'] as int?,
    );

Map<String, dynamic> _$OrderReturnProductDtoToJson(
        OrderReturnProductDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'quantity': instance.quantity,
    };
