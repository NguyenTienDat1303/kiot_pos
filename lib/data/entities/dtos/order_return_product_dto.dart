import 'package:json_annotation/json_annotation.dart';

part 'order_return_product_dto.g.dart';

@JsonSerializable()
class OrderReturnProductDto {
  OrderReturnProductDto({
    this.id,
    this.quantity,
   
  });
 
  int? id;
  int? quantity;
  
  factory OrderReturnProductDto.fromJson(Map<String, dynamic> json) =>
      _$OrderReturnProductDtoFromJson(json);

  Map<String, dynamic> toJson() => _$OrderReturnProductDtoToJson(this);
}
