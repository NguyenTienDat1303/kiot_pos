import 'package:floor/floor.dart';

@entity
class PrinterConfig {
  @PrimaryKey(autoGenerate: true)
  int? id;
  int? type;
  String? name;
  int? order;
  int? paperSize;
  int? formatId;
  String? ipAddress;
  String? port;

  PrinterConfig(this.id, this.type, this.name, this.order, this.paperSize,
      this.formatId, this.ipAddress, this.port);
}
