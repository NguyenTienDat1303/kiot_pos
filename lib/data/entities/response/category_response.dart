import 'package:json_annotation/json_annotation.dart';
import 'package:kiot_pos/kiot_pos.dart';

part 'category_response.g.dart';

@JsonSerializable()
class CategoryData {
  CategoryData({
    this.danhMuc,
    this.soLuongSanPham,
    this.cacSanPham,
  });

  CategoryDetail? danhMuc;
  String? soLuongSanPham;
  List<ProductData>? cacSanPham;

  factory CategoryData.fromJson(Map<String, dynamic> json) =>
      _$CategoryDataFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryDataToJson(this);
}

@JsonSerializable()
class CategoryDetail {
  CategoryDetail({
    this.id,
    this.prdGroupName,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'prd_group_name')
  String? prdGroupName;

  factory CategoryDetail.fromJson(Map<String, dynamic> json) =>
      _$CategoryDetailFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryDetailToJson(this);
}
