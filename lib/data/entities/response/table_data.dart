import 'package:json_annotation/json_annotation.dart';

part 'table_data.g.dart';

@JsonSerializable()
class TableData {
  TableData({
    this.ban,
    this.status,
    this.donHangID,
    this.giaTien,
  });

  TableDetail? ban;
  int? donHangID;
  String? status;
  String? giaTien;

  factory TableData.fromJson(Map<String, dynamic> json) =>
      _$TableDataFromJson(json);

  Map<String, dynamic> toJson() => _$TableDataToJson(this);
}

@JsonSerializable()
class TableDetail {
  TableDetail({
    this.id,
    this.tableName,
    this.tableStatus,
    this.from1,
    this.to1,
    this.tablePrice1,
    this.from2,
    this.to2,
    this.tablePrice2,
    this.from3,
    this.to3,
    this.tablePrice3,
    this.startDate,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'table_name')
  String? tableName;
  @JsonKey(name: 'table_status')
  int? tableStatus;
  int? from1;
  int? to1;
  @JsonKey(name: 'table_price1')
  int? tablePrice1;
  int? from2;
  int? to2;
  @JsonKey(name: 'table_price2')
  int? tablePrice2;
  int? from3;
  int? to3;
  @JsonKey(name: 'table_price3')
  int? tablePrice3;
  @JsonKey(name: 'start_date')
  String? startDate;

  factory TableDetail.fromJson(Map<String, dynamic> json) =>
      _$TableDetailFromJson(json);

  Map<String, dynamic> toJson() => _$TableDetailToJson(this);
}
