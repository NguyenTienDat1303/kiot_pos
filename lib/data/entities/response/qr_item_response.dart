import 'package:json_annotation/json_annotation.dart';

part 'qr_item_response.g.dart';

@JsonSerializable()
class ItemQrResponse {
  ItemQrResponse({
    this.id,
    this.prdCode,
    this.prdName,
    this.prdSellPrice,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'prd_code')
  String? prdCode;
  @JsonKey(name: 'prd_name')
  String? prdName;
  @JsonKey(name: 'prd_sell_price')
  int? prdSellPrice;

  factory ItemQrResponse.fromJson(Map<String, dynamic> json) =>
      _$ItemQrResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ItemQrResponseToJson(this);

}
