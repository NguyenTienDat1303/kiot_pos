import 'package:json_annotation/json_annotation.dart';
import 'package:kiot_pos/data/entities/response/unit_conversion_response.dart';
part 'detail_product_response.g.dart';

@JsonSerializable()
class DetailProductResponse {
  DetailProductResponse(
      {this.sanPham, this.danhMuc, this.nhaSanXuat, this.quyDoi});

  DetailProductData? sanPham;
  String? danhMuc;
  String? nhaSanXuat;
  List<UnitConversionResponse>? quyDoi;

  factory DetailProductResponse.fromJson(Map<String, dynamic> json) =>
      _$DetailProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DetailProductResponseToJson(this);
}

@JsonSerializable()
class DetailProductData {
  DetailProductData(
      {this.id,
      this.prdImageUrl,
      this.prdName,
      this.prdSls,
      this.prdCode,
      this.unitName,
      this.prdOriginPrice,
      this.prdSellPrice,
      this.prdSellPrice2,
      this.prdGroupId,
      this.prdManufactureId,
      this.prdMin,
      this.prdMax,
      this.prdEditPrice,
      this.prdAllownegative,
      this.prd_add,
      this.prd_material,
      this.prd_point,
      this.prd_print});
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'prd_image_url')
  String? prdImageUrl;
  @JsonKey(name: 'prd_name')
  String? prdName;
  @JsonKey(name: 'prd_sls')
  String? prdSls;
  @JsonKey(name: 'prd_code')
  String? prdCode;
  @JsonKey(name: 'unit_name')
  String? unitName;
  @JsonKey(name: 'prd_origin_price')
  int? prdOriginPrice;
  @JsonKey(name: 'prd_sell_price')
  int? prdSellPrice;
  @JsonKey(name: 'prd_sell_price2')
  int? prdSellPrice2;
  @JsonKey(name: 'prd_group_id')
  int? prdGroupId;
  @JsonKey(name: 'prd_manufacture_id')
  int? prdManufactureId;
  @JsonKey(name: 'prd_min')
  String? prdMin;
  @JsonKey(name: 'prd_max')
  String? prdMax;
  @JsonKey(name: 'prd_edit_price')
  int? prdEditPrice;
  @JsonKey(name: 'prd_allownegative')
  int? prdAllownegative;
  int? prd_material;
  int? prd_add;
  int? prd_point;
  int? prd_print;
  factory DetailProductData.fromJson(Map<String, dynamic> json) =>
      _$DetailProductDataFromJson(json);

  Map<String, dynamic> toJson() => _$DetailProductDataToJson(this);
}
