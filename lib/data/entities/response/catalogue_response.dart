import 'package:json_annotation/json_annotation.dart';

part 'catalogue_response.g.dart';

@JsonSerializable()
class CatalogueResponse {
  CatalogueResponse({
    this.danhMuc,
    this.soLuongSanPham,
    this.cacSanPham,
  });

  CatalogueData? danhMuc;
  String? soLuongSanPham;

  List<Products>? cacSanPham;

  factory CatalogueResponse.fromJson(Map<String, dynamic> json) =>
      _$CatalogueResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CatalogueResponseToJson(this);
}

@JsonSerializable()
class Products {
  Products({
    this.prdCode,
    this.prdName,
    this.prdSellPrice,
  });

  @JsonKey(name: 'prd_code')
  String? prdCode;
  @JsonKey(name: 'prd_name')
  String? prdName;
  @JsonKey(name: 'prd_sell_price')
  int? prdSellPrice;

  factory Products.fromJson(Map<String, dynamic> json) =>
      _$ProductsFromJson(json);

  Map<String, dynamic> toJson() => _$ProductsToJson(this);
}

@JsonSerializable()
class CatalogueData {
  CatalogueData({
    this.ID,
    this.prdGroupName,
  });

  int? ID;
  @JsonKey(name: 'prd_group_name')
  String? prdGroupName;

  factory CatalogueData.fromJson(Map<String, dynamic> json) =>
      _$CatalogueDataFromJson(json);

  Map<String, dynamic> toJson() => _$CatalogueDataToJson(this);
}
