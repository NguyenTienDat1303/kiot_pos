import 'package:json_annotation/json_annotation.dart';
import 'package:kiot_pos/data/entities/entities.dart';

part 'area_data.g.dart';




@JsonSerializable()
class AreaData {
  AreaData({
    this.id,
    this.areaName,
    this.numberTable,
    this.from1,
    this.to1,
    this.areaPrice1,
    this.from2,
    this.to2,
    this.areaPrice2,
    this.from3,
    this.to3,
    this.areaPrice3,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'area_name')
  String? areaName;
  @JsonKey(name: 'number_table')
  int? numberTable;
  int? from1;
  int? to1;
  @JsonKey(name: 'area_price1')
  int? areaPrice1;
  int? from2;
  int? to2;
  @JsonKey(name: 'area_price2')
  int? areaPrice2;
  int? from3;
  int? to3;
  @JsonKey(name: 'area_price3')
  int? areaPrice3;


  factory AreaData.fromJson(Map<String, dynamic> json) =>
      _$AreaDataFromJson(json);

  Map<String, dynamic> toJson() => _$AreaDataToJson(this);
}


@JsonSerializable()
class AreaDetailData {
  AreaData? khuVuc;
  StoreData? kho;

  AreaDetailData({this.khuVuc, this.kho});

  factory AreaDetailData.fromJson(Map<String, dynamic> json) =>
      _$AreaDetailDataFromJson(json);

  Map<String, dynamic> toJson() => _$AreaDetailDataToJson(this);
}