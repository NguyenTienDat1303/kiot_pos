import 'package:json_annotation/json_annotation.dart';

part 'pay_response.g.dart';

@JsonSerializable()
class PayResponse {
  Pay? phieuChi;
  String? tenNguoiChi;
  PayIn? phieuNhap;
  String? nhaCungCap;
  PayResponse(
      {this.phieuChi, this.tenNguoiChi, this.nhaCungCap, this.phieuNhap});

  factory PayResponse.fromJson(Map<String, dynamic> json) =>
      _$PayResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PayResponseToJson(this);
}

@JsonSerializable()
class Pay {
  Pay({
    this.id,
    this.inputId,
    this.customerId,
    this.paymentCode,
    this.paymentImage,
    this.typeId,
    this.storeId,
    this.paymentDate,
    this.notes,
    this.paymentMethod,
    this.paymentFor,
    this.totalMoney,
    this.deleted,
    this.created,
    this.updated,
    this.userInit,
    this.userUpd,
    this.active,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'input_id')
  int? inputId;
  @JsonKey(name: 'customer_id')
  int? customerId;
  @JsonKey(name: 'payment_code')
  String? paymentCode;
  @JsonKey(name: 'payment_image')
  String? paymentImage;
  @JsonKey(name: 'type_id')
  int? typeId;
  @JsonKey(name: 'store_id')
  int? storeId;
  @JsonKey(name: 'payment_date')
  String? paymentDate;

  String? notes;
  @JsonKey(name: 'payment_method')
  int? paymentMethod;
  @JsonKey(name: 'payment_for')
  int? paymentFor;
  @JsonKey(name: 'total_money')
  int? totalMoney;

  int? deleted;
  String? created;
  String? updated;
  @JsonKey(name: 'user_init')
  int? userInit;
  @JsonKey(name: 'user_upd')
  int? userUpd;
  int? active;

  factory Pay.fromJson(Map<String, dynamic> json) =>
      _$PayFromJson(json);

  Map<String, dynamic> toJson() => _$PayToJson(this);
}

@JsonSerializable()
class PayIn {
  String? input_code;
  PayIn({
    this.input_code,
  });

  factory PayIn.fromJson(Map<String, dynamic> json) =>
      _$PayInFromJson(json);

  Map<String, dynamic> toJson() => _$PayInToJson(this);
}
