import 'package:json_annotation/json_annotation.dart';

part 'overview_collect_pay_response.g.dart';

@JsonSerializable()


class OverviewCollectPayResponse {
    OverviewCollectPayResponse({
        this.soPhieuThu,
        this.tongTienThu,
        this.soPhieuChi,
        this.tongTienChi,
        this.conLai,
        this.tienMat,
        this.chuyenKhoan,
        this.the,
    });

    String? soPhieuThu;
    String? tongTienThu;
    String? soPhieuChi;
    String? tongTienChi;
    int? conLai;
    String? tienMat;
    String? chuyenKhoan;
    String? the;

    factory OverviewCollectPayResponse.fromJson(Map<String, dynamic> json) =>
      _$OverviewCollectPayResponseFromJson(json);

  Map<String, dynamic> toJson() => _$OverviewCollectPayResponseToJson(this);


}
