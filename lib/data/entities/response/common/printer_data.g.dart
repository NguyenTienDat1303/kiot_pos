// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'printer_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrinterData _$PrinterDataFromJson(Map<String, dynamic> json) => PrinterData(
      id: json['id'] as int?,
      description: json['description'] as String?,
      ip: json['ip'] as String?,
      port: json['port'] as String?,
      khoGiay: json['kho_giay'] as String?,
      kieuIn: json['kieu_in'] as int?,
    );

Map<String, dynamic> _$PrinterDataToJson(PrinterData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'ip': instance.ip,
      'port': instance.port,
      'kho_giay': instance.khoGiay,
      'kieu_in': instance.kieuIn,
    };
