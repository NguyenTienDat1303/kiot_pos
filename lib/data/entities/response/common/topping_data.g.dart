// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'topping_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ToppingData _$ToppingDataFromJson(Map<String, dynamic> json) => ToppingData(
      id: json['ID'] as int?,
      prdName: json['prd_name'] as String?,
      prdSellPrice: json['prd_sell_price'] as int?,
      prdSls: json['prd_sls'] as String?,
      prdImageUrl: json['prd_image_url'] as String?,
    );

Map<String, dynamic> _$ToppingDataToJson(ToppingData instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'prd_name': instance.prdName,
      'prd_sell_price': instance.prdSellPrice,
      'prd_sls': instance.prdSls,
      'prd_image_url': instance.prdImageUrl,
    };
