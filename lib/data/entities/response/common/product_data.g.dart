// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductData _$ProductDataFromJson(Map<String, dynamic> json) => ProductData(
      id: json['ID'] as int?,
      prdCode: json['prd_code'] as String?,
      prdName: json['prd_name'] as String?,
      prdSellPrice: json['prd_sell_price'] as int?,
      prdSls: json['prd_sls'] as String?,
      prdImageUrl: json['prd_image_url'] as String?,
    );

Map<String, dynamic> _$ProductDataToJson(ProductData instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'prd_code': instance.prdCode,
      'prd_name': instance.prdName,
      'prd_sell_price': instance.prdSellPrice,
      'prd_sls': instance.prdSls,
      'prd_image_url': instance.prdImageUrl,
    };
