import 'package:json_annotation/json_annotation.dart';

part 'printer_data.g.dart';

@JsonSerializable()
class PrinterData {
  PrinterData({
    this.id,
    this.description,
    this.ip,
    this.port,
    this.khoGiay,
    this.kieuIn,
  });

  int? id;
  String? description;
  String? ip;
  String? port;
  @JsonKey(name: 'kho_giay')
  String? khoGiay;
  @JsonKey(name: 'kieu_in')
  int? kieuIn;

  factory PrinterData.fromJson(Map<String, dynamic> json) =>
      _$PrinterDataFromJson(json);

  Map<String, dynamic> toJson() => _$PrinterDataToJson(this);
}
