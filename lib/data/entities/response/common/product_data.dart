import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kiot_pos/data/data.dart';

part 'product_data.g.dart';

@JsonSerializable()
class ProductData extends Equatable {
  ProductData({
    this.id,
    this.prdCode,
    this.prdName,
    this.prdSellPrice,
    this.prdSls,
    this.prdImageUrl,
  });

  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'prd_code')
  String? prdCode;
  @JsonKey(name: 'prd_name')
  String? prdName;
  @JsonKey(name: 'prd_sell_price')
  int? prdSellPrice;
  @JsonKey(name: 'prd_sls')
  String? prdSls;
  @JsonKey(name: 'prd_image_url')
  String? prdImageUrl;
  @JsonKey(ignore: true)
  int version = DateTime.now().microsecondsSinceEpoch;
  @JsonKey(ignore: true)
  int selectedQuantity = 0;
  @JsonKey(ignore: true)
  List<ToppingData> toppingList = [];

  factory ProductData.fromJson(Map<String, dynamic> json) =>
      _$ProductDataFromJson(json);

  Map<String, dynamic> toJson() => _$ProductDataToJson(this);

  @override
  String toString() {
    return "toString $id $version $selectedQuantity";
  }

  @override
  List<Object?> get props => [id];

  ODProductData toODProductData() {
    var result = ODProductData(
        sanPham: ODProductDetail(
            id: this.id, prdCode: this.prdCode, prdName: this.prdName),
        quantity: this.selectedQuantity,
        price: this.prdSellPrice,
        cc1: "0",
        cc2: "0",
        discount: 0,
        discountVnd: 0,
        note: "",
        truocChietKhau: 0,
        sauChietKhau: 0,
        topping: []);
    return result;
  }
}
