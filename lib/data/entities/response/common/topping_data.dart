import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kiot_pos/data/data.dart';

part 'topping_data.g.dart';

@JsonSerializable()
class ToppingData extends Equatable {
  ToppingData({
    this.id,
    // this.prdCode,
    this.prdName,
    this.prdSellPrice,
    this.prdSls,
    this.prdImageUrl,
  });

  @JsonKey(name: 'ID')
  int? id;
  // @JsonKey(name: 'prd_code')
  // String? prdCode;
  @JsonKey(name: 'prd_name')
  String? prdName;
  @JsonKey(name: 'prd_sell_price')
  int? prdSellPrice;
  @JsonKey(name: 'prd_sls')
  String? prdSls;
  @JsonKey(name: 'prd_image_url')
  String? prdImageUrl;
  @JsonKey(ignore: true)
  int selectedQuantity = 0;

  factory ToppingData.fromJson(Map<String, dynamic> json) =>
      _$ToppingDataFromJson(json);

  Map<String, dynamic> toJson() => _$ToppingDataToJson(this);

  @override
  List<Object?> get props => [id];

  ODToppingData toODToppingData() {
    var result = ODToppingData(
      topping: ODToppingDetail(id: this.id, prdName: this.prdName),
      quantityAdd: 1,
      priceAdd: prdSellPrice,
    );
    return result;
  }

  ToppingData copyWith({
    int? id,
    String? prdName,
    int? prdSellPrice,
    String? prdSls,
    String? prdImageUrl,
  }) {
    var toppingData = ToppingData(
      id: id ?? this.id,
      prdName: prdName ?? this.prdName,
      prdSellPrice: prdSellPrice ?? this.prdSellPrice,
      prdSls: prdSls ?? this.prdSls,
      prdImageUrl: prdImageUrl ?? this.prdImageUrl,
    );
    toppingData.selectedQuantity = selectedQuantity;

    return toppingData;
  }

  int get totalSellPrice => selectedQuantity * prdSellPrice!;
}
