// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'table_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TableListResponse _$TableListResponseFromJson(Map<String, dynamic> json) =>
    TableListResponse(
      (json['tamTinh'] as num).toDouble(),
      (json['danhSach'] as List<dynamic>)
          .map((e) => TableData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$TableListResponseToJson(TableListResponse instance) =>
    <String, dynamic>{
      'tamTinh': instance.tamTinh,
      'danhSach': instance.danhSach,
    };
