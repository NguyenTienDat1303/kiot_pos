import 'package:json_annotation/json_annotation.dart';

part 'stock_detail_response.g.dart';

@JsonSerializable()
class StockDetailResponse {
  StockDetailResponse(
      {this.kiemKe,
      this.chiTiet,
      this.lechAm,
      this.lechDuong,
      this.nguoiKiem,
      this.soLuongKiem});

  KiemKe? kiemKe;
  List<ChiTiet>? chiTiet;
  String? nguoiKiem;
  int? soLuongKiem;
  int? lechDuong;
  int? lechAm;

  factory StockDetailResponse.fromJson(Map<String, dynamic> json) =>
      _$StockDetailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$StockDetailResponseToJson(this);
}

@JsonSerializable()
class ChiTiet {
  ChiTiet({this.tenSP, this.tonKho, this.thucTe, this.lech, this.id});
  int? id;
  String? tenSP;
  int? tonKho;
  int? thucTe;
  int? lech;
  // ChiTietKiemKe? chiTietKiemKe;

  factory ChiTiet.fromJson(Map<String, dynamic> json) =>
      _$ChiTietFromJson(json);

  Map<String, dynamic> toJson() => _$ChiTietToJson(this);

  @override
  String toString() {
    return 'ChiTiet(tenSP: $tenSP, tonKho: $tonKho, thucTe: $thucTe, lech: $lech)';
  }
}

@JsonSerializable()
class ChiTietKiemKe {
  ChiTietKiemKe({
    this.id,
    this.adjustId,
    this.storeId,
    this.productId,
    this.inventory,
    this.checkError,
    this.quantity,
    this.deleted,
    this.created,
    this.updated,
    this.userInit,
    this.userUpd,
    this.active,
  });

  int? id;
  int? adjustId;
  int? storeId;
  int? productId;
  int? inventory;
  int? checkError;
  int? quantity;
  int? deleted;
  String? created;
  String? updated;
  int? userInit;
  int? userUpd;
  int? active;

  factory ChiTietKiemKe.fromJson(Map<String, dynamic> json) =>
      _$ChiTietKiemKeFromJson(json);

  Map<String, dynamic> toJson() => _$ChiTietKiemKeToJson(this);
}

@JsonSerializable()
class KiemKe {
  KiemKe({
    this.ID,
    this.adjustCode,
    this.storeId,
    this.notes,
    this.totalDifferent,
    this.totalQuantity,
    this.detailAdjust,
    this.adjustDate,
    this.adjustStatus,
    this.deleted,
    this.created,
    this.updated,
    this.userInit,
    this.userUpd,
    this.finishDate,
    this.updateData,
    this.active,
  });

  int? ID;
  @JsonKey(name: 'adjust_code')
  String? adjustCode;
  @JsonKey(name: 'store_id')
  int? storeId;
  String? notes;
  @JsonKey(name: 'total_different')
  String? totalDifferent;
  @JsonKey(name: 'total_quantity')
  String? totalQuantity;
  @JsonKey(name: 'detail_adjust')
  String? detailAdjust;
  @JsonKey(name: 'adjust_date')
  String? adjustDate;
  @JsonKey(name: 'adjust_status')
  int? adjustStatus;
  int? deleted;
  String? created;
  String? updated;
  @JsonKey(name: 'user_init')
  int? userInit;
  @JsonKey(name: 'user_upd')
  int? userUpd;
  @JsonKey(name: 'finish_date')
  String? finishDate;
  @JsonKey(name: 'update_data')
  int? updateData;
  int? active;

  factory KiemKe.fromJson(Map<String, dynamic> json) => _$KiemKeFromJson(json);

  Map<String, dynamic> toJson() => _$KiemKeToJson(this);
}
