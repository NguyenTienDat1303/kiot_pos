// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unit_conversion_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UnitConversionResponse _$UnitConversionResponseFromJson(
        Map<String, dynamic> json) =>
    UnitConversionResponse(
      prdCode: json['prd_code'] as String?,
      prdOriginPrice: json['prd_origin_price'] as int?,
      prdSellPrice: json['prd_sell_price'] as int?,
      prdSellPrice2: json['prd_sell_price2'] as int?,
      unitName: json['unit_name'] as String?,
      number: (json['number'] as num?)?.toInt(),
      id: json['ID'] as int?,
    );

Map<String, dynamic> _$UnitConversionResponseToJson(
        UnitConversionResponse instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'prd_code': instance.prdCode,
      'prd_origin_price': instance.prdOriginPrice,
      'prd_sell_price': instance.prdSellPrice,
      'prd_sell_price2': instance.prdSellPrice2,
      'unit_name': instance.unitName,
      'number': instance.number,
    };
