// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stock_detail_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StockDetailResponse _$StockDetailResponseFromJson(Map<String, dynamic> json) =>
    StockDetailResponse(
      kiemKe: json['kiemKe'] == null
          ? null
          : KiemKe.fromJson(json['kiemKe'] as Map<String, dynamic>),
      chiTiet: (json['chiTiet'] as List<dynamic>?)
          ?.map((e) => ChiTiet.fromJson(e as Map<String, dynamic>))
          .toList(),
      lechAm: json['lechAm'] as int?,
      lechDuong: json['lechDuong'] as int?,
      nguoiKiem: json['nguoiKiem'] as String?,
      soLuongKiem: json['soLuongKiem'] as int?,
    );

Map<String, dynamic> _$StockDetailResponseToJson(
        StockDetailResponse instance) =>
    <String, dynamic>{
      'kiemKe': instance.kiemKe,
      'chiTiet': instance.chiTiet,
      'nguoiKiem': instance.nguoiKiem,
      'soLuongKiem': instance.soLuongKiem,
      'lechDuong': instance.lechDuong,
      'lechAm': instance.lechAm,
    };

ChiTiet _$ChiTietFromJson(Map<String, dynamic> json) => ChiTiet(
      tenSP: json['tenSP'] as String?,
      tonKho: json['tonKho'] as int?,
      thucTe: json['thucTe'] as int?,
      lech: json['lech'] as int?,
      id: json['id'] as int?,
    );

Map<String, dynamic> _$ChiTietToJson(ChiTiet instance) => <String, dynamic>{
      'id': instance.id,
      'tenSP': instance.tenSP,
      'tonKho': instance.tonKho,
      'thucTe': instance.thucTe,
      'lech': instance.lech,
    };

ChiTietKiemKe _$ChiTietKiemKeFromJson(Map<String, dynamic> json) =>
    ChiTietKiemKe(
      id: json['id'] as int?,
      adjustId: json['adjustId'] as int?,
      storeId: json['storeId'] as int?,
      productId: json['productId'] as int?,
      inventory: json['inventory'] as int?,
      checkError: json['checkError'] as int?,
      quantity: json['quantity'] as int?,
      deleted: json['deleted'] as int?,
      created: json['created'] as String?,
      updated: json['updated'] as String?,
      userInit: json['userInit'] as int?,
      userUpd: json['userUpd'] as int?,
      active: json['active'] as int?,
    );

Map<String, dynamic> _$ChiTietKiemKeToJson(ChiTietKiemKe instance) =>
    <String, dynamic>{
      'id': instance.id,
      'adjustId': instance.adjustId,
      'storeId': instance.storeId,
      'productId': instance.productId,
      'inventory': instance.inventory,
      'checkError': instance.checkError,
      'quantity': instance.quantity,
      'deleted': instance.deleted,
      'created': instance.created,
      'updated': instance.updated,
      'userInit': instance.userInit,
      'userUpd': instance.userUpd,
      'active': instance.active,
    };

KiemKe _$KiemKeFromJson(Map<String, dynamic> json) => KiemKe(
      ID: json['ID'] as int?,
      adjustCode: json['adjust_code'] as String?,
      storeId: json['store_id'] as int?,
      notes: json['notes'] as String?,
      totalDifferent: json['total_different'] as String?,
      totalQuantity: json['total_quantity'] as String?,
      detailAdjust: json['detail_adjust'] as String?,
      adjustDate: json['adjust_date'] as String?,
      adjustStatus: json['adjust_status'] as int?,
      deleted: json['deleted'] as int?,
      created: json['created'] as String?,
      updated: json['updated'] as String?,
      userInit: json['user_init'] as int?,
      userUpd: json['user_upd'] as int?,
      finishDate: json['finish_date'] as String?,
      updateData: json['update_data'] as int?,
      active: json['active'] as int?,
    );

Map<String, dynamic> _$KiemKeToJson(KiemKe instance) => <String, dynamic>{
      'ID': instance.ID,
      'adjust_code': instance.adjustCode,
      'store_id': instance.storeId,
      'notes': instance.notes,
      'total_different': instance.totalDifferent,
      'total_quantity': instance.totalQuantity,
      'detail_adjust': instance.detailAdjust,
      'adjust_date': instance.adjustDate,
      'adjust_status': instance.adjustStatus,
      'deleted': instance.deleted,
      'created': instance.created,
      'updated': instance.updated,
      'user_init': instance.userInit,
      'user_upd': instance.userUpd,
      'finish_date': instance.finishDate,
      'update_data': instance.updateData,
      'active': instance.active,
    };
