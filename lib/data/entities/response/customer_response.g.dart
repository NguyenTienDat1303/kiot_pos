// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerResponse _$CustomerResponseFromJson(Map<String, dynamic> json) =>
    CustomerResponse(
      id: json['ID'] as int?,
      customerName: json['customer_name'] as String?,
      customerCode: json['customer_code'] as String?,
      customerPhone: json['customer_phone'] as String?,
      customerEmail: json['customer_email'] as String?,
      customerAddr: json['customer_addr'] as String?,
    );

Map<String, dynamic> _$CustomerResponseToJson(CustomerResponse instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'customer_name': instance.customerName,
      'customer_code': instance.customerCode,
      'customer_phone': instance.customerPhone,
      'customer_email': instance.customerEmail,
      'customer_addr': instance.customerAddr,
    };

GroupCustomerResponse _$GroupCustomerResponseFromJson(
        Map<String, dynamic> json) =>
    GroupCustomerResponse(
      id: json['ID'] as int?,
      customerGroupName: json['customer_group_name'] as String?,
    );

Map<String, dynamic> _$GroupCustomerResponseToJson(
        GroupCustomerResponse instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'customer_group_name': instance.customerGroupName,
    };
