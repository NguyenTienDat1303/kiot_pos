// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'area_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AreaData _$AreaDataFromJson(Map<String, dynamic> json) => AreaData(
      id: json['ID'] as int?,
      areaName: json['area_name'] as String?,
      numberTable: json['number_table'] as int?,
      from1: json['from1'] as int?,
      to1: json['to1'] as int?,
      areaPrice1: json['area_price1'] as int?,
      from2: json['from2'] as int?,
      to2: json['to2'] as int?,
      areaPrice2: json['area_price2'] as int?,
      from3: json['from3'] as int?,
      to3: json['to3'] as int?,
      areaPrice3: json['area_price3'] as int?,
    );

Map<String, dynamic> _$AreaDataToJson(AreaData instance) => <String, dynamic>{
      'ID': instance.id,
      'area_name': instance.areaName,
      'number_table': instance.numberTable,
      'from1': instance.from1,
      'to1': instance.to1,
      'area_price1': instance.areaPrice1,
      'from2': instance.from2,
      'to2': instance.to2,
      'area_price2': instance.areaPrice2,
      'from3': instance.from3,
      'to3': instance.to3,
      'area_price3': instance.areaPrice3,
    };

AreaDetailData _$AreaDetailDataFromJson(Map<String, dynamic> json) =>
    AreaDetailData(
      khuVuc: json['khuVuc'] == null
          ? null
          : AreaData.fromJson(json['khuVuc'] as Map<String, dynamic>),
      kho: json['kho'] == null
          ? null
          : StoreData.fromJson(json['kho'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AreaDetailDataToJson(AreaDetailData instance) =>
    <String, dynamic>{
      'khuVuc': instance.khuVuc,
      'kho': instance.kho,
    };
