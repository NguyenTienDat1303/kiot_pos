import 'package:json_annotation/json_annotation.dart';

part 'overview_response.g.dart';

@JsonSerializable()
class OverviewResponse {
  OverviewResponse({
    this.banDangDung,
    this.tongSoBan,
    this.huyTra,
    this.giaTriHuyTra,
    this.doanhThu,
    this.chart
  });
  int? banDangDung;
  int? tongSoBan;
  String? huyTra;
  int? giaTriHuyTra;
  int? doanhThu;
  List<OverviewChartData>? chart;

  factory OverviewResponse.fromJson(Map<String, dynamic> json) =>
      _$OverviewResponseFromJson(json);

  Map<String, dynamic> toJson() => _$OverviewResponseToJson(this);
}

@JsonSerializable()
class OverviewChartData {
  OverviewChartData({
    this.label,
    this.doanhThu,
  });

  String? label;
  String? doanhThu;

  factory OverviewChartData.fromJson(Map<String, dynamic> json) =>
      _$OverviewChartDataFromJson(json);

  Map<String, dynamic> toJson() => _$OverviewChartDataToJson(this);
}
