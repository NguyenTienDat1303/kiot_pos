import 'package:get/get.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kiot_pos/data/entities/entities.dart';

part 'load_table_response.g.dart';

@JsonSerializable()
class LoadTableResponse {
  LoadTableResponse({
    this.donHang,
    this.sanPhams,
    this.khuyenMai,
    this.tenkhach,
    this.nguoiThu,
    this.nhanVien,
    this.tongChietKhau,
    this.tongThanhToan,
    this.tienVAT,
    this.tienNhanVien,
    this.tienBan,
    this.tongSoLuong,
    this.tongTienHang,
  });
  OrderDetailData? donHang;
  List<ODProductData>? sanPhams;
  List<ODProductData>? khuyenMai;
  ODCustomerData? tenkhach;
  ODPeopleReceveiData? nguoiThu;
  List<ODStaffData>? nhanVien;
  double? tongChietKhau;
  double? tongThanhToan;
  double? tienVAT;
  double? tienNhanVien;
  double? tienBan;
  int? tongSoLuong;
  double? tongTienHang;

  factory LoadTableResponse.fromJson(Map<String, dynamic> json) =>
      _$LoadTableResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoadTableResponseToJson(this);

  ODProductData? findProduct(int id, {String? version, int? type}) {
    var products = this.sanPhams;
    // if (type == OrderProductSelectType.normal) {
    //   products = products!.where((e) => int.parse(e.price!) != 0).toList();
    // }
    // if (type == OrderProductSelectType.gift) {
    //   products = products!.where((e) => int.parse(e.price!) == 0).toList();
    // }

    print("OrderProductSelectType ${type}");
    if (version == null) {
      return products!.firstWhereOrNull((e) => e.sanPham!.id! == id);
    } else {
      return products!.firstWhereOrNull(
          (e) => e.sanPham!.id! == id && e.version == version);
    }
  }

  // double get productListTotalPrice => sanPhams!.fold(
  //     0,
  //     (previousValue, e) =>
  //         previousValue + (e.quantity! * (e.sauChietKhau ?? 0)));
  // double get productListTotalQuantity =>
  //     sanPhams!.fold(0, (previousValue, e) => previousValue + e.quantity!);
}
