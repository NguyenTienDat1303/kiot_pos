// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'staff_detail_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StaffDetailResponse _$StaffDetailResponseFromJson(Map<String, dynamic> json) =>
    StaffDetailResponse(
      nhanVien: json['nhanVien'] == null
          ? null
          : StaffResponse.fromJson(json['nhanVien'] as Map<String, dynamic>),
      tongDoanhSo: json['tongDoanhSo'] as int?,
    );

Map<String, dynamic> _$StaffDetailResponseToJson(
        StaffDetailResponse instance) =>
    <String, dynamic>{
      'nhanVien': instance.nhanVien,
      'tongDoanhSo': instance.tongDoanhSo,
    };

StaffResponse _$StaffResponseFromJson(Map<String, dynamic> json) =>
    StaffResponse(
      ID: json['ID'] as int?,
      employeeName: json['employee_name'] as String?,
      employeeCode: json['employee_code'] as String?,
      employeePrice: json['employee_price'] as int?,
      employeePhone: json['employee_phone'] as String?,
    );

Map<String, dynamic> _$StaffResponseToJson(StaffResponse instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'employee_name': instance.employeeName,
      'employee_code': instance.employeeCode,
      'employee_phone': instance.employeePhone,
      'employee_price': instance.employeePrice,
    };
