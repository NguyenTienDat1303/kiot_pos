import 'package:json_annotation/json_annotation.dart';
import 'package:kiot_pos/data/data.dart';

part 'staff_detail_response.g.dart';

@JsonSerializable()
class StaffDetailResponse {
  StaffDetailResponse({
    this.nhanVien,
    this.tongDoanhSo,
  });

  StaffResponse? nhanVien;
  int? tongDoanhSo;

  factory StaffDetailResponse.fromJson(Map<String, dynamic> json) =>
      _$StaffDetailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$StaffDetailResponseToJson(this);
  @override
  String toString() {
    return 'StaffDetailResponse{nhanVien: $nhanVien, tongDoanhSo: $tongDoanhSo}';
  }
}

@JsonSerializable()
class StaffResponse {
  StaffResponse(
      {this.ID,
      this.employeeName,
      this.employeeCode,
      this.employeePrice,
      this.employeePhone});

  int? ID;
  @JsonKey(name: 'employee_name')
  String? employeeName;
  @JsonKey(name: 'employee_code')
  String? employeeCode;
  @JsonKey(name: 'employee_phone')
  String? employeePhone;
  @JsonKey(name: 'employee_price')
  int? employeePrice;

  factory StaffResponse.fromJson(Map<String, dynamic> json) =>
      _$StaffResponseFromJson(json);

  Map<String, dynamic> toJson() => _$StaffResponseToJson(this);
  @override
  String toString() {
    return 'DataLoginResponse{employee_name: $employeeName, employeePhone: $employeePhone}';
  }

  ODStaffData toODStaffData() {
    var result = ODStaffData(
      nhanVien: ODStaffDetail(id: this.ID, employee_name: this.employeeName),
      price: this.employeePrice!.toDouble(),
    );
    return result;
  }
}
