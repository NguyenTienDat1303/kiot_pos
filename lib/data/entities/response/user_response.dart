import 'package:json_annotation/json_annotation.dart';

part 'user_response.g.dart';

@JsonSerializable()
class UserResponse {
  UserInfo? userInfo;
  String? group;
  UserResponse({required this.userInfo, required this.group});

  factory UserResponse.fromJson(Map<String, dynamic> json) =>
      _$UserResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserResponseToJson(this);
}

@JsonSerializable()
class UserInfo {
  String? username;
   @JsonKey(name: 'display_name')
  String? displayName;
  String? email;
   @JsonKey(name: 'group_id')
  int? groupId;
  int? status;

  UserInfo(
      this.username, this.displayName, this.email, this.groupId, this.status);

  factory UserInfo.fromJson(Map<String, dynamic> json) =>
      _$UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}
