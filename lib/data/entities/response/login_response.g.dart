// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) =>
    LoginResponse(
      user: json['user'] == null
          ? null
          : LoginUser.fromJson(json['user'] as Map<String, dynamic>),
      kho: (json['kho'] as List<dynamic>?)
          ?.map((e) => UserStore.fromJson(e as Map<String, dynamic>))
          .toList(),
      options: json['options'] as String?,
    );

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'user': instance.user,
      'kho': instance.kho,
      'options': instance.options,
    };

UserStore _$UserStoreFromJson(Map<String, dynamic> json) => UserStore(
      id: json['ID'] as int?,
      name: json['name'] as String?,
    );

Map<String, dynamic> _$UserStoreToJson(UserStore instance) => <String, dynamic>{
      'ID': instance.id,
      'name': instance.name,
    };

LoginUser _$LoginUserFromJson(Map<String, dynamic> json) => LoginUser(
      username: json['username'] as String?,
      groupId: json['group_id'] as int?,
      email: json['email'] as String?,
      passwordHash: json['password_hash'] as String?,
      authKey: json['auth_key'] as String?,
      status: json['status'] as int?,
      id: json['id'] as int?,
      storePermission: json['store_permission'] as String?,
      storeId: json['store_id'] as int?,
    );

Map<String, dynamic> _$LoginUserToJson(LoginUser instance) => <String, dynamic>{
      'username': instance.username,
      'group_id': instance.groupId,
      'email': instance.email,
      'password_hash': instance.passwordHash,
      'auth_key': instance.authKey,
      'status': instance.status,
      'id': instance.id,
      'store_permission': instance.storePermission,
      'store_id': instance.storeId,
    };
