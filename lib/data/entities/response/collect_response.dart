import 'package:json_annotation/json_annotation.dart';

part 'collect_response.g.dart';

@JsonSerializable()
class CollectResponse {
  CollectResponse(
      {this.phieuThu, this.khachHang, this.tenNguoiThu, this.phieuXuat});

  Collect? phieuThu;
  String? tenNguoiThu;
  String? khachHang;
  Bill? phieuXuat;

  factory CollectResponse.fromJson(Map<String, dynamic> json) =>
      _$CollectResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CollectResponseToJson(this);
}

@JsonSerializable()
class Collect {
  Collect({
    this.id,
    this.orderId,
    this.customerId,
    this.receiptCode,
    this.receiptImage,
    this.typeId,
    this.storeId,
    this.receiptDate,
    this.notes,
    this.receiptMethod,
    this.totalMoney,
    this.receiptFor,
    this.deleted,
    this.created,
    this.updated,
    this.userInit,
    this.userUpd,
    this.active,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'order_id')
  int? orderId;
  @JsonKey(name: 'customer_id')
  int? customerId;
  @JsonKey(name: 'receipt_code')
  String? receiptCode;
  @JsonKey(name: 'receipt_image')
  String? receiptImage;
  @JsonKey(name: 'type_id')
  int? typeId;
  @JsonKey(name: 'store_id')
  int? storeId;
  @JsonKey(name: 'receipt_date')
  String? receiptDate;

  String? notes;
  @JsonKey(name: 'receipt_method')
  int? receiptMethod;
  @JsonKey(name: 'total_money')
  int? totalMoney;
  @JsonKey(name: 'receipt_for')
  int? receiptFor;

  int? deleted;
  String? created;
  String? updated;
  @JsonKey(name: 'user_init')
  int? userInit;
  @JsonKey(name: 'user_upd')
  int? userUpd;
  int? active;

  factory Collect.fromJson(Map<String, dynamic> json) =>
      _$CollectFromJson(json);

  Map<String, dynamic> toJson() => _$CollectToJson(this);
}

@JsonSerializable()
class Bill {
  String? output_code;
  Bill({
    this.output_code,
  });

  factory Bill.fromJson(Map<String, dynamic> json) =>
      _$BillFromJson(json);

  Map<String, dynamic> toJson() => _$BillToJson(this);
}
