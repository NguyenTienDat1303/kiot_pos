import 'package:json_annotation/json_annotation.dart';
import 'package:kiot_pos/kiot_pos.dart';

part 'customer_response.g.dart';

@JsonSerializable()
class CustomerResponse {
  CustomerResponse(
      {this.id,
      this.customerName,
      this.customerCode,
      this.customerPhone,
      this.customerEmail,
      this.customerAddr});
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'customer_name')
  String? customerName;
  @JsonKey(name: 'customer_code')
  String? customerCode;
  @JsonKey(name: 'customer_phone')
  String? customerPhone;
  @JsonKey(name: 'customer_email')
  String? customerEmail;
  @JsonKey(name: 'customer_addr')
  String? customerAddr;

  factory CustomerResponse.fromJson(Map<String, dynamic> json) =>
      _$CustomerResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CustomerResponseToJson(this);

  ODCustomerData toODCustomerData() {
    var result = ODCustomerData(id: this.id, customerName: this.customerName);
    return result;
  }
}

@JsonSerializable()
class GroupCustomerResponse {
  GroupCustomerResponse({
    this.id,
    this.customerGroupName,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'customer_group_name')
  String? customerGroupName;

  factory GroupCustomerResponse.fromJson(Map<String, dynamic> json) =>
      _$GroupCustomerResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GroupCustomerResponseToJson(this);
}
