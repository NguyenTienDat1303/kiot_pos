// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'collect_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CollectResponse _$CollectResponseFromJson(Map<String, dynamic> json) =>
    CollectResponse(
      phieuThu: json['phieuThu'] == null
          ? null
          : Collect.fromJson(json['phieuThu'] as Map<String, dynamic>),
      khachHang: json['khachHang'] as String?,
      tenNguoiThu: json['tenNguoiThu'] as String?,
      phieuXuat: json['phieuXuat'] == null
          ? null
          : Bill.fromJson(json['phieuXuat'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CollectResponseToJson(CollectResponse instance) =>
    <String, dynamic>{
      'phieuThu': instance.phieuThu,
      'tenNguoiThu': instance.tenNguoiThu,
      'khachHang': instance.khachHang,
      'phieuXuat': instance.phieuXuat,
    };

Collect _$CollectFromJson(Map<String, dynamic> json) => Collect(
      id: json['ID'] as int?,
      orderId: json['order_id'] as int?,
      customerId: json['customer_id'] as int?,
      receiptCode: json['receipt_code'] as String?,
      receiptImage: json['receipt_image'] as String?,
      typeId: json['type_id'] as int?,
      storeId: json['store_id'] as int?,
      receiptDate: json['receipt_date'] as String?,
      notes: json['notes'] as String?,
      receiptMethod: json['receipt_method'] as int?,
      totalMoney: json['total_money'] as int?,
      receiptFor: json['receipt_for'] as int?,
      deleted: json['deleted'] as int?,
      created: json['created'] as String?,
      updated: json['updated'] as String?,
      userInit: json['user_init'] as int?,
      userUpd: json['user_upd'] as int?,
      active: json['active'] as int?,
    );

Map<String, dynamic> _$CollectToJson(Collect instance) => <String, dynamic>{
      'ID': instance.id,
      'order_id': instance.orderId,
      'customer_id': instance.customerId,
      'receipt_code': instance.receiptCode,
      'receipt_image': instance.receiptImage,
      'type_id': instance.typeId,
      'store_id': instance.storeId,
      'receipt_date': instance.receiptDate,
      'notes': instance.notes,
      'receipt_method': instance.receiptMethod,
      'total_money': instance.totalMoney,
      'receipt_for': instance.receiptFor,
      'deleted': instance.deleted,
      'created': instance.created,
      'updated': instance.updated,
      'user_init': instance.userInit,
      'user_upd': instance.userUpd,
      'active': instance.active,
    };

Bill _$BillFromJson(Map<String, dynamic> json) => Bill(
      output_code: json['output_code'] as String?,
    );

Map<String, dynamic> _$BillToJson(Bill instance) => <String, dynamic>{
      'output_code': instance.output_code,
    };
