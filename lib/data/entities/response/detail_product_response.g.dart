// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_product_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailProductResponse _$DetailProductResponseFromJson(
        Map<String, dynamic> json) =>
    DetailProductResponse(
      sanPham: json['sanPham'] == null
          ? null
          : DetailProductData.fromJson(json['sanPham'] as Map<String, dynamic>),
      danhMuc: json['danhMuc'] as String?,
      nhaSanXuat: json['nhaSanXuat'] as String?,
      quyDoi: (json['quyDoi'] as List<dynamic>?)
          ?.map(
              (e) => UnitConversionResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DetailProductResponseToJson(
        DetailProductResponse instance) =>
    <String, dynamic>{
      'sanPham': instance.sanPham,
      'danhMuc': instance.danhMuc,
      'nhaSanXuat': instance.nhaSanXuat,
      'quyDoi': instance.quyDoi,
    };

DetailProductData _$DetailProductDataFromJson(Map<String, dynamic> json) =>
    DetailProductData(
      id: json['ID'] as int?,
      prdImageUrl: json['prd_image_url'] as String?,
      prdName: json['prd_name'] as String?,
      prdSls: json['prd_sls'] as String?,
      prdCode: json['prd_code'] as String?,
      unitName: json['unit_name'] as String?,
      prdOriginPrice: json['prd_origin_price'] as int?,
      prdSellPrice: json['prd_sell_price'] as int?,
      prdSellPrice2: json['prd_sell_price2'] as int?,
      prdGroupId: json['prd_group_id'] as int?,
      prdManufactureId: json['prd_manufacture_id'] as int?,
      prdMin: json['prd_min'] as String?,
      prdMax: json['prd_max'] as String?,
      prdEditPrice: json['prd_edit_price'] as int?,
      prdAllownegative: json['prd_allownegative'] as int?,
      prd_add: json['prd_add'] as int?,
      prd_material: json['prd_material'] as int?,
      prd_point: json['prd_point'] as int?,
      prd_print: json['prd_print'] as int?,
    );

Map<String, dynamic> _$DetailProductDataToJson(DetailProductData instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'prd_image_url': instance.prdImageUrl,
      'prd_name': instance.prdName,
      'prd_sls': instance.prdSls,
      'prd_code': instance.prdCode,
      'unit_name': instance.unitName,
      'prd_origin_price': instance.prdOriginPrice,
      'prd_sell_price': instance.prdSellPrice,
      'prd_sell_price2': instance.prdSellPrice2,
      'prd_group_id': instance.prdGroupId,
      'prd_manufacture_id': instance.prdManufactureId,
      'prd_min': instance.prdMin,
      'prd_max': instance.prdMax,
      'prd_edit_price': instance.prdEditPrice,
      'prd_allownegative': instance.prdAllownegative,
      'prd_material': instance.prd_material,
      'prd_add': instance.prd_add,
      'prd_point': instance.prd_point,
      'prd_print': instance.prd_print,
    };
