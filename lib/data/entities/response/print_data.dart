import 'package:json_annotation/json_annotation.dart';

part 'print_data.g.dart';

@JsonSerializable()
class PrintData {
  PrintData({
    this.id,
    this.type,
    this.name,
    this.content,
  });

  int? id;
  int? type;
  String? name;
  String? content;

  factory PrintData.fromJson(Map<String, dynamic> json) =>
      _$PrintDataFromJson(json);

  Map<String, dynamic> toJson() => _$PrintDataToJson(this);
}
