// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'permission_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PermissionData _$PermissionDataFromJson(Map<String, dynamic> json) =>
    PermissionData(
      json['id'] as int,
      json['permission_url'] as String,
      json['permission_name'] as String?,
    );

Map<String, dynamic> _$PermissionDataToJson(PermissionData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'permission_url': instance.permissionUrl,
      'permission_name': instance.permissionName,
    };
