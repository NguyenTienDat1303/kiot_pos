import 'package:json_annotation/json_annotation.dart';

part 'unit_response.g.dart';

@JsonSerializable()
class ProducerResponse {
  ProducerResponse({
    this.id,
    this.prdManufName,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'prd_manuf_name')
  String? prdManufName;

  factory ProducerResponse.fromJson(Map<String, dynamic> json) =>
      _$ProducerResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProducerResponseToJson(this);
}

@JsonSerializable()
class UnitResponse {
  UnitResponse({
    this.id,
    this.prdUnitName,
  });
@JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'prd_unit_name')
  String? prdUnitName;

  factory UnitResponse.fromJson(Map<String, dynamic> json) =>
      _$UnitResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UnitResponseToJson(this);
}


@JsonSerializable()
class UsersResponse {
  UsersResponse({
    this.id,
    this.displayName,
  });
  int? id;
  @JsonKey(name: 'display_name')
  String? displayName;

  factory UsersResponse.fromJson(Map<String, dynamic> json) =>
      _$UsersResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UsersResponseToJson(this);
}