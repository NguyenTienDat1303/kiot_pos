import 'package:json_annotation/json_annotation.dart';

part 'permission_data.g.dart';

@JsonSerializable()
class PermissionData {
  PermissionData(
    this.id,
    this.permissionUrl,
    this.permissionName,
  );

  int id;
  @JsonKey(name: 'permission_url')
  String permissionUrl;
  @JsonKey(name: 'permission_name')
  String? permissionName;

  factory PermissionData.fromJson(Map<String, dynamic> json) =>
      _$PermissionDataFromJson(json);

  Map<String, dynamic> toJson() => _$PermissionDataToJson(this);
}
