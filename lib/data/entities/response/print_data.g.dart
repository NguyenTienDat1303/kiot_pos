// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'print_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrintData _$PrintDataFromJson(Map<String, dynamic> json) => PrintData(
      id: json['id'] as int?,
      type: json['type'] as int?,
      name: json['name'] as String?,
      content: json['content'] as String?,
    );

Map<String, dynamic> _$PrintDataToJson(PrintData instance) => <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'name': instance.name,
      'content': instance.content,
    };
