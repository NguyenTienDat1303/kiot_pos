import 'package:json_annotation/json_annotation.dart';

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponse {
  LoginResponse({
    this.user,
    this.kho,
    this.options,
  });

  LoginUser? user;
  List<UserStore>? kho;
  String? options;

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);
}

@JsonSerializable()
class UserStore {
  UserStore({
    this.id,
    this.name,
  });
  @JsonKey(name: 'ID')
  int? id;
  String? name;

  factory UserStore.fromJson(Map<String, dynamic> json) =>
      _$UserStoreFromJson(json);

  Map<String, dynamic> toJson() => _$UserStoreToJson(this);
}

@JsonSerializable()
class LoginUser {
  LoginUser({
    this.username,
    this.groupId,
    this.email,
    this.passwordHash,
    this.authKey,
    this.status,
    this.id,
    this.storePermission,
    this.storeId,
  });

  String? username;
  @JsonKey(name: 'group_id')
  int? groupId;
  String? email;
  @JsonKey(name: 'password_hash')
  String? passwordHash;
  @JsonKey(name: 'auth_key')
  String? authKey;
  int? status;
  int? id;
  @JsonKey(name: 'store_permission')
  String? storePermission;
  @JsonKey(name: 'store_id')
  int? storeId;

  factory LoginUser.fromJson(Map<String, dynamic> json) =>
      _$LoginUserFromJson(json);

  Map<String, dynamic> toJson() => _$LoginUserToJson(this);
}
