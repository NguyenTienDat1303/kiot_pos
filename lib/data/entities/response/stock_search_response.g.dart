// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stock_search_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StockSearchResponse _$StockSearchResponseFromJson(Map<String, dynamic> json) =>
    StockSearchResponse(
      item: json['item'] == null
          ? null
          : ItemKiemKe.fromJson(json['item'] as Map<String, dynamic>),
      nguoiKiem: json['nguoiKiem'] as String?,
    );

Map<String, dynamic> _$StockSearchResponseToJson(
        StockSearchResponse instance) =>
    <String, dynamic>{
      'item': instance.item,
      'nguoiKiem': instance.nguoiKiem,
    };

ItemKiemKe _$ItemKiemKeFromJson(Map<String, dynamic> json) => ItemKiemKe(
      ID: json['ID'] as int?,
      adjustCode: json['adjust_code'] as String?,
      storeId: json['store_id'] as int?,
      notes: json['notes'] as String?,
      totalDifferent: json['total_different'] as String?,
      totalQuantity: json['total_quantity'] as String?,
      detailAdjust: json['detail_adjust'] as String?,
      adjustDate: json['adjust_date'] as String?,
      adjustStatus: json['adjust_status'] as int?,
      deleted: json['deleted'] as int?,
      created: json['created'] as String?,
      updated: json['updated'] as String?,
      userInit: json['user_init'] as int?,
      userUpd: json['user_upd'] as int?,
      finishDate: json['finish_date'] as String?,
      updateData: json['update_data'] as int?,
      active: json['active'] as int?,
    );

Map<String, dynamic> _$ItemKiemKeToJson(ItemKiemKe instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'adjust_code': instance.adjustCode,
      'store_id': instance.storeId,
      'notes': instance.notes,
      'total_different': instance.totalDifferent,
      'total_quantity': instance.totalQuantity,
      'detail_adjust': instance.detailAdjust,
      'adjust_date': instance.adjustDate,
      'adjust_status': instance.adjustStatus,
      'deleted': instance.deleted,
      'created': instance.created,
      'updated': instance.updated,
      'user_init': instance.userInit,
      'user_upd': instance.userUpd,
      'finish_date': instance.finishDate,
      'update_data': instance.updateData,
      'active': instance.active,
    };
