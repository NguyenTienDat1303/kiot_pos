// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pay_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PayResponse _$PayResponseFromJson(Map<String, dynamic> json) => PayResponse(
      phieuChi: json['phieuChi'] == null
          ? null
          : Pay.fromJson(json['phieuChi'] as Map<String, dynamic>),
      tenNguoiChi: json['tenNguoiChi'] as String?,
      nhaCungCap: json['nhaCungCap'] as String?,
      phieuNhap: json['phieuNhap'] == null
          ? null
          : PayIn.fromJson(json['phieuNhap'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PayResponseToJson(PayResponse instance) =>
    <String, dynamic>{
      'phieuChi': instance.phieuChi,
      'tenNguoiChi': instance.tenNguoiChi,
      'phieuNhap': instance.phieuNhap,
      'nhaCungCap': instance.nhaCungCap,
    };

Pay _$PayFromJson(Map<String, dynamic> json) => Pay(
      id: json['ID'] as int?,
      inputId: json['input_id'] as int?,
      customerId: json['customer_id'] as int?,
      paymentCode: json['payment_code'] as String?,
      paymentImage: json['payment_image'] as String?,
      typeId: json['type_id'] as int?,
      storeId: json['store_id'] as int?,
      paymentDate: json['payment_date'] as String?,
      notes: json['notes'] as String?,
      paymentMethod: json['payment_method'] as int?,
      paymentFor: json['payment_for'] as int?,
      totalMoney: json['total_money'] as int?,
      deleted: json['deleted'] as int?,
      created: json['created'] as String?,
      updated: json['updated'] as String?,
      userInit: json['user_init'] as int?,
      userUpd: json['user_upd'] as int?,
      active: json['active'] as int?,
    );

Map<String, dynamic> _$PayToJson(Pay instance) => <String, dynamic>{
      'ID': instance.id,
      'input_id': instance.inputId,
      'customer_id': instance.customerId,
      'payment_code': instance.paymentCode,
      'payment_image': instance.paymentImage,
      'type_id': instance.typeId,
      'store_id': instance.storeId,
      'payment_date': instance.paymentDate,
      'notes': instance.notes,
      'payment_method': instance.paymentMethod,
      'payment_for': instance.paymentFor,
      'total_money': instance.totalMoney,
      'deleted': instance.deleted,
      'created': instance.created,
      'updated': instance.updated,
      'user_init': instance.userInit,
      'user_upd': instance.userUpd,
      'active': instance.active,
    };

PayIn _$PayInFromJson(Map<String, dynamic> json) => PayIn(
      input_code: json['input_code'] as String?,
    );

Map<String, dynamic> _$PayInToJson(PayIn instance) => <String, dynamic>{
      'input_code': instance.input_code,
    };
