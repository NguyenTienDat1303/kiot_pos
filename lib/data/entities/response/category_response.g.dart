// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryData _$CategoryDataFromJson(Map<String, dynamic> json) => CategoryData(
      danhMuc: json['danhMuc'] == null
          ? null
          : CategoryDetail.fromJson(json['danhMuc'] as Map<String, dynamic>),
      soLuongSanPham: json['soLuongSanPham'] as String?,
      cacSanPham: (json['cacSanPham'] as List<dynamic>?)
          ?.map((e) => ProductData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CategoryDataToJson(CategoryData instance) =>
    <String, dynamic>{
      'danhMuc': instance.danhMuc,
      'soLuongSanPham': instance.soLuongSanPham,
      'cacSanPham': instance.cacSanPham,
    };

CategoryDetail _$CategoryDetailFromJson(Map<String, dynamic> json) =>
    CategoryDetail(
      id: json['ID'] as int?,
      prdGroupName: json['prd_group_name'] as String?,
    );

Map<String, dynamic> _$CategoryDetailToJson(CategoryDetail instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'prd_group_name': instance.prdGroupName,
    };
