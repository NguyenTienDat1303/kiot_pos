// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'overview_collect_pay_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OverviewCollectPayResponse _$OverviewCollectPayResponseFromJson(
        Map<String, dynamic> json) =>
    OverviewCollectPayResponse(
      soPhieuThu: json['soPhieuThu'] as String?,
      tongTienThu: json['tongTienThu'] as String?,
      soPhieuChi: json['soPhieuChi'] as String?,
      tongTienChi: json['tongTienChi'] as String?,
      conLai: json['conLai'] as int?,
      tienMat: json['tienMat'] as String?,
      chuyenKhoan: json['chuyenKhoan'] as String?,
      the: json['the'] as String?,
    );

Map<String, dynamic> _$OverviewCollectPayResponseToJson(
        OverviewCollectPayResponse instance) =>
    <String, dynamic>{
      'soPhieuThu': instance.soPhieuThu,
      'tongTienThu': instance.tongTienThu,
      'soPhieuChi': instance.soPhieuChi,
      'tongTienChi': instance.tongTienChi,
      'conLai': instance.conLai,
      'tienMat': instance.tienMat,
      'chuyenKhoan': instance.chuyenKhoan,
      'the': instance.the,
    };
