import 'package:json_annotation/json_annotation.dart';

part 'unit_conversion_response.g.dart';

@JsonSerializable()

class UnitConversionResponse {
    UnitConversionResponse({
        this.prdCode,
        this.prdOriginPrice,
        this.prdSellPrice,
        this.prdSellPrice2,
        this.unitName,
        this.number,
        this.id
    });
    @JsonKey(name: 'ID')
    int? id;
@JsonKey(name: 'prd_code')
    String? prdCode;
    @JsonKey(name: 'prd_origin_price')
    int? prdOriginPrice;
    @JsonKey(name: 'prd_sell_price')
    int? prdSellPrice;
    @JsonKey(name: 'prd_sell_price2')
    int? prdSellPrice2;
    @JsonKey(name: 'unit_name')
    String? unitName;
    int? number;

    factory UnitConversionResponse.fromJson(Map<String, dynamic> json) =>
      _$UnitConversionResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UnitConversionResponseToJson(this);
}