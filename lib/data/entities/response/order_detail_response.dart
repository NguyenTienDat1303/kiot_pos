import 'package:equatable/equatable.dart';
import 'package:get/get.dart';
import 'package:json_annotation/json_annotation.dart';

import 'package:kiot_pos/kiot_pos.dart';

part 'order_detail_response.g.dart';

@JsonSerializable()
class OrderDetailResponse extends BaseResponse {
  OrderDetailResponse({
    this.donHang,
    this.sanPhams,
    this.khuyenMai,
    this.tenkhach,
    this.nguoiThu,
    this.nhanVien,
    this.tongChietKhau,
    this.sauChietKhau,
    this.tienVAT,
    this.tienNhanVien,
    this.tienBan,
    this.tongSoLuong,
    this.tongTienHang,
  });

  OrderDetailData? donHang;
  List<ODProductData>? sanPhams;
  List<ODProductData>? khuyenMai;
  ODCustomerData? tenkhach;
  ODPeopleReceveiData? nguoiThu;
  List<ODStaffData>? nhanVien;
  double? tongChietKhau;
  double? sauChietKhau;
  double? tienVAT;
  double? tienNhanVien;
  double? tienBan;
  double? tongSoLuong;
  double? tongTienHang;

  factory OrderDetailResponse.fromJson(Map<String, dynamic> json) =>
      _$OrderDetailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$OrderDetailResponseToJson(this);
  ODProductData? findProduct(int id, {String? version, int? type}) {
    var products = this.sanPhams;
    // if (type == OrderProductSelectType.normal) {
    //   products = products!.where((e) => int.parse(e.price!) != 0).toList();
    // }
    // if (type == OrderProductSelectType.gift) {
    //   products = products!.where((e) => int.parse(e.price!) == 0).toList();
    // }

    print("OrderProductSelectType ${type}");
    if (version == null) {
      return products!.firstWhereOrNull((e) => e.sanPham!.id! == id);
    } else {
      return products!.firstWhereOrNull(
          (e) => e.sanPham!.id! == id && e.version == version);
    }
  }

  double get productListTotalPrice => sanPhams!.fold(
      0,
      (previousValue, e) =>
          previousValue + (e.quantity! * (e.sauChietKhau ?? 0)));
  double get productListTotalQuantity =>
      sanPhams!.fold(0, (previousValue, e) => previousValue + e.quantity!);
}

@JsonSerializable()
class OrderDetailData {
  OrderDetailData({
    this.id,
    this.outputCode,
    // this.customerId,
    this.storeId,
    this.sellDate,
    this.fromDate,
    this.finishDate,
    this.notes,
    this.paymentMethod,
    this.totalPrice,
    this.totalOriginPrice,
    this.discountItem,
    this.coupon,
    this.vat,
    this.customerPay,
    this.tableId,
    this.totalMoney,
    this.totalQuantity,
    this.payDate,
    this.canReturn,
    this.orderStatus,
    this.totalEmployeePrice,
    this.totalTablePrice,
    // this.userInit,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'output_code')
  String? outputCode;
  // @JsonKey(name: 'customer_id')
  // int? customerId;
  @JsonKey(name: 'store_id')
  int? storeId;
  @JsonKey(name: 'sell_date')
  String? sellDate;
  @JsonKey(name: 'from_date')
  String? fromDate;
  @JsonKey(name: 'finish_date')
  String? finishDate;
  String? notes;
  @JsonKey(name: 'payment_method')
  int? paymentMethod;
  @JsonKey(name: 'total_price')
  int? totalPrice;
  @JsonKey(name: 'total_origin_price')
  int? totalOriginPrice;
  @JsonKey(name: 'discount_item')
  int? discountItem;
  @JsonKey(name: 'coupon')
  int? coupon;
  @JsonKey(name: 'vat')
  int? vat;
  @JsonKey(name: 'customer_pay')
  int? customerPay;
  @JsonKey(name: 'table_id')
  int? tableId;
  @JsonKey(name: 'total_money')
  int? totalMoney;
  @JsonKey(name: 'total_quantity')
  int? totalQuantity;
  @JsonKey(name: 'pay_date')
  String? payDate;
  @JsonKey(name: 'canreturn')
  int? canReturn;
  @JsonKey(name: 'order_status')
  int? orderStatus;
  @JsonKey(name: 'total_employee_price')
  int? totalEmployeePrice;
  @JsonKey(name: 'total_table_price')
  int? totalTablePrice;
  // @JsonKey(name: 'user_init')
  // int? userInit;

  factory OrderDetailData.fromJson(Map<String, dynamic> json) =>
      _$OrderDetailDataFromJson(json);

  Map<String, dynamic> toJson() => _$OrderDetailDataToJson(this);
}

@JsonSerializable()
class ODProductData {
  ODProductData(
      {this.sanPham,
      this.quantity,
      this.price,
      this.cc1,
      this.cc2,
      this.vat,
      this.discount,
      this.discountVnd,
      this.note,
      this.truocChietKhau,
      this.sauChietKhau,
      this.topping,
      // this.mayIn,
      this.quantitySelected});
  ODProductDetail? sanPham;
  int? quantitySelected;
  int? quantity;
  int? price;
  String? cc1;
  String? cc2;
  int? vat;
  int? discount;
  @JsonKey(name: 'discount_vnd')
  int? discountVnd;
  String? note;

  double? truocChietKhau;
  double? sauChietKhau;
  List<ODToppingData>? topping = [];
  // PrinterData? mayIn;
  @JsonKey(ignore: true)
  String version = StringUtil.generateRandomString(100);

  factory ODProductData.fromJson(Map<String, dynamic> json) =>
      _$ODProductDataFromJson(json);

  Map<String, dynamic> toJson() => _$ODProductDataToJson(this);

  ODToppingData? findTopping(
    int id,
  ) {
    return topping!.firstWhereOrNull((e) => e.topping!.id! == id);
  }

  ODProductData copyWith({
    int? quantity,
    int? price,
    String? cc1,
    String? cc2,
    int? vat,
    int? discount,
    int? discountVnd,
    String? note,
  }) {
    var productData = ODProductData(
      sanPham: ODProductDetail(
          id: sanPham!.id,
          prdCode: sanPham!.prdCode,
          prdName: sanPham!.prdName),
      quantity: quantity ?? this.quantity,
      price: price ?? this.price,
      cc1: cc1 ?? this.cc1,
      cc2: cc2 ?? this.cc2,
      vat: vat ?? this.vat,
      discount: discount ?? this.discount,
      discountVnd: discountVnd ?? this.discountVnd,
      truocChietKhau: truocChietKhau ?? 0,
      sauChietKhau: sauChietKhau ?? 0,
      note: note ?? this.note,
    );

    productData.version = version;

    List<ODToppingData> toppingListClone = [];
    if (this.topping != null) {
      this.topping!.forEach((e) {
        toppingListClone.add(e);
      });
      productData.topping = toppingListClone;
    }

    return productData;
  }

  double get totalUnitPrice => totalToppingSellPrice + price!;
  double get totalToppingSellPrice => topping!.fold(
      0,
      (previousValue, element) =>
          previousValue + element.quantityAdd! * element.priceAdd!);
  double get totalToppingQuantity => topping!.fold(
      0, (previousValue, element) => previousValue + element.quantityAdd!);

  // double get totalSellPrice => quantity! * (sauChietKhau ?? 0);

  double get quantityInitial => quantity!.toDouble();

  @override
  String toString() => 'ODProductData(quantitySelected: $quantitySelected)';
}

@JsonSerializable()
class ODProductDetail {
  ODProductDetail({
    this.prdCode,
    this.prdName,
    this.id,
  });

  @JsonKey(name: 'prd_code')
  String? prdCode;
  @JsonKey(name: 'prd_name')
  String? prdName;
  @JsonKey(name: 'ID')
  int? id;

  factory ODProductDetail.fromJson(Map<String, dynamic> json) =>
      _$ODProductDetailFromJson(json);

  Map<String, dynamic> toJson() => _$ODProductDetailToJson(this);
}

@JsonSerializable()
class ODToppingData {
  ODToppingData({
    this.topping,
    this.quantityAdd,
    this.priceAdd,
  });

  ODToppingDetail? topping;
  @JsonKey(name: 'quantity_add')
  int? quantityAdd;
  @JsonKey(name: 'price_add')
  int? priceAdd;

  factory ODToppingData.fromJson(Map<String, dynamic> json) =>
      _$ODToppingDataFromJson(json);

  Map<String, dynamic> toJson() => _$ODToppingDataToJson(this);
}

@JsonSerializable()
class ODToppingDetail {
  ODToppingDetail({
    this.prdCode,
    this.prdName,
    this.id,
  });

  @JsonKey(name: 'prd_code')
  String? prdCode;
  @JsonKey(name: 'prd_name')
  String? prdName;
  @JsonKey(name: 'ID')
  int? id;

  factory ODToppingDetail.fromJson(Map<String, dynamic> json) =>
      _$ODToppingDetailFromJson(json);

  Map<String, dynamic> toJson() => _$ODToppingDetailToJson(this);
}

@JsonSerializable()
class ODStaffData extends Equatable {
  ODStaffData({
    this.nhanVien,
    this.start,
    this.end,
    this.time,
    this.price,
    this.totalPrice,
  });
  ODStaffDetail? nhanVien;
  String? start;
  String? end;
  double? time;
  double? price;
  @JsonKey(name: 'total_price')
  double? totalPrice;

  factory ODStaffData.fromJson(Map<String, dynamic> json) =>
      _$ODStaffDataFromJson(json);

  Map<String, dynamic> toJson() => _$ODStaffDataToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props => [nhanVien!.id];
}

@JsonSerializable()
class ODStaffDetail {
  ODStaffDetail({
    this.id,
    this.employee_name,
  });

  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'employee_name')
  String? employee_name;

  factory ODStaffDetail.fromJson(Map<String, dynamic> json) =>
      _$ODStaffDetailFromJson(json);

  Map<String, dynamic> toJson() => _$ODStaffDetailToJson(this);
}

@JsonSerializable()
class ODCustomerData extends Equatable {
  ODCustomerData({
    this.id,
    this.customerName,
  });

  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'customer_name')
  String? customerName;

  factory ODCustomerData.fromJson(Map<String, dynamic> json) =>
      _$ODCustomerDataFromJson(json);

  Map<String, dynamic> toJson() => _$ODCustomerDataToJson(this);

  @override
  List<Object?> get props => [id];
}

@JsonSerializable()
class ODPeopleReceveiData {
  ODPeopleReceveiData({
    this.id,
    this.displayName,
  });

  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'display_name')
  String? displayName;

  factory ODPeopleReceveiData.fromJson(Map<String, dynamic> json) =>
      _$ODPeopleReceveiDataFromJson(json);

  Map<String, dynamic> toJson() => _$ODPeopleReceveiDataToJson(this);
}
