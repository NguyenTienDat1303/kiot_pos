// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'table_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TableData _$TableDataFromJson(Map<String, dynamic> json) => TableData(
      ban: json['ban'] == null
          ? null
          : TableDetail.fromJson(json['ban'] as Map<String, dynamic>),
      status: json['status'] as String?,
      donHangID: json['donHangID'] as int?,
      giaTien: json['giaTien'] as String?,
    );

Map<String, dynamic> _$TableDataToJson(TableData instance) => <String, dynamic>{
      'ban': instance.ban,
      'donHangID': instance.donHangID,
      'status': instance.status,
      'giaTien': instance.giaTien,
    };

TableDetail _$TableDetailFromJson(Map<String, dynamic> json) => TableDetail(
      id: json['ID'] as int?,
      tableName: json['table_name'] as String?,
      tableStatus: json['table_status'] as int?,
      from1: json['from1'] as int?,
      to1: json['to1'] as int?,
      tablePrice1: json['table_price1'] as int?,
      from2: json['from2'] as int?,
      to2: json['to2'] as int?,
      tablePrice2: json['table_price2'] as int?,
      from3: json['from3'] as int?,
      to3: json['to3'] as int?,
      tablePrice3: json['table_price3'] as int?,
      startDate: json['start_date'] as String?,
    );

Map<String, dynamic> _$TableDetailToJson(TableDetail instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'table_name': instance.tableName,
      'table_status': instance.tableStatus,
      'from1': instance.from1,
      'to1': instance.to1,
      'table_price1': instance.tablePrice1,
      'from2': instance.from2,
      'to2': instance.to2,
      'table_price2': instance.tablePrice2,
      'from3': instance.from3,
      'to3': instance.to3,
      'table_price3': instance.tablePrice3,
      'start_date': instance.startDate,
    };
