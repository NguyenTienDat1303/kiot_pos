import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'store_data.g.dart';

@JsonSerializable()
class StoreData extends Equatable {
  StoreData({
    this.id,
    this.storeName,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'store_name')
  String? storeName;
  factory StoreData.fromJson(Map<String, dynamic> json) =>
      _$StoreDataFromJson(json);

  Map<String, dynamic> toJson() => _$StoreDataToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props => [id, storeName];
}
