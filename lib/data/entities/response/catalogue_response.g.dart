// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'catalogue_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CatalogueResponse _$CatalogueResponseFromJson(Map<String, dynamic> json) =>
    CatalogueResponse(
      danhMuc: json['danhMuc'] == null
          ? null
          : CatalogueData.fromJson(json['danhMuc'] as Map<String, dynamic>),
      soLuongSanPham: json['soLuongSanPham'] as String?,
      cacSanPham: (json['cacSanPham'] as List<dynamic>?)
          ?.map((e) => Products.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CatalogueResponseToJson(CatalogueResponse instance) =>
    <String, dynamic>{
      'danhMuc': instance.danhMuc,
      'soLuongSanPham': instance.soLuongSanPham,
      'cacSanPham': instance.cacSanPham,
    };

Products _$ProductsFromJson(Map<String, dynamic> json) => Products(
      prdCode: json['prd_code'] as String?,
      prdName: json['prd_name'] as String?,
      prdSellPrice: json['prd_sell_price'] as int?,
    );

Map<String, dynamic> _$ProductsToJson(Products instance) => <String, dynamic>{
      'prd_code': instance.prdCode,
      'prd_name': instance.prdName,
      'prd_sell_price': instance.prdSellPrice,
    };

CatalogueData _$CatalogueDataFromJson(Map<String, dynamic> json) =>
    CatalogueData(
      ID: json['ID'] as int?,
      prdGroupName: json['prd_group_name'] as String?,
    );

Map<String, dynamic> _$CatalogueDataToJson(CatalogueData instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'prd_group_name': instance.prdGroupName,
    };
