import 'package:json_annotation/json_annotation.dart';

part 'order_response.g.dart';

@JsonSerializable()
class OrderResponse {
  OrderResponse({
    this.id,
    this.outputCode,
    this.customerId,
    this.storeId,
    this.inputId,
    this.orderCancel,
    this.sellDate,
    this.notes,
    this.paymentMethod,
    this.totalPrice,
    this.totalOriginPrice,
    this.tableHour,
    this.employeeHour,
    this.discountItem,
    this.coupon,
    this.customerPay,
    this.tableId,
    this.vat,
    this.totalMoney,
    this.isFast,
    this.totalQuantity,
    this.lack,
    this.payDay,
    this.payDate,
    this.fromDate,
    this.toDate,
    this.detailOrder,
    this.detailEmployee,
    this.orderStatus,
    this.deleted,
    this.created,
    this.updated,
    this.userInit,
    this.userUpd,
    this.saleId,
    this.saleId2,
    this.saleId3,
    this.canreturn,
    this.coinIncrease,
    this.coinDecrease,
    this.coinMoney,
    this.detailQuantityOrder,
    this.totalM,
    this.totalOriginPrice3,
    this.totalOriginPrice2,
    this.payType,
    this.finishDate,
    this.updateData,
    this.active,
  });
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'output_code')
  String? outputCode;
  @JsonKey(name: 'customer_id')
  int? customerId;
  @JsonKey(name: 'store_id')
  int? storeId;
  @JsonKey(name: 'input_id')
  int? inputId;
  @JsonKey(name: 'order_cancel')
  int? orderCancel;
  @JsonKey(name: 'sell_date')
  String? sellDate;

  String? notes;
  @JsonKey(name: 'payment_method')
  int? paymentMethod;
  @JsonKey(name: 'total_price')
  int? totalPrice;
  @JsonKey(name: 'total_origin_price')
  int? totalOriginPrice;
  @JsonKey(name: 'table_hour')
  int? tableHour;
  @JsonKey(name: 'employee_hour')
  int? employeeHour;
  @JsonKey(name: 'discount_item')
  int? discountItem;

  int? coupon;
  @JsonKey(name: 'customer_pay')
  int? customerPay;
  @JsonKey(name: 'table_id')
  int? tableId;
  int? vat;
  @JsonKey(name: 'total_money')
  int? totalMoney;
  @JsonKey(name: 'is_fast')
  int? isFast;
  @JsonKey(name: 'total_quantity')
  String? totalQuantity;
  int? lack;
  @JsonKey(name: 'pay_day')
  int? payDay;
  @JsonKey(name: 'pay_date')
  String? payDate;
  @JsonKey(name: 'from_date')
  String? fromDate;
  @JsonKey(name: 'to_date')
  String? toDate;
  @JsonKey(name: 'detail_order')
  String? detailOrder;
  @JsonKey(name: 'detail_employee')
  String? detailEmployee;
  @JsonKey(name: 'order_status')
  int? orderStatus;
  int? deleted;
  String? created;
  String? updated;
  @JsonKey(name: 'user_init')
  int? userInit;
  @JsonKey(name: 'user_upd')
  int? userUpd;
  @JsonKey(name: 'sale_id')
  int? saleId;
  @JsonKey(name: 'sale_id2')
  int? saleId2;
  @JsonKey(name: 'sale_id3')
  int? saleId3;
  int? canreturn;
  @JsonKey(name: 'coin_increase')
  int? coinIncrease;
  @JsonKey(name: 'coin_decrease')
  int? coinDecrease;
  @JsonKey(name: 'coin_money')
  int? coinMoney;
  @JsonKey(name: 'detail_quantity_order')
  String? detailQuantityOrder;
  @JsonKey(name: 'total_m')
  String? totalM;
  @JsonKey(name: 'total_origin_price_3')
  int? totalOriginPrice3;
  @JsonKey(name: 'total_origin_price_2')
  int? totalOriginPrice2;
  @JsonKey(name: 'pay_type')
  int? payType;
  @JsonKey(name: 'finish_date')
  String? finishDate;
  @JsonKey(name: 'update_data')
  int? updateData;

  int? active;

  factory OrderResponse.fromJson(Map<String, dynamic> json) =>
      _$OrderResponseFromJson(json);

  Map<String, dynamic> toJson() => _$OrderResponseToJson(this);
}
