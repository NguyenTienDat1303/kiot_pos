import 'package:json_annotation/json_annotation.dart';
import 'package:kiot_pos/data/data.dart';

part 'table_list_response.g.dart';

@JsonSerializable()
class TableListResponse {
  TableListResponse(this.tamTinh, this.danhSach);

  double tamTinh;
  List<TableData> danhSach;


  factory TableListResponse.fromJson(Map<String, dynamic> json) =>
      _$TableListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TableListResponseToJson(this);
}