// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unit_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProducerResponse _$ProducerResponseFromJson(Map<String, dynamic> json) =>
    ProducerResponse(
      id: json['ID'] as int?,
      prdManufName: json['prd_manuf_name'] as String?,
    );

Map<String, dynamic> _$ProducerResponseToJson(ProducerResponse instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'prd_manuf_name': instance.prdManufName,
    };

UnitResponse _$UnitResponseFromJson(Map<String, dynamic> json) => UnitResponse(
      id: json['ID'] as int?,
      prdUnitName: json['prd_unit_name'] as String?,
    );

Map<String, dynamic> _$UnitResponseToJson(UnitResponse instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'prd_unit_name': instance.prdUnitName,
    };

UsersResponse _$UsersResponseFromJson(Map<String, dynamic> json) =>
    UsersResponse(
      id: json['id'] as int?,
      displayName: json['display_name'] as String?,
    );

Map<String, dynamic> _$UsersResponseToJson(UsersResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'display_name': instance.displayName,
    };
