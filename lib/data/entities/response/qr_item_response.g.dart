// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'qr_item_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemQrResponse _$ItemQrResponseFromJson(Map<String, dynamic> json) =>
    ItemQrResponse(
      id: json['ID'] as int?,
      prdCode: json['prd_code'] as String?,
      prdName: json['prd_name'] as String?,
      prdSellPrice: json['prd_sell_price'] as int?,
    );

Map<String, dynamic> _$ItemQrResponseToJson(ItemQrResponse instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'prd_code': instance.prdCode,
      'prd_name': instance.prdName,
      'prd_sell_price': instance.prdSellPrice,
    };
