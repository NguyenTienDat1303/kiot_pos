// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'load_table_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoadTableResponse _$LoadTableResponseFromJson(Map<String, dynamic> json) =>
    LoadTableResponse(
      donHang: json['donHang'] == null
          ? null
          : OrderDetailData.fromJson(json['donHang'] as Map<String, dynamic>),
      sanPhams: (json['sanPhams'] as List<dynamic>?)
          ?.map((e) => ODProductData.fromJson(e as Map<String, dynamic>))
          .toList(),
      khuyenMai: (json['khuyenMai'] as List<dynamic>?)
          ?.map((e) => ODProductData.fromJson(e as Map<String, dynamic>))
          .toList(),
      tenkhach: json['tenkhach'] == null
          ? null
          : ODCustomerData.fromJson(json['tenkhach'] as Map<String, dynamic>),
      nguoiThu: json['nguoiThu'] == null
          ? null
          : ODPeopleReceveiData.fromJson(
              json['nguoiThu'] as Map<String, dynamic>),
      nhanVien: (json['nhanVien'] as List<dynamic>?)
          ?.map((e) => ODStaffData.fromJson(e as Map<String, dynamic>))
          .toList(),
      tongChietKhau: (json['tongChietKhau'] as num?)?.toDouble(),
      tongThanhToan: (json['tongThanhToan'] as num?)?.toDouble(),
      tienVAT: (json['tienVAT'] as num?)?.toDouble(),
      tienNhanVien: (json['tienNhanVien'] as num?)?.toDouble(),
      tienBan: (json['tienBan'] as num?)?.toDouble(),
      tongSoLuong: json['tongSoLuong'] as int?,
      tongTienHang: (json['tongTienHang'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$LoadTableResponseToJson(LoadTableResponse instance) =>
    <String, dynamic>{
      'donHang': instance.donHang,
      'sanPhams': instance.sanPhams,
      'khuyenMai': instance.khuyenMai,
      'tenkhach': instance.tenkhach,
      'nguoiThu': instance.nguoiThu,
      'nhanVien': instance.nhanVien,
      'tongChietKhau': instance.tongChietKhau,
      'tongThanhToan': instance.tongThanhToan,
      'tienVAT': instance.tienVAT,
      'tienNhanVien': instance.tienNhanVien,
      'tienBan': instance.tienBan,
      'tongSoLuong': instance.tongSoLuong,
      'tongTienHang': instance.tongTienHang,
    };
