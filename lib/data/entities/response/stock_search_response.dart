import 'package:json_annotation/json_annotation.dart';

part 'stock_search_response.g.dart';

@JsonSerializable()
class StockSearchResponse {
  StockSearchResponse({
    this.item,
    this.nguoiKiem,
  });

  ItemKiemKe? item;
  String? nguoiKiem;

  factory StockSearchResponse.fromJson(Map<String, dynamic> json) =>
      _$StockSearchResponseFromJson(json);

  Map<String, dynamic> toJson() => _$StockSearchResponseToJson(this);
}

@JsonSerializable()
class ItemKiemKe {
  ItemKiemKe({
    this.ID,
    this.adjustCode,
    this.storeId,
    this.notes,
    this.totalDifferent,
    this.totalQuantity,
    this.detailAdjust,
    this.adjustDate,
    this.adjustStatus,
    this.deleted,
    this.created,
    this.updated,
    this.userInit,
    this.userUpd,
    this.finishDate,
    this.updateData,
    this.active,
  });

  int? ID;
  @JsonKey(name: 'adjust_code')
  String? adjustCode;
  @JsonKey(name: 'store_id')
  int? storeId;
  String? notes;
  @JsonKey(name: 'total_different')
  String? totalDifferent;
  @JsonKey(name: 'total_quantity')
  String? totalQuantity;
  @JsonKey(name: 'detail_adjust')
  String? detailAdjust;
  @JsonKey(name: 'adjust_date')
  String? adjustDate;
  @JsonKey(name: 'adjust_status')
  int? adjustStatus;
  int? deleted;
  String? created;
  String? updated;
  @JsonKey(name: 'user_init')
  int? userInit;
  @JsonKey(name: 'user_upd')
  int? userUpd;
  @JsonKey(name: 'finish_date')
  String? finishDate;
  @JsonKey(name: 'update_data')
  int? updateData;
  int? active;

  factory ItemKiemKe.fromJson(Map<String, dynamic> json) =>
      _$ItemKiemKeFromJson(json);

  Map<String, dynamic> toJson() => _$ItemKiemKeToJson(this);
}
