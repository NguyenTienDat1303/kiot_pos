// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'overview_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OverviewResponse _$OverviewResponseFromJson(Map<String, dynamic> json) =>
    OverviewResponse(
      banDangDung: json['banDangDung'] as int?,
      tongSoBan: json['tongSoBan'] as int?,
      huyTra: json['huyTra'] as String?,
      giaTriHuyTra: json['giaTriHuyTra'] as int?,
      doanhThu: json['doanhThu'] as int?,
      chart: (json['chart'] as List<dynamic>?)
          ?.map((e) => OverviewChartData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$OverviewResponseToJson(OverviewResponse instance) =>
    <String, dynamic>{
      'banDangDung': instance.banDangDung,
      'tongSoBan': instance.tongSoBan,
      'huyTra': instance.huyTra,
      'giaTriHuyTra': instance.giaTriHuyTra,
      'doanhThu': instance.doanhThu,
      'chart': instance.chart,
    };

OverviewChartData _$OverviewChartDataFromJson(Map<String, dynamic> json) =>
    OverviewChartData(
      label: json['label'] as String?,
      doanhThu: json['doanhThu'] as String?,
    );

Map<String, dynamic> _$OverviewChartDataToJson(OverviewChartData instance) =>
    <String, dynamic>{
      'label': instance.label,
      'doanhThu': instance.doanhThu,
    };
