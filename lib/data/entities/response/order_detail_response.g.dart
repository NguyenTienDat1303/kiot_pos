// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_detail_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderDetailResponse _$OrderDetailResponseFromJson(Map<String, dynamic> json) =>
    OrderDetailResponse(
      donHang: json['donHang'] == null
          ? null
          : OrderDetailData.fromJson(json['donHang'] as Map<String, dynamic>),
      sanPhams: (json['sanPhams'] as List<dynamic>?)
          ?.map((e) => ODProductData.fromJson(e as Map<String, dynamic>))
          .toList(),
      khuyenMai: (json['khuyenMai'] as List<dynamic>?)
          ?.map((e) => ODProductData.fromJson(e as Map<String, dynamic>))
          .toList(),
      tenkhach: json['tenkhach'] == null
          ? null
          : ODCustomerData.fromJson(json['tenkhach'] as Map<String, dynamic>),
      nguoiThu: json['nguoiThu'] == null
          ? null
          : ODPeopleReceveiData.fromJson(
              json['nguoiThu'] as Map<String, dynamic>),
      nhanVien: (json['nhanVien'] as List<dynamic>?)
          ?.map((e) => ODStaffData.fromJson(e as Map<String, dynamic>))
          .toList(),
      tongChietKhau: (json['tongChietKhau'] as num?)?.toDouble(),
      sauChietKhau: (json['sauChietKhau'] as num?)?.toDouble(),
      tienVAT: (json['tienVAT'] as num?)?.toDouble(),
      tienNhanVien: (json['tienNhanVien'] as num?)?.toDouble(),
      tienBan: (json['tienBan'] as num?)?.toDouble(),
      tongSoLuong: (json['tongSoLuong'] as num?)?.toDouble(),
      tongTienHang: (json['tongTienHang'] as num?)?.toDouble(),
    )
      ..type = json['type'] as String?
      ..message = json['message'] as String?;

Map<String, dynamic> _$OrderDetailResponseToJson(
        OrderDetailResponse instance) =>
    <String, dynamic>{
      'type': instance.type,
      'message': instance.message,
      'donHang': instance.donHang,
      'sanPhams': instance.sanPhams,
      'khuyenMai': instance.khuyenMai,
      'tenkhach': instance.tenkhach,
      'nguoiThu': instance.nguoiThu,
      'nhanVien': instance.nhanVien,
      'tongChietKhau': instance.tongChietKhau,
      'sauChietKhau': instance.sauChietKhau,
      'tienVAT': instance.tienVAT,
      'tienNhanVien': instance.tienNhanVien,
      'tienBan': instance.tienBan,
      'tongSoLuong': instance.tongSoLuong,
      'tongTienHang': instance.tongTienHang,
    };

OrderDetailData _$OrderDetailDataFromJson(Map<String, dynamic> json) =>
    OrderDetailData(
      id: json['ID'] as int?,
      outputCode: json['output_code'] as String?,
      storeId: json['store_id'] as int?,
      sellDate: json['sell_date'] as String?,
      fromDate: json['from_date'] as String?,
      finishDate: json['finish_date'] as String?,
      notes: json['notes'] as String?,
      paymentMethod: json['payment_method'] as int?,
      totalPrice: json['total_price'] as int?,
      totalOriginPrice: json['total_origin_price'] as int?,
      discountItem: json['discount_item'] as int?,
      coupon: json['coupon'] as int?,
      vat: json['vat'] as int?,
      customerPay: json['customer_pay'] as int?,
      tableId: json['table_id'] as int?,
      totalMoney: json['total_money'] as int?,
      totalQuantity: json['total_quantity'] as int?,
      payDate: json['pay_date'] as String?,
      canReturn: json['canreturn'] as int?,
      orderStatus: json['order_status'] as int?,
      totalEmployeePrice: json['total_employee_price'] as int?,
      totalTablePrice: json['total_table_price'] as int?,
    );

Map<String, dynamic> _$OrderDetailDataToJson(OrderDetailData instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'output_code': instance.outputCode,
      'store_id': instance.storeId,
      'sell_date': instance.sellDate,
      'from_date': instance.fromDate,
      'finish_date': instance.finishDate,
      'notes': instance.notes,
      'payment_method': instance.paymentMethod,
      'total_price': instance.totalPrice,
      'total_origin_price': instance.totalOriginPrice,
      'discount_item': instance.discountItem,
      'coupon': instance.coupon,
      'vat': instance.vat,
      'customer_pay': instance.customerPay,
      'table_id': instance.tableId,
      'total_money': instance.totalMoney,
      'total_quantity': instance.totalQuantity,
      'pay_date': instance.payDate,
      'canreturn': instance.canReturn,
      'order_status': instance.orderStatus,
      'total_employee_price': instance.totalEmployeePrice,
      'total_table_price': instance.totalTablePrice,
    };

ODProductData _$ODProductDataFromJson(Map<String, dynamic> json) =>
    ODProductData(
      sanPham: json['sanPham'] == null
          ? null
          : ODProductDetail.fromJson(json['sanPham'] as Map<String, dynamic>),
      quantity: json['quantity'] as int?,
      price: json['price'] as int?,
      cc1: json['cc1'] as String?,
      cc2: json['cc2'] as String?,
      vat: json['vat'] as int?,
      discount: json['discount'] as int?,
      discountVnd: json['discount_vnd'] as int?,
      note: json['note'] as String?,
      truocChietKhau: (json['truocChietKhau'] as num?)?.toDouble(),
      sauChietKhau: (json['sauChietKhau'] as num?)?.toDouble(),
      topping: (json['topping'] as List<dynamic>?)
          ?.map((e) => ODToppingData.fromJson(e as Map<String, dynamic>))
          .toList(),
      quantitySelected: json['quantitySelected'] as int?,
    );

Map<String, dynamic> _$ODProductDataToJson(ODProductData instance) =>
    <String, dynamic>{
      'sanPham': instance.sanPham,
      'quantitySelected': instance.quantitySelected,
      'quantity': instance.quantity,
      'price': instance.price,
      'cc1': instance.cc1,
      'cc2': instance.cc2,
      'vat': instance.vat,
      'discount': instance.discount,
      'discount_vnd': instance.discountVnd,
      'note': instance.note,
      'truocChietKhau': instance.truocChietKhau,
      'sauChietKhau': instance.sauChietKhau,
      'topping': instance.topping,
    };

ODProductDetail _$ODProductDetailFromJson(Map<String, dynamic> json) =>
    ODProductDetail(
      prdCode: json['prd_code'] as String?,
      prdName: json['prd_name'] as String?,
      id: json['ID'] as int?,
    );

Map<String, dynamic> _$ODProductDetailToJson(ODProductDetail instance) =>
    <String, dynamic>{
      'prd_code': instance.prdCode,
      'prd_name': instance.prdName,
      'ID': instance.id,
    };

ODToppingData _$ODToppingDataFromJson(Map<String, dynamic> json) =>
    ODToppingData(
      topping: json['topping'] == null
          ? null
          : ODToppingDetail.fromJson(json['topping'] as Map<String, dynamic>),
      quantityAdd: json['quantity_add'] as int?,
      priceAdd: json['price_add'] as int?,
    );

Map<String, dynamic> _$ODToppingDataToJson(ODToppingData instance) =>
    <String, dynamic>{
      'topping': instance.topping,
      'quantity_add': instance.quantityAdd,
      'price_add': instance.priceAdd,
    };

ODToppingDetail _$ODToppingDetailFromJson(Map<String, dynamic> json) =>
    ODToppingDetail(
      prdCode: json['prd_code'] as String?,
      prdName: json['prd_name'] as String?,
      id: json['ID'] as int?,
    );

Map<String, dynamic> _$ODToppingDetailToJson(ODToppingDetail instance) =>
    <String, dynamic>{
      'prd_code': instance.prdCode,
      'prd_name': instance.prdName,
      'ID': instance.id,
    };

ODStaffData _$ODStaffDataFromJson(Map<String, dynamic> json) => ODStaffData(
      nhanVien: json['nhanVien'] == null
          ? null
          : ODStaffDetail.fromJson(json['nhanVien'] as Map<String, dynamic>),
      start: json['start'] as String?,
      end: json['end'] as String?,
      time: (json['time'] as num?)?.toDouble(),
      price: (json['price'] as num?)?.toDouble(),
      totalPrice: (json['total_price'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$ODStaffDataToJson(ODStaffData instance) =>
    <String, dynamic>{
      'nhanVien': instance.nhanVien,
      'start': instance.start,
      'end': instance.end,
      'time': instance.time,
      'price': instance.price,
      'total_price': instance.totalPrice,
    };

ODStaffDetail _$ODStaffDetailFromJson(Map<String, dynamic> json) =>
    ODStaffDetail(
      id: json['ID'] as int?,
      employee_name: json['employee_name'] as String?,
    );

Map<String, dynamic> _$ODStaffDetailToJson(ODStaffDetail instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'employee_name': instance.employee_name,
    };

ODCustomerData _$ODCustomerDataFromJson(Map<String, dynamic> json) =>
    ODCustomerData(
      id: json['ID'] as int?,
      customerName: json['customer_name'] as String?,
    );

Map<String, dynamic> _$ODCustomerDataToJson(ODCustomerData instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'customer_name': instance.customerName,
    };

ODPeopleReceveiData _$ODPeopleReceveiDataFromJson(Map<String, dynamic> json) =>
    ODPeopleReceveiData(
      id: json['ID'] as int?,
      displayName: json['display_name'] as String?,
    );

Map<String, dynamic> _$ODPeopleReceveiDataToJson(
        ODPeopleReceveiData instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'display_name': instance.displayName,
    };
