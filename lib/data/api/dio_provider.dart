import 'package:dio/dio.dart';
import 'api.dart';
import '../../res/strings.dart';

class DioProvider {
  static Future<Dio> instance() async {
    final dio = Dio();
    dio
      ..options.baseUrl = Endpoint.baseUrl
      ..options.connectTimeout = Endpoint.connectionTimeout
      ..options.receiveTimeout = Endpoint.receiveTimeout
      // ..options.receiveDataWhenStatusError = false
      ..options.headers = {
        AppStrings.content_type: AppStrings.multipart_form_data
      }
      ..interceptors.add(LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
        requestHeader: true,
      ))
      ..interceptors.add(DioInterceptors());

    return dio;
  }
}
