class Endpoint {
  Endpoint._();

  static const baseUrl = 'https://apppos.kiotsoft.com/api/';
  static const login = 'services/login';
  static const forgot_password = '/services/quen-mat-khau';
  static const permission = 'quyen-han/danh-sach';
  static const overview = 'services/tong-quan';
  static const store = 'kho/danh-sach';
  static const area = 'khu-vuc/danh-sach-khu-vuc';
  static const table = 'khu-vuc/danh-sach';
  static const category = 'danh-muc/danh-sach';
  static const product = 'san-pham/danh-sach';
  static const topping = 'ban-hang/topping';
  static const payment = 'ban-hang/thanh-toan';
  static const temp_payment = 'ban-hang/in-tam-tinh';


  static const move_table = 'ban/chuyen-ban';

// customer
  static const list_customer = 'khach-hang/danh-sach';
  static const customer_create = 'khach-hang/tao-moi';
  static const group_customer = 'nhom-khach/danh-sach';

//staff
  static const search_staff = 'nhan-vien/tim-kiem';
  static const staff_create = 'nhan-vien/tao-moi';
  static const staff_edit = 'nhan-vien/sua';
  static const staff_delete = 'nhan-vien/xoa';
  static const detail_staff = 'nhan-vien/chi-tiet';

// khu vuc

  static const list_area = 'khu-vuc/danh-sach-khu-vuc';
  static const detail_area = 'khu-vuc/chi-tiet';
  static const area_create = 'khu-vuc/tao-moi';
  static const area_edit = 'khu-vuc/sua';
  static const area_delete = 'khu-vuc/xoa';
  static const list_store = 'kho/danh-sach';
  static const table_edit = 'ban/sua';

  static const overview_collect_pay = 'services/thu-chi-tong-quan';
  //thu
  static const list_collect = 'phieu-thu/tim-kiem';
  static const detail_collect = 'phieu-thu/chi-tiet';
  static const collect_create = 'phieu-thu/tao-moi';
  static const collect_edit = 'phieu-thu/sua';
  static const collect_delete = 'phieu-thu/xoa';
  // chi
  static const list_pay = 'phieu-chi/tim-kiem';
  static const detail_pay = 'phieu-chi/chi-tiet';
  static const pay_create = 'phieu-chi/tao-moi';
  static const pay_edit = 'phieu-chi/sua';
  static const pay_delete = 'phieu-chi/xoa';
  static const print_form_list = 'ban-mau/danh-sach';
  static const print_form = 'ban-mau/chi-tiet';

  // order
  static const list_order = 'don-hang/tim-kiem';
  static const detail_order = 'don-hang/chi-tiet';
  static const order_detail = 'don-hang/chi-tiet';
  static const load_table = 'ban-hang/load-ban';
 

  //goods
  static const catalogue_create = 'danh-muc/tao-danh-muc';
  static const catalogue_delete = 'danh-muc/xoa-danh-muc';
  static const catalogue_edit = 'danh-muc/sua-danh-muc';
  static const detail_product = 'san-pham/chi-tiet';
  static const product_create = 'san-pham/tao-moi';
  static const product_edit = 'san-pham/sua';
  static const product_delete = 'san-pham/xoa';
  static const unit_create = 'don-vi-tinh/tao-moi';
  static const producer_create = 'nha-san-xuat/tao-moi';

  static const unit = 'don-vi-tinh/danh-sach';
  static const producer = 'nha-san-xuat/danh-sach';
  static const users = 'user/danh-sach';

  static const stock_detail = 'kiem-ke/chi-tiet';
  static const stock_search = 'kiem-ke/tim-kiem';
  static const stock_create = 'kiem-ke/tao-moi';
  static const stock_edit = 'kiem-ke/sua';
  static const stock_delete = 'kiem-ke/xoa';
  static const user_detail = 'cai-dat/xem-ho-so';
  static const user_edit = 'cai-dat/sua-ho-so';
  static const order_return = 'don-hang/tra-hang';
  static const qr = 'ban-hang/quet-ma';
  static const notification_config = 'cai-dat/am-thanh';
  static const change_password = 'services/doi-mat-khau';
  static const printer_list = 'may-in/danh-sach';
  static const printer_create = 'may-in/them';

  static const int DEFAULT_LIMIT = 20;

  // request failed
  static const int FAILURE = 0;

  // request success
  static const int SUCCESS = 1;

  // request with token expire
  static const int TOKEN_EXPIRE = 2;

  // receiveTimeout
  static const int receiveTimeout = 30000;

  // connectTimeout
  static const int connectionTimeout = 30000;

  // method
  static const GET = 'GET';
  static const POST = 'POST';
  static const PUT = 'PUT';
  static const DELETE = 'DELETE';

  // get path
  static String getPath(String path) {
    return '$baseUrl$path';
  }
}
