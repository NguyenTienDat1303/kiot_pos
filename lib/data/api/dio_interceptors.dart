import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' as GetX;
import 'package:kiot_pos/kiot_pos.dart';

class DioInterceptors extends Interceptor {
  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    options.onSendProgress = (progress, total) {};
    var _appController = GetX.Get.find<AppController>();
    _appController.pushLoading();
    // getting token

    print('----token-----');
    var data = options.data;
    print('header ${options.headers}');
    print('data ${options.data}');
    print(data);
    if (data is FormData) {
      print('---form-data---');

      data.fields
          .add(MapEntry<String, String>("token", TokenUtil.generateMd5Token()));
      var prefs = LocalPref();
      var auth = await prefs.get(LocalPref.auth_key);
      var uid = await prefs.get(LocalPref.uid);
      var storeId = await prefs.get(LocalPref.store_id);
      if (auth != null) {
        data.fields.add(MapEntry<String, String>("auth", auth));
      }
      if (uid != null) {
        data.fields.add(MapEntry<String, String>("uid", uid.toString()));
      }
      if (storeId != null) {
        data.fields
            .add(MapEntry<String, String>("store_id", storeId.toString()));
      }

      print(data.boundary);
      print(data.fields);
      print(data.files);
    }

    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    super.onResponse(response, handler);
    print('onResponse');
    var _appController = GetX.Get.find<AppController>();
    Future.delayed(
        Duration(
          milliseconds: 300,
        ), () {
      _appController.popLoading();
    });
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    handler.reject(err);

    BaseResponse response = BaseResponse.fromJson(err.response!.data!);
    var _appController = GetX.Get.find<AppController>();
    Future.delayed(
        Duration(
          milliseconds: 300,
        ), () {
      _appController.popLoading();
    });

    if (err.response!.statusCode == 401) {
      var prefs = LocalPref();
      prefs.save(LocalPref.auth_key, "");
      GetX.Get.toNamed(AppRoutes.login);
    } else {}
    if (GetX.Get.isDialogOpen ?? false) {
      GetX.Get.back();
    } else {
      GetX.Get.dialog(DialogCustom(
          title: "dialog.title_error".tr,
          onTapOK: () {
            GetX.Get.back();
          },
          children: [Text("${response.message}")]));
    }

    //  Response? a =
    //     Response(data: null, requestOptions: RequestOptions(path: ''));
    // handler.resolve(a);

    // GetX.Get.dialog(DialogError(messsage: response.messages![0].message));
  }
}
