// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_rest_client.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _AppRestClient implements AppRestClient {
  _AppRestClient(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://apppos.kiotsoft.com/api/';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<LoginResponse> login(username, password) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('username', username));
    _data.fields.add(MapEntry('password', password));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<LoginResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'services/login',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = LoginResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> forgotPassword(info) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('info', info));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, '/services/quen-mat-khau',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<PermissionData>> permission(contentType) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('content-type', contentType));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<PermissionData>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'quyen-han/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => PermissionData.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<OverviewResponse> overview(chart_type) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('chart_type', chart_type));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<OverviewResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'services/tong-quan',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = OverviewResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<CustomerResponse>> getListCustomer(tuKhoa) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (tuKhoa != null) {
      _data.fields.add(MapEntry('tuKhoa', tuKhoa));
    }
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<CustomerResponse>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'khach-hang/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map(
            (dynamic i) => CustomerResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<BaseResponse> createCustomer(
      customerCode,
      customerName,
      customerPhone,
      customerEmail,
      customerAddr,
      notes,
      customerBirthday,
      customerGender,
      customerGroup,
      customerCoin) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (customerCode != null) {
      _data.fields.add(MapEntry('customer_code', customerCode));
    }
    _data.fields.add(MapEntry('customer_name', customerName));
    if (customerPhone != null) {
      _data.fields.add(MapEntry('customer_phone', customerPhone));
    }
    if (customerEmail != null) {
      _data.fields.add(MapEntry('customer_email', customerEmail));
    }
    if (customerAddr != null) {
      _data.fields.add(MapEntry('customer_addr', customerAddr));
    }
    if (notes != null) {
      _data.fields.add(MapEntry('notes', notes));
    }
    if (customerBirthday != null) {
      _data.fields.add(MapEntry('customer_birthday', customerBirthday));
    }
    if (customerGender != null) {
      _data.fields.add(MapEntry('customer_gender', customerGender.toString()));
    }
    if (customerGroup != null) {
      _data.fields.add(MapEntry('customer_group', customerGroup.toString()));
    }
    if (customerCoin != null) {
      _data.fields.add(MapEntry('customer_coin', customerCoin.toString()));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'khach-hang/tao-moi',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<GroupCustomerResponse>> getGroupCustomer(contentType) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (contentType != null) {
      _data.fields.add(MapEntry('content-type', contentType));
    }
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<GroupCustomerResponse>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'nhom-khach/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) =>
            GroupCustomerResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<List<StoreData>> store(tuKhoa) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (tuKhoa != null) {
      _data.fields.add(MapEntry('tuKhoa', tuKhoa));
    }
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<StoreData>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'kho/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => StoreData.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<List<AreaData>> area(contentType) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (contentType != null) {
      _data.fields.add(MapEntry('content-type', contentType));
    }
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<AreaData>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'khu-vuc/danh-sach-khu-vuc',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => AreaData.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<TableListResponse> table({areaId, trangThai}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (areaId != null) {
      _data.fields.add(MapEntry('id', areaId.toString()));
    }
    if (trangThai != null) {
      _data.fields.add(MapEntry('trangThai', trangThai.toString()));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<TableListResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'khu-vuc/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = TableListResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<CategoryData>> category(tuKhoa) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('tuKhoa', tuKhoa));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<CategoryData>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'danh-muc/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => CategoryData.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<List<ProductData>> product(categoryId, tuKhoa) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (categoryId != null) {
      _data.fields.add(MapEntry('id', categoryId.toString()));
    }
    _data.fields.add(MapEntry('tuKhoa', tuKhoa));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<ProductData>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'san-pham/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => ProductData.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<List<ToppingData>> topping(tuKhoa) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('tuKhoa', tuKhoa));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<ToppingData>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'ban-hang/topping',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => ToppingData.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<List<StaffDetailResponse>> searchStaff(tuKhoa) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (tuKhoa != null) {
      _data.fields.add(MapEntry('tuKhoa', tuKhoa));
    }
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<StaffDetailResponse>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'nhan-vien/tim-kiem',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) =>
            StaffDetailResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<StaffDetailResponse> detailStaff(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (id != null) {
      _data.fields.add(MapEntry('id', id.toString()));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<StaffDetailResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'nhan-vien/chi-tiet',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = StaffDetailResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> staffCreate(
      employeeCode, employeeName, employeePhone, employeePrice) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('employee_code', employeeCode));
    _data.fields.add(MapEntry('employee_name', employeeName));
    if (employeePhone != null) {
      _data.fields.add(MapEntry('employee_phone', employeePhone));
    }
    if (employeePrice != null) {
      _data.fields.add(MapEntry('employee_price', employeePrice));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'nhan-vien/tao-moi',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> staffEdit(
      id, employeeCode, employeeName, employeePhone, employeePrice) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    _data.fields.add(MapEntry('employee_code', employeeCode));
    _data.fields.add(MapEntry('employee_name', employeeName));
    if (employeePhone != null) {
      _data.fields.add(MapEntry('employee_phone', employeePhone));
    }
    if (employeePrice != null) {
      _data.fields.add(MapEntry('employee_price', employeePrice));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'nhan-vien/sua',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> staffDelete(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'nhan-vien/xoa',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> areaCreate(
      areaName,
      numberTable,
      from1,
      to1,
      area_price1,
      from2,
      to2,
      area_price2,
      from3,
      to3,
      area_price3,
      storeId) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('area_name', areaName));
    _data.fields.add(MapEntry('number_table', numberTable.toString()));
    _data.fields.add(MapEntry('from1', from1.toString()));
    _data.fields.add(MapEntry('to1', to1.toString()));
    _data.fields.add(MapEntry('area_price1', area_price1.toString()));
    _data.fields.add(MapEntry('from2', from2.toString()));
    _data.fields.add(MapEntry('to2', to2.toString()));
    _data.fields.add(MapEntry('area_price2', area_price2.toString()));
    _data.fields.add(MapEntry('from3', from3.toString()));
    _data.fields.add(MapEntry('to3', to3.toString()));
    _data.fields.add(MapEntry('area_price3', area_price3.toString()));
    _data.fields.add(MapEntry('store_id', storeId.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'khu-vuc/tao-moi',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> areaEdit(
      id,
      areaName,
      numberTable,
      from1,
      to1,
      area_price1,
      from2,
      to2,
      area_price2,
      from3,
      to3,
      area_price3,
      storeId) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    _data.fields.add(MapEntry('area_name', areaName));
    _data.fields.add(MapEntry('number_table', numberTable.toString()));
    _data.fields.add(MapEntry('from1', from1.toString()));
    _data.fields.add(MapEntry('to1', to1.toString()));
    _data.fields.add(MapEntry('area_price1', area_price1.toString()));
    _data.fields.add(MapEntry('from2', from2.toString()));
    _data.fields.add(MapEntry('to2', to2.toString()));
    _data.fields.add(MapEntry('area_price2', area_price2.toString()));
    _data.fields.add(MapEntry('from3', from3.toString()));
    _data.fields.add(MapEntry('to3', to3.toString()));
    _data.fields.add(MapEntry('area_price3', area_price3.toString()));
    _data.fields.add(MapEntry('store_id', storeId.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'khu-vuc/sua',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> areaDelete(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'khu-vuc/xoa',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<AreaDetailData> detailArea(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<AreaDetailData>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'khu-vuc/chi-tiet',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = AreaDetailData.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> tableEdit(id, name) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    _data.fields.add(MapEntry('name', name));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'ban/sua',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<OverviewCollectPayResponse> getOverviewCollectPay(auth) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (auth != null) {
      _data.fields.add(MapEntry('auth', auth));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<OverviewCollectPayResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'services/thu-chi-tong-quan',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = OverviewCollectPayResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<CollectResponse>> getListCollect(
      tuKhoa, isDaXoa, hinhThucThu, tuNgay, denNgay, per_page) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('tuKhoa', tuKhoa));
    if (isDaXoa != null) {
      _data.fields.add(MapEntry('isDaXoa', isDaXoa.toString()));
    }
    if (hinhThucThu != null) {
      _data.fields.add(MapEntry('hinhThucThu', hinhThucThu.toString()));
    }
    if (tuNgay != null) {
      _data.fields.add(MapEntry('tuNgay', tuNgay));
    }
    if (denNgay != null) {
      _data.fields.add(MapEntry('denNgay', denNgay));
    }
    _data.fields.add(MapEntry('per_page', per_page.toString()));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<CollectResponse>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'phieu-thu/tim-kiem',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => CollectResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<CollectResponse> getDetailCollect(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CollectResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'phieu-thu/chi-tiet',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CollectResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> collectCreate(receiptFor, totalMoney, receiptDate,
      storeId, notes, receiptMethod, typeId, customerId) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('receipt_for', receiptFor.toString()));
    _data.fields.add(MapEntry('total_money', totalMoney.toString()));
    _data.fields.add(MapEntry('receipt_date', receiptDate));
    _data.fields.add(MapEntry('store_id', storeId.toString()));
    _data.fields.add(MapEntry('notes', notes));
    _data.fields.add(MapEntry('receipt_method', receiptMethod.toString()));
    _data.fields.add(MapEntry('type_id', typeId.toString()));
    _data.fields.add(MapEntry('customer_id', customerId.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'phieu-thu/tao-moi',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> collectEdit(id, receiptFor, totalMoney, receiptDate,
      storeId, notes, receiptMethod, typeId, customerId) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    _data.fields.add(MapEntry('receipt_for', receiptFor.toString()));
    _data.fields.add(MapEntry('total_money', totalMoney.toString()));
    _data.fields.add(MapEntry('receipt_date', receiptDate));
    _data.fields.add(MapEntry('store_id', storeId.toString()));
    _data.fields.add(MapEntry('notes', notes));
    _data.fields.add(MapEntry('receipt_method', receiptMethod.toString()));
    _data.fields.add(MapEntry('type_id', typeId.toString()));
    _data.fields.add(MapEntry('customer_id', customerId.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'phieu-thu/sua',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> collectDelete(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'phieu-thu/xoa',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<PayResponse>> getListPay(
      tuKhoa, isDaXoa, hinhThucChi, tuNgay, denNgay, per_page) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('tuKhoa', tuKhoa));
    if (isDaXoa != null) {
      _data.fields.add(MapEntry('isDaXoa', isDaXoa.toString()));
    }
    if (hinhThucChi != null) {
      _data.fields.add(MapEntry('hinhThucChi', hinhThucChi.toString()));
    }
    if (tuNgay != null) {
      _data.fields.add(MapEntry('tuNgay', tuNgay));
    }
    if (denNgay != null) {
      _data.fields.add(MapEntry('denNgay', denNgay));
    }
    _data.fields.add(MapEntry('per_page', per_page.toString()));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<PayResponse>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'phieu-chi/tim-kiem',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => PayResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<PayResponse> getDetailPay(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PayResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'phieu-chi/chi-tiet',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PayResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> payCreate(paymentFor, totalMoney, paymentDate, storeId,
      notes, paymentMethod, typeId, customerId) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('payment_for', paymentFor.toString()));
    _data.fields.add(MapEntry('total_money', totalMoney.toString()));
    _data.fields.add(MapEntry('payment_date', paymentDate));
    _data.fields.add(MapEntry('store_id', storeId.toString()));
    _data.fields.add(MapEntry('notes', notes));
    _data.fields.add(MapEntry('payment_method', paymentMethod.toString()));
    _data.fields.add(MapEntry('type_id', typeId.toString()));
    _data.fields.add(MapEntry('customer_id', customerId.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'phieu-chi/tao-moi',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> payEdit(id, paymentFor, totalMoney, paymentDate, storeId,
      notes, paymentMethod, typeId, customerId) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    _data.fields.add(MapEntry('payment_for', paymentFor.toString()));
    _data.fields.add(MapEntry('total_money', totalMoney.toString()));
    _data.fields.add(MapEntry('payment_date', paymentDate));
    _data.fields.add(MapEntry('store_id', storeId.toString()));
    _data.fields.add(MapEntry('notes', notes));
    _data.fields.add(MapEntry('payment_method', paymentMethod.toString()));
    _data.fields.add(MapEntry('type_id', typeId.toString()));
    _data.fields.add(MapEntry('customer_id', customerId.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'phieu-chi/sua',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> payDelete(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'phieu-chi/xoa',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<OrderResponse>> getListOrder(
      tuKhoa, phongID, trangThaiID, tuNgay, denNgay, perPage) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (tuKhoa != null) {
      _data.fields.add(MapEntry('tuKhoa', tuKhoa));
    }
    if (phongID != null) {
      _data.fields.add(MapEntry('phongID', phongID.toString()));
    }
    if (trangThaiID != null) {
      _data.fields.add(MapEntry('trangThaiID', trangThaiID.toString()));
    }
    if (tuNgay != null) {
      _data.fields.add(MapEntry('tuNgay', tuNgay));
    }
    if (denNgay != null) {
      _data.fields.add(MapEntry('denNgay', denNgay));
    }
    _data.fields.add(MapEntry('per_page', perPage.toString()));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<OrderResponse>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'don-hang/tim-kiem',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => OrderResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<OrderResponse> getDetailPOrder(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<OrderResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'don-hang/chi-tiet',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = OrderResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<OrderDetailResponse> orderDetail(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (id != null) {
      _data.fields.add(MapEntry('id', id.toString()));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<OrderDetailResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'don-hang/chi-tiet',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = OrderDetailResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<OrderDetailResponse?> payment(detailOrder,
      {id,
      customerId,
      vat = 0,
      coupon = 0,
      paymentMethod = 1,
      detailEmployee,
      tableId,
      orderStatus,
      customerPay,
      notes = "",
      detailQuantityOrder}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('detail_order', jsonEncode(detailOrder)));
    if (id != null) {
      _data.fields.add(MapEntry('id', id.toString()));
    }
    if (customerId != null) {
      _data.fields.add(MapEntry('customer_id', customerId.toString()));
    }
    if (vat != null) {
      _data.fields.add(MapEntry('vat', vat.toString()));
    }
    if (coupon != null) {
      _data.fields.add(MapEntry('coupon', coupon.toString()));
    }
    if (paymentMethod != null) {
      _data.fields.add(MapEntry('payment_method', paymentMethod.toString()));
    }
    _data.fields.add(MapEntry('detail_employee', jsonEncode(detailEmployee)));
    if (tableId != null) {
      _data.fields.add(MapEntry('table_id', tableId.toString()));
    }
    if (orderStatus != null) {
      _data.fields.add(MapEntry('order_status', orderStatus.toString()));
    }
    if (customerPay != null) {
      _data.fields.add(MapEntry('customer_pay', customerPay.toString()));
    }
    if (notes != null) {
      _data.fields.add(MapEntry('notes', notes));
    }
    _data.fields.add(
        MapEntry('detail_quantity_order', jsonEncode(detailQuantityOrder)));
    final _result = await _dio.fetch<Map<String, dynamic>?>(
        _setStreamType<OrderDetailResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'ban-hang/thanh-toan',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = _result.data == null
        ? null
        : OrderDetailResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<LoadTableResponse> loadTable(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (id != null) {
      _data.fields.add(MapEntry('id', id.toString()));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<LoadTableResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'ban-hang/load-ban',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = LoadTableResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<OrderDetailResponse?> tempPayment(detailOrder,
      {id,
      customerId,
      vat = 0,
      coupon = 0,
      paymentMethod = 1,
      detailEmployee,
      tableId,
      orderStatus,
      customerPay,
      notes = "",
      detailQuantityOrder}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('detail_order', jsonEncode(detailOrder)));
    if (id != null) {
      _data.fields.add(MapEntry('id', id.toString()));
    }
    if (customerId != null) {
      _data.fields.add(MapEntry('customer_id', customerId.toString()));
    }
    if (vat != null) {
      _data.fields.add(MapEntry('vat', vat.toString()));
    }
    if (coupon != null) {
      _data.fields.add(MapEntry('coupon', coupon.toString()));
    }
    if (paymentMethod != null) {
      _data.fields.add(MapEntry('payment_method', paymentMethod.toString()));
    }
    _data.fields.add(MapEntry('detail_employee', jsonEncode(detailEmployee)));
    if (tableId != null) {
      _data.fields.add(MapEntry('table_id', tableId.toString()));
    }
    if (orderStatus != null) {
      _data.fields.add(MapEntry('order_status', orderStatus.toString()));
    }
    if (customerPay != null) {
      _data.fields.add(MapEntry('customer_pay', customerPay.toString()));
    }
    if (notes != null) {
      _data.fields.add(MapEntry('notes', notes));
    }
    _data.fields.add(
        MapEntry('detail_quantity_order', jsonEncode(detailQuantityOrder)));
    final _result = await _dio.fetch<Map<String, dynamic>?>(
        _setStreamType<OrderDetailResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'ban-hang/in-tam-tinh',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = _result.data == null
        ? null
        : OrderDetailResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> moveTable(oldTableID, newTableID, oldOrderID, type,
      {newOrderID}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('oldTableID', oldTableID.toString()));
    _data.fields.add(MapEntry('newTableID', newTableID.toString()));
    _data.fields.add(MapEntry('oldOrderID', oldOrderID.toString()));
    _data.fields.add(MapEntry('type', type.toString()));
    if (newOrderID != null) {
      _data.fields.add(MapEntry('newOrderID', newOrderID.toString()));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'ban/chuyen-ban',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<PrintData>> printFormList(contentType) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (contentType != null) {
      _data.fields.add(MapEntry('content-type', contentType));
    }
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<PrintData>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'ban-mau/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => PrintData.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<PrintData> printForm(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PrintData>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'ban-mau/chi-tiet',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PrintData.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> catalogueCreate(tenDanhMuc, parentID) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('tenDanhMuc', tenDanhMuc));
    _data.fields.add(MapEntry('parentID', parentID.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'danh-muc/tao-danh-muc',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> catalogueDelete(danhMucID) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('danhMucID', danhMucID.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'danh-muc/xoa-danh-muc',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> catalogueEdit(danhMucID, tenDanhMuc) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('danhMucID', danhMucID.toString()));
    _data.fields.add(MapEntry('tenDanhMuc', tenDanhMuc));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'danh-muc/sua-danh-muc',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<DetailProductResponse> getDetailProduct(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<DetailProductResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'san-pham/chi-tiet',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = DetailProductResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> productCreate(
      prdName,
      prdCode,
      prd_sls,
      prd_image_url,
      prd_edit_price,
      prd_print,
      prd_allownegative,
      prd_material,
      prdAdd,
      infor,
      prd_origin_price,
      prd_sell_price,
      prd_sell_price2,
      prd_group_id,
      prd_manufacture_id,
      prd_unit_id,
      prd_new,
      prd_hot,
      prd_highlight,
      prd_max,
      prd_min,
      print_machine_id,
      quyDoi) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('prd_name', prdName));
    if (prdCode != null) {
      _data.fields.add(MapEntry('prd_code', prdCode));
    }
    if (prd_sls != null) {
      _data.fields.add(MapEntry('prd_sls', prd_sls.toString()));
    }
    if (prd_image_url != null)
      _data.files.add(MapEntry(
          'prd_image_url',
          MultipartFile.fromFileSync(prd_image_url.path,
              filename:
                  prd_image_url.path.split(Platform.pathSeparator).last)));
    _data.fields.add(MapEntry('prd_edit_price', prd_edit_price.toString()));
    _data.fields.add(MapEntry('prd_print', prd_print.toString()));
    _data.fields
        .add(MapEntry('prd_allownegative', prd_allownegative.toString()));
    _data.fields.add(MapEntry('prd_material', prd_material.toString()));
    _data.fields.add(MapEntry('prd_add', prdAdd.toString()));
    _data.fields.add(MapEntry('infor', infor));
    if (prd_origin_price != null) {
      _data.fields
          .add(MapEntry('prd_origin_price', prd_origin_price.toString()));
    }
    if (prd_sell_price != null) {
      _data.fields.add(MapEntry('prd_sell_price', prd_sell_price.toString()));
    }
    if (prd_sell_price2 != null) {
      _data.fields.add(MapEntry('prd_sell_price2', prd_sell_price2.toString()));
    }
    if (prd_group_id != null) {
      _data.fields.add(MapEntry('prd_group_id', prd_group_id.toString()));
    }
    if (prd_manufacture_id != null) {
      _data.fields
          .add(MapEntry('prd_manufacture_id', prd_manufacture_id.toString()));
    }
    if (prd_unit_id != null) {
      _data.fields.add(MapEntry('prd_unit_id', prd_unit_id.toString()));
    }
    _data.fields.add(MapEntry('prd_new', prd_new.toString()));
    _data.fields.add(MapEntry('prd_hot', prd_hot.toString()));
    _data.fields.add(MapEntry('prd_highlight', prd_highlight.toString()));
    if (prd_max != null) {
      _data.fields.add(MapEntry('prd_max', prd_max.toString()));
    }
    if (prd_min != null) {
      _data.fields.add(MapEntry('prd_min', prd_min.toString()));
    }
    if (print_machine_id != null) {
      _data.fields
          .add(MapEntry('print_machine_id', print_machine_id.toString()));
    }
    _data.fields.add(MapEntry('quyDoi', jsonEncode(quyDoi)));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'san-pham/tao-moi',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> productEdit(
      id,
      prdName,
      prdCode,
      prd_sls,
      prd_image_url,
      prd_edit_price,
      prd_print,
      prd_allownegative,
      prd_material,
      prdAdd,
      infor,
      prd_origin_price,
      prd_sell_price,
      prd_sell_price2,
      prd_group_id,
      prd_manufacture_id,
      prd_unit_id,
      prd_new,
      prd_hot,
      prd_highlight,
      prd_max,
      prd_min,
      quyDoi) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    _data.fields.add(MapEntry('prd_name', prdName));
    if (prdCode != null) {
      _data.fields.add(MapEntry('prd_code', prdCode));
    }
    if (prd_sls != null) {
      _data.fields.add(MapEntry('prd_sls', prd_sls.toString()));
    }
    if (prd_image_url != null)
    _data.files.add(MapEntry(
        'prd_image_url',
        MultipartFile.fromFileSync(prd_image_url.path,
            filename: prd_image_url.path.split(Platform.pathSeparator).last)));
    _data.fields.add(MapEntry('prd_edit_price', prd_edit_price.toString()));
    _data.fields.add(MapEntry('prd_print', prd_print.toString()));
    _data.fields
        .add(MapEntry('prd_allownegative', prd_allownegative.toString()));
    _data.fields.add(MapEntry('prd_material', prd_material.toString()));
    _data.fields.add(MapEntry('prd_add', prdAdd.toString()));
    _data.fields.add(MapEntry('infor', infor));
    if (prd_origin_price != null) {
      _data.fields
          .add(MapEntry('prd_origin_price', prd_origin_price.toString()));
    }
    if (prd_sell_price != null) {
      _data.fields.add(MapEntry('prd_sell_price', prd_sell_price.toString()));
    }
    if (prd_sell_price2 != null) {
      _data.fields.add(MapEntry('prd_sell_price2', prd_sell_price2.toString()));
    }
    if (prd_group_id != null) {
      _data.fields.add(MapEntry('prd_group_id', prd_group_id.toString()));
    }
    if (prd_manufacture_id != null) {
      _data.fields
          .add(MapEntry('prd_manufacture_id', prd_manufacture_id.toString()));
    }
    if (prd_unit_id != null) {
      _data.fields.add(MapEntry('prd_unit_id', prd_unit_id.toString()));
    }
    _data.fields.add(MapEntry('prd_new', prd_new.toString()));
    _data.fields.add(MapEntry('prd_hot', prd_hot.toString()));
    _data.fields.add(MapEntry('prd_highlight', prd_highlight.toString()));
    if (prd_max != null) {
      _data.fields.add(MapEntry('prd_max', prd_max.toString()));
    }
    if (prd_min != null) {
      _data.fields.add(MapEntry('prd_min', prd_min.toString()));
    }
    _data.fields.add(MapEntry('quyDoi', jsonEncode(quyDoi)));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'san-pham/sua',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> productDelete(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'san-pham/xoa',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> unitCreate(prd_unit_name) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('prd_unit_name', prd_unit_name));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'don-vi-tinh/tao-moi',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> producerCreate(prd_manuf_name) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('prd_manuf_name', prd_manuf_name));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'nha-san-xuat/tao-moi',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<UnitResponse>> unit(token) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('token', token));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<UnitResponse>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'don-vi-tinh/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => UnitResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<List<ProducerResponse>> producer(token) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('token', token));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<ProducerResponse>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'nha-san-xuat/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map(
            (dynamic i) => ProducerResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<List<UsersResponse>> users(store_id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('store_id', store_id.toString()));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<UsersResponse>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'user/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => UsersResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<StockDetailResponse> getDetailStock(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<StockDetailResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'kiem-ke/chi-tiet',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = StockDetailResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<StockSearchResponse>> getStockSearch(perPage, tuKhoa) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('per_page', perPage.toString()));
    _data.fields.add(MapEntry('tuKhoa', tuKhoa));
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<StockSearchResponse>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'kiem-ke/tim-kiem',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) =>
            StockSearchResponse.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<BaseResponse> stockEdit(
      id, total_different, total_quantity, notes, adjustStatus, data) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    _data.fields.add(MapEntry('total_different', total_different.toString()));
    _data.fields.add(MapEntry('total_quantity', total_quantity.toString()));
    _data.fields.add(MapEntry('notes', notes));
    _data.fields.add(MapEntry('adjust_status', adjustStatus.toString()));
    _data.fields.add(MapEntry('detail_adjust', jsonEncode(data)));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'kiem-ke/sua',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> stockCreate(
      total_different, total_quantity, adjustStatus, notes, data) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('total_different', total_different.toString()));
    _data.fields.add(MapEntry('total_quantity', total_quantity.toString()));
    _data.fields.add(MapEntry('adjust_status', adjustStatus.toString()));
    _data.fields.add(MapEntry('notes', notes));
    _data.fields.add(MapEntry('data', jsonEncode(data)));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'kiem-ke/tao-moi',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<UserResponse> getUserDetail(token) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('token', token));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<UserResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'cai-dat/xem-ho-so',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = UserResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> userEdit(id, displayName, email) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (id != null) {
      _data.fields.add(MapEntry('id', id.toString()));
    }
    if (displayName != null) {
      _data.fields.add(MapEntry('display_name', displayName));
    }
    if (email != null) {
      _data.fields.add(MapEntry('email', email));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'cai-dat/sua-ho-so',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> stockDelete(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (id != null) {
      _data.fields.add(MapEntry('id', id.toString()));
    }
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'kiem-ke/xoa',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> orderReturn(id, sanPhams, notes) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    _data.fields.add(MapEntry('sanPhams', jsonEncode(sanPhams)));
    _data.fields.add(MapEntry('notes', notes));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'don-hang/tra-hang',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ItemQrResponse> loadQr(code) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('code', code));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ItemQrResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'ban-hang/quet-ma',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ItemQrResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> notificationConfig(id, status) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('id', id.toString()));
    _data.fields.add(MapEntry('status', status.toString()));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'cai-dat/am-thanh',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BaseResponse> changePassword(
      password, newPassword, newRePassword) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('password', password));
    _data.fields.add(MapEntry('new_password', newPassword));
    _data.fields.add(MapEntry('new_re_password', newRePassword));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'services/doi-mat-khau',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<List<PrinterData>> printerList(contentType) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = FormData();
    if (contentType != null) {
      _data.fields.add(MapEntry('content-type', contentType));
    }
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<PrinterData>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'may-in/danh-sach',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => PrinterData.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  @override
  Future<BaseResponse> printerCreate(
      description, kieuIn, khoGiay, ip, port) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('description', description));
    _data.fields.add(MapEntry('kieu_in', kieuIn.toString()));
    _data.fields.add(MapEntry('kho_giay', khoGiay.toString()));
    _data.fields.add(MapEntry('ip', ip));
    _data.fields.add(MapEntry('port', port));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BaseResponse>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, 'may-in/them',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BaseResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
