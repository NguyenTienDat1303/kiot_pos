import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';

import 'package:kiot_pos/kiot_pos.dart';
import 'package:retrofit/retrofit.dart';
part 'app_rest_client.g.dart';

@RestApi(baseUrl: Endpoint.baseUrl)
abstract class AppRestClient {
  factory AppRestClient(Dio dio) = _AppRestClient;

  @POST(Endpoint.login)
  Future<LoginResponse> login(
    @Part(name: 'username') String username,
    @Part(name: 'password') String password,
  );

  @POST(Endpoint.forgot_password)
  Future<BaseResponse> forgotPassword(
    @Part(name: 'info') String info,
  );

  @POST(Endpoint.permission)
  Future<List<PermissionData>> permission(
      @Part(name: 'content-type') String contentType);

  @POST(Endpoint.overview)
  Future<OverviewResponse> overview(
      @Part(name: 'chart_type') String chart_type);

  @POST(Endpoint.list_customer)
  Future<List<CustomerResponse>> getListCustomer(
    @Part(name: 'tuKhoa') String? tuKhoa,
  );

  @POST(Endpoint.customer_create)
  Future<BaseResponse> createCustomer(
    @Part(name: 'customer_code') String? customerCode,
    @Part(name: 'customer_name') String customerName,
    @Part(name: 'customer_phone') String? customerPhone,
    @Part(name: 'customer_email') String? customerEmail,
    @Part(name: 'customer_addr') String? customerAddr,
    @Part(name: 'notes') String? notes,
    @Part(name: 'customer_birthday') String? customerBirthday,
    @Part(name: 'customer_gender') int? customerGender,
    @Part(name: 'customer_group') int? customerGroup,
    @Part(name: 'customer_coin') int? customerCoin,
  );

  @POST(Endpoint.group_customer)
  Future<List<GroupCustomerResponse>> getGroupCustomer(
      @Part(name: 'content-type') String? contentType);

  @POST(Endpoint.store)
  Future<List<StoreData>> store(@Part(name: 'tuKhoa') String? tuKhoa);

  @POST(Endpoint.area)
  Future<List<AreaData>> area(@Part(name: 'content-type') String? contentType);

  @POST(Endpoint.table)
  Future<TableListResponse> table(
      {@Part(name: 'id') int? areaId, @Part(name: 'trangThai') int? trangThai});

  @POST(Endpoint.category)
  Future<List<CategoryData>> category(@Part(name: 'tuKhoa') String tuKhoa);

  @POST(Endpoint.product)
  Future<List<ProductData>> product(
      @Part(name: 'id') int? categoryId, @Part(name: 'tuKhoa') String tuKhoa);

  @POST(Endpoint.topping)
  Future<List<ToppingData>> topping(@Part(name: 'tuKhoa') String tuKhoa);

  @POST(Endpoint.search_staff)
  Future<List<StaffDetailResponse>> searchStaff(
    @Part(name: 'tuKhoa') String? tuKhoa,
  );

  @POST(Endpoint.detail_staff)
  Future<StaffDetailResponse> detailStaff(
    @Part(name: 'id') int? id,
  );

  @POST(Endpoint.staff_create)
  Future<BaseResponse> staffCreate(
    @Part(name: 'employee_code') String employeeCode,
    @Part(name: 'employee_name') String employeeName,
    @Part(name: 'employee_phone') String? employeePhone,
    @Part(name: 'employee_price') String? employeePrice,
  );

  @POST(Endpoint.staff_edit)
  Future<BaseResponse> staffEdit(
    @Part(name: 'id') int id,
    @Part(name: 'employee_code') String employeeCode,
    @Part(name: 'employee_name') String employeeName,
    @Part(name: 'employee_phone') String? employeePhone,
    @Part(name: 'employee_price') String? employeePrice,
  );

  @POST(Endpoint.staff_delete)
  Future<BaseResponse> staffDelete(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.area_create)
  Future<BaseResponse> areaCreate(
    @Part(name: 'area_name') String areaName,
    @Part(name: 'number_table') int numberTable,
    @Part(name: 'from1') int from1,
    @Part(name: 'to1') int to1,
    @Part(name: 'area_price1') int area_price1,
    @Part(name: 'from2') int from2,
    @Part(name: 'to2') int to2,
    @Part(name: 'area_price2') int area_price2,
    @Part(name: 'from3') int from3,
    @Part(name: 'to3') int to3,
    @Part(name: 'area_price3') int area_price3,
    @Part(name: 'store_id') int storeId,
  );

  @POST(Endpoint.area_edit)
  Future<BaseResponse> areaEdit(
    @Part(name: 'id') int id,
    @Part(name: 'area_name') String areaName,
    @Part(name: 'number_table') int numberTable,
    @Part(name: 'from1') int from1,
    @Part(name: 'to1') int to1,
    @Part(name: 'area_price1') int area_price1,
    @Part(name: 'from2') int from2,
    @Part(name: 'to2') int to2,
    @Part(name: 'area_price2') int area_price2,
    @Part(name: 'from3') int from3,
    @Part(name: 'to3') int to3,
    @Part(name: 'area_price3') int area_price3,
    @Part(name: 'store_id') int storeId,
  );

  @POST(Endpoint.area_delete)
  Future<BaseResponse> areaDelete(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.detail_area)
  Future<AreaDetailData> detailArea(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.table_edit)
  Future<BaseResponse> tableEdit(
    @Part(name: 'id') int id,
    @Part(name: 'name') String name,
  );

  @POST(Endpoint.overview_collect_pay)
  Future<OverviewCollectPayResponse> getOverviewCollectPay(
    @Part(name: 'auth') String? auth,
  );

  @POST(Endpoint.list_collect)
  Future<List<CollectResponse>> getListCollect(
    @Part(name: 'tuKhoa') String tuKhoa,
    @Part(name: 'isDaXoa') int? isDaXoa,
    @Part(name: 'hinhThucThu') int? hinhThucThu,
    @Part(name: 'tuNgay') String? tuNgay,
    @Part(name: 'denNgay') String? denNgay,
    @Part(name: 'per_page') int per_page,
  );

  @POST(Endpoint.detail_collect)
  Future<CollectResponse> getDetailCollect(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.collect_create)
  Future<BaseResponse> collectCreate(
    @Part(name: 'receipt_for') int receiptFor,
    @Part(name: 'total_money') int totalMoney,
    @Part(name: 'receipt_date') String receiptDate,
    @Part(name: 'store_id') int storeId,
    @Part(name: 'notes') String notes,
    @Part(name: 'receipt_method') int receiptMethod,
    @Part(name: 'type_id') int typeId,
    @Part(name: 'customer_id') int customerId,
  );

  @POST(Endpoint.collect_edit)
  Future<BaseResponse> collectEdit(
    @Part(name: 'id') int id,
    @Part(name: 'receipt_for') int receiptFor,
    @Part(name: 'total_money') int totalMoney,
    @Part(name: 'receipt_date') String receiptDate,
    @Part(name: 'store_id') int storeId,
    @Part(name: 'notes') String notes,
    @Part(name: 'receipt_method') int receiptMethod,
    @Part(name: 'type_id') int typeId,
    @Part(name: 'customer_id') int customerId,
  );

  @POST(Endpoint.collect_delete)
  Future<BaseResponse> collectDelete(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.list_pay)
  Future<List<PayResponse>> getListPay(
    @Part(name: 'tuKhoa') String tuKhoa,
    @Part(name: 'isDaXoa') int? isDaXoa,
    @Part(name: 'hinhThucChi') int? hinhThucChi,
    @Part(name: 'tuNgay') String? tuNgay,
    @Part(name: 'denNgay') String? denNgay,
    @Part(name: 'per_page') int per_page,
  );

  @POST(Endpoint.detail_pay)
  Future<PayResponse> getDetailPay(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.pay_create)
  Future<BaseResponse> payCreate(
    @Part(name: 'payment_for') int paymentFor,
    @Part(name: 'total_money') int totalMoney,
    @Part(name: 'payment_date') String paymentDate,
    @Part(name: 'store_id') int storeId,
    @Part(name: 'notes') String notes,
    @Part(name: 'payment_method') int paymentMethod,
    @Part(name: 'type_id') int typeId,
    @Part(name: 'customer_id') int customerId,
  );

  @POST(Endpoint.pay_edit)
  Future<BaseResponse> payEdit(
    @Part(name: 'id') int id,
    @Part(name: 'payment_for') int paymentFor,
    @Part(name: 'total_money') int totalMoney,
    @Part(name: 'payment_date') String paymentDate,
    @Part(name: 'store_id') int storeId,
    @Part(name: 'notes') String notes,
    @Part(name: 'payment_method') int paymentMethod,
    @Part(name: 'type_id') int typeId,
    @Part(name: 'customer_id') int customerId,
  );

  @POST(Endpoint.pay_delete)
  Future<BaseResponse> payDelete(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.list_order)
  Future<List<OrderResponse>> getListOrder(
    @Part(name: 'tuKhoa') String? tuKhoa,
    @Part(name: 'phongID') int? phongID,
    @Part(name: 'trangThaiID') int? trangThaiID,
    @Part(name: 'tuNgay') String? tuNgay,
    @Part(name: 'denNgay') String? denNgay,
    @Part(name: 'per_page') int perPage,
  );

  @POST(Endpoint.detail_order)
  Future<OrderResponse> getDetailPOrder(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.order_detail)
  Future<OrderDetailResponse> orderDetail(
    @Part(name: 'id') int? id,
  );

  @POST(Endpoint.payment)
  Future<OrderDetailResponse?> payment(
      @Part(name: 'detail_order')
          List<DetailOrderDto> detailOrder,
      {@Part(name: 'id')
          int? id,
      @Part(name: 'customer_id')
          int? customerId,
      @Part(name: 'vat')
          int? vat = 0,
      @Part(name: 'coupon')
          int? coupon = 0,
      @Part(name: 'payment_method')
          int? paymentMethod = 1,
      @Part(name: 'detail_employee')
          List<DetailEmployeeDto>? detailEmployee,
      @Part(name: 'table_id')
          int? tableId,
      @Part(name: 'order_status')
          int? orderStatus,
      @Part(name: 'customer_pay')
          int? customerPay,
      @Part(name: 'notes')
          String? notes = "",
      @Part(name: 'detail_quantity_order')
          List<DetailOrderDto>? detailQuantityOrder});

  @POST(Endpoint.load_table)
  Future<LoadTableResponse> loadTable(
    @Part(name: 'id') int? id,
  );

  @POST(Endpoint.temp_payment)
  Future<OrderDetailResponse?> tempPayment(
      @Part(name: 'detail_order')
          List<DetailOrderDto> detailOrder,
      {@Part(name: 'id')
          int? id,
      @Part(name: 'customer_id')
          int? customerId,
      @Part(name: 'vat')
          int? vat = 0,
      @Part(name: 'coupon')
          int? coupon = 0,
      @Part(name: 'payment_method')
          int? paymentMethod = 1,
      @Part(name: 'detail_employee')
          List<DetailEmployeeDto>? detailEmployee,
      @Part(name: 'table_id')
          int? tableId,
      @Part(name: 'order_status')
          int? orderStatus,
      @Part(name: 'customer_pay')
          int? customerPay,
      @Part(name: 'notes')
          String? notes = "",
      @Part(name: 'detail_quantity_order')
          List<DetailOrderDto>? detailQuantityOrder});

  @POST(Endpoint.move_table)
  Future<BaseResponse> moveTable(
    @Part(name: 'oldTableID') int oldTableID,
    @Part(name: 'newTableID') int newTableID,
    @Part(name: 'oldOrderID') int oldOrderID,
    @Part(name: 'type') int type, {
    @Part(name: 'newOrderID') int? newOrderID,
  });

  @POST(Endpoint.print_form_list)
  Future<List<PrintData>> printFormList(
      @Part(name: 'content-type') String? contentType);

  @POST(Endpoint.print_form)
  Future<PrintData> printForm(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.catalogue_create)
  Future<BaseResponse> catalogueCreate(
    @Part(name: 'tenDanhMuc') String tenDanhMuc,
    @Part(name: 'parentID') int parentID,
  );

  @POST(Endpoint.catalogue_delete)
  Future<BaseResponse> catalogueDelete(
    @Part(name: 'danhMucID') int danhMucID,
  );

  @POST(Endpoint.catalogue_edit)
  Future<BaseResponse> catalogueEdit(
    @Part(name: 'danhMucID') int danhMucID,
    @Part(name: 'tenDanhMuc') String tenDanhMuc,
  );

  @POST(Endpoint.detail_product)
  Future<DetailProductResponse> getDetailProduct(
    @Part(name: 'id') String id,
  );

  @POST(Endpoint.product_create)
  Future<BaseResponse> productCreate(
    @Part(name: 'prd_name') String prdName,
    @Part(name: 'prd_code') String? prdCode,
    @Part(name: 'prd_sls') int? prd_sls,
    @Part(name: 'prd_image_url') File? prd_image_url,
    @Part(name: 'prd_edit_price') int prd_edit_price,
    @Part(name: 'prd_print') int prd_print,
    @Part(name: 'prd_allownegative') int prd_allownegative,
    @Part(name: 'prd_material') int prd_material,
    @Part(name: 'prd_add') int prdAdd,
    @Part(name: 'infor') String infor,
    @Part(name: 'prd_origin_price') int? prd_origin_price,
    @Part(name: 'prd_sell_price') int? prd_sell_price,
    @Part(name: 'prd_sell_price2') int? prd_sell_price2,
    @Part(name: 'prd_group_id') int? prd_group_id,
    @Part(name: 'prd_manufacture_id') int? prd_manufacture_id,
    @Part(name: 'prd_unit_id') int? prd_unit_id,
    @Part(name: 'prd_new') int prd_new,
    @Part(name: 'prd_hot') int prd_hot,
    @Part(name: 'prd_highlight') int prd_highlight,
    @Part(name: 'prd_max') int? prd_max,
    @Part(name: 'prd_min') int? prd_min,
    @Part(name: 'print_machine_id') int? print_machine_id,
    @Part(name: 'quyDoi') List<UnitConversionResponse> quyDoi,
  );

  @POST(Endpoint.product_edit)
  Future<BaseResponse> productEdit(
    @Part(name: 'id') int id,
    @Part(name: 'prd_name') String prdName,
    @Part(name: 'prd_code') String? prdCode,
    @Part(name: 'prd_sls') int? prd_sls,
    @Part(name: 'prd_image_url') File? prd_image_url,
    @Part(name: 'prd_edit_price') int prd_edit_price,
    @Part(name: 'prd_print') int prd_print,
    @Part(name: 'prd_allownegative') int prd_allownegative,
    @Part(name: 'prd_material') int prd_material,
    @Part(name: 'prd_add') int prdAdd,
    @Part(name: 'infor') String infor,
    @Part(name: 'prd_origin_price') int? prd_origin_price,
    @Part(name: 'prd_sell_price') int? prd_sell_price,
    @Part(name: 'prd_sell_price2') int? prd_sell_price2,
    @Part(name: 'prd_group_id') int? prd_group_id,
    @Part(name: 'prd_manufacture_id') int? prd_manufacture_id,
    @Part(name: 'prd_unit_id') int? prd_unit_id,
    @Part(name: 'prd_new') int prd_new,
    @Part(name: 'prd_hot') int prd_hot,
    @Part(name: 'prd_highlight') int prd_highlight,
    @Part(name: 'prd_max') int? prd_max,
    @Part(name: 'prd_min') int? prd_min,
    @Part(name: 'quyDoi') List<UnitConversionResponse> quyDoi,
  );

  @POST(Endpoint.product_delete)
  Future<BaseResponse> productDelete(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.unit_create)
  Future<BaseResponse> unitCreate(
    @Part(name: 'prd_unit_name') String prd_unit_name,
  );

  @POST(Endpoint.producer_create)
  Future<BaseResponse> producerCreate(
    @Part(name: 'prd_manuf_name') String prd_manuf_name,
  );

  @POST(Endpoint.unit)
  Future<List<UnitResponse>> unit(
    @Part(name: 'token') String token,
  );

  @POST(Endpoint.producer)
  Future<List<ProducerResponse>> producer(
    @Part(name: 'token') String token,
  );

  @POST(Endpoint.users)
  Future<List<UsersResponse>> users(
    @Part(name: 'store_id') int store_id,
  );

  @POST(Endpoint.stock_detail)
  Future<StockDetailResponse> getDetailStock(
    @Part(name: 'id') int id,
  );

  @POST(Endpoint.stock_search)
  Future<List<StockSearchResponse>> getStockSearch(
    @Part(name: 'per_page') int perPage,
    @Part(name: 'tuKhoa') String tuKhoa,
  );

  @POST(Endpoint.stock_edit)
  Future<BaseResponse> stockEdit(
    @Part(name: 'id') int id,
    @Part(name: 'total_different') int total_different,
    @Part(name: 'total_quantity') int total_quantity,
    @Part(name: 'notes') String notes,
    @Part(name: 'adjust_status') int adjustStatus,
    @Part(name: 'detail_adjust') List<StockDto> data,
  );

  @POST(Endpoint.stock_create)
  Future<BaseResponse> stockCreate(
    @Part(name: 'total_different') int total_different,
    @Part(name: 'total_quantity') int total_quantity,
    @Part(name: 'adjust_status') int adjustStatus,
    @Part(name: 'notes') String notes,
    @Part(name: 'data') List<StockDto> data,
  );

  @POST(Endpoint.user_detail)
  Future<UserResponse> getUserDetail(
    @Part(name: 'token') String token,
  );

  @POST(Endpoint.user_edit)
  Future<BaseResponse> userEdit(
    @Part(name: 'id') int? id,
    @Part(name: 'display_name') String? displayName,
    @Part(name: 'email') String? email,
  );
  @POST(Endpoint.stock_delete)
  Future<BaseResponse> stockDelete(
    @Part(name: 'id') int? id,
  );

  @POST(Endpoint.order_return)
  Future<BaseResponse> orderReturn(
    @Part(name: 'id') int id,
    @Part(name: 'sanPhams') List<ReturnAddDto> sanPhams,
    @Part(name: 'notes') String notes,
  );
  @POST(Endpoint.qr)
  Future<ItemQrResponse> loadQr(
    @Part(name: 'code') String code,
  );

  @POST(Endpoint.notification_config)
  Future<BaseResponse> notificationConfig(
    @Part(name: 'id') int id,
    @Part(name: 'status') int status,
  );

  @POST(Endpoint.change_password)
  Future<BaseResponse> changePassword(
    @Part(name: 'password') String password,
    @Part(name: 'new_password') String newPassword,
    @Part(name: 'new_re_password') String newRePassword,
  );

  @POST(Endpoint.printer_list)
  Future<List<PrinterData>> printerList(
      @Part(name: 'content-type') String? contentType);

  @POST(Endpoint.printer_create)
  Future<BaseResponse> printerCreate(
    @Part(name: 'description') String description,
    @Part(name: 'kieu_in') int kieuIn,
    @Part(name: 'kho_giay') int khoGiay,
    @Part(name: 'ip') String ip,
    @Part(name: 'port') String port,
  );
}
