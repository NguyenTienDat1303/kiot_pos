import 'dart:io';

import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

class CommonRepository {
  final AppRestClient _client = Get.find<AppRestClient>();

  CommonRepository();

  Future<LoginResponse> login(String username, String password) async {
    return _client.login(username, password);
  }

  Future<BaseResponse> forgotPassword(
    String info,
  ) async {
    return _client.forgotPassword(info);
  }

  Future<List<PermissionData>> permission() async {
    return _client.permission('multipart/form-data');
  }

  Future<OverviewResponse> overview(String chart_type) async {
    return _client.overview(chart_type);
  }

  Future<List<CustomerResponse>> getListCustomer(String tuKhoa) async {
    return _client.getListCustomer(tuKhoa);
  }

  Future<BaseResponse> createCustomer(
      String? customerCode,
      String customerName,
      String? customerPhone,
      String? customerEmail,
      String? customerAddr,
      String? notes,
      String? customerBirthday,
      int? customerGender,
      int? customerGroup,
      int? customerCoin) async {
    return _client.createCustomer(
        customerCode,
        customerName,
        customerPhone,
        customerEmail,
        customerAddr,
        notes,
        customerBirthday,
        customerGender,
        customerGroup,
        customerCoin);
  }

  Future<List<GroupCustomerResponse>> getGroupCustomer() async {
    return _client.getGroupCustomer('sadklbsa');
  }

  Future<List<StoreData>> store() async {
    return _client.store('multipart/form-data');
  }

  Future<List<AreaData>> area() async {
    return _client.area('multipart/form-data');
  }

  Future<TableListResponse> table({int? areaId, int? trangThai}) async {
    return _client.table(areaId: areaId, trangThai: trangThai);
  }

  Future<List<CategoryData>> category(String tuKhoa) async {
    return _client.category(tuKhoa);
  }

  Future<List<ProductData>> product(int? categoryId, String tuKhoa) async {
    return _client.product(categoryId, tuKhoa);
  }

  Future<List<ToppingData>> topping(String tuKhoa) async {
    return _client.topping(tuKhoa);
  }

   Future<LoadTableResponse> loadTable(int id) async {
    return _client.loadTable(id);
  }

  Future<OrderDetailResponse> orderDetail(int id) async {
    return _client.orderDetail(id);
  }

  Future<OrderDetailResponse?> payment(List<DetailOrderDto> detailOrder,
      {int? id,
      int? customerId,
      String? notes,
      int? vat = 0,
      int? coupon = 0,
      int? paymentMethod = 1,
      List<DetailEmployeeDto>? detailEmployee,
      int? tableId,
      int? orderStatus,
      int? customerPay,
      List<DetailOrderDto>? detailQuantityOrder}) async {
    return _client.payment(detailOrder,
        detailEmployee: detailEmployee,
        id: id,
        customerId: customerId,
        notes: notes,
        vat: vat,
        coupon: coupon,
        paymentMethod: paymentMethod,
        tableId: tableId,
        orderStatus: orderStatus,
        customerPay: customerPay,
        detailQuantityOrder: detailQuantityOrder);
  }

  Future<OrderDetailResponse?> tempPayment(List<DetailOrderDto> detailOrder,
      {int? id,
      int? customerId,
      String? notes,
      int? vat = 0,
      int? coupon = 0,
      int? paymentMethod = 1,
      List<DetailEmployeeDto>? detailEmployee,
      int? tableId,
      int? orderStatus,
      int? customerPay,
      List<DetailOrderDto>? detailQuantityOrder}) async {
    return _client.tempPayment(detailOrder,
        detailEmployee: detailEmployee,
        id: id,
        customerId: customerId,
        notes: notes,
        vat: vat,
        coupon: coupon,
        paymentMethod: paymentMethod,
        tableId: tableId,
        orderStatus: orderStatus,
        customerPay: customerPay,
        detailQuantityOrder: detailQuantityOrder);
  }

  Future<BaseResponse> moveTable(
    int oldTableID,
    int newTableID,
    int oldOrderID,
    int type, {
    int? newOrderID,
  }) async {
    return _client.moveTable(oldTableID, newTableID, oldOrderID, type,
        newOrderID: newOrderID);
  }

  Future<List<StaffDetailResponse>> searchStaff(String tuKhoa) async {
    return _client.searchStaff(tuKhoa);
  }

  Future<StaffDetailResponse> getDetailStaff(int id) async {
    return _client.detailStaff(id);
  }

  Future<BaseResponse> staffCreate(String employeeCode, String employeeName,
      String employeePhone, String employeePrice) async {
    return _client.staffCreate(
        employeeCode, employeeName, employeePhone, employeePrice);
  }

  Future<BaseResponse> staffEdit(int id, String employeeCode,
      String employeeName, String employeePhone, String employeePrice) async {
    return _client.staffEdit(
        id, employeeCode, employeeName, employeePhone, employeePrice);
  }

  Future<BaseResponse> staffDelete(int id) async {
    return _client.staffDelete(id);
  }

  Future<BaseResponse> areaCreate(
      String areaName,
      int numberTable,
      int from1,
      int to1,
      int area_price1,
      int from2,
      int to2,
      int area_price2,
      int from3,
      int to3,
      int area_price3,
      int storeId) async {
    return _client.areaCreate(areaName, numberTable, from1, to1, area_price1,
        from2, to2, area_price2, from3, to3, area_price3, storeId);
  }

  Future<BaseResponse> areaEdit(
      int id,
      String areaName,
      int numberTable,
      int from1,
      int to1,
      int area_price1,
      int from2,
      int to2,
      int area_price2,
      int from3,
      int to3,
      int area_price3,
      int storeId) async {
    return _client.areaEdit(id, areaName, numberTable, from1, to1, area_price1,
        from2, to2, area_price2, from3, to3, area_price3, storeId);
  }

  Future<BaseResponse> areaDelete(int id) async {
    return _client.areaDelete(id);
  }

  Future<AreaDetailData> detailArea(int id) async {
    return _client.detailArea(id);
  }

  Future<BaseResponse> tableEdit(int id, String name) async {
    return _client.tableEdit(id, name);
  }

  Future<OverviewCollectPayResponse> getOverviewCollectPay() async {
    return _client.getOverviewCollectPay('');
  }

  Future<List<CollectResponse>> getListCollect(String tuKhoa, int? isDaXoa,
      int? hinhThucThu, String? tuNgay, String? denNgay, int per_page) async {
    return _client.getListCollect(
        tuKhoa, isDaXoa, hinhThucThu, tuNgay, denNgay, per_page);
  }

  Future<CollectResponse> getDetailCollect(int id) async {
    return _client.getDetailCollect(id);
  }

  Future<BaseResponse> collectCreate(
      int receiptFor,
      int totalMoney,
      String receiptDate,
      int storeId,
      String notes,
      int receiptMethod,
      int typeId,
      int customerId) async {
    return _client.collectCreate(receiptFor, totalMoney, receiptDate, storeId,
        notes, receiptMethod, typeId, customerId);
  }

  Future<BaseResponse> collectEdit(
      int id,
      int receiptFor,
      int totalMoney,
      String receiptDate,
      int storeId,
      String notes,
      int receiptMethod,
      int typeId,
      int customerId) async {
    return _client.collectEdit(id, receiptFor, totalMoney, receiptDate, storeId,
        notes, receiptMethod, typeId, customerId);
  }

  Future<BaseResponse> collectDelete(int id) async {
    return _client.collectDelete(id);
  }

  Future<List<PayResponse>> getListPay(String tuKhoa, int? isDaXoa,
      int? hinhThucChi, String? tuNgay, String? denNgay, int per_page) async {
    return _client.getListPay(
        tuKhoa, isDaXoa, hinhThucChi, tuNgay, denNgay, per_page);
  }

  Future<PayResponse> getDetailPay(int id) async {
    return _client.getDetailPay(id);
  }

  Future<BaseResponse> payCreate(
      int paymentFor,
      int totalMoney,
      String paymentDate,
      int storeId,
      String notes,
      int paymentMethod,
      int typeId,
      int customerId) async {
    return _client.payCreate(paymentFor, totalMoney, paymentDate, storeId,
        notes, paymentMethod, typeId, customerId);
  }

  Future<BaseResponse> payEdit(
      int id,
      int paymentFor,
      int totalMoney,
      String paymentDate,
      int storeId,
      String notes,
      int paymentMethod,
      int typeId,
      int customerId) async {
    return _client.payEdit(id, paymentFor, totalMoney, paymentDate, storeId,
        notes, paymentMethod, typeId, customerId);
  }

  Future<BaseResponse> payDelete(int id) async {
    return _client.payDelete(id);
  }

  Future<List<OrderResponse>> getListOrder(
      String tuKhoa,
      int? isDaXoa,
      int? phongID,
      int? trangThaiID,
      String? tuNgay,
      String? denNgay,
      int perPage) async {
    return _client.getListOrder(
        tuKhoa, phongID, trangThaiID, tuNgay, denNgay, perPage);
  }

  Future<OrderResponse> getDetailPOrder(int id) async {
    return _client.getDetailPOrder(id);
  }

  Future<BaseResponse> catalogueCreate(String tenDanhMuc, int parentID) async {
    return _client.catalogueCreate(tenDanhMuc, parentID);
  }

  Future<List<UsersResponse>> users() async {
    return _client.users(0);
  }

  Future<List<ProducerResponse>> producer() async {
    return _client.producer('token');
  }

  Future<DetailProductResponse> getDetailProduct(String id) async {
    return _client.getDetailProduct(id);
  }

  Future<BaseResponse> productCreate(
      String prdName,
      String? prdCode,
      int? prd_sls,
      File? prd_image_url,
      int prd_edit_price,
      int prd_print,
      int prd_allownegative,
      int prd_material,
      int prdAdd,
      String infor,
      int? prd_origin_price,
      int? prd_sell_price,
      int? prd_sell_price2,
      int? prd_group_id,
      int? prd_manufacture_id,
      int? prd_unit_id,
      int prd_new,
      int prd_hot,
      int prd_highlight,
      int? prd_max,
      int? prd_min,
      int? print_machine_id,
      List<UnitConversionResponse> quyDoi) async {
    return _client.productCreate(
        prdName,
        prdCode,
        prd_sls,
        prd_image_url,
        prd_edit_price,
        prd_print,
        prd_allownegative,
        prd_material,
        prdAdd,
        infor,
        prd_origin_price,
        prd_sell_price,
        prd_sell_price2,
        prd_group_id,
        prd_manufacture_id,
        prd_unit_id,
        prd_new,
        prd_hot,
        prd_highlight,
        prd_max,
        prd_min,
        print_machine_id,
        quyDoi);
  }

  Future<BaseResponse> productEdit(
      int id,
      String prdName,
      String? prdCode,
      int? prd_sls,
      File? prd_image_url,
      int prd_edit_price,
      int prd_print,
      int prd_allownegative,
      int prd_material,
      int prdAdd,
      String infor,
      int? prd_origin_price,
      int? prd_sell_price,
      int? prd_sell_price2,
      int? prd_group_id,
      int? prd_manufacture_id,
      int? prd_unit_id,
      int prd_new,
      int prd_hot,
      int prd_highlight,
      int? prd_max,
      int? prd_min,
      List<UnitConversionResponse> quyDoi) async {
    return _client.productEdit(
        id,
        prdName,
        prdCode,
        prd_sls,
        prd_image_url,
        prd_edit_price,
        prd_print,
        prd_allownegative,
        prd_material,
        prdAdd,
        infor,
        prd_origin_price,
        prd_sell_price,
        prd_sell_price2,
        prd_group_id,
        prd_manufacture_id,
        prd_unit_id,
        prd_new,
        prd_hot,
        prd_highlight,
        prd_max,
        prd_min,
        quyDoi);
  }

  Future<BaseResponse> productDelete(int id) async {
    return _client.productDelete(id);
  }

  Future<StockDetailResponse> getDetailStock(int id) async {
    return _client.getDetailStock(id);
  }

  Future<List<StockSearchResponse>> getStockSearch(
      int perPage, String tuKhoa) async {
    return _client.getStockSearch(perPage, tuKhoa);
  }

  Future<List<UnitResponse>> unit() async {
    return _client.unit('');
  }

  Future<List<PrintData>> printFormList() async {
    return _client.printFormList('multipart/form-data');
  }

  Future<PrintData> printForm(int id) async {
    return _client.printForm(id);
  }

  Future<UserResponse> getUserDetail() async {
    return _client.getUserDetail('token');
  }

  Future<BaseResponse> userEdit(
      int id, String displayName, String email) async {
    return _client.userEdit(id, displayName, email);
  }

  Future<BaseResponse> catalogueEdit(int danhMucID, String tenDanhMuc) async {
    return _client.catalogueEdit(danhMucID, tenDanhMuc);
  }

  Future<BaseResponse> catalogueDelete(int danhMucID) async {
    return _client.catalogueDelete(danhMucID);
  }

  Future<BaseResponse> unitCreate(String prd_unit_name) async {
    return _client.unitCreate(prd_unit_name);
  }

  Future<BaseResponse> producerCreate(String prd_manuf_name) async {
    return _client.producerCreate(prd_manuf_name);
  }

  Future<BaseResponse> stockEdit(
      int id, int adjustStatus, List<StockDto> data) async {
    return _client.stockEdit(id, 8, 100, '', adjustStatus, data);
  }

  Future<BaseResponse> stockCreate(
      int adjustStatus, List<StockDto> data) async {
    return _client.stockCreate(8, 100, adjustStatus, '', data);
  }

  Future<BaseResponse> stockDelete(int id) async {
    return _client.stockDelete(id);
  }

  Future<BaseResponse> orderReturn(
      int id, List<ReturnAddDto> sanPhams, String notes) async {
    return _client.orderReturn(id, sanPhams, notes);
  }

  Future<ItemQrResponse> loadQr(String code) async {
    return _client.loadQr(code);
  }

  Future<BaseResponse> notificationConfig(int id, int status) async {
    return _client.notificationConfig(id, status);
  }

  Future<BaseResponse> changePassword(
      String password, String newPassword, String newRePassword) async {
    return _client.changePassword(password, newPassword, newRePassword);
  }

  Future<List<PrinterData>> printerList() async {
    return _client.printerList('multipart/form-data');
  }

  Future<BaseResponse> printerCreate(
    String description,
    int kieuIn,
    int khoGiay,
    String ip,
    String port,
  ) async {
    return _client.printerCreate(description, kieuIn, khoGiay, ip, port);
  }
}
