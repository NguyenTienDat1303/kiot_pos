import 'package:kiot_pos/kiot_pos.dart';

class SessionRepository {
  Pref pref;
  AppDatabase database;

  SessionRepository({required this.pref, required this.database});

  Future<T?> getField<T>(
    String key,
  ) async {
    return await pref.get<T>(key);
  }

  Future<bool> saveField<T>(String key, T? value) {
    return pref.save(key, value);
  }
}
