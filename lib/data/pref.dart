import 'package:shared_preferences/shared_preferences.dart';

class LocalPref extends Pref {
  static const String auth_token = "auth_token";
  static const String app_language = "app_language";
  static const String app_theme = "app_theme";
  static const String is_first_run = "is_first_run";
  static const String remember_password = "remember_password";

  static const String print_type = "print_type";
  static const String print_kitchen = "print_kitchen";
  static const String print_double = "print_double";
  static const String print_auto = "print_auto";

  static const String store_name = "store_name";
  static const String username = "username";
  static const String password = "password";
  static const String auto_login = "auto_login";
  static const String auth_key = "auth_key";
  static const String uid = "uid";
  static const String store_id = "store_id";
  static const String version = "version";

  @override
  Future<T?> get<T>(String key) async {
    final prefs = await SharedPreferences.getInstance();
    var value = prefs.get(key);
    print("prefs $value");
    print("prefs ${value.runtimeType}");
    return value as T;
  }

  @override
  Future<bool> save<T>(String key, T value) async {
    // print("save ${T}");
    final prefs = await SharedPreferences.getInstance();

    print("value ${value.runtimeType}");
    if (value.runtimeType.toString().contains('String')) {
      return prefs.setString(key, value.toString());
    } else if (value.runtimeType.toString().contains('int')) {
      return prefs.setInt(key, int.parse(value.toString()));
    } else if (value.runtimeType.toString().contains('bool')) {
      print("value setBool");
      return prefs.setBool(
        key,
        value as bool,
      );
    } else if (T is double || T is double?) {
      return prefs.setDouble(key, double.parse(value.toString()));
    }
    // return false;
    return prefs.setString(key, value.toString());
  }
}

// class MemoryPref extends Pref {
//   Map<String, Object> memoryMap = new Map<String, Object>();

//   @override
//   Future<String?> getString(String key) {
//     return Future.value(memoryMap[key].toString());
//   }

//   @override
//   Future<bool?> getBool(String key) {
//     return Future.value(memoryMap[key] as bool);
//   }

//   @override
//   Future<bool> saveString(String key, String value) async {
//     memoryMap[key] = value;
//     return Future.value(true);
//   }

//   @override
//   Future<bool> saveBool(String key, bool value) async {
//     memoryMap[key] = value;
//     return Future.value(true);
//   }

//   @override
//   Future<int?> getInt(String key) {
//     return Future.value(int.parse(memoryMap[key].toString()));
//   }

//   @override
//   Future<bool> saveInt(String key, int value) {
//     memoryMap[key] = value;
//     return Future.value(true);
//   }
// }

abstract class Pref {
  Future<bool> save<T>(String key, T value);
  Future<T?> get<T>(String key);
}
