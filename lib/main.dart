import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:kiot_pos/kiot_pos.dart';

void main() async {
  Initializer.instance.init(() {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        ScreenUtilInit(
          designSize: const Size(360, 690),
          minTextAdapt: true,
          splitScreenMode: true,
          builder: (BuildContext context, Widget? child) {
            return GetMaterialApp(
              title: 'Ck Active',
              debugShowCheckedModeBanner: false,
              translations: AppStrings(),
              locale: Locale('vi', 'VN'),
              supportedLocales: [Locale('en', 'US'), Locale('vi', 'VN')],
              theme: ThemeData(
                primarySwatch: Colors.blue,
              ),
              getPages: AppRoutes().pages,
              initialRoute: AppRoutes.initial,
              initialBinding: AppBinding(),
              localizationsDelegates: [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
            );
          },
        ),
        GetX<AppController>(
          builder: (controller) {
            if (controller.loadingStack > 0)
              return Container(
                  color: Colors.grey.withOpacity(.5),
                  child: Center(child: CircularProgressIndicator()));
            else
              return SizedBox();
          },
        )
      ],
    );
  }
}
